package com.interrupt.bookkeeping.cc;

import org.apache.log4j.Logger;

import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;
import com.interrupt.bob.base.IVisitor;
import com.interrupt.bookkeeping.cc.bkell.Bkell;

public class ReplaceVisitor implements IVisitor { 
	
	private Logger logger = Logger.getLogger(ReplaceVisitor.class); 
	public String _id = null;
	public String _tname = null; 
	public IBob replacor = null; 
	
	public void visit(IBob bob) { 
		
		String tname = bob.getTagName(); 
		org.xml.sax.Attributes attrs = bob.getAttributes(); 
		String id = attrs.getValue("id"); 
		//logger.debug(""); 
		//logger.debug("ReplaceVisitor / Visiting tname["+ tname +"] / id["+ id +"]"); 
		
		for( int i = 0; i < attrs.getLength(); i++ ) { 
			String qname = attrs.getQName(i);
			String qvalue = attrs.getValue(qname); 
			//logger.debug("Attribute qname["+ qname +"] / value["+ qvalue +"]"); 
		}
		
		if(_tname != null && _tname.equals(tname)) { 
			if(_id != null && _id.equals(id)) { 
				
				logger.debug("Replacing..."); 
				logger.debug("1. parent ["+bob.getParent()+"] / ["+bob.toXML()+"]"); 
				logger.debug("2. parent ["+replacor.getParent()+"] / ["+replacor.toXML()+"]"); 
				
				bob.replace(replacor); 
				
			}	
		}	
	}
}
