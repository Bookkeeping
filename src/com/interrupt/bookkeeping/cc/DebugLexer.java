package com.interrupt.bookkeeping.cc;

import java.io.PushbackReader;

import org.apache.log4j.Logger;

import com.interrupt.bookkeeping.cc.bkell.Bkell;
import com.interrupt.bookkeeping.cc.lexer.Lexer;

public class DebugLexer extends Lexer {
	
	private Logger logger = Logger.getLogger(DebugLexer.class); 
	
    public DebugLexer(PushbackReader reader) {
    	super(reader);
    }
    
    protected void filter() { 
    	
    	logger.debug(""); 
    	logger.debug(token.getClass());
    	logger.debug("state: [" + state.id()  + "]"); 
    	logger.debug("text: [" + token.getText() + "]"); 
		
    }
    
}

