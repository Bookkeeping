
package com.interrupt.bookkeeping.cc;  

import java.io.PushbackReader;
import java.io.InputStreamReader;
import java.io.Reader;
import org.apache.log4j.Logger;

import com.interrupt.bookkeeping.cc.bkell.Bkell;
import com.interrupt.bookkeeping.cc.lexer.Lexer;
import com.interrupt.bookkeeping.cc.node.Start;
import com.interrupt.bookkeeping.cc.parser.Parser;


public class Compiler { 
	
	
	private Logger logger = Logger.getLogger(Compiler.class); 
	public Compiler() { 
		
		this._init(new InputStreamReader(System.in)); 
	}
	public Compiler(Reader reader) { 
		
		this._init(reader);
	}
	
	public void _init(Reader reader) {  
		
		
		logger.debug("Running Compiler. Input Expression >"); 
		
		try { 
			
			PushbackReader pbreader = new PushbackReader( reader, 1024 );
			Lexer lexer = new Lexer(pbreader);
			Parser parser = new Parser(lexer); 
				
			// parse the input 
			Start tree = parser.parse(); 
			
			// apply the Visitor
			tree.apply( new ExpressionVisitor() ); 
			
		}
		catch(Exception e) { 
			e.printStackTrace(); 
		}
		
	}
	
	
	public static void main(String args[]) { 
		
		Logger.getLogger(Compiler.class).debug("Running Compiler. Input Expression >"); 
		try { 
			
			PushbackReader pbreader = new PushbackReader( new InputStreamReader(System.in), 1024 );
			Lexer lexer = new Lexer(pbreader);
			Parser parser = new Parser(lexer); 
				
			// parse the input 
			Start tree = parser.parse(); 
			
			// apply the Visitor
			tree.apply( new ExpressionVisitor() ); 
			
		}
		catch(Exception e) { 
			e.printStackTrace(); 
		}
	}
	
}


