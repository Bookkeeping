package com.interrupt.bookkeeping.cc; 

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;

import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;
import com.interrupt.bob.base.IVisitor;
import com.interrupt.bookkeeping.cc.bkell.aauth.Aauthentication;
import com.interrupt.bookkeeping.cc.bkell.command.IMemory;
import com.interrupt.bookkeeping.ISystem;
import com.interrupt.bookkeeping.cc.analysis.DepthFirstAdapter;
import com.interrupt.bookkeeping.cc.bkell.Bkell;
import com.interrupt.bookkeeping.cc.bkell.command.AbstractCommand;
import com.interrupt.bookkeeping.cc.bkell.command.AddCommand;
import com.interrupt.bookkeeping.cc.bkell.command.CommitCommand;
import com.interrupt.bookkeeping.cc.bkell.command.CreateCommand;
import com.interrupt.bookkeeping.cc.bkell.command.FindCommand;
import com.interrupt.bookkeeping.cc.bkell.command.FindVisitor;
import com.interrupt.bookkeeping.cc.bkell.command.GResult;
import com.interrupt.bookkeeping.cc.bkell.command.GTokenLiterals;
import com.interrupt.bookkeeping.cc.bkell.command.IOption;
import com.interrupt.bookkeeping.cc.bkell.command.IResult;
import com.interrupt.bookkeeping.cc.bkell.command.ITokenLiterals;
import com.interrupt.bookkeeping.cc.bkell.command.IVariable;
import com.interrupt.bookkeeping.cc.bkell.command.LoadCommand;
import com.interrupt.bookkeeping.cc.bkell.command.LoginCommand;
import com.interrupt.bookkeeping.cc.bkell.command.LogoutCommand;
import com.interrupt.bookkeeping.cc.bkell.command.ReverseCommand;
import com.interrupt.bookkeeping.cc.bkell.command.Variable;
import com.interrupt.bookkeeping.cc.executor.CreateExecutor;
import com.interrupt.bookkeeping.cc.executor.CreateHelper;
import com.interrupt.bookkeeping.cc.executor.LoadExecutor;
import com.interrupt.bookkeeping.cc.executor.LoadHelper;
import com.interrupt.bookkeeping.cc.executor.VisitorApplyRemove;
import com.interrupt.bookkeeping.cc.node.AAddCommand1;
import com.interrupt.bookkeeping.cc.node.AC1Command;
import com.interrupt.bookkeeping.cc.node.AC2Command;
import com.interrupt.bookkeeping.cc.node.AC3Command;
import com.interrupt.bookkeeping.cc.node.AC4Command;
import com.interrupt.bookkeeping.cc.node.AC5Command;
import com.interrupt.bookkeeping.cc.node.ACmdCommandInput;
import com.interrupt.bookkeeping.cc.node.ACmdExpr;
import com.interrupt.bookkeeping.cc.node.ACommitCommand7;
import com.interrupt.bookkeeping.cc.node.ACreateCommand3;
import com.interrupt.bookkeeping.cc.node.AExitCommand4;
import com.interrupt.bookkeeping.cc.node.AFindCommand2;
import com.interrupt.bookkeeping.cc.node.AIlist;
import com.interrupt.bookkeeping.cc.node.AIlistpart;
import com.interrupt.bookkeeping.cc.node.AListCommand2;
import com.interrupt.bookkeeping.cc.node.ALoadCommand3;
import com.interrupt.bookkeeping.cc.node.ALoginCommand3;
import com.interrupt.bookkeeping.cc.node.ALogoutCommand4;
import com.interrupt.bookkeeping.cc.node.AOptsCommandInput;
import com.interrupt.bookkeeping.cc.node.APrintCommand6;
import com.interrupt.bookkeeping.cc.node.ARemoveCommand1;
import com.interrupt.bookkeeping.cc.node.AReverseCommand5;
import com.interrupt.bookkeeping.cc.node.ATwohandexpr;
import com.interrupt.bookkeeping.cc.node.AUpdateCommand1;
import com.interrupt.bookkeeping.cc.node.AVarCommandInput;
import com.interrupt.bookkeeping.cc.node.AXmlCommandInput;
import com.interrupt.bookkeeping.cc.node.AXmlCommandInputRef;
import com.interrupt.bookkeeping.cc.node.Node;
import com.interrupt.bookkeeping.cc.node.PCommandInput;
import com.interrupt.bookkeeping.cc.node.PIlist;
import com.interrupt.bookkeeping.exception.AuthorisationException;
import com.interrupt.bookkeeping.journal.IEntries;
import com.interrupt.bookkeeping.system.BookkeepingSystem;
import com.interrupt.bookkeeping.system.BookkeepingSystemProperties;
import com.interrupt.bookkeeping.users.IUser;
import com.interrupt.bookkeeping.users.IUserSession;
import com.interrupt.bookkeeping.users.User;
import com.interrupt.bookkeeping.util.Util;
import com.interrupt.spittoon.Spittoon;
import com.interrupt.util.IdGenerator;

public class ExpressionVisitor extends DepthFirstAdapter implements Serializable { 
	
	
	private Logger logger = Logger.getLogger(ExpressionVisitor.class); 
	
	private IUserSession userSession = null; 
	private Aauthentication aauthentication = null; 
	private AXmlCommandInputRef previousCommandResult = null; 
	private Bkell _bkellEnv = null; 
	private boolean active = true; 
	private Spittoon spittoon = null; 
	private String contextUrl = null; 
	
	public ExpressionVisitor() { 
	}
	public ExpressionVisitor(Spittoon spitt) { 
		spittoon = spitt; 
	}
	
	
    public boolean isActive() { return active; }
	public void setActive(boolean active) { this.active = active; }
	
	
	public Bkell getBkellEnv() { return _bkellEnv; }
	public void setBkellEnv(Bkell env) { _bkellEnv = env; }
	
	public AXmlCommandInputRef getPreviousCommandResult() {
		return previousCommandResult;
	}
	public void setPreviousCommandResult(AXmlCommandInputRef previousCommandResult) {
		this.previousCommandResult = previousCommandResult;
	}
	
	public Aauthentication getAauthentication() { return aauthentication; }
    public void setAauthentication(Aauthentication aauthentication) { this.aauthentication = aauthentication; }
    
    public IUserSession getUserSession() { return userSession; }
    public void setUserSession(IUserSession userSession) { this.userSession = userSession; }
    
    public String getContextUrl() { return contextUrl; }
	public void setContextUrl(String contextUrl) { this.contextUrl = contextUrl; }
	
	
	/*********************
	 * COMMANDS
	 */
	
	/* EXPR scenarios 
	 * writing out a variable to memory 
	 */
	public void caseACmdExpr(ACmdExpr node) { 
		
		
        inACmdExpr(node);
        
        // get variable name
        String variableName = "previous"; 
        IBob commandResult = null;
        if(node.getCommand() != null) { 
        	
        	//** the 'node' should now be an 'AXmlCommandInput' 
            node.getCommand().apply(this); 
            
            // get result of command 
            if(this.getPreviousCommandResult() != null) { 
            	commandResult = this.getPreviousCommandResult().getReference(); 
            }
            
        }
        if(node.getSemicolon() != null) { 
        	
            node.getSemicolon().apply(this);
        }
        
        
        // the 'previousCommandResult' has already been evaluated 
        // ... 
        
        // store in the bkell environments
        IVariable variable = new Variable(); 
        variable.setName(variableName); 
        variable.addChild(commandResult); 
        ((IMemory)this.getBkellEnv().findMemoryById("main.memory")).removeVariableByName(variableName); 
        ((IMemory)this.getBkellEnv().findMemoryById("main.memory")).addVariable(variable); 
        
        outACmdExpr(node);
        
    }
	
	public void caseATwohandexpr(ATwohandexpr node) { 
		
		
        inATwohandexpr(node); 
        
        String variableName = null; 
        IBob commandResult = null; 
        
        if(node.getVar() != null) { 
        	
            node.getVar().apply(this);
        }
        if(node.getWord() != null) { 
        	
            node.getWord().apply(this);
            
            // get variable name 
            variableName = node.getWord().getText(); 
            variableName.trim(); 
            //logger.debug("putting variableName into memory["+variableName+"]"); 
            
        }
        if(node.getEquals() != null) { 
        	
            node.getEquals().apply(this);
        }
        if(node.getCommand() != null) { 
        	
            node.getCommand().apply(this); 
            
            // get result of command 
            commandResult = this._createBlock(this.getPreviousCommandResult().getXmlCommandInput()); 
            
        }
        
        // the 'previousCommandResult' has already been set 
        // ... 
        
        // store in the bkell environments
        String previousVarName = "previous"; 
        IVariable variablePrev = new Variable(); 
        variablePrev.setName(previousVarName); 
        variablePrev.addChild(commandResult); 
        ((IMemory)this.getBkellEnv().findMemoryById("main.memory")).removeVariableByName(previousVarName); 
        ((IMemory)this.getBkellEnv().findMemoryById("main.memory")).addVariable(variablePrev); 
        
        IVariable variable = new Variable(); 
        variable.setName(variableName); 
        variable.addChild(commandResult); 
        ((IMemory)this.getBkellEnv().findMemoryById("main.memory")).removeVariableByName(variableName); 
        ((IMemory)this.getBkellEnv().findMemoryById("main.memory")).addVariable(variable); 
        
        outATwohandexpr(node); 
        
    }
	
	
	/* reading a variable as input 
	 */
    public void caseAVarCommandInput(AVarCommandInput node) { 
    	
        inAVarCommandInput(node); 
        
        String variableName = null; 
        if(node.getVarname() != null) { 
            
        	node.getVarname().apply(this); 
            variableName = node.getVarname().toString(); 
            
            // remove the '@' sign 
            variableName = variableName.substring(1); //** if this fails, then the user only put in the '@' 
            variableName = variableName.trim(); 
            
        }
        
        
        // retrieve from the bkell environments 
        IBob commandResult = null;  
        IMemory memory = (IMemory)this.getBkellEnv().findMemoryById("main.memory"); 
    	if(variableName.equals("memory")) { 
        	
    		HashMap allPrefixes = new HashMap(); 
    		
    		IBob eachbob = null; 
        	ISystem system = BookkeepingSystem.instance().getMODEL(); 
        	while(system.canPull()) { 
        		
        		eachbob = system.pullNext(); 
        		allPrefixes.putAll(eachbob.getPrefixMappings()); 
        	}
        	memory.setPrefixMappings(allPrefixes); 
    		commandResult = memory; 
        }
        else { 
        	
        	IVariable variable = memory.findVariableByName(variableName);
        	commandResult = (IBob)variable.getChildren().get(0);
        }
        
        
        // setting the 'previousCommandResult'
        AXmlCommandInput replacor = LoadHelper.reparse(commandResult.toXML(false)); 
        AXmlCommandInputRef replacorReference = new AXmlCommandInputRef(); 
        replacorReference.setXmlCommandInput(replacor); 
        replacorReference.setReference(commandResult); 
        this.setPreviousCommandResult(replacorReference); 
        
        
        node.replaceBy(replacor); 
        
        outAVarCommandInput(node); 
        
    }
    
    
    
    /* commit command 
     */
    public void caseACommitCommand7(ACommitCommand7 node) { 
    	
        inACommitCommand7(node); 
        
        logger.debug("caseACommitCommand7 CALLED > node["+node+"] > contextUrl["+this.getContextUrl()+"]"); 
        
        String cxpath = null; 
        String inputXML = null; 
        
        if(this.userSession == null) { 
        	throw new AuthorisationException("You are not logged in"); 
        }
        IUser user = this.aauthentication.findUser(this.userSession.getUserid()); 
        
        
        inACommitCommand7(node);
        if(node.getCommit() != null)
        {
            node.getCommit().apply(this);
        }
        if(node.getLbdepth1() != null)
        {
            node.getLbdepth1().apply(this);
        }
        if(node.getLbdepth2() != null)
        {
            node.getLbdepth2().apply(this);
        }
        if(node.getInput1() != null) { 
        	
        	logger.debug("caseACommitCommand7 > input1 type["+ node.getInput1().getClass() +"]"); 
            if( node.getInput1().getClass() != AXmlCommandInput.class) { 
	        	node.getInput1().apply(this); 
	            cxpath = node.getInput1().toString(); 
	            cxpath = cxpath.replaceAll("`", ""); 
	            cxpath = cxpath.replaceAll(" ", ""); 
	            cxpath = cxpath.trim(); 
            }
            else if(node.getInput1().getClass() != ACmdCommandInput.class) { 
            	node.getInput1().apply(this); 
            	cxpath = this.getContextUrl(); 
            }
            else { 
            	cxpath = this.getContextUrl(); 
            }
        }
        if(node.getRbdepth2() != null)
        {
            node.getRbdepth2().apply(this);
        }
        if(node.getInput2() != null) {
        	
            node.getInput2().apply(this); 
            
            logger.debug("caseACommitCommand7::getInput2 > class["+node.getInput2().getClass()+"] > data["+node.getInput2().toString()+"]"); 
            
            if( node.getInput2().getClass() == AXmlCommandInput.class ) { // raw XML input
            	
            	String cinput_s = node.getInput2().toString(); 
	        	String input_3 = Util.filterSpacesFromXML(cinput_s); 
	        	logger.debug("XML input > BEFORE["+cinput_s+"] > AFTER["+input_3+"]"); 
	        	
	    		IBob created = new Bob().load( input_3, BookkeepingSystemProperties.instance().getProperty("bob.def") ); 
	    		inputXML = created.toXML(false); 

            	//** add returnInput option here 
            	/*if( node.getInput2() != null && ( node.getInput2(). ) ) { 
            		
            		if( ((AIlistpart)((AIlist)e).getIlistpart().get(0)).getCommandInput() 
            			instanceof com.interrupt.bookkeeping.cc.node.AOptsCommandInput) { 
	                	
	    	            /*PCommandInput cinput = ((AIlistpart)((AIlist)e).getIlistpart().get(0)).getCommandInput(); 
	    	            
	    	            logger.debug("command option["+ cinput +"]"); 
	    	            
	    	            CommandOptionVisitor covisitor = new CommandOptionVisitor(); 
	    	            covisitor.setCommand( createCommand ); 
	    	            cinput.apply( covisitor ); 
	    	            
	    	            IOption option = covisitor.getCommand().getOption("returninput"); 
	    	            returnInput = Boolean.parseBoolean(option.getValue()); 
	    	            
	    	            logger.debug("caseAAddCommand1 > input option["+option+"]"); 
	    	            * /
	                }
            	}
            	*/
            }
            else if( node.getInput2().getClass() == AVarCommandInput.class ) { // VARIABLE input 
            	
            	inputXML = node.getInput2().toString(); 
            }
            else { 	// result from PREVIOUS command 
            	
	            AXmlCommandInputRef cinput = this.getPreviousCommandResult(); 
	            String cinput_s = cinput.getXmlCommandInput().toString(); 
	        	String input_3 = Util.filterSpacesFromXML(cinput_s); 
	        	
	    		IBob created = new Bob().load( input_3, BookkeepingSystemProperties.instance().getProperty("bob.def") ); 
	    		inputXML = created.toXML(false); 
            }
            
            
            IBob populateResult = Util.populateEmptyIDs(inputXML); 
            inputXML = populateResult.toXML(false); 
            
            
            logger.debug("caseACommitCommand7:: setContextXPath["+cxpath+"] > setInputXML["+inputXML+"]"); 
            CommitCommand commitc = new CommitCommand(spittoon); 
            commitc.setAauthentication(this.getAauthentication()); 
            commitc.setContextXPath(cxpath); 
            commitc.setContextUrl(this.getContextUrl()); 
            commitc.setInputXML(inputXML); 
            
            commitc.execute(user); 
            
        }
        if(node.getRbdepth1() != null)
        {
            node.getRbdepth1().apply(this);
        }
        outACommitCommand7(node);
		
    }
    
    
    /* print command 
     */
    public void caseAPrintCommand6(APrintCommand6 node) { 
    	
        inAPrintCommand6(node);
        if(node.getPrint() != null) {
        	
            node.getPrint().apply(this);
        }
        if(node.getLbracket() != null) { 
        	
            node.getLbracket().apply(this);
        }
        if(node.getCommandInput() != null) {
        	
            
        	node.getCommandInput().apply(this); 
        	
        	if(this.userSession == null) { 
        		throw new AuthorisationException("You are not logged in"); 
        	}
        	IUser user = this.aauthentication.findUser(this.userSession.getUserid()); 
        	
        	CreateCommand ccmd = new CreateCommand(); 
        	ccmd.setAauthentication(this.aauthentication); 
        	
        	/*IResult result = CreateHelper.execute(user, ccmd, this.getPreviousCommandResult().getXmlCommandInput()); 
        	if(result.allChildren().get(0) != null) { 
        		IBob bobResult = (IBob)result.allChildren().get(0); 
				
        		//logger.debug("");
        		logger.debug(bobResult.toXML(false));
        		logger.debug("\n"); 
        	}
            */
        	IBob result = this.getPreviousCommandResult().getReference();  
        	logger.debug(result.toXML(false));
        	
        }
        if(node.getRbracket() != null) { 
        	
            node.getRbracket().apply(this);
        }
        outAPrintCommand6(node);
        
    }
    
    
    /* bookkeeping structure manipulations - create, load, add, remove, reverse, find, list 
     */
	public void caseACreateCommand3(ACreateCommand3 node) { 
    	
		
		IResult result = null; 
		IBob bobReplacor = null; 
		
        inACreateCommand3(node);
        
    	//logger.debug("caseACreateCommand3["+node+"] CALLED"); 
		if(node.getCreate() != null) { 
			
            node.getCreate().apply(this);
        }
        if(node.getLbracket() != null) {
        	
        	node.getLbracket().apply(this);
        }
        if(node.getCommandInput() != null) {	
        	
        	node.getCommandInput().apply(this); 
    		
        	
        	CreateExecutor cexecutor = new CreateExecutor(); 
        	cexecutor.setAauthenticationToInject(this.aauthentication); 
        	
        	if(this.userSession == null) { 
            	throw new AuthorisationException("You are not logged in"); 
            }
            IUser user = this.aauthentication.findUser(this.userSession.getUserid()); 
            result = cexecutor.execute(user, node); 
    		
            // replaceby XMLCommandInput 
    		//logger.debug("1. "+ node.getClass()); 
            //logger.debug("2. "+ node.parent().getClass()); 
            //logger.debug("3. "+ node.parent().parent().getClass()); 
            //logger.debug("4. "+ node.parent().parent().parent().getClass()); 
            
            
            List children = result.getChildren(); 
        	bobReplacor = (IBob)children.get(0); 
        	
        	
            //if(node.parent().parent() instanceof ACmdCommandInput) { 

                // setting the 'previousCommandResult'
                AXmlCommandInput replacor = LoadHelper.reparse(bobReplacor.toXML(false)); 
                AXmlCommandInputRef replacorReference = new AXmlCommandInputRef(); 
                replacorReference.setXmlCommandInput(replacor); 
                replacorReference.setReference(bobReplacor); 
                this.setPreviousCommandResult(replacorReference); 
                
                System.out.println("1. caseACreateCommand3: setting the previous command result - : "+ result.toXML(false)); 
                
            	// for commands that use the result of create, we are replacing this node with the XML result 
	            // - replaceby XMLCommandInput 
	            //node.parent().parent().replaceBy(replacor); 
	            
            //}
            
        }
        if(node.getRbracket() != null) {
        	
        	node.getRbracket().apply(this);
        }
        
        System.out.println("2. caseACreateCommand3: setting the previous command result: "+ result.toXML(false)); 
        
        outACreateCommand3(node);
        
        //logger.debug("Create THE END: ["+bobReplacor+"] CALLED"); 
		
    }
    
	public void caseALoadCommand3(ALoadCommand3 node) {
		
		
        inALoadCommand3(node); 
        
    	
		if(node.getLoad() != null) {
            node.getLoad().apply(this);
        }
        if(node.getLbracket() != null) {
            node.getLbracket().apply(this);
        }
        if(node.getCommandInput() != null) {
        	
        	// collect tag & all attributes 
        	// list all possible closest results 
            // return first one; warn user that there were other possibilities, and to isolate search 
        	logger.debug("caseALoadCommand3 > CommandInput type ["+node.getCommandInput().getClass()+"]"); 
        	
        	IResult result = null; 
        	if(node.getCommandInput() instanceof com.interrupt.bookkeeping.cc.node.AOptsCommandInput) { 
        		
        		logger.debug("TODO - Re-Implement load via - AOptsCommandInput"); 
        	}
        	if(node.getCommandInput() instanceof com.interrupt.bookkeeping.cc.node.AXpathCommandInput) {
        		
        		String cxpath = node.getCommandInput().toString(); 
	        	logger.debug("contextXPath BEFORE ["+ cxpath +"]"); 
	        	
	        	cxpath = cxpath.replaceAll("`", ""); 
	            cxpath = cxpath.replaceAll(" ", ""); 
	            cxpath = cxpath.trim(); 
	            logger.debug("contextXPath AFTER ["+ cxpath +"]"); 
	        	
	        	//** 
	        	node.getCommandInput().apply(this);
	            LoadExecutor lexecutor = new LoadExecutor(); 
	            if(this.userSession == null) { 
	            	throw new AuthorisationException("You are not logged in"); 
	            }
	            IUser user = this.aauthentication.findUser(this.userSession.getUserid()); 
	            
	            lexecutor.setAauthenticationToInject( this.getAauthentication() ); 
	            lexecutor.setSpittoon(spittoon); 
	            lexecutor.setContextXPath( cxpath ); 
	            result = lexecutor.execute(user, node); 
	            
	            setContextUrl(lexecutor.getContextUrl()); 
        	}
        	
            //logger.debug("1. "+ node.getClass()); 
            //logger.debug("2. "+ node.parent().getClass()); 
            //logger.debug("3. "+ node.parent().parent().getClass()); 
            //logger.debug("4. "+ node.parent().parent().parent().getClass()); 
            
        	List children = result.getChildren(); 
        	IBob bobReplacor = (IBob)children.get(0); 
        	
            // replace by XMLCommandInput 
        	Logger.getLogger("bobstate").debug(bobReplacor.toXML(false)); 
        	
            AXmlCommandInput replacor = LoadHelper.reparse(bobReplacor.toXML(false)); 
            AXmlCommandInputRef replacorReference = new AXmlCommandInputRef(); 
            replacorReference.setXmlCommandInput(replacor); 
            replacorReference.setReference(bobReplacor); 
            
            // setting the 'previousCommandResult'
        	this.setPreviousCommandResult(replacorReference); 
			if(node.parent().parent() instanceof ACmdCommandInput) { 
	            node.parent().parent().replaceBy(replacor); 
            }
            
        }
        if(node.getRbracket() != null) {
            node.getRbracket().apply(this);
        }
        outALoadCommand3(node);
    }
    
	
	public void caseALoginCommand3(ALoginCommand3 node) { 
		
	    
        inALoginCommand3(node);
        if(node.getLogin() != null) {
            node.getLogin().apply(this);
        }
        if(node.getLbracket() != null) { 
            node.getLbracket().apply(this); 
        }
        if(node.getCommandInput() != null) { 
        	
        	
            node.getCommandInput().apply(this); 
            logger.debug("caseALoginCommand3 > CommandInput type ["+ node.getCommandInput().getClass() +"]"); 
            
            IResult userResult = null;  
            if(node.getCommandInput() instanceof com.interrupt.bookkeeping.cc.node.AOptsCommandInput) { 
            	
	            PCommandInput cinput = node.getCommandInput(); 
	            LoginCommand lcommand = new LoginCommand(spittoon); 
	            lcommand.setParent(this.getBkellEnv()); // ** TODO - a kludge to make searchTree work   
	            
	            
	            CommandOptionVisitor covisitor = new CommandOptionVisitor(); 
	            covisitor.setCommand(lcommand); 
	            cinput.apply(covisitor); 
	            
	            userResult = lcommand.execute(null); // null user entry b/c this is the command that will log the user in 
	            if(userResult == null) { 
	            	throw new AuthorisationException("Incorrect username / password combination"); 
	            }
            }
            this.aauthentication = (Aauthentication)userResult.getChildren().get(0); 
            this.userSession = this.aauthentication.getUserSession(); 
            logger.debug("caseALoginCommand3::userSession: "+ this.userSession.toXML(false)); 
		    
            AXmlCommandInput replacor = LoadHelper.reparse(this.userSession.toXML(false)); 
            AXmlCommandInputRef replacorReference = new AXmlCommandInputRef(); 
            replacorReference.setXmlCommandInput(replacor); 
            replacorReference.setReference(this.userSession); 
            
            // setting the 'previousCommandResult'
        	this.setPreviousCommandResult(replacorReference); 
			
        }
        if(node.getRbracket() != null) {
            node.getRbracket().apply(this);
        }
        outALoginCommand3(node); 
        
        logger.debug("Login: userSession: " + this.userSession); 
        
    }
	
    public void caseALogoutCommand4(ALogoutCommand4 node) { 
    	
        inALogoutCommand4(node);
        if(node.getLogout() != null) { 
        	
            node.getLogout().apply(this); 
            
            //1. logout the user 
            LogoutCommand lcommand = new LogoutCommand(this.aauthentication); 
            lcommand.execute(this.aauthentication.findUser(this.userSession.getUserid())); // null user entry b/c this is the command that will log te user in 
            
            //2. set the userSession to null 
            if(this.userSession == null) { 
            	throw new AuthorisationException("You are not logged in"); 
            }
            
        }
        outALogoutCommand4(node);
    }
    
    
	public void caseAAddCommand1(AAddCommand1 node) { 
		
		
		IBob addee = null; 
		if(this.userSession == null) { 
        	throw new AuthorisationException("You are not logged in"); 
        }
		IUser user = this.aauthentication.findUser(this.userSession.getUserid()); 
    	String variableName = null; 
    	String originalTagId = null; 
    	boolean varInput = false; 
    	AVarCommandInput ceeOne = null; 
    	
    	
        inAAddCommand1(node);
        if(node.getAdd() != null) {
            node.getAdd().apply(this);
        }
        if(node.getLbdepth1() != null) { 
            node.getLbdepth1().apply(this);
        }
        if(node.getLbdepth2() != null) {
            node.getLbdepth2().apply(this);
        }
        if(node.getCommandInput() != null) { 
        	
        	
        	if(node.getCommandInput() instanceof AVarCommandInput) { 
        		varInput = true; 
        		ceeOne = (AVarCommandInput)node.getCommandInput(); 
        		
        		//ceeOne.getVarname().apply(this); 
                variableName = ceeOne.getVarname().toString(); 
                
                // remove the '@' sign 
                variableName = variableName.substring(1); //** if this fails, then the user only put in the '@' 
                variableName = variableName.trim(); 
            }
        	
        	/** 
        	 * The Token Acceptor 
        	 * 
        	 * IMPORTANT. XML, Options, or Command will be the input option 
        	 * [X]	i. The Command will be processed in the visitor. 
        	 * [OK]	ii. and the XML should be processed (not loaded) 
        	 * [OK]	iii. We do want to process the Options though 
        	 */
        	node.getCommandInput().apply(this); 
        	
        	logger.debug("caseAAddCommand1 > XPath command input["+
        			node.getCommandInput().getClass() + "] > previous command result["+this.getPreviousCommandResult()+"]"); 
        	
            
        	if(node.getCommandInput() instanceof com.interrupt.bookkeeping.cc.node.AXpathCommandInput && 
        			this.getPreviousCommandResult() == null ) {
        		
        		String cxpath = node.getCommandInput().toString(); 
	        	logger.debug("contextXPath BEFORE ["+ cxpath +"]"); 
	        	
	        	cxpath = cxpath.replaceAll("`", ""); 
	            cxpath = cxpath.replaceAll(" ", ""); 
	            cxpath = cxpath.trim(); 
	            logger.debug("contextXPath AFTER ["+ cxpath +"]"); 
	        	
	        	//** 
	        	node.getCommandInput().apply(this);
	            LoadExecutor lexecutor = new LoadExecutor(); 
	            if(this.userSession == null) { 
	            	throw new AuthorisationException("You are not logged in"); 
	            }
	            
	            lexecutor.setAauthenticationToInject( this.getAauthentication() ); 
	            lexecutor.setSpittoon(spittoon); 
	            lexecutor.setContextXPath( cxpath ); 
	            addee = lexecutor.execute(user, node); 
	            
	            setContextUrl(lexecutor.getContextUrl()); 
        	}
            else if(node.getCommandInput() instanceof AXmlCommandInput) { 
            	
            	// if the input was raw XML, reparse into Bob object 
            	CreateCommand createCommand = new CreateCommand(); 
            	createCommand.setAauthentication(this.getAauthentication()); 
            	
            	IResult result = CreateHelper.execute(user, createCommand, (AXmlCommandInput)node.getCommandInput()); 
            	addee = (IBob)result.getChild(0); 
            	
            }
            else { 
            	addee = this.getPreviousCommandResult().getReference(); 
            }
            
            
            if(addee instanceof IResult) { 
            	addee = (IBob)addee.allChildren().get(0); 
            }
            logger.debug("caseAAddCommand1 addee: "+ addee.toXML(false)); 
            
            originalTagId = addee.getAttributeValue("id");
            
        }
        if(node.getRbdepth2() != null) { 
            node.getRbdepth2().apply(this);
        }
        
        IBob input = null; 
        boolean returnInput = false; 
        {
        	
			//logger.debug( "caseAAddCommand1 addee: "+ ((ISystem)addee).findAauthenticationById("main.authentication") ); 
			//** process list of inputs... 
			
			AddCommand addCommand = new AddCommand(); 
			addCommand.setTokenReciever(addee); 
			
            List<PIlist> copy = new ArrayList<PIlist>(node.getIlist());
            for(PIlist e : copy) { 
                
            	e.apply(this); 
            	logger.debug("caseAAddCommand1 each input["+e+"] / input class["+e.getClass()+"] / previous["+this.getPreviousCommandResult()+"]"); 
            	
            	
            	//** now add the gear 
            	if( ((AIlist)e).getCommandInput() instanceof AXmlCommandInput ) { 
					
            		CreateCommand createCommand = new CreateCommand(); 
                	createCommand.setAauthentication(this.getAauthentication()); 
                	
	            	IResult result = CreateHelper.execute(user, createCommand, (AXmlCommandInput)((AIlist)e).getCommandInput()); 
					input = (IBob)result.allChildren().get(0); 
					
					addCommand.getTokenLiterals().addChild( input ); 
	            	logger.debug("caseAAddCommand1 <AXmlCommandInput>: " + (IBob)result.allChildren().get(0)); 
	            	//logger.debug("((AIlist)e).getIlistpart()["+ ((AIlist)e).getIlistpart() +"] / ["+ 
	            	//		((AIlistpart)((AIlist)e).getIlistpart().get(0)).getCommandInput().getClass() +"]"); 
					
	            	//** add returnInput option here 
	            	if( ((AIlist)e).getIlistpart() != null && !(((AIlist)e).getIlistpart()).isEmpty() ) { 
	            		
	            		if( ((AIlistpart)((AIlist)e).getIlistpart().get(0)).getCommandInput() 
	            			instanceof com.interrupt.bookkeeping.cc.node.AOptsCommandInput) { 
		                	
		    	            PCommandInput cinput = ((AIlistpart)((AIlist)e).getIlistpart().get(0)).getCommandInput(); 
		    	            
		    	            logger.debug("command option["+ cinput +"]"); 
		    	            
		    	            CommandOptionVisitor covisitor = new CommandOptionVisitor(); 
		    	            covisitor.setCommand( createCommand ); 
		    	            cinput.apply( covisitor ); 
		    	            
		    	            IOption option = covisitor.getCommand().getOption("returninput"); 
		    	            returnInput = Boolean.parseBoolean(option.getValue()); 
		    	            
		    	            logger.debug("caseAAddCommand1 > input option["+option+"]"); 
		                }
	            	}
	            }
				else if(this.getPreviousCommandResult() != null) { 
					
					logger.debug("caseAAddCommand1 <previous command result>: "+ this.getPreviousCommandResult().getReference()); 
					addCommand.getTokenLiterals().addChild(this.getPreviousCommandResult().getReference()); 
	            	
				}
				
			}
            
            logger.debug("Add Command BEFORE ID population["+ addCommand +"]"); 
            Util.populateEmptyIDs(addCommand); 
            logger.debug("Add Command AFTER ID population["+ addCommand +"]"); 
            
            //logger.debug( "caseAAddCommand2 addee USERS: "+ ((Aauthentication)((ISystem)addee).findAauthenticationById("main.authentication")).getUsers() ); 
            addCommand.setAauthentication(this.getAauthentication()); 
            addCommand.execute(user); //** 
			
        }
        
        if(node.getRbdepth1() != null) { 
            node.getRbdepth1().apply(this);
        }
        outAAddCommand1(node); 
        
        //logger.debug("Add: THE END ["+addee+"]"); 
        if(varInput) { 
        	
        	logger.debug(">> main memory: "+ ((IMemory)this.getBkellEnv().findMemoryById("main.memory")) ); 
        	logger.debug(">> variable name ["+ variableName +"] / variable ["+ ((IMemory)this.getBkellEnv().findMemoryById("main.memory"))
    				.findVariableByName(variableName) +"]"); 
        	
        	((IMemory)this.getBkellEnv().findMemoryById("main.memory"))
				.findVariableByName(variableName).replace(addee.getTagName(), originalTagId, addee); 
        }
        
        if(!returnInput) 
        	input = addee; 
        logger.debug(">> returnInput["+returnInput+"] > ["+input+"]"); 
        
        // setting the 'previousCommandResult'
        AXmlCommandInput replacor = LoadHelper.reparse(input.toXML(false)); 
        AXmlCommandInputRef replacorReference = new AXmlCommandInputRef(); 
        replacorReference.setXmlCommandInput(replacor); 
        replacorReference.setReference(input); 
        this.setPreviousCommandResult(replacorReference); 
        //logger.debug("Add / Previous Command "+ addee.toXML()); 
        
    }
	
	public void caseAUpdateCommand1(AUpdateCommand1 node) {	
		
		IBob updatee = null; 
		String originalTagId = null; 
		String variableName = null; 
		if(this.userSession == null) { 
        	throw new AuthorisationException("You are not logged in"); 
        }
		IUser user = this.aauthentication.findUser(this.userSession.getUserid()); 
    	
    	logger.debug("c1 class["+node.getC1().getClass()+"]"); 
    	logger.debug(node.getC1() instanceof AXmlCommandInput); 
    	logger.debug(node.getC1() instanceof com.interrupt.bookkeeping.cc.node.AVarCommandInput); 
    	
    	/** 
    	 * KLUDGE since 'node.getC1()' gets converted to an 'AXmlCommandInput' on first apply
    	 */
    	boolean xmlInput = false; 
    	boolean varInput = false; 
    	AVarCommandInput ceeOne = null;   
    	if(node.getC1() instanceof AXmlCommandInput) { 
    		xmlInput = true; 
    	}
    	else if(node.getC1() instanceof com.interrupt.bookkeeping.cc.node.AVarCommandInput) { 
    		varInput = true; 
    		ceeOne = (AVarCommandInput)node.getC1();
    	}
    	
    	
        inAUpdateCommand1(node);
        if(node.getUpdate() != null)
        {
            node.getUpdate().apply(this);
        }
        if(node.getLbdepth1() != null)
        {
            node.getLbdepth1().apply(this);
        }
        if(node.getLbdepth2() != null)
        {
            node.getLbdepth2().apply(this);
        }
        if(node.getC1() != null)
        {
        	
        	logger.debug("Grrr 1"); 
        	
        	// 
            node.getC1().apply(this); 
            if(xmlInput) { 
            	
            	logger.debug("Grrr 2"); 
            	
            	// 1. for raw XML 
            	IResult result = CreateHelper.execute(user, new CreateCommand(), (AXmlCommandInput)node.getC1());
            	updatee = (IBob)result.getChild(0); 
            }
            else if (varInput) { 
            	
            	logger.debug("Grrr 3"); 
            	
            	// 2. for variable name 
            	if( ceeOne != null) { 
                    
                	ceeOne.getVarname().apply(this); 
                    variableName = ceeOne.getVarname().toString(); 
                    
                    // remove the '@' sign 
                    variableName = variableName.substring(1); //** if this fails, then the user only put in the '@' 
                    variableName = variableName.trim(); 
                    
                }
                
                // retrieve from the bkell environments 
                IMemory memory = (IMemory)this.getBkellEnv().findMemoryById("main.memory"); 
            	if(variableName.equals("memory")) { 
                	
            		HashMap allPrefixes = new HashMap(); 
            		IBob eachbob = null; 
                	ISystem system = BookkeepingSystem.instance().getMODEL(); 
                	while(system.canPull()) { 
                		
                		eachbob = system.pullNext(); 
                		allPrefixes.putAll(eachbob.getPrefixMappings()); 
                	}
                	memory.setPrefixMappings(allPrefixes); 
                	updatee = memory; 
                }
                else { 
                	
                	IVariable variable = memory.findVariableByName(variableName);
                	updatee = (IBob)variable.getChildren().get(0);
                }
            	
            }
            else { 
            	
            	logger.debug("Grrr 4"); 
            	
            	// 3. for function result 
            	//updatee = ((IResult)this.getPreviousCommandResult().getReference()).getChild(0);
            	updatee = this.getPreviousCommandResult().getReference();
            }
            
            logger.debug("caseAUpdateCommand1 updatee 1: "+ updatee.toXML(false)); 
            if(updatee instanceof IResult) { 
            	logger.debug("here 1"); 
            	updatee = updatee.getChild(0); 
            }
            
            logger.debug("caseAUpdateCommand1 updatee 2: "+ updatee.toXML(false)); 
            originalTagId = updatee.getAttributeValue("id"); 
            
        }
        if(node.getRbdepth2() != null)
        {
            node.getRbdepth2().apply(this);
        }
        if(node.getC2() != null)
        {
        	
        	logger.debug("Grrr 5"); 
        	
        	IBob updator = null; 
        	boolean xmlInput2 = false; 
        	boolean varInput2 = false; 
        	AVarCommandInput ceeTwo = null; 
        	if(node.getC2() instanceof AXmlCommandInput) { 
        		xmlInput2 = true; 
        	}
        	else if(node.getC2() instanceof AVarCommandInput) { 
        		varInput2 = true; 
        		ceeTwo = (AVarCommandInput)node.getC2();
        	}
        	
        	// 
            node.getC2().apply(this); 
            if( xmlInput2 ) { 
				
            	CreateCommand ccommand = new CreateCommand(); 
            	ccommand.setAauthentication(this.aauthentication); 
            	IResult result = CreateHelper.execute(user, ccommand, (AXmlCommandInput)node.getC2()); 
				
            	updator = (IBob)result.allChildren().get(0); 
            	updatee = updator; 
            	
            	logger.debug("caseAUpdateCommand1 ZING 2!!!! " + updatee.toXML(false)); 
				
			}
			else if( varInput2 ) { 
				
				logger.debug("caseAUpdateCommand1 ZING 3!!!!"); 
				updator = this.getPreviousCommandResult().getReference(); 
				updatee = updator; 
			}
            
            logger.debug("Grrr 6"); 
        	
            // ** replace variable in mem if applicable 
			if ( varInput ) {
            	
				logger.debug("Grrr 7"); 
				logger.debug("caseAUpdateCommand1 ZING 4!!!! A. / "+ ((IMemory)this.getBkellEnv().findMemoryById("main.memory"))
					.findVariableByName(variableName).find(updatee.getTagName(), originalTagId) ); 
				
				logger.debug("caseAUpdateCommand1 ZING 4!!!! B. / updatee: "+ updatee); 
				logger.debug("caseAUpdateCommand1 ZING 4!!!! D. / tagName["+updatee.getTagName()+"] / originalId["+originalTagId+"]");
				
				logger.debug("caseAUpdateCommand1 ZING 4!!!! E. / to replace: "+ ((IMemory)this.getBkellEnv().findMemoryById("main.memory")).findVariableByName(variableName)); 
				
				((IMemory)this.getBkellEnv().findMemoryById("main.memory"))
					.findVariableByName(variableName).replace(updatee.getTagName(), originalTagId, updator); 
				
            }
			
        }
        if(node.getRbdepth1() != null)
        {
            node.getRbdepth1().apply(this);
        }
        outAUpdateCommand1(node); 

        // setting the 'previousCommandResult'
        AXmlCommandInput replacor = LoadHelper.reparse(updatee.toXML(false)); 
        AXmlCommandInputRef replacorReference = new AXmlCommandInputRef(); 
        replacorReference.setXmlCommandInput(replacor); 
        replacorReference.setReference(updatee); 
        this.setPreviousCommandResult(replacorReference); 
    }
    
    
	public void caseARemoveCommand1(ARemoveCommand1 node) { 
		
		
		IBob removee = null; 
		String variableName = null; 
		boolean varInput = false; 
		AVarCommandInput ceeOne = null; 
		
		if(this.userSession == null) { 
        	throw new AuthorisationException("You are not logged in"); 
        }
		IUser user = this.aauthentication.findUser(this.userSession.getUserid());
		String originalTagId = null; 
		
		inARemoveCommand1(node);
        if(node.getRemove() != null) {
            node.getRemove().apply(this);
        }
        if(node.getLbdepth1() != null) {
            node.getLbdepth1().apply(this);
        }
        if(node.getLbdepth2() != null) {
            node.getLbdepth2().apply(this);
        }
        
        String cxpath = null;  
    	if(node.getCommandInput() != null) { 
            
        	logger.debug(">> remove / varInput["+ (node.getCommandInput() instanceof AVarCommandInput) +"]"); 
        	logger.debug(">> remove / class["+ node.getCommandInput().getClass() +"]"); 
        	if(node.getCommandInput() instanceof AVarCommandInput) { 		//** @variable CONTEXT 
        		
        		varInput = true; 
        		ceeOne = (AVarCommandInput)node.getCommandInput(); 
        		
        		//ceeOne.getVarname().apply(this); 
                variableName = ceeOne.getVarname().toString(); 
                
                // remove the '@' sign 
                variableName = variableName.substring(1); //** if this fails, then the user only put in the '@' 
                variableName = variableName.trim(); 
                
                logger.debug(">> remove / varName["+ variableName +"]"); 
        		
            }
        	if(node.getCommandInput() instanceof com.interrupt.bookkeeping.cc.node.AXpathCommandInput) { //** XPATH CONTEXT 
        		
        		
	        	//** 
	        	node.getCommandInput().apply(this);
	            LoadExecutor lexecutor = new LoadExecutor(); 
	            
	            lexecutor.setAauthenticationToInject( this.getAauthentication() ); 
	            lexecutor.setSpittoon(spittoon); 
	            lexecutor.setContextXPath( cxpath ); 
	            removee = lexecutor.execute(user, node); 
	            
	            setContextUrl(lexecutor.getContextUrl()); 
	            
        	}
        	
        	String ciString = node.getCommandInput().toString(); 		logger.debug("1["+ ciString +"]");
        	ciString = ciString.substring(ciString.indexOf("`") + 1); 		logger.debug("2["+ ciString +"]");
        	
        	int i = 0; 		int j = ciString.lastIndexOf("`"); 
        	logger.debug("3 > length["+ ciString.length() +"] > begin index["+ i +"] > end index["+ j +"]");
        	
        	ciString = ciString.substring(i, j); 	
        	logger.debug("4["+ ciString +"]");
        	
        	
        	cxpath = this._yankContextPath(ciString); 
        	logger.debug("Remove -1 > cxpath["+ cxpath +"]"); 
        	
        	
        	//** 
        	node.getCommandInput().apply(this);
        	
        	logger.debug("Remove - 0 > contextPath["+ this.getContextUrl() +"]"); 
        	
        	/** 
        	 * The Token Acceptor
        	 * 
        	 * IMPORTANT. XML, Options, or Command will be the input option
        	 * [X]	i. The Command will be processed in the visitor. 
        	 * [OK]	ii. and the XML should be processed (not loaded)
        	 * [OK]	iii. We do want to process the Options though 
        	 */
            node.getCommandInput().apply(this); 
            if(node.getCommandInput() instanceof AXmlCommandInput) { 	//** XML CONTEXT 
            	
            	// if the input was raw XML, reparse into Bob object 
            	CreateCommand ccommand = new CreateCommand(); 
            	ccommand.setAauthentication(this.aauthentication); 
            	IResult result = CreateHelper.execute(user, ccommand, (AXmlCommandInput)node.getCommandInput());
            	removee = (IBob)result.getChild(0); 
            }
            else { 
            	
            	removee = this.getPreviousCommandResult().getReference(); 
                
            }
            originalTagId = removee.getAttributeValue("id"); 
            
        }
        if(node.getRbdepth2() != null) {
            node.getRbdepth2().apply(this);
        }
        {
            
            { 
        		
	        	VisitorApplyRemove vremove = new VisitorApplyRemove(); 
	        	vremove.setUser(user); 
	        	vremove.setRemovee(removee); 
	        	vremove.setAauthentication(aauthentication);
	        	vremove.setSpittoon(spittoon); 
	        	vremove.setContextXPath( cxpath ); 
	            
	        	logger.debug("Remove - 1"); 
	        	
	        	//** process list of inputs... 
	            List<PIlist> copy = new ArrayList<PIlist>(node.getIlist());
	            for(PIlist e : copy) { 
	            	
	            	e.apply(this); 
	            	
	            	//** now remove the gear 
	            	e.apply(vremove); 
	            	
	            }
	            
                if(varInput) { 	//** if there's a variable, replace it 
                	
                	//logger.debug(">> main memory: "+ ((IMemory)this.getBkellEnv().findMemoryById("main.memory")) ); 
                	logger.debug(">> variable name ["+ variableName +"] / variable ["+ 
                		((IMemory)this.getBkellEnv().findMemoryById("main.memory"))
            				.findVariableByName(variableName) +"]"); 
                	
                	((IMemory)this.getBkellEnv().findMemoryById("main.memory"))
        				.findVariableByName(variableName).replace(removee.getTagName(), originalTagId, removee); 
                }
                
        	}
            
        }
        if(node.getRbdepth1() != null) { 
            node.getRbdepth1().apply(this);
        }
        outARemoveCommand1(node); 
        
        //logger.debug("Remove: THE END ["+removee+"]"); 
        
        
        // setting the 'previousCommandResult'
        AXmlCommandInput replacor = LoadHelper.reparse(removee.toXML(false)); 
        AXmlCommandInputRef replacorReference = new AXmlCommandInputRef(); 
        replacorReference.setXmlCommandInput(replacor); 
        replacorReference.setReference(removee); 
        this.setPreviousCommandResult(replacorReference); 
        
    }
	
	
	
    public void caseAReverseCommand5(AReverseCommand5 node) { 
    	
    	
    	IBob parentReversee = null; 
    	IBob reversee = null; 
    	IBob reversor = null; 
    	
    	IResult result = null; 
    	
        inAReverseCommand5(node);
        if(node.getReverse() != null) { 
        	
            node.getReverse().apply(this);
        }
        if(node.getLbdepth1() != null) { 
        	
            node.getLbdepth1().apply(this);
        }
        if(node.getLbdepth2() != null){ 
        	
            node.getLbdepth2().apply(this);
        }
        if(node.getCi1() != null){ 
        	
        	//**
            node.getCi1().apply(this);
            //parentReversee = this._addRemoveReverseLoadBlock(node.getCi1()); 
            parentReversee = this.getPreviousCommandResult().getReference(); 
            
        }
        if(node.getRbdepth2() != null){ 
        	
            node.getRbdepth2().apply(this);
        }
        if(node.getLbdepth3() != null){ 
        	
            node.getLbdepth3().apply(this);
        }
        if(node.getCi2() != null){ 
        	
        	//**
            node.getCi2().apply(this); 
            reversee = this._addRemoveReverseLoadBlock(node.getCi2()); 
    		
        }
        if(node.getRbdepth3() != null){ 
        	
            node.getRbdepth3().apply(this);
        }
        if(node.getCi3() != null){ 
        	
        	//**
            node.getCi3().apply(this);
            reversor = this._addRemoveReverseLoadBlock(node.getCi3()); 
    		
            ITokenLiterals tliterals = new GTokenLiterals();
            tliterals.addChild(reversor); 
            
            ReverseCommand rcommand = new ReverseCommand(); 
            rcommand.setTokenRecieverParent((IEntries)parentReversee); 
            rcommand.setTokenReciever(reversee);
            rcommand.setTokenLiterals(tliterals); 
            
            IUser user = new User(); 
    		user.setId("root"); 
    		user.setUsername("root"); 
    		this.aauthentication.authenticate("webkell", user); 
    		result = rcommand.execute(user); 
    		
        }
        if(node.getRbdepth1() != null){ 
        	
            node.getRbdepth1().apply(this);
        }
        outAReverseCommand5(node);
        
        IBob bobResult = (IBob)result.getChildren().get(0); 
        //logger.debug("Reverse: THE END ["+ bobResult +"]");
        
        
        // setting the 'previousCommandResult'
        AXmlCommandInput replacor = LoadHelper.reparse(bobResult.toXML(false)); 
        AXmlCommandInputRef replacorReference = new AXmlCommandInputRef(); 
        replacorReference.setXmlCommandInput(replacor); 
        replacorReference.setReference(bobResult); 
        this.setPreviousCommandResult(replacorReference); 
        
        
    }
    
    
	
	public void caseAFindCommand2(AFindCommand2 node) { 
		
		
		IBob findContext = null; 
		IBob bobResult = null; 
		
		if(this.userSession == null) { 
        	throw new AuthorisationException("You are not logged in"); 
        }
		IUser user = this.aauthentication.findUser(this.userSession.getUserid()); 
		
        inAFindCommand2(node);
        if(node.getFind() != null) { 
            node.getFind().apply(this);
        }
        if(node.getLbdepth1() != null) {
            node.getLbdepth1().apply(this);
        }
        if(node.getLbdepth2() != null) {
            node.getLbdepth2().apply(this);
        }
        if(node.getC1() != null) { 
        	
        	//** 
            node.getC1().apply(this); 
            findContext = this.getPreviousCommandResult().getReference(); 
            
        }
        if(node.getRbdepth2() != null) {
            node.getRbdepth2().apply(this);
        }
        if(node.getC2() != null) {
        	
        	boolean isOptions = (node.getC2().getClass() == AOptsCommandInput.class); 
        	
        	//** 
            node.getC2().apply(this); 
            
            ////////////
            if( isOptions ) { 
	        	
            	FindCommand fcommand = new FindCommand(); 
                fcommand.setTokenReciever(findContext); 
                
            	CommandOptionVisitor covisitor = new CommandOptionVisitor(); 
	    		covisitor.setCommand(fcommand); 
	    		node.getC2().apply(covisitor); 
	    		
	    		// execute command 
	    		//Logger.getLogger("bobstate").debug(loadCommand); 
	    		
	    		bobResult = fcommand.execute(user); 
	    		
            }
            else { 
            	
            	// if we are dealing with XML 

                IBob findor = this._addRemoveReverseLoadBlock(node.getC2()); 

                logger.debug("findee ["+findContext+"]"); 
                logger.debug("findor ["+findor+"]"); 
                
    			//--> load this 'tliteral' xml from system
    			FindVisitor fvisitor = new FindVisitor(); 
                fvisitor.setTagName(
                	findor.getTagName() 
                ); 
    			
    			// go 
                findContext.accept(fvisitor); 
    			
    	    	// results 
    			List presults = fvisitor.getPossibleResults(); 
    			List presults2 = new FindCommand().isolateSearchList(	//** TODO - this is really bad, but I need the isolate function 
    					findor.getAttributes()
    					, presults); 
    			
    			//logger.debug("WARNING. These are possible results. Returning first one"); 
    			//logger.debug(presults2); 
    			
    			if(presults2.size() > 0) { 
    				
    				bobResult = (IBob)presults2.get(0); 
    			}
            }
            ////////////
    		
        }
        if(node.getRbdepth1() != null) {
            node.getRbdepth1().apply(this);
        }
        outAFindCommand2(node); 
        
        //logger.debug("Find: THE END ["+ result +"]"); 
        
        
        // setting the 'previousCommandResult'
        AXmlCommandInput replacor = LoadHelper.reparse(bobResult.toXML(false)); 
        AXmlCommandInputRef replacorReference = new AXmlCommandInputRef(); 
        replacorReference.setXmlCommandInput(replacor); 
        replacorReference.setReference(bobResult); 
        this.setPreviousCommandResult(replacorReference); 
        
    }
    
	
	public void caseAListCommand2(AListCommand2 node) { 
		
		
		IBob listee = null; 
		IResult result = new GResult(); 
		
        inAListCommand2(node);
        if(node.getList() != null) {
            node.getList().apply(this);
        }
        if(node.getLbdepth1() != null) {
            node.getLbdepth1().apply(this);
        }
        if(node.getLbdepth2() != null) {
            node.getLbdepth2().apply(this);
        }
        if(node.getC1() != null) {
            
        	//** 
        	node.getC1().apply(this);
        	listee = this.getPreviousCommandResult().getReference(); 
        	
        	logger.debug("caseAListCommand2 > context["+ listee +"]"); 
        	
        }
        if(node.getRbdepth2() != null) {
            node.getRbdepth2().apply(this);
        }
        if(node.getC2() != null) {
            
        	//** 
        	node.getC2().apply(this);
        	
            IBob listor = this._addRemoveReverseLoadBlock(node, node.getC2()); 
            logger.debug("caseAListCommand2 > input["+ listor +"]"); 
        	
			//--> load this 'tliteral' xml from system
			FindVisitor fvisitor = new FindVisitor(); 
            fvisitor.setTagName( listor.getTagName() ); 
			listee.acceptFirst(fvisitor); 
			
	    	// results 
			List presults = fvisitor.getPossibleResults(); 
			if(listor.getAttributes().getLength() > 0) { 
				
				presults = new FindCommand().isolateSearchList(	//** this is really bad, but I need the isolate function 
					listor.getAttributes()
					, presults); 
			} 
			
			logger.debug("isolated results["+ presults +"]"); 
			
			if(presults.size() > 0) { 
				result.addChildren(presults); 
			}
			
        }
        if(node.getRbdepth1() != null) {
            node.getRbdepth1().apply(this);
        }
        outAListCommand2(node);
        
        logger.debug("List: THE END ["+ result +"]"); 
        
        
        // setting the 'previousCommandResult'
        IBob bobResult = null;
        try { 
        	bobResult = (IBob)result.getChildren().get(0); 
        }
        catch(IndexOutOfBoundsException e) { 
        	logger.error("java.lang.IndexOutOfBoundsException["+e.getMessage()+"]"); 
        	bobResult = new Bob(); 
        }
        AXmlCommandInput replacor = LoadHelper.reparse(bobResult.toXML(false)); 
        AXmlCommandInputRef replacorReference = new AXmlCommandInputRef(); 
        replacorReference.setXmlCommandInput(replacor); 
        replacorReference.setReference(bobResult); 
        this.setPreviousCommandResult(replacorReference); 
        
    }
	
	
	public void caseAExitCommand4(AExitCommand4 node) { 
		
		logger.debug("caseAExitCommand4 CALLED"); 
    	inAExitCommand4(node);
        
        if(node.getExit() != null) {
            node.getExit().apply(this);
        }
        
        //** turn off this visitor's switch 
        this.setActive(false); 
        logger.debug("De-Activating ExpressionVisitor ["+ this.isActive() +"]"); 
    	
        outAExitCommand4(node); 
    }
	
	
	private String yankContextPath( ALoadCommand3 node ) { 
		return this._yankContextPath( node.getCommandInput().toString() );
	}
	private String yankContextPath( ARemoveCommand1 node ) { 
		return this._yankContextPath( node.getCommandInput().toString() ); 
	}
	private String _yankContextPath( String inputString ) { 
		
		String cxpath = inputString; 
    	logger.debug("contextXPath BEFORE ["+ cxpath +"]"); 
    	
    	cxpath = cxpath.replaceAll("`", ""); 
        cxpath = cxpath.replaceAll(" ", ""); 
        cxpath = cxpath.trim(); 
        logger.debug("contextXPath AFTER ["+ cxpath +"]"); 
    	
        return cxpath; 
	}
	private IBob _createBlock(Node cinput) { 
		
		
		IBob bobee = null; 
        CreateCommand createCommand = (com.interrupt.bookkeeping.cc.bkell.command.CreateCommand)
			Bob.make( "com.interrupt.bookkeeping.cc.bkell.command.CreateCommand" ); 
        
        if(this.userSession == null) { 
    		throw new AuthorisationException("You are not logged in"); 
    	}
    	IUser user = this.aauthentication.findUser(this.userSession.getUserid());
    	
    	createCommand.setAauthentication(this.getAauthentication()); 
    	IResult addee_r = CreateHelper.execute(user, createCommand, (AXmlCommandInput)cinput);
    	List addeeList = addee_r.allChildren(); 
    	bobee = (IBob)addeeList.get(0); 
    	
    	return bobee; 
	}
	
	private IBob _addRemoveReverseLoadBlock( PCommandInput cinput ) { 
		return this._addRemoveReverseLoadBlock(null, cinput); 
	}
	private IBob _addRemoveReverseLoadBlock( AListCommand2 node, PCommandInput cinput ) {
		
		
		IBob bobee = null; 
        
		LoadCommand loadCommand = (com.interrupt.bookkeeping.cc.bkell.command.LoadCommand)
			Bob.make( "com.interrupt.bookkeeping.cc.bkell.command.LoadCommand" ); 
		CreateCommand createCommand = (com.interrupt.bookkeeping.cc.bkell.command.CreateCommand)
			Bob.make( "com.interrupt.bookkeeping.cc.bkell.command.CreateCommand" ); 
		
		if( cinput instanceof com.interrupt.bookkeeping.cc.node.AXpathCommandInput ) {
    		
    		String cxpath = cinput.toString(); 
        	logger.info("contextXPath BEFORE ["+ cxpath +"]"); 
        	
        	cxpath = cxpath.replaceAll("`", ""); 
            cxpath = cxpath.replaceAll(" ", ""); 
            cxpath = cxpath.trim(); 
            logger.info("contextXPath AFTER ["+ cxpath +"]"); 
        	
        	//** 
        	cinput.apply(this);
            LoadExecutor lexecutor = new LoadExecutor(); 
            if(this.userSession == null) { 
            	throw new AuthorisationException("You are not logged in"); 
            }
            
            lexecutor.setAauthenticationToInject( this.getAauthentication() ); 
            lexecutor.setSpittoon(spittoon); 
            lexecutor.setContextXPath( cxpath ); 
            IResult addee_r = lexecutor.execute(this.aauthentication.findUser(this.userSession.getUserid()), node); 
            List addeeList = addee_r.allChildren(); 
    		bobee = (IBob)addeeList.get(0);
    		
    		logger.debug("_addRemoveReverseLoadBlock > load executor result["+ bobee +"]"); 
            
            setContextUrl(lexecutor.getContextUrl()); 
    	}
		else if(cinput instanceof AXmlCommandInput) { 
        	
    		if(this.userSession == null) { 
        		throw new AuthorisationException("You are not logged in"); 
        	}
        	IUser user = this.aauthentication.findUser(this.userSession.getUserid()); 
        	IResult addee_r = CreateHelper.execute(user, createCommand, (AXmlCommandInput)cinput);
    		List addeeList = addee_r.allChildren(); 
    		bobee = (IBob)addeeList.get(0); 
    		
        }
		else if(cinput instanceof AOptsCommandInput) { 
        	
			if(this.userSession == null) { 
        		throw new AuthorisationException("You are not logged in"); 
        	}
        	IUser user = this.aauthentication.findUser(this.userSession.getUserid()); 
        	IResult addee_r = LoadHelper.execute(user, loadCommand, (AOptsCommandInput)cinput);
    		List addeeList = addee_r.allChildren(); 
    		bobee = (IBob)addeeList.get(0); 
    		
        }
    	
    	return bobee; 
	}
	
    public static void main(String args[]) { 
    	
    	
    	// setup command 
		Bob bob = new Bob(); 
		(System.getProperties()).setProperty("bob.gen", "gen/"); 
		(System.getProperties()).setProperty("bob.base", "."); 
		
		
		//logger.debug("HERE 1"); 
		
		String input_s = "< account xmlns = ' com / interrupt / bookkeeping / account ' id = ' 1 ' name = ' office equipment ' type = ' asset ' counterWeight = ' debit ' > < debit xmlns = ' com / interrupt / bookkeeping / account ' id = ' ' amount = ' 10.00 ' entryid = ' ' accountid = ' 1 ' / > < / account >"; 
		String input_1 = input_s.replaceAll("< ", "<");  
		String input_2 = input_1.replaceAll("\\/ >", "/>"); 
		String input_x = input_2.replaceAll("< \\/", "</"); 
		String input_y = input_x.replaceAll("<\\/ ", "</"); 
		String input_xx = input_y.replaceAll(" \\/ ", "/"); 
		String input_3 = input_xx.replaceAll(" = ", "="); 
		
		//String input_3 = "<account xmlns = 'com/interrupt/bookkeeping/account' id = ' 1 ' name = ' office equipment ' type = ' asset ' counterWeight = ' debit ' > <debit xmlns = 'com/interrupt/bookkeeping/account' id = ' ' amount = ' 10.00 ' entryid = ' ' accountid = ' 1 ' /> </account>"; 
		
		//logger.debug("HERE 2 ["+ input_3 +"]"); 
		
		IBob created = bob.load( input_3, BookkeepingSystemProperties.instance().getProperty("bob.def") ); 
		//IBob created = bob.load( "<account xmlns='com/interrupt/bookkeeping/account' id='1' name='office equipment' type='asset' counterWeight='debit' > <debit xmlns='com/interrupt/bookkeeping/account' id='' amount='10.00' entryid='' accountid = ' 1 ' /> </account>", 
		//	"xml/bookkeeping.2.bookkeeping.xml xml/bookkeeping.2.transactions.xml xml/bookkeeping.2.users.xml" ); 
		
		
		
		//logger.debug("HERE 3"); 
		
		//created.toXML(System.out); 
    	
    }
    
    
    
    /** 
     * return XMLCommandInput from these commands 
     */
    public void caseAC1Command(AC1Command node) {
        
    	inAC1Command(node);
        if(node.getCommand1() != null) {
            node.getCommand1().apply(this);
        }
        outAC1Command(node);
    }
    
    
    public void caseAC2Command(AC2Command node) {
        
    	inAC2Command(node);
        if(node.getCommand2() != null) {
            node.getCommand2().apply(this);
        }
        outAC2Command(node);
    }
    
    
    public void caseAC5Command(AC5Command node) {
    	
        inAC5Command(node);
        if(node.getCommand5() != null) {
            node.getCommand5().apply(this);
        }
        outAC5Command(node);
    }
    
    public void caseAC3Command(AC3Command node) { 
    	
        inAC3Command(node);
        if(node.getCommand3() != null) {
            node.getCommand3().apply(this);
        }
        outAC3Command(node);
    }
    
    public void caseAC4Command(AC4Command node) { 
    	
        inAC4Command(node);
        if(node.getCommand4() != null) {
            node.getCommand4().apply(this);
        }
        outAC4Command(node);
    }
    
	
}


