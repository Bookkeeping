package com.interrupt.bookkeeping.cc.executor;

import org.apache.log4j.Logger;

import com.interrupt.bob.base.BobSystem;

import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;
import com.interrupt.bookkeeping.cc.CommandOptionVisitor;
import com.interrupt.bookkeeping.cc.bkell.aauth.Aauthentication;
import com.interrupt.bookkeeping.cc.bkell.command.CreateCommand;
import com.interrupt.bookkeeping.cc.bkell.command.IResult;
import com.interrupt.bookkeeping.cc.node.ACreateCommand3;
import com.interrupt.bookkeeping.cc.node.AOptsCommandInput;
import com.interrupt.bookkeeping.cc.node.AXmlCommandInput;
import com.interrupt.bookkeeping.cc.node.AXpathCommandInput;
import com.interrupt.bookkeeping.cc.node.Node;
import com.interrupt.bookkeeping.cc.node.PCommandInput;
import com.interrupt.bookkeeping.users.IUser;
import com.interrupt.bookkeeping.util.Util;

public class CreateExecutor extends Executor {
	
	
	private Logger logger = Logger.getLogger(CreateExecutor.class); 
	private IBob parentToInject = null;
	public IBob getParentToInject() {
        return parentToInject;
    }
    public void setParentToInject(IBob parentToInject) {
        this.parentToInject = parentToInject;
    }
    
	
	public IResult execute(IUser user, Node node) { 
		
		
		ACreateCommand3 nodeCasted = (ACreateCommand3)node;
		
    	/** I'm interested in the command input 
    	 */ 
    	PCommandInput cinput = nodeCasted.getCommandInput(); 
    	String cinput_s = cinput.toString(); 
    	
    	CreateCommand createCommand = (com.interrupt.bookkeeping.cc.bkell.command.CreateCommand)
			Bob.make( "com.interrupt.bookkeeping.cc.bkell.command.CreateCommand" ); 
    	createCommand.setParent(parentToInject); //** TODO - kludge kludge kludge 
    	createCommand.setAauthentication(this.getAauthenticationToInject()); 
    	
    	IResult result = null; 
    	if(cinput instanceof AXmlCommandInput) { 
    		
    		result = CreateHelper.execute(user, createCommand, (AXmlCommandInput)cinput); 
    	}
    	else if(cinput instanceof AOptsCommandInput) { 
    		
    		result = CreateHelper.execute(user, createCommand, (AOptsCommandInput)cinput); 
    	}
    	else if(cinput instanceof AXpathCommandInput) { 
    		
    		result = CreateHelper.execute(user, createCommand, (AXpathCommandInput)cinput); 
    	}
    	
		return result; 
		
	}
	
}


