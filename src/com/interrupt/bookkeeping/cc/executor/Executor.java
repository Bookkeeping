package com.interrupt.bookkeeping.cc.executor;

import com.interrupt.bookkeeping.cc.bkell.aauth.Aauthentication;
import com.interrupt.bookkeeping.cc.bkell.command.IResult;
import com.interrupt.bookkeeping.cc.node.Node;
import com.interrupt.bookkeeping.users.IUser;
import com.interrupt.spittoon.Spittoon;

public abstract class Executor {
	
	private Spittoon spittoon = null; 
	private Aauthentication aauthenticationToInject = null;
	private String contextXPath = null; 
	private String contextUrl = null; 
	
	public Aauthentication getAauthenticationToInject() {
		return aauthenticationToInject;
	}
	public void setAauthenticationToInject(Aauthentication aauthenticationToInject) {
		this.aauthenticationToInject = aauthenticationToInject;
	}
	public Spittoon getSpittoon() { return spittoon; }
	public void setSpittoon(Spittoon spittoon) { this.spittoon = spittoon; } 
	public String getContextXPath() { return contextXPath; }
	public void setContextXPath(String contextXPath) { this.contextXPath = contextXPath; }
	public String getContextUrl() { return contextUrl; }
	public void setContextUrl(String contextUrl) { this.contextUrl = contextUrl; }
	
	
	public IResult execute(IUser user, Node node) { return null; } 
	
}
