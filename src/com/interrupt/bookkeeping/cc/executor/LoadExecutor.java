package com.interrupt.bookkeeping.cc.executor;

import org.apache.log4j.Logger;

import com.interrupt.bob.base.BobSystem;

import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;
import com.interrupt.bookkeeping.cc.CommandOptionVisitor;
import com.interrupt.bookkeeping.cc.bkell.command.CreateCommand;
import com.interrupt.bookkeeping.cc.bkell.command.IResult;
import com.interrupt.bookkeeping.cc.bkell.command.LoadCommand;
import com.interrupt.bookkeeping.cc.node.AListCommand2;
import com.interrupt.bookkeeping.cc.node.ALoadCommand3;
import com.interrupt.bookkeeping.cc.node.AOptsCommandInput;
import com.interrupt.bookkeeping.cc.node.AXmlCommandInput;
import com.interrupt.bookkeeping.cc.node.AXpathCommandInput;
import com.interrupt.bookkeeping.cc.node.Node;
import com.interrupt.bookkeeping.cc.node.PCommandInput;
import com.interrupt.bookkeeping.users.IUser;
import com.interrupt.bookkeeping.util.Util;

public class LoadExecutor extends Executor {
	
	private Logger logger = Logger.getLogger(LoadExecutor.class); 
	
	public IResult execute(IUser user, Node node) { 
		
    	/*** I'm interested in the command input 
    	 */ 
		PCommandInput cinput = null;  
    	String cinput_s = null;  
    	try { 
	    	ALoadCommand3 nodeCasted = (ALoadCommand3)node;
			
			cinput = nodeCasted.getCommandInput(); 
	    	cinput_s = cinput.toString(); 
    	}
    	catch(ClassCastException e) { 
    		AListCommand2 nodeCasted = (AListCommand2)node; 

			cinput = nodeCasted.getC2(); 
	    	cinput_s = cinput.toString(); 
    	}
    	
    	IResult result = null; 
    	LoadCommand loadCommand = new LoadCommand(getSpittoon()); 
    	loadCommand.setAauthentication(this.getAauthenticationToInject()); 
    	loadCommand.setContextXPath(this.getContextXPath()); 
    	
    	if(cinput instanceof AXmlCommandInput) { 
    		result = LoadHelper.execute(user, loadCommand, (AXmlCommandInput)cinput); 
    	}
    	else if(cinput instanceof AOptsCommandInput) { 
    		result = LoadHelper.execute(user, loadCommand, (AOptsCommandInput)cinput); 
    	}
    	else if(cinput instanceof AXpathCommandInput) { 
    		result = LoadHelper.execute(user, loadCommand, (AXpathCommandInput)cinput); 
    	}
    	this.setContextUrl(loadCommand.getContextUrl()); 
    	
		return result; 
		
	}
	
}


