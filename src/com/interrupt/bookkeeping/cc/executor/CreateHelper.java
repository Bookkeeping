package com.interrupt.bookkeeping.cc.executor;

import org.apache.log4j.Logger;

import com.interrupt.bob.base.BobSystem;

import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;
import com.interrupt.bookkeeping.cc.CommandOptionVisitor;
import com.interrupt.bookkeeping.cc.bkell.aauth.Aauthentication;
import com.interrupt.bookkeeping.cc.bkell.command.CreateCommand;
import com.interrupt.bookkeeping.cc.bkell.command.IResult;
import com.interrupt.bookkeeping.cc.bkell.command.LoadCommand;
import com.interrupt.bookkeeping.cc.node.AOptsCommandInput;
import com.interrupt.bookkeeping.cc.node.AXmlCommandInput;
import com.interrupt.bookkeeping.cc.node.PIlist;
import com.interrupt.bookkeeping.system.BookkeepingSystemProperties;
import com.interrupt.bookkeeping.users.IUser;
import com.interrupt.bookkeeping.users.User;
import com.interrupt.bookkeeping.util.Util;

public class CreateHelper {
	
	
	private Logger logger = Logger.getLogger(CreateHelper.class); 
	
	public static IResult execute(IUser user, CreateCommand createCommand, PIlist cinput) { 

		String cinput_s = cinput.toString(); 
		return CreateHelper.execute(user, createCommand, cinput_s); 
	}
	public static IResult execute(IUser user, CreateCommand createCommand, AXmlCommandInput cinput) { 
		
		String cinput_s = cinput.toString(); 
		return CreateHelper.execute(user, createCommand, cinput_s); 
	}
	public static IResult execute(IUser user, CreateCommand createCommand, String rawXML) { 
		
    	/** filter XML to get rid of spaces 
    	 */
    	String input_3 = Util.filterSpacesFromXML(rawXML); 
		
		// setup command 
		IBob created = Bob.loadS( input_3, BookkeepingSystemProperties.instance().getProperty("bob.def") ); 
		
		IBob populateResult = Util.populateEmptyIDs(created); 
        
		createCommand.addTokenLiteral(populateResult); 
    	
		// execute command 
		IResult result = createCommand.execute(user); 
		return result; 
    	
	}
	
	public static IResult execute(IUser user, CreateCommand createCommand, AOptsCommandInput cinput) { 
		
		CommandOptionVisitor covisitor = new CommandOptionVisitor(); 
		covisitor.setCommand(createCommand); 
		
		cinput.apply(covisitor); 
		
		// execute command 
		IResult result = createCommand.execute(user); 
		return result; 
    	
	}
	
	public static IResult execute(IUser user, CreateCommand createCommand, Object cinput) { 
		
		IResult result = createCommand.execute(user); 
		return result; 
	}
	
}

