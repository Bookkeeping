package com.interrupt.bookkeeping.cc.executor;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;
import com.interrupt.bookkeeping.cc.analysis.DepthFirstAdapter;
import com.interrupt.bookkeeping.cc.bkell.Bkell;
import com.interrupt.bookkeeping.cc.bkell.command.CreateCommand;
import com.interrupt.bookkeeping.cc.bkell.command.IResult;
import com.interrupt.bookkeeping.cc.bkell.command.LoadCommand;
import com.interrupt.bookkeeping.cc.node.AIlist;
import com.interrupt.bookkeeping.cc.node.AIlistpart;
import com.interrupt.bookkeeping.cc.node.AOptsCommandInput;
import com.interrupt.bookkeeping.cc.node.AXmlCommandInput;
import com.interrupt.bookkeeping.cc.node.PCommandInput;
import com.interrupt.bookkeeping.cc.node.PIlistpart;
import com.interrupt.bookkeeping.users.IUser;

public class VisitorApplyAdd extends DepthFirstAdapter { 
	
	
	private IBob addee = null; 
	private IUser user = null; 
	private Logger logger = Logger.getLogger(VisitorApplyAdd.class); 
	
	public IBob getAddee() {
		return addee;
	}
	public void setAddee(IBob addee) {
		this.addee = addee;
	}
	public IUser getUser() {
		return this.user;
	}
	public void setUser(IUser user) {
		this.user = user;
	}
	
	public void caseAIlist(AIlist node) { 
		
        inAIlist(node);
        if(node.getCommandInput() != null) { 
        	
        	
            node.getCommandInput().apply(this); 
            
            
    		LoadCommand loadCommand = (com.interrupt.bookkeeping.cc.bkell.command.LoadCommand)
    			Bob.make( "com.interrupt.bookkeeping.cc.bkell.command.LoadCommand" ); 
    		
    		CreateCommand createCommand = (com.interrupt.bookkeeping.cc.bkell.command.CreateCommand)
				Bob.make( "com.interrupt.bookkeeping.cc.bkell.command.CreateCommand" ); 
    		
    		
            PCommandInput cinput = node.getCommandInput(); 
        	IResult result = null; 
        	IBob resultBob = null; 
        	if(cinput instanceof AXmlCommandInput) { 
            	
            	result = CreateHelper.execute(user, createCommand, (AXmlCommandInput)cinput); 
				logger.debug("caseAIlist / create result / "+ result.toXML()); 
				
            	resultBob = (IBob)result.allChildren().get(0); 
            	addee.addChild(resultBob); 
            }
			else if(cinput instanceof AOptsCommandInput) { 
            	
            	result = LoadHelper.execute(user, loadCommand, (AOptsCommandInput)cinput); 
				logger.debug("caseAIlist / load result / "+ result.toXML()); 
				
            	resultBob = (IBob)result.allChildren().get(0); 
            	addee.addChild(resultBob);  
            }
        	
        }
        {
            List<PIlistpart> copy = new ArrayList<PIlistpart>(node.getIlistpart());
            for(PIlistpart e : copy) { 
                e.apply(this);
            }
        }
        outAIlist(node);
    }
	
	public void caseAIlistpart(AIlistpart node) { 
		
        inAIlistpart(node);
        if(node.getListdelimiter() != null) { 
            node.getListdelimiter().apply(this);
        }
        if(node.getCommandInput() != null) { 
            
        	node.getCommandInput().apply(this);

    		LoadCommand loadCommand = (com.interrupt.bookkeeping.cc.bkell.command.LoadCommand)
    			Bob.make( "com.interrupt.bookkeeping.cc.bkell.command.LoadCommand" ); 
    		
    		CreateCommand createCommand = (com.interrupt.bookkeeping.cc.bkell.command.CreateCommand)
				Bob.make( "com.interrupt.bookkeeping.cc.bkell.command.CreateCommand" ); 
    		
    		
            PCommandInput cinput = node.getCommandInput(); 
        	IResult result = null; 
        	IBob resultBob = null; 
        	if(cinput instanceof AXmlCommandInput) { 
            	
            	result = CreateHelper.execute(user, createCommand, (AXmlCommandInput)cinput); 
				logger.debug("caseAIlistpart / load result / "+ result.toXML()); 
				
            	resultBob = (IBob)result.allChildren().get(0); 
            	addee.addChild(resultBob); 
            }
			else if(cinput instanceof AOptsCommandInput) { 
            	
            	result = LoadHelper.execute(user, loadCommand, (AOptsCommandInput)cinput); 
				logger.debug("caseAIlistpart / load result / "+ result.toXML()); 
				
            	resultBob = (IBob)result.allChildren().get(0); 
            	addee.addChild(resultBob); 
            }
        	
        }
        outAIlistpart(node); 
        
    }

}


