package com.interrupt.bookkeeping.cc.executor;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.PushbackReader;

import org.apache.log4j.Logger;

import com.interrupt.bob.base.BobSystem;

import com.interrupt.bookkeeping.cc.lexer.Lexer;
import com.interrupt.bookkeeping.cc.lexer.LexerException;
import com.interrupt.bookkeeping.cc.node.AXmlCommandInput;
import com.interrupt.bookkeeping.cc.node.Start;
import com.interrupt.bookkeeping.cc.parser.Parser;
import com.interrupt.bookkeeping.cc.parser.ParserException;

public abstract class AbstractHelper {
	
	private Logger logger = Logger.getLogger(AbstractHelper.class); 
	
	public static AXmlCommandInput reparse(String xmlString) { 
		
		
		xmlString = "create( "+xmlString+" );"; 
		Logger.getLogger(AbstractHelper.class).debug(""); 
		Logger.getLogger(AbstractHelper.class).debug("AXmlCommandInput reparse: "); 
		Logger.getLogger(AbstractHelper.class).debug(xmlString); 
		
		
		VisitorFindFirstXMLBlock fvisitorBlock = new VisitorFindFirstXMLBlock(); 
		ByteArrayInputStream bs = null; 
		try { 
			
			bs = new ByteArrayInputStream(xmlString.getBytes("UTF-8")); 
			
			PushbackReader pbreader = new PushbackReader( new InputStreamReader(bs), 1024 );
			
			//** parse the syntax and provide the MODEL 
			Lexer lexer = new Lexer(pbreader);
			Parser parser = new Parser(lexer); 
			
			// parse the input 
			Start tree = parser.parse(); 
			
			// find first XMLBlock
			tree.apply( fvisitorBlock ); 
			
		}
		catch(java.io.UnsupportedEncodingException e) { 
			e.printStackTrace();
		}
		catch(java.io.IOException e) { 
			e.printStackTrace(); 
		}
		catch(LexerException e) { 
			e.printStackTrace(); 
		}
		catch(ParserException e) { 
			e.printStackTrace(); 
		}
		
		AXmlCommandInput result = fvisitorBlock.getXmlCommandInput(); 
		//logger.debug("LoadHelper.reparse / THE END["+ result +"]"); 
		
		return result; 
		
	}
	
	
}
