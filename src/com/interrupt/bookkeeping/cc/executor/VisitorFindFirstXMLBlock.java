package com.interrupt.bookkeeping.cc.executor;

import org.apache.log4j.Logger;

import com.interrupt.bookkeeping.cc.analysis.DepthFirstAdapter;
import com.interrupt.bookkeeping.cc.bkell.Bkell;
import com.interrupt.bookkeeping.cc.node.AXmlCommandInput;
import com.interrupt.bookkeeping.cc.node.AXmlblock;

public class VisitorFindFirstXMLBlock extends DepthFirstAdapter { 
	
	
	private Logger logger = Logger.getLogger(VisitorFindFirstXMLBlock.class); 
	
	private AXmlCommandInput xmlCommandInput = null; 
	
	
	public AXmlCommandInput getXmlCommandInput() {
		return xmlCommandInput;
	}
	public void setXmlCommandInput(AXmlCommandInput xmlCommandInput) {
		this.xmlCommandInput = xmlCommandInput;
	}
	
	
	public void caseAXmlCommandInput(AXmlCommandInput node) { 
		
		//logger.debug("VisitorFindFirstXMLBlock.caseAXmlCommandInput["+node+"]"); 
		this.setXmlCommandInput(node); 
		
    }
	
}
