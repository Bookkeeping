package com.interrupt.bookkeeping.cc.executor;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;
import com.interrupt.bookkeeping.cc.ExpressionVisitor;
import com.interrupt.bookkeeping.cc.analysis.DepthFirstAdapter;
import com.interrupt.bookkeeping.cc.bkell.aauth.Aauthentication;
import com.interrupt.bookkeeping.cc.bkell.command.CreateCommand;
import com.interrupt.bookkeeping.cc.bkell.command.IOption;
import com.interrupt.bookkeeping.cc.bkell.command.IResult;
import com.interrupt.bookkeeping.cc.bkell.command.LoadCommand;
import com.interrupt.bookkeeping.cc.node.AIlist;
import com.interrupt.bookkeeping.cc.node.AIlistpart;
import com.interrupt.bookkeeping.cc.node.AOptsCommandInput;
import com.interrupt.bookkeeping.cc.node.AXmlCommandInput;
import com.interrupt.bookkeeping.cc.node.PCommandInput;
import com.interrupt.bookkeeping.cc.node.PIlistpart;
import com.interrupt.bookkeeping.users.IUser;
import com.interrupt.spittoon.Spittoon;

public class VisitorApplyRemove extends DepthFirstAdapter { 
	
	
	private Logger logger = Logger.getLogger(ExpressionVisitor.class); 
	
	private IBob removee = null; 
	private IUser user = null; 
	private Aauthentication aauthentication = null; 
	private Spittoon spittoon = null; 
	private String contextXPath = null; 
	
	
	private LoadCommand loadCommand = null; 
	private CreateCommand createCommand = null;
	
	public IBob getRemovee() { return removee; }
	public void setRemovee(IBob removee) { this.removee = removee; } 
	
	public IUser getUser() { return this.user; }
	public void setUser(IUser user) { this.user = user; }
	
	public LoadCommand getLoadCommand() { return loadCommand; }
	public void setLoadCommand(LoadCommand loadCommand) { this.loadCommand = loadCommand; }
	
	public CreateCommand getCreateCommand() { return createCommand; }
	public void setCreateCommand(CreateCommand createCommand) { this.createCommand = createCommand; }
	
	public Aauthentication getAauthentication() { return aauthentication; }
	public void setAauthentication(Aauthentication aauthentication) { this.aauthentication = aauthentication; } 
	
	public Spittoon getSpittoon() { return spittoon; }
	public void setSpittoon(Spittoon spittoon) { this.spittoon = spittoon; }
	
	public String getContextXPath() { return contextXPath; }
	public void setContextXPath(String contextXPath) { this.contextXPath = contextXPath; } 
	
	
	public void caseAIlist(AIlist node) { 
		
        inAIlist(node);
        
        logger.debug("Remove - 2 / commandInput["+ node.getCommandInput() +"]"); 
        
        
		//** get tag name 
		String commandString = node.getCommandInput().toString(); 
		logger.debug("commandString["+ commandString +"]"); 
        
		String tagName = commandString.substring(0, commandString.indexOf(" ")); 
		logger.debug("tagName["+ tagName +"]"); 
        
		
        if(node.getCommandInput() != null) { 
        	
            node.getCommandInput().apply(this); 
            
            
    		loadCommand = (com.interrupt.bookkeeping.cc.bkell.command.LoadCommand)
    			Bob.make( "com.interrupt.bookkeeping.cc.bkell.command.LoadCommand" ); 
    		createCommand = (com.interrupt.bookkeeping.cc.bkell.command.CreateCommand)
				Bob.make( "com.interrupt.bookkeeping.cc.bkell.command.CreateCommand" ); 
    		
    		createCommand.setAauthentication(aauthentication); 
    		
            PCommandInput cinput = node.getCommandInput(); 
        	IResult result = null; 
        	IBob resultBob = null; 
        	if(cinput instanceof AXmlCommandInput) { 
            	
        		logger.debug("Remove - 3 / XML Command Input"); 
                
            	result = CreateHelper.execute(user, createCommand, (AXmlCommandInput)cinput); 
            	resultBob = (IBob)result.allChildren().get(0); 
            	removee.removeChild(resultBob); 
            }
			else if(cinput instanceof AOptsCommandInput) { 
				
				logger.debug("Remove - 4 / Options Command Input"); 
                
            	result = CreateHelper.execute(user, createCommand, (AOptsCommandInput)cinput); 
            	IOption option = (IOption)createCommand.getOptions().allOption().get(0); 
            	
            	String path = null; 
        		if( removee.remove(createCommand.getToken().getName(), option.getName(), option.getValue()) ) { 	//** if we successfully remove the child in XML, then remove it from DB 
    				
        			/*
        			spittoon.
        			this.getContextXPath() 
        			/system[@id="main.system"]
        			       /groups[@id="main.groups"]
        			                /group[@id="seven.group"]
        			                       /bookkeeping[@id="main.bookkeeping"]
        			                              /journals[@id="main.journals"]
        			                                     /journal[@id="generalledger"]
        			                                            /entries[@id="main.entries"]
        			                                                   /entry.qwer
        			*/
        			StringTokenizer tokenizer = new StringTokenizer(this.getContextXPath(), "/"); 
        			String ntoken = null; 
        			String tagname = null; 
        			String idname = null; 
        			String pathFINAL = new String(); 
        			while(tokenizer.hasMoreTokens()) { 
        				
        				ntoken = tokenizer.nextToken(); 
        				logger.debug("each context token["+ ntoken +"]"); 
        				
        				tagname = ntoken.substring( (ntoken.indexOf("/") + 1) , ntoken.indexOf("[") ); 	//** tagname 
        				idname = ntoken.substring( (ntoken.indexOf("id") + 4) , (ntoken.lastIndexOf("]") - 1) );  //** idname 
        				logger.debug("each tagname["+ tagname +"] > idname["+ idname +"]"); 
        				
        				pathFINAL += "/" + tagname + "." + idname; 
        				
        			}
        			
    				path = "/rootDir" + pathFINAL + "/" + tagName + "." + option.getValue(); 
    				logger.debug("DB XML > removing > context url["+ spittoon.getAauthDbUrl() +"] / path["+ path +"]"); 
    				spittoon.removeCollection(spittoon.getAauthDbUrl(), path); 
    				
    			}
    			
            }
        	
        }
        {
            List<PIlistpart> copy = new ArrayList<PIlistpart>(node.getIlistpart());
            for(PIlistpart e : copy) { 
                e.apply(this);
            }
        }
        outAIlist(node);
    }
	
	public void caseAIlistpart(AIlistpart node) { 
		
        inAIlistpart(node);
        if(node.getListdelimiter() != null) { 
            node.getListdelimiter().apply(this);
        }
        if(node.getCommandInput() != null) { 
            
        	
        	node.getCommandInput().apply(this); 
        	createCommand.setAauthentication(aauthentication); 
    		
    		loadCommand = (com.interrupt.bookkeeping.cc.bkell.command.LoadCommand)
    			Bob.make( "com.interrupt.bookkeeping.cc.bkell.command.LoadCommand" ); 
    		createCommand = (com.interrupt.bookkeeping.cc.bkell.command.CreateCommand)
				Bob.make( "com.interrupt.bookkeeping.cc.bkell.command.CreateCommand" ); 
    		
            PCommandInput cinput = node.getCommandInput(); 
        	IResult result = null; 
        	IBob resultBob = null; 
        	if(cinput instanceof AXmlCommandInput) { 
            	
            	result = CreateHelper.execute(user,createCommand, (AXmlCommandInput)cinput); 
            	resultBob = (IBob)result.allChildren().get(0); 
            	removee.removeChild(resultBob); 
            }
			else if(cinput instanceof AOptsCommandInput) { 
				
            	result = CreateHelper.execute(user, createCommand, (AOptsCommandInput)cinput); 
            	IOption option = (IOption)createCommand.getOptions().allOption().get(0); 
            	
            	removee.remove(createCommand.getToken().getName(), option.getName(), option.getValue()); 
            	
            }
        	
        }
        outAIlistpart(node); 
        
    }

}


