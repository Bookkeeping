package com.interrupt.bookkeeping.cc.executor;


import org.apache.log4j.Logger;

import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;
import com.interrupt.bookkeeping.cc.CommandOptionVisitor;
import com.interrupt.bookkeeping.cc.ExpressionVisitor;
import com.interrupt.bookkeeping.cc.bkell.Bkell;
import com.interrupt.bookkeeping.cc.bkell.aauth.Aauthentication;
import com.interrupt.bookkeeping.cc.bkell.command.IResult;
import com.interrupt.bookkeeping.cc.bkell.command.LoadCommand;
import com.interrupt.bookkeeping.cc.node.AOptsCommandInput;
import com.interrupt.bookkeeping.cc.node.AXmlCommandInput;
//import com.interrupt.bookkeeping.cc.node.AXpathCommandInput;
import com.interrupt.bookkeeping.system.BookkeepingSystemProperties;
import com.interrupt.bookkeeping.users.IUser;
import com.interrupt.bookkeeping.users.User;
import com.interrupt.bookkeeping.util.Util;

public class LoadHelper extends AbstractHelper {
	
	private Logger logger = Logger.getLogger(LoadHelper.class); 
	public static IResult execute(IUser user, LoadCommand loadCommand, AXmlCommandInput cinput) { 
		
		String cinput_s = cinput.toString(); 
    	
		/** filter XML to get rid of spaces 
    	 */
    	String input_3 = Util.filterSpacesFromXML(cinput_s); 
    	Logger.getLogger(LoadHelper.class).debug("LoadHelper / CommandInput / XML["+input_3+"]"); //** generate token, option, input
    	Logger.getLogger(LoadHelper.class).debug(loadCommand.toXML(false)); 
    	
		// setup command 
		Bob bob = new Bob(); 
		IBob created = bob.load( input_3, BookkeepingSystemProperties.instance().getProperty("bob.def") ); 
		loadCommand.addTokenLiteral(created); 
    	
		// execute command 
		IResult result = loadCommand.execute(user); 
		return result; 
	}
	public static IResult execute(IUser user, LoadCommand loadCommand, AOptsCommandInput cinput) { 
		
    	CommandOptionVisitor covisitor = new CommandOptionVisitor(); 
		covisitor.setCommand(loadCommand); 
		cinput.apply(covisitor); 
		
		// execute command 
		Logger.getLogger(LoadHelper.class).debug(loadCommand); 
		
		IResult result = loadCommand.execute(user); 
		return result; 
	}
	//public static IResult execute(IUser user, LoadCommand loadCommand, AXpathCommandInput cinput) { 
	public static IResult execute(IUser user, LoadCommand loadCommand, Object cinput) { 
			
		IResult result = loadCommand.execute(user); 
		return result; 
	}
	
	
	public static void main(String args[]) { 
		
		LoadHelper.reparse("<debit xmlns='com/interrupt/bookkeeping/account' id='def' amount='1.50' />"); 
	}
	
}


