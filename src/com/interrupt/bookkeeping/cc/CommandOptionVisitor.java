package com.interrupt.bookkeeping.cc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.interrupt.bookkeeping.account.GDebit;
import com.interrupt.bookkeeping.cc.analysis.DepthFirstAdapter;
import com.interrupt.bookkeeping.cc.bkell.Bkell;
import com.interrupt.bookkeeping.cc.bkell.command.AbstractCommand;
import com.interrupt.bookkeeping.cc.bkell.command.GOption;
import com.interrupt.bookkeeping.cc.bkell.command.Token;
import com.interrupt.bookkeeping.cc.node.AAccountCommandoption;
import com.interrupt.bookkeeping.cc.node.AAccountCommandtoken;
import com.interrupt.bookkeeping.cc.node.AAccountsCommandtoken;
import com.interrupt.bookkeeping.cc.node.AAmountCommandoption;
import com.interrupt.bookkeeping.cc.node.ACreditCommandtoken;
import com.interrupt.bookkeeping.cc.node.ACurrencyCommandoption; 
import com.interrupt.bookkeeping.cc.node.ACweightCommandoption;
import com.interrupt.bookkeeping.cc.node.ADateCommandoption;
import com.interrupt.bookkeeping.cc.node.ADebitCommandtoken;
import com.interrupt.bookkeeping.cc.node.AEntriesCommandtoken;
import com.interrupt.bookkeeping.cc.node.AEntryCommandoption;
import com.interrupt.bookkeeping.cc.node.AEntryCommandtoken;
import com.interrupt.bookkeeping.cc.node.AEntryidCommandoption;
import com.interrupt.bookkeeping.cc.node.AEntrynumCommandoption;
import com.interrupt.bookkeeping.cc.node.AFileCommandoption;
import com.interrupt.bookkeeping.cc.node.AGroupCommandoption;
import com.interrupt.bookkeeping.cc.node.AGroupidCommandoption;
import com.interrupt.bookkeeping.cc.node.AIdCommandoption;
import com.interrupt.bookkeeping.cc.node.AInputOption;
import com.interrupt.bookkeeping.cc.node.AJournalCommandoption;
import com.interrupt.bookkeeping.cc.node.AJournalCommandtoken;
import com.interrupt.bookkeeping.cc.node.AJournalsCommandtoken;
import com.interrupt.bookkeeping.cc.node.ANameCommandoption;
import com.interrupt.bookkeeping.cc.node.AOptsCommandInput;
import com.interrupt.bookkeeping.cc.node.APasswdCommandoption;
import com.interrupt.bookkeeping.cc.node.AProfiledetailCommandtoken;
import com.interrupt.bookkeeping.cc.node.AProfiledetailsCommandtoken;
import com.interrupt.bookkeeping.cc.node.AReturnCommandoption;
import com.interrupt.bookkeeping.cc.node.ASystemCommandtoken;
import com.interrupt.bookkeeping.cc.node.ATransactionCommandtoken;
import com.interrupt.bookkeeping.cc.node.ATypeCommandoption;
import com.interrupt.bookkeeping.cc.node.AUnameCommandoption;
import com.interrupt.bookkeeping.cc.node.AUserCommandtoken;
import com.interrupt.bookkeeping.cc.node.AUseridCommandoption;
import com.interrupt.bookkeeping.cc.node.AUsersCommandtoken;
import com.interrupt.bookkeeping.cc.node.PCommandoption;

public class CommandOptionVisitor extends DepthFirstAdapter {
	
	
	private AbstractCommand command = null; 
	private Logger logger = Logger.getLogger(CommandOptionVisitor.class); 
	
	
	public AbstractCommand getCommand() {
		return command;
	}
	public void setCommand(AbstractCommand command) {
		this.command = command;
	}
	
	
	/** 
     * Command Options 
     */
    public void caseAEntryCommandoption(AEntryCommandoption node) {
    	
    	String node_t = node.getEntryOpt().getText(); 
    	//logger.debug("caseAEntryCommandoption CALLED ["+node_t+"]"); 
    	
    	String result = node_t.replaceAll("-[a-zA-Z]+", ""); 
    	result = result.trim(); 
    	
    	GOption option = new GOption(); 
    	option.setName("entry"); 
    	option.setValue(result); 
    	this.command.addOption(option); 
    	
    }
    
    public void caseAEntryidCommandoption(AEntryidCommandoption node) { 
    	
        String node_t = node.getEntryidOpt().getText(); 
    	//logger.debug("caseAEntryidCommandoption CALLED ["+node_t+"]"); 
    	
    	String result = node_t.replaceAll("-[a-zA-Z]+", ""); 
    	result = result.trim(); 
    	 
    	
    	GOption option = new GOption(); 
    	option.setName("entryid"); 
    	option.setValue(result); 
    	this.command.addOption(option); 
    	
    }
    
    
    public void caseAAccountCommandoption(AAccountCommandoption node) {
    	
    	String node_t = node.getAccountOpt().getText(); 
    	//logger.debug("caseAAccountCommandoption CALLED ["+node_t+"]"); 

    	String result = node_t.replaceAll("-[a-zA-Z]+", ""); 
    	result = result.trim(); 
    	 
    	
    	GOption option = new GOption(); 
    	option.setName("account"); 
    	option.setValue(result); 
    	this.command.addOption(option); 
    	
    }
    
    public void caseAJournalCommandoption(AJournalCommandoption node) {
    	
    	String node_t = node.getJournalOpt().getText(); 
    	//logger.debug("caseAJournalCommandoption CALLED ["+node_t+"]"); 

    	String result = node_t.replaceAll("-[a-zA-Z]+", ""); 
    	result = result.trim(); 
    	 
    	
    	GOption option = new GOption(); 
    	option.setName("journal"); 
    	option.setValue(result); 
    	this.command.addOption(option); 
    	
    }
    
    
    public void caseANameCommandoption(ANameCommandoption node) {
    	
    	String node_t = node.getNameOpt().getText(); 
    	//logger.debug("caseANameCommandoption CALLED ["+node_t+"]"); 

    	String result = node_t.replaceAll("-[a-zA-Z]+", ""); 
    	result = result.trim(); 
    	 
    	
    	GOption option = new GOption(); 
    	option.setName("name"); 
    	option.setValue(result); 
    	this.command.addOption(option); 
    	
    }
    
    
    public void caseATypeCommandoption(ATypeCommandoption node) {
    	
    	String node_t = node.getTypeOpt().getText(); 
    	//logger.debug("caseATypeCommandoption CALLED ["+node_t+"]"); 

    	String result = node_t.replaceAll("-[a-zA-Z]+", ""); 
    	result = result.trim(); 
    	 
    	
    	GOption option = new GOption(); 
    	option.setName("type"); 
    	option.setValue(result); 
    	this.command.addOption(option); 
    	
    }
    
    
    public void caseACweightCommandoption(ACweightCommandoption node) {
    	
    	String node_t = node.getCounterweightOpt().getText(); 
    	//logger.debug("caseACweightCommandoption CALLED ["+node_t+"]"); 

    	String result = node_t.replaceAll("-[a-zA-Z]+", ""); 
    	result = result.trim(); 
    	 
    	
    	GOption option = new GOption(); 
    	option.setName("counterWeight"); 
    	option.setValue(result); 
    	this.command.addOption(option); 
    	
    }
    
    
    public void caseAAmountCommandoption(AAmountCommandoption node) {
    	
    	String node_t = node.getAmountOpt().getText(); 
    	//logger.debug("caseAAmountCommandoption CALLED ["+node_t+"]"); 

    	String result = node_t.replaceAll("-[a-zA-Z]+", ""); 
    	result = result.trim(); 
    	 
    	
    	GOption option = new GOption(); 
    	option.setName("amount"); 
    	option.setValue(result); 
    	this.command.addOption(option); 
    	
    }
    
    
    
    public void caseAIdCommandoption(AIdCommandoption node) {
    	
    	String node_t = node.getIdOpt().getText(); 
    	//logger.debug("caseAIdCommandoption CALLED ["+node_t+"]"); 
    	
    	String result = node_t.replaceAll("-[a-zA-Z]+", ""); 
    	result = result.trim(); 
    	 
    	
    	GOption option = new GOption(); 
    	option.setName("id"); 
    	option.setValue(result); 
    	this.command.addOption(option); 
    	
    }
    
    public void caseAGroupCommandoption(AGroupCommandoption node) { 

    	String node_t = node.getGroupOpt().getText();  
    	//logger.debug("caseAGroupCommandoption CALLED ["+node_t+"]"); 
    	
    	String result = node_t.replaceAll("-[a-zA-Z]+", ""); 
    	result = result.trim(); 
    	
    	GOption option = new GOption(); 
    	option.setName("group"); 
    	option.setValue(result); 
    	this.command.addOption(option); 
    	
    }
    public void caseAUnameCommandoption(AUnameCommandoption node) { 

    	String node_t = node.getUnameOpt().getText();  
    	
    	String result = node_t.replaceAll("-[a-zA-Z]+", ""); 
    	result = result.trim(); 
    	
    	GOption option = new GOption(); 
    	option.setName("username"); 
    	option.setValue(result); 
    	this.command.addOption(option); 
    	
    }
    public void caseAPasswdCommandoption(APasswdCommandoption node) { 

    	String node_t = node.getPasswdOpt().getText();  
    	//logger.debug("caseAIdCommandoption CALLED ["+node_t+"]"); 
    	
    	String result = node_t.replaceAll("-[a-zA-Z]+", ""); 
    	result = result.trim(); 
    	
    	GOption option = new GOption(); 
    	option.setName("password"); 
    	option.setValue(result); 
    	this.command.addOption(option); 
    	
    }
    
    
    
    
    public void caseAEntrynumCommandoption(AEntrynumCommandoption node) {
    	
    	String node_t = node.getEntrynumOpt().getText(); 
    	//logger.debug("caseAEntrynumCommandoption CALLED ["+node_t+"]"); 

    	String result = node_t.replaceAll("-[a-zA-Z]+", ""); 
    	result = result.trim(); 
    	 
    	
    	GOption option = new GOption(); 
    	option.setName("entrynum"); 
    	option.setValue(result); 
    	this.command.addOption(option); 
    	
    }
    
    
    public void caseADateCommandoption(ADateCommandoption node) {
    	
    	String node_t = node.getDateOpt().getText(); 
    	//logger.debug("caseADateCommandoption CALLED ["+node_t+"]"); 

    	String result = node_t.replaceAll("-[a-zA-Z]+", ""); 
    	result = result.trim(); 
    	 
    	
    	GOption option = new GOption(); 
    	option.setName("date"); 
    	option.setValue(result); 
    	this.command.addOption(option); 
    	
    }
    
    
    public void caseAFileCommandoption(AFileCommandoption node) {
    	
    	String node_t = node.getFileOpt().getText(); 
        //logger.debug("caseAFileCommandoption CALLED ["+node_t+"]"); 

    	String result = node_t.replaceAll("-[a-zA-Z]+", ""); 
    	result = result.trim(); 
    	 
    	
    	GOption option = new GOption(); 
    	option.setName("file"); 
    	option.setValue(result); 
    	this.command.addOption(option); 
    	
    }
    
    
    /* {groupid} groupid_opt | 
	{userid} userid_opt | 
	{currency} currency_opt;  
	*/
    public void caseAGroupidCommandoption(AGroupidCommandoption node) {

    	String node_t = node.getGroupidOpt().getText(); 
        String result = node_t.replaceAll("-[a-zA-Z]+", ""); 
    	result = result.trim(); 
    	
    	GOption option = new GOption(); 
    	option.setName("groupid"); 
    	option.setValue(result); 
    	this.command.addOption(option); 
    	
    }
    
    public void caseAUseridCommandoption(AUseridCommandoption node) {

    	String node_t = node.getUseridOpt().getText(); 
        String result = node_t.replaceAll("-[a-zA-Z]+", ""); 
    	result = result.trim(); 
    	
    	GOption option = new GOption(); 
    	option.setName("userid"); 
    	option.setValue(result); 
    	this.command.addOption(option); 
    	
    }
    
    public void caseACurrencyCommandoption(ACurrencyCommandoption node) { 
    	
    	String node_t = node.getCurrencyOpt().getText(); 
        String result = node_t.replaceAll("-[a-zA-Z]+", ""); 
    	result = result.trim(); 
    	
    	GOption option = new GOption(); 
    	option.setName("currency"); 
    	option.setValue(result); 
    	this.command.addOption(option); 
    	
    }
    
    
    /*public void caseAReturninputCommandoption(AReturninputCommandoption node) { 
    	
    	String node_t = node.getReturninputOpt().getText(); 
        String result = node_t.replaceAll("-[a-zA-Z]+", ""); 
    	result = result.trim(); 
    	
    	GOption option = new GOption(); 
    	option.setName("returninput"); 
    	option.setValue(result); 
    	this.command.addOption(option); 
    	
    }
    public void caseAReturnCommandoption(AReturnCommandoption node)
    {
        inAReturnCommandoption(node);
        if(node.getReturninputOpt() != null)
        {
            node.getReturninputOpt().apply(this);
        }
        outAReturnCommandoption(node);
    }
    */
    public void caseAReturnCommandoption(AReturnCommandoption node) { 
    	
    	String node_t = node.getReturninputOpt().getText(); 
        String result = node_t.replaceAll("-[a-zA-Z]+", ""); 
    	result = result.trim(); 
    	
    	GOption option = new GOption(); 
    	option.setName("returninput"); 
    	option.setValue(result); 
    	this.command.addOption(option); 
    	
    }
    
    /** 
     * Command Tokens 
     */
    public void caseASystemCommandtoken(ASystemCommandtoken node) { 
    	
    	//logger.debug("caseADebitCommandtoken CALLED ["+node.getDebitTok().getText()+"]"); 
    	
    	Token token = new Token(); 
    	token.setName("system"); 
    	this.command.setToken(token); 
    	
    }
    public void caseADebitCommandtoken(ADebitCommandtoken node) {
    	
    	//logger.debug("caseADebitCommandtoken CALLED ["+node.getDebitTok().getText()+"]"); 
    	
    	Token token = new Token(); 
    	token.setName("debit"); 
    	this.command.setToken(token); 
    	
    }
    public void caseACreditCommandtoken(ACreditCommandtoken node) {
    	
    	//logger.debug("caseACreditCommandtoken CALLED ["+node.getCreditTok().getText()+"]"); 
    	
    	Token token = new Token(); 
    	token.setName("credit"); 
    	this.command.setToken(token); 
    	
    }
    public void caseAEntryCommandtoken(AEntryCommandtoken node) {
    	
    	//logger.debug("caseAEntryCommandtoken CALLED ["+node.getEntryTok().getText()+"]"); 
    	
    	Token token = new Token(); 
    	token.setName("entry"); 
    	this.command.setToken(token); 
    	
    }
    public void caseAEntriesCommandtoken(AEntriesCommandtoken node) { 
    	
    	//logger.debug("caseAEntriesCommandtoken CALLED ["+node.getEntriesTok().getText()+"]"); 
    	
    	Token token = new Token(); 
    	token.setName("entries"); 
    	this.command.setToken(token); 
    	
    }
    public void caseAJournalCommandtoken(AJournalCommandtoken node) {
    	
    	//logger.debug("caseAJournalCommandtoken CALLED ["+node.getJournalTok().getText()+"]"); 
    	
    	Token token = new Token(); 
    	token.setName("journal"); 
    	this.command.setToken(token); 
    	
    }
    public void caseAJournalsCommandtoken(AJournalsCommandtoken node) {
    	
    	//logger.debug("caseAJournalsCommandtoken CALLED ["+node.getJournalsTok().getText()+"]"); 
    	
    	Token token = new Token(); 
    	token.setName("journals"); 
    	this.command.setToken(token); 
    	
    }
    public void caseATransactionCommandtoken(ATransactionCommandtoken node) {
    	
    	//logger.debug("caseATransactionCommandtoken CALLED ["+node.getTransactionTok().getText()+"]"); 
    	
    	Token token = new Token(); 
    	token.setName("transaction"); 
    	this.command.setToken(token); 
    	
    }
    public void caseAAccountCommandtoken(AAccountCommandtoken node) {
    	
    	//logger.debug("caseAAccountCommandtoken CALLED ["+node.getAccountTok().getText()+"]"); 
    	
    	Token token = new Token(); 
    	token.setName("account"); 
    	this.command.setToken(token); 
    	
    }
    public void caseAAccountsCommandtoken(AAccountsCommandtoken node) {
    	
    	//logger.debug("caseAAccountCommandtoken CALLED ["+node.getAccountTok().getText()+"]"); 
    	
    	Token token = new Token(); 
    	token.setName("accounts"); 
    	this.command.setToken(token); 
    	
    }
    public void caseAUserCommandtoken(AUserCommandtoken node) { 

    	//logger.debug("caseAUserCommandtoken CALLED ["+node.getAccountTok().getText()+"]"); 
    	
    	Token token = new Token(); 
    	token.setName("user"); 
    	this.command.setToken(token); 
    	
    }
    public void caseAUsersCommandtoken(AUsersCommandtoken node) { 
    	
    	//logger.debug("caseAUsersCommandtoken CALLED ["+node.getAccountTok().getText()+"]"); 
    	
    	Token token = new Token(); 
    	token.setName("users"); 
    	this.command.setToken(token); 
    	
    }
    public void caseAProfiledetailsCommandtoken(AProfiledetailsCommandtoken node) { 
    	
    	//logger.debug("caseAProfiledetailsCommandtoken CALLED ["+node.getAccountTok().getText()+"]"); 
    	
    	Token token = new Token(); 
    	token.setName("profileDetails"); 
    	this.command.setToken(token); 
    	
    }
    
    public void caseAProfiledetailCommandtoken(AProfiledetailCommandtoken node) { 

    	//logger.debug("caseAProfiledetailCommandtoken CALLED ["+node.getAccountTok().getText()+"]"); 
    	
    	Token token = new Token(); 
    	token.setName("profileDetail"); 
    	this.command.setToken(token); 
    	
    }
    
    
    public static void main(String args[]) { 
    	
    	//-type type -name some -id qerfb1435c -date 06/24/2003 
    	String string_t = "-type type"; 
    	String string_n = "-name some"; 
    	String string_i = "-id qerfb1435c"; 
    	String string_d = "-date 06/24/2003"; 
    	
    	
    	String result = string_t.replaceAll("-[a-zA-Z]+", ""); 
    	//logger.debug( "string_t Result: ["+result.trim()+"]" ); 
    	
    	result = string_n.replaceAll("-[a-zA-Z]+", ""); 
    	//logger.debug( "string_n Result: ["+result.trim()+"]" ); 
    	
    	result = string_i.replaceAll("-[a-zA-Z]+", ""); 
    	//logger.debug( "string_i Result: ["+result.trim()+"]" ); 
    	
    	result = string_d.replaceAll("-[a-zA-Z]+", ""); 
    	//logger.debug( "string_d Result: ["+result.trim()+"]" ); 
    	
    }
    
    
}


