package com.interrupt.bookkeeping.cc.bkell;


import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PushbackReader;
import java.io.Reader;
import java.io.Serializable;
import java.lang.Thread.UncaughtExceptionHandler;
import java.lang.reflect.InvocationTargetException;

import org.apache.log4j.Logger;
import com.interrupt.bob.base.IBob;
import com.interrupt.bob.base.IVisitor;

import com.interrupt.bob.base.Bob;
import com.interrupt.bookkeeping.ISystem;
import com.interrupt.bookkeeping.cc.ExpressionVisitor;
import com.interrupt.bookkeeping.cc.bkell.aauth.Aauthentication;
import com.interrupt.bookkeeping.cc.lexer.Lexer;
import com.interrupt.bookkeeping.cc.lexer.LexerException;
import com.interrupt.bookkeeping.cc.node.AXmlCommandInputRef;
import com.interrupt.bookkeeping.cc.node.Start;
import com.interrupt.bookkeeping.cc.parser.Parser;
import com.interrupt.bookkeeping.cc.parser.ParserException;
import com.interrupt.bookkeeping.exception.AuthorisationException;
import com.interrupt.bookkeeping.exception.BkellException;
import com.interrupt.bookkeeping.exception.BkellUncaughtExceptionHandler;
import com.interrupt.bookkeeping.system.BookkeepingSystem;
import com.interrupt.bookkeeping.system.BookkeepingSystemProperties;
import com.interrupt.bookkeeping.users.IUserSession;
import com.interrupt.logs.GLog;
import com.interrupt.logs.GLogMessage;
import com.interrupt.logs.GLogMessages;
import com.interrupt.logs.GLogs;
import com.interrupt.logs.ILog;
import com.interrupt.logs.ILogMessage;
import com.interrupt.logs.ILogMessages;
import com.interrupt.logs.ILogs;
import com.interrupt.spittoon.Spittoon;

public class Bkell extends GBkell implements Runnable, Serializable {
	
	
    public static final String BKELL_NAME = "bookkeeping.shell"; 
    private Logger logger = Logger.getLogger(Bkell.class); 
	private transient InputStream inputStream = null; 
	private BkellException bkellException = null; 
	
	private ExpressionVisitor expVisitor = null; 
	private PushbackReader pbreader = null; 
	
	public Bkell() { 
		
		inputStream = System.in; 
		pbreader = new PushbackReader( new InputStreamReader(inputStream), 1024 ); 
	}
	
	
	public BkellException getBkellException() { return bkellException; }
	public void setBkellException(BkellException bkellException) { this.bkellException = bkellException; }
	
	
	// inputStream 
	public InputStream getInputStream() {
		return inputStream;
	}
	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
		pbreader = new PushbackReader( new InputStreamReader(this.inputStream), 1024 ); 
	}
	
	// pushbackReader
	public void updatePushbackReader(Reader reader) { 
	    pbreader = new PushbackReader( reader, 1024 );
	}
	
	public synchronized void connectPipe(PipedOutputStream postream) throws IOException { 
		((PipedInputStream)inputStream).connect(postream); 
	}
	
	// expression visitor 
	public ExpressionVisitor getExpVisitor() {
        return expVisitor;
    }
    public void setExpVisitor(ExpressionVisitor expVisitor) {
        this.expVisitor = expVisitor; 
        this.expVisitor.setBkellEnv(this); 
    }
    
    
    public IUserSession getUserSession() {
        return this.getExpVisitor().getUserSession(); 
    }
    public void setUserSession(IUserSession userSession) {
        this.getExpVisitor().setUserSession(userSession); 
    }
    
    
    // previousCommandResult 
	public IBob getPreviousCommandResult() {
		return this.expVisitor.getPreviousCommandResult().getReference(); 
	}
	
	
	public void initialise() { 
	    
		logger.debug("Bkell.initialise() CALLED"); 
		com.interrupt.spittoon.Spittoon spittoon = new com.interrupt.spittoon.Spittoon(); 
		spittoon.initialise(); 
		
        com.interrupt.bookkeeping.System system = (com.interrupt.bookkeeping.System)spittoon.
			retrieve(spittoon.getAauthDbUrl(), 
				"/system[ @id='main.system' ]", true); 
		
        Aauthentication aauthentication = system.getAauthentication(); 
        expVisitor = new ExpressionVisitor(spittoon); 
        expVisitor.setAauthentication(aauthentication); 
        expVisitor.setBkellEnv(this); 
        
	}
	public void shutdown() throws IOException { 
		
		((PipedInputStream)inputStream).close(); 
		this.getExpVisitor().setActive(false); 
		 
	} 
	
	public void run() throws BkellException { 
		
		
		while(expVisitor.isActive()) { 
			
			logger.debug("bkell > "); 
			try { 
				
				//** parse the syntax and provide the MODEL 
				Parser parser = new Parser(new Lexer(pbreader) {
		        	
					Logger logger = Logger.getLogger(Lexer.class); 
					protected void filter() {
		            	logger.debug(token.getClass() + ", state : " + state.id() + ", text : [" + token.getText() + "]");
		            }
		        }); 
		        
				logger.debug("bkell > 1"); 
	            
				// parse the input 
				Start tree = parser.parse(); 
				
				logger.debug("bkell > 2"); 
	            
				// apply the Visitor 
				tree.apply(expVisitor); 
				
				logger.debug("bkell > 3"); 
	            
				synchronized(this) { this.notify(); //** if any thread is waiting on this thread's monitor, wake them up 
				}
	    		
	    		logger.debug("bkell > 4 > expVisitor.isActive ["+ expVisitor.isActive() +"]"); 
	            
			}
			catch(java.io.IOException e) { 
				
				logger.error(e.getMessage(), e);
				BkellException bke = com.interrupt.bookkeeping.util.Util.generateBkellException(e); 
				logger.error(bke.getLogMessages().toXML(false)); 
				
				this.setBkellException(bke); 
				this.fillInPreviousCommandResult(bke.getLogMessages());  
				
				synchronized(this) { this.notify(); //** if any thread is waiting on this thread's monitor, wake them up 
				} 
			}
			catch(LexerException e) { 

				logger.error(e.getMessage(), e);
				BkellException bke = com.interrupt.bookkeeping.util.Util.generateBkellException(e); 
				logger.error(bke.getLogMessages().toXML(false)); 
				
				this.setBkellException(bke); 
				this.fillInPreviousCommandResult(bke.getLogMessages());  
				
				synchronized(this) { this.notify(); //** if any thread is waiting on this thread's monitor, wake them up 
				}
			}
			catch(ParserException e) { 

				logger.error(e.getMessage(), e);
				BkellException bke = com.interrupt.bookkeeping.util.Util.generateBkellException(e); 
				logger.error(bke.getLogMessages().toXML(false)); 
				
				this.setBkellException(bke); 
				this.fillInPreviousCommandResult(bke.getLogMessages());  
				
				synchronized(this) { this.notify(); //** if any thread is waiting on this thread's monitor, wake them up 
				}
			}
			catch(AuthorisationException e) { 
				
				logger.error(e.getMessage(), e);
				BkellException bke = com.interrupt.bookkeeping.util.Util.generateBkellException(e); 
				logger.error(bke.getLogMessages().toXML(false)); 
				
				this.setBkellException(bke); 
				this.fillInPreviousCommandResult(bke.getLogMessages());  
				
				synchronized(this) { this.notify(); //** if any thread is waiting on this thread's monitor, wake them up 
				}
			}
			catch(BkellException e) { 
				
				System.out.println("Here... ["+ e.getLogMessages().toXML(false) +"]"); 
				
				this.setBkellException(e); 
				this.fillInPreviousCommandResult(e.getLogMessages());  
				
				synchronized(this) { this.notify(); //** if any thread is waiting on this thread's monitor, wake them up 
				}
			}
			catch(Exception e) { 
				
				logger.error(e.getMessage(), e);
				BkellException bke = com.interrupt.bookkeeping.util.Util.generateBkellException(e); 
				logger.error(bke.getLogMessages().toXML(false)); 
				
				this.setBkellException(bke); 
				this.fillInPreviousCommandResult(bke.getLogMessages());  
				
				synchronized(this) { this.notify(); //** if any thread is waiting on this thread's monitor, wake them up 
				}
			}
			catch(Throwable e) { 
				
				logger.error(e.getMessage(), e);
				BkellException bke = com.interrupt.bookkeeping.util.Util.generateBkellException(e); 
				logger.error(bke.getLogMessages().toXML(false)); 
				
				this.setBkellException(bke); 
				this.fillInPreviousCommandResult(bke.getLogMessages());  
				
				synchronized(this) { this.notify(); //** if any thread is waiting on this thread's monitor, wake them up 
				}
			}
			
		}
		
		logger.debug("bkell no longer active. EXITing"); 
		this.notify(); 
		
	}
	
	
	private void fillInPreviousCommandResult(IBob bob) { 
		
		AXmlCommandInputRef replacorReference = new AXmlCommandInputRef(); 
        replacorReference.setReference(bob); 
        this.expVisitor.setPreviousCommandResult(replacorReference); 
	}
	
	public static void main(String args[]) { 
		
		
		Logger logger = Logger.getLogger(Bkell.class); 
		logger.debug("bkell >");
		
		System.getProperties().setProperty(com.interrupt.bob.util.Util.DEF, BookkeepingSystemProperties.instance().getProperty("bob.def")); 
		System.getProperties().setProperty(com.interrupt.bob.util.Util.BASE, "."); 
		System.getProperties().setProperty(com.interrupt.bob.util.Util.END, ".xml"); 
		
		ISystem system = 
			(ISystem)Bob.loadS( 
				Bkell.class.getResourceAsStream("/bookkeeping.system.xml"), 
					BookkeepingSystemProperties.instance().getProperty("bob.def") ); 
		
		//logger.debug("Loaded Bookkkeeping system > "+ system.toXML(false)); 
		
		Bkell bkell = null; 
		try { 
			bkell = (Bkell)((com.interrupt.bookkeeping.System)system).findBkellById("bkell.main"); 
		}
		catch(Exception e) { 
			e.printStackTrace(); 
		}
		bkell.initialise(); 
		
		
		//** 
		BkellUncaughtExceptionHandler ehandler = new BkellUncaughtExceptionHandler(); 
		Thread bkellThread = new Thread(bkell); 
		bkellThread.setDefaultUncaughtExceptionHandler(ehandler); 
		try { 
			
			bkellThread.start(); 
			
			// 'join()' is a block method for the thread
			bkellThread.join(); 
		}
		catch(BkellException e) { 
			
			ILogs logs = e.getLogMessages(); 
			logger.error(logs.toXML(false)); 
			
			e.printStackTrace(); 
		}
		catch(InterruptedException e) { 
			e.printStackTrace();
		}
		System.exit(0); 
		
	}
	
	
}



