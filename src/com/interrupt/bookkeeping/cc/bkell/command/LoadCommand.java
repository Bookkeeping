package com.interrupt.bookkeeping.cc.bkell.command;


import java.io.File;
import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;

import java.util.List; 
import java.util.Iterator; 

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.AttributesImpl;

import com.interrupt.bob.base.BobSystem;

import com.interrupt.bob.base.IBob; 
import com.interrupt.bob.base.Bob; 
import com.interrupt.bob.util.StringUtil;
import com.interrupt.bookkeeping.GSystem;
import com.interrupt.bookkeeping.ISystem;
import com.interrupt.bookkeeping.cc.bkell.Bkell;
import com.interrupt.bookkeeping.cc.bkell.IBkell;
import com.interrupt.bookkeeping.system.BookkeepingSystem;
import com.interrupt.bookkeeping.users.IUser;
import com.interrupt.spittoon.Spittoon;
import com.interrupt.spittoon.SpittoonException;

public class LoadCommand extends AbstractCommand {
	
	
	private Logger logger = Logger.getLogger(AbstractCommand.class); 
	public LoadCommand() { 
		this(null); 
	}
	public LoadCommand(Spittoon spitt) { 
		
		this.setName("load"); 
		
		/** 
		 * get the possible set of 
		 * 	i. tokens for a command & 
		 * 	ii. options for those tokens
		 * 
		 */ 
		//ISystem bsystem = (GSystem)Bob.loadS( new File(BookkeepingSystemProperties.instance().getProperty("bob.def")) ); 
		ISystem bsystem = (GSystem)Bob.loadS( LoadCommand.class.getResourceAsStream("/bookkeeping.system.xml") ); 
		
		//logger.debug( "1 > "+ bsystem ); 
		
		IBkell bkell = bsystem.findBkellById("bkell.main"); 
		//logger.debug( "2 > "+ bkell ); 
		
		ITokens tokens = bkell.findTokensById("tokens.def"); 
		ICommands commands = bkell.findCommandsById("commands.def"); 
		
		//logger.debug( "3 > "+ tokens ); 
		//logger.debug( "4 > "+ commands ); 
		
		
		ICommand command = commands.findCommandByName("load"); 
		ITokens possibleTokens = (ITokens)command.allTokens().get(0); 
		
		
		// set the list of possible tokens 
		this.setTokens(possibleTokens); 
		
		spittoon = spitt; 
		
	}
	
	
	public IResult execute(IUser user) { 
		
		logger.info("LoadComand.execute:: command["+this+"] / user["+user+"]"); 
		super.execute(user); 
		
		IResult result = new GResult(); 
		
		//** TOKEN LITERAL - this should accept token literals as well as options 
		ITokenLiterals tliterals = this.getTokenLiterals(); 
		logger.debug("token literals["+tliterals+"]"); 
		
		List literalList = tliterals.allChildren(); 
		IBob tliteral = null; 
		String txpath = null; 
		if(contextXPath != null) { 
			
			txpath = contextXPath; 
			logger.debug("contextXPath ["+ txpath +"]"); 
		}
		else if( literalList.size() > 0 ) { 
			
			tliteral = (IBob)literalList.get(0); 
			txpath = tliteral.xpath(false); 
			logger.debug("tliteral XML ["+ tliteral.toXML(false) +"]"); 
			logger.debug("tliteral XPath ["+ txpath +"]"); 
		}
		
		this.setContextUrl( spittoon.getGroupsDbUrl() );  
		logger.debug("LoadCommand::execute > dbUrl["+this.getContent()+"] > txpath["+txpath+"]"); 
		
		IBob resultBob = null;
		try { 
			
			resultBob = spittoon.retrieve(this.getContextUrl(), txpath, true); 
		}
		catch(NullPointerException e) { 
			
			logger.error("NullPointerException["+e.getMessage()+"] / Did NOT find resource in Groups dbUrl["+
					this.getContextUrl()+"] / trying Aauth dbUrl["+spittoon.getAauthDbUrl()+"]"); 
			
			this.setContextUrl(spittoon.getAauthDbUrl()); 
			resultBob = spittoon.retrieve(spittoon.getAauthDbUrl(), txpath, true); 
		}
		catch(SpittoonException e) { 
			
			logger.error("SpittoonException["+e.getMessage()+"] / Did NOT find resource in Groups dbUrl["+
					this.getContextUrl()+"] / trying Aauth dbUrl["+spittoon.getAauthDbUrl()+"]"); 
			
			this.setContextUrl(spittoon.getAauthDbUrl()); 
			resultBob = spittoon.retrieve(this.getContextUrl(), txpath, true); 
		}
		
		
		result.addChild(resultBob); 
		return result; 
		
	}
	
}
