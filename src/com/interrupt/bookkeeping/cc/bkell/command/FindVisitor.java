package com.interrupt.bookkeeping.cc.bkell.command;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.AttributesImpl;

import com.interrupt.bob.base.BobSystem;

import com.interrupt.bob.base.IBob;
import com.interrupt.bob.base.IVisitor;

public class FindVisitor implements IVisitor {
	
	
	private List possibleResults = null; 
	private String tagName = null; 
	private Logger logger = Logger.getLogger(FindVisitor.class); 
	
    public FindVisitor() { 
    	this.possibleResults = new ArrayList(); 
    }
	
    public List getPossibleResults() {
		return possibleResults;
	}
	public void setPossibleResults(List possibleResults) {
		this.possibleResults = possibleResults;
	}
	
	public String getTagName() {
		return tagName;
	}
	public void setTagName(String tagName) {
		this.tagName = tagName;
	}
	
	public void visit(IBob bob) {
		
		// 1. isolate on tagName 
	    String tname = bob.getTagName(); 
	    Attributes battributes = null;  
	    
	    logger.debug("FindVisitor visit / tagName["+this.tagName+"] / tname["+tname+"]"); 
		if(tname.equals(this.tagName)) { 
	    	
			logger.debug("adding["+bob+"] to possible results");
	    	this.possibleResults.add(bob); 
	    }
	    
	}
	
}



