
package com.interrupt.bookkeeping.cc.bkell.command; 


import java.util.List; 
import java.util.Iterator; 
import org.apache.log4j.Logger;
import com.interrupt.bob.base.BobSystem;

import com.interrupt.bob.base.IBob;
import com.interrupt.bookkeeping.account.ICredit;
import com.interrupt.bookkeeping.action.Action;
import com.interrupt.bookkeeping.action.CloseAction;
import com.interrupt.bookkeeping.action.ReverseAction;
import com.interrupt.bookkeeping.action.ReverseCloseAction;

import com.interrupt.bookkeeping.cc.bkell.command.ITokenLiterals;
import com.interrupt.bookkeeping.cc.bkell.command.IResult;
import com.interrupt.bookkeeping.cc.bkell.command.GResult;
import com.interrupt.bookkeeping.exception.CommandException; 
import com.interrupt.bookkeeping.journal.IEntries;
import com.interrupt.bookkeeping.journal.IEntry;
import com.interrupt.bookkeeping.journal.IJournal;
import com.interrupt.bookkeeping.users.IUser;
import com.interrupt.bookkeeping.users.User;

public class ReverseCommand extends AddRemoveReverseCommon { 
	
	
	private IEntries tokenRecieverParent = null; 
	private Logger logger = Logger.getLogger(ReverseCommand.class); 
	
	public ReverseCommand() { 
		
		super();
		this.setName("reverse");
	}
	
	public IEntries getTokenRecieverParent() {
		return tokenRecieverParent;
	}
	public void setTokenRecieverParent(IEntries tokenRecieverParent) {
		this.tokenRecieverParent = tokenRecieverParent;
	}
	
	
	/* execute this command, add the 'token.literals' to the 'token.reciever'
	 */
	public IResult execute(IUser user) { 
		
		super.execute(user); 
		
		//** the token.reciever must be <entry/> for a 'reverse' command 
		IBob treciever = this.getTokenReciever(); 
		IEntry entryReciever = null; 
		try { 
			entryReciever = (IEntry)treciever;
		}
		catch(ClassCastException e) { 
			throw new CommandException("the token.reciever must be <entry/> for a 'reverse' command "); 
		}
		
		
		//** only 1 <entry/> token.literal can reverse another <entry/>
		ITokenLiterals tliterals = this.getTokenLiterals(); 
		List allLiterals = tliterals.allChildren();
		if(allLiterals.size() != 1) { 
			throw new CommandException("only 1 token.literal allowed for reversal"); 
		}
		if( !(allLiterals.get(0) instanceof IEntry) ) { 
			throw new CommandException("only 1 <entry/> token.literal can reverse another <entry/>"); 
		}
		IEntry entryLiteral = (IEntry)allLiterals.get(0); 
		
		
		
		//** i) reverse reciever, ii) close.reverse the literal 
		Action reverseAction = new ReverseAction(); 
		Action reverseCloseAction = new ReverseCloseAction(); 
		
		
		//** 'reverse' and 'reverse.close' the entry(s)
		User nullUser = new User(); 
		reverseAction.execute(entryReciever, nullUser); 
		reverseCloseAction.execute(entryLiteral, nullUser); 
		
		
		//logger.debug( "1 > HERE["+entryLiteral.toXML()+"]" ); 
		
		//** add the reversal to the <entries/>
		IEntries parentEntries = null; 
		if(this.tokenRecieverParent != null) { 
			
			parentEntries = this.tokenRecieverParent; 
		}
		else { 
			
			parentEntries = (IEntries)entryReciever.getParent(); 
		}
		if(parentEntries == null) { 
			throw new CommandException("There is no parent <entries> in which to reverse entry["+entryLiteral.getId()+"]"); 
		}
		parentEntries.addEntry(entryLiteral); 
		
		
		//logger.debug( "2 > HERE["+entryLiteral.toXML()+"]" ); 
		
		IResult result = new GResult(); 
		result.addChild(parentEntries); 
		return result; 
		
	}
	
}


