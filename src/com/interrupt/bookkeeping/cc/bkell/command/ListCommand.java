package com.interrupt.bookkeeping.cc.bkell.command; 


import java.util.List; 
import java.util.Iterator; 

import org.apache.log4j.Logger;
import org.xml.sax.helpers.AttributesImpl;
import com.interrupt.bob.base.BobSystem;

import com.interrupt.bob.base.IBob;
import com.interrupt.bob.base.IVisitor;
import com.interrupt.bookkeeping.cc.bkell.command.ITokenLiterals;
import com.interrupt.bookkeeping.cc.bkell.command.IResult;
import com.interrupt.bookkeeping.cc.bkell.command.GResult;
import com.interrupt.bookkeeping.exception.CommandException; 
import com.interrupt.bookkeeping.users.IUser;

public class ListCommand extends AbstractCommand { 
	
	
	private Logger logger = Logger.getLogger(ListCommand.class); 
	
	public ListCommand() { 
		
		super();
		this.setName("list");
	}
	
	
	/* execute this command, add the 'token.literals' to the 'token.reciever'
	 */
	public IResult execute(IUser user) { 
		
		super.execute(user); 
		
		IBob treciever = this.getTokenReciever();	//... 
		logger.debug("ListCommand.execute / token.reciever["+treciever.toXML()+"]"); 
		
		
		IToken token = this.getToken(); //... 
		logger.debug("ListCommand.execute / token["+token.toXML()+"]"); 
		
		
		String tokenName = token.getName(); 
		IOptions options = this.getOptions();	//... 
		logger.debug("ListCommand.execute / options["+options.toXML()+"]"); 
		
		
		List allOption = options.allOption(); 
		if(allOption.size() != 1) { 
			throw new CommandException("Can currently only find on 1 option"); 
		}
		
		IOption option = (IOption)allOption.get(0); 
		String oname = option.getName(); 
		String ovalue = option.getValue(); 
		
		
		ListVisitor lvisitor = new ListVisitor(); 
		lvisitor.setTagName(tokenName); 
		lvisitor.setName_v(oname); 
		lvisitor.setValue_v(ovalue); 
		
		// run visitor
		treciever.accept(lvisitor); 
		
		// return results 
		IResult result = lvisitor.getListResult(); 
		logger.debug("ListCommand.execute / result["+result.toXML()+"]"); 
		
		return result; 
		
	}
	
	class ListVisitor implements IVisitor { 
		
		
		private String tagName = null; 
		private String name_v = null; 
		private String value_v = null; 		
		private IResult listResult = null; 
		
		public ListVisitor() { 
			listResult = new GResult(); 
		}
		public String getTagName() {
			return tagName;
		}
		public void setTagName(String tagName) {
			this.tagName = tagName;
		}
		public IResult getListResult() {
			return listResult;
		}
		public void setListResult(IResult listResult) {
			this.listResult = listResult;
		}
		public String getName_v() {
			return name_v;
		}
		public void setName_v(String name_v) {
			this.name_v = name_v;
		}
		public String getValue_v() {
			return value_v;
		}
		public void setValue_v(String value_v) {
			this.value_v = value_v;
		}
		
		
		public void visit(IBob bob) { 
			
			
			String tname = bob.getTagName(); 
			
			logger.debug(">>>> ListVisitor / visiting / ["+bob.toXML()+"] / ["+tagName+"/"+name_v+"/"+value_v+"]"); 
			//logger.debug(">>>> ListVisitor / tname["+tname+"] / tagName["+tagName+"]"); 
			
			// find first occurence of given criteria 
			if( tname.equals(tagName) ) { 
				
				// not found yet - visiting
				AttributesImpl _attributes = (AttributesImpl)bob.getAttributes(); 
				
			    int length = _attributes.getLength();
			    String lname = null;
			    
			    
			    // if there's criteria
			    if( (name_v != null) && (name_v.trim().length() != 0) ) { 
			    	
				    for(int i=0; i < length; i++) {
				    	
						lname = _attributes.getLocalName(i); 
						
						//logger.debug("attribute localname["+lname+"]");
						if(lname.equals(name_v)) {
							
							String value = _attributes.getValue(i); 
							//logger.debug("attribute value["+value+"]");
							
							if(value.equals(value_v)) { 
								
								// found our result 
								logger.debug("MATCH!!!");
								listResult.addChild(bob); 
							}
							
						}
						
				    }
				    
			    }
			    else { 	// ... otherwise 
			    	
			    	listResult.addChild(bob); 
			    }
			    
			}
		    
			//logger.debug("");
		}
		
	}
	// end of visitor class definition 
	
	
}


