package com.interrupt.bookkeeping.cc.bkell.command; 


import java.util.List; 
import java.util.Iterator; 

import org.apache.log4j.Logger;
import org.xml.sax.helpers.AttributesImpl;
import com.interrupt.bob.base.BobSystem;

import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;
import com.interrupt.bob.base.IVisitor;
import com.interrupt.bookkeeping.GSystem;
import com.interrupt.bookkeeping.ISystem;
import com.interrupt.bookkeeping.cc.bkell.IBkell;
import com.interrupt.bookkeeping.cc.bkell.command.ITokenLiterals;
import com.interrupt.bookkeeping.cc.bkell.command.IResult;
import com.interrupt.bookkeeping.cc.bkell.command.GResult;
import com.interrupt.bookkeeping.exception.CommandException; 
import com.interrupt.bookkeeping.users.IUser;
import com.interrupt.spittoon.Spittoon;

public class FindCommand extends AbstractCommand { 
	
	
	private Logger logger = Logger.getLogger(FindCommand.class); 
	
	public FindCommand() { 
		
		super("find", null);
	}
	public FindCommand(Spittoon spitt) { 
		
		super("find", spitt);
	}
	
	public FindCommand(ICommand command) { 
		
		super("find", null);
		
		this.setAttributes(command.getAttributes()); 
		this.setChildren(command.getChildren()); 
		
	}
	
	/* execute this command, add the 'token.literals' to the 'token.reciever'
	 * 
	 * token 
	 * token.reciever 
	 * options 
	 */
	public IResult execute(IUser user) { 
		
		super.execute(user); 
		
		IBob treciever = this.getTokenReciever();	//... 
		logger.debug("FindCommand.execute / token.reciever["+treciever.toXML()+"]"); 
		
		
		IToken token = this.getToken(); //... 
		logger.debug("FindCommand.execute / token["+token.toXML()+"]"); 
		
		
		String tokenName = token.getName(); 
		IOptions options = this.getOptions();	//... 
		logger.debug("FindCommand.execute / options["+options.toXML()+"]"); 
		
		
		List allOption = options.allOption(); 
		if(allOption.size() != 1) { 
			throw new CommandException("'find' command should only have 1 option"); 
		}
		
		IOption option = (IOption)allOption.get(0); 
		String oname = option.getName(); 
		String ovalue = option.getValue(); 
		
		
		FindVisitor fvisitor = new FindVisitor(); 
		fvisitor.setTagName(tokenName); 
		fvisitor.setName_v(oname); 
		fvisitor.setValue_v(ovalue); 
		
		// run visitor
		treciever.accept(fvisitor); 
		
		// return results 
		IBob resultBob = fvisitor.getFindResult(); 
		IResult result = new GResult(); 
		result.addChild(resultBob); 
		
		//logger.debug("FindCommand.execute / result["+result.toXML()+"]"); 
		
		return result; 
		
	}
	
	class FindVisitor implements IVisitor { 
		
		private String tagName = null; 
		private String name_v = null; 
		private String value_v = null; 		
		private IBob findResult = null; 
		
		public String getTagName() {
			return tagName;
		}
		public void setTagName(String tagName) {
			this.tagName = tagName;
		}
		public IBob getFindResult() {
			return findResult;
		}
		public void setFindResult(IBob findResult) {
			this.findResult = findResult;
		}
		public String getName_v() {
			return name_v;
		}
		public void setName_v(String name_v) {
			this.name_v = name_v;
		}
		public String getValue_v() {
			return value_v;
		}
		public void setValue_v(String value_v) {
			this.value_v = value_v;
		}

		public void visit(IBob bob) { 
			
			
			String tname = bob.getTagName(); 
			
			logger.debug(">>>> FindVisitor / visiting / ["+bob.toXML()+"] / ["+tagName+"/"+name_v+"/"+value_v+"]"); 
			//logger.debug(">>>> FindVisitor / tname["+tname+"] / tagName["+tagName+"]"); 
			
			// find first occurence of given criteria 
			if( (findResult == null) && (tname.equals(tagName))) { 
				
				// not found yet - visiting
				AttributesImpl _attributes = (AttributesImpl)bob.getAttributes(); 
				
			    int length = _attributes.getLength();
			    String lname = null;
			    for(int i=0; i < length; i++) {
			    	
					lname = _attributes.getLocalName(i); 
					
					//logger.debug("attribute localname["+lname+"]");
					if(lname.equals(name_v)) {
						
						String value = _attributes.getValue(i); 
						//logger.debug("attribute value["+value+"]");
						
						if(value.equals(value_v)) { 
							
							// found our result 
							logger.debug("MATCH!!!");
							findResult = bob;
						}
					}
			    }
			    
			}
		    
			//logger.debug("");
		}
		
	}
	// end of visitor class definition 
	
	
}


