package com.interrupt.bookkeeping.cc.bkell.command;

import com.interrupt.bookkeeping.exception.AuthorisationException;
import com.interrupt.bookkeeping.users.IUser;
import com.interrupt.spittoon.Spittoon;

public class CommitCommand extends AbstractCommand {
	
	private String inputXML = null; 
	
	public CommitCommand() { 
		this("commit", null); 
	}
	public CommitCommand(Spittoon spitt) { 
		this("commit", spitt); 
	}
	public CommitCommand(String cname, Spittoon spitt) { 
		super(cname, spitt); 
	}
	
	
	public String getInputXML() {
		return inputXML;
	}
	public void setInputXML(String inputXML) {
		this.inputXML = inputXML;
	}
	
	
	public IResult execute(IUser user) { 
		
		super.execute(user); 
		//String dburl = spittoon.getGroupsDbUrl(); 
		
		logger.debug("CommitCommand.execute / dbUrl["+this.getContextUrl()+"] / contextXPath["+
				this.getContextXPath()+"] / inputXML["+this.getInputXML()+"]"); 
		spittoon.createR(this.getContextUrl(), this.getContextXPath(), this.getInputXML()); 
		
		return new GResult(); 
		
	}
	
}
