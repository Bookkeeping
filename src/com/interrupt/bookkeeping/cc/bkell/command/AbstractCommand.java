
package com.interrupt.bookkeeping.cc.bkell.command;

import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;

import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.BobSystem;

import com.interrupt.bob.base.IBob; 
import com.interrupt.bookkeeping.GSystem;
import com.interrupt.bookkeeping.ISystem;
import com.interrupt.bookkeeping.cc.bkell.IBkell;
import com.interrupt.bookkeeping.cc.bkell.aauth.Aauthentication;
import com.interrupt.bookkeeping.exception.AuthorisationException;
import com.interrupt.bookkeeping.exception.CommandException; 
import com.interrupt.bookkeeping.system.BookkeepingSystemProperties;
import com.interrupt.bookkeeping.users.IUser;
import com.interrupt.spittoon.Spittoon;

public abstract class AbstractCommand extends GCommand { 
	
    
	protected Logger logger = Logger.getLogger(AbstractCommand.class); 
	private Map tokenMap = null; 
	private IToken _token = null; 
	private IBob _tokenReciever = null; 
	private IOptions _options = null; 
	private Aauthentication aauthentication = null; 
	
	protected Spittoon spittoon = null; 
	protected String contextXPath = null; 
	protected String contextUrl = null; 
	
	public AbstractCommand() { 
		
		this(null, null); 
	}
	public AbstractCommand(Spittoon spitt) { 
		
		this(null, spitt); 
	}
	public AbstractCommand(String commandName, Spittoon spitt) { 
		
		spittoon = spitt; 
		if(commandName != null) { 
			
			this.setName(commandName); 
			
			ISystem bsystem = (GSystem)Bob.loadS( CreateCommand.class.getResourceAsStream("/bookkeeping.system.xml"), BookkeepingSystemProperties.instance().getProperty("bob.def") ); 
			IBkell bkell = bsystem.findBkellById("bkell.main"); 
			ITokens tokens = bkell.findTokensById("tokens.def"); 
			ICommands commands = bkell.findCommandsById("commands.def"); 
			
			ICommand command = commands.findCommandByName(commandName); 
			ITokens possibleTokens = (ITokens)command.allTokens().get(0); 
			
			// set the list of possible tokens 
			this.setTokens(possibleTokens); 
		}
		
		this.setTokenLiterals(new GTokenLiterals());
		tokenMap = new HashMap(); 
		tokenMap.put("debit", "com.interrupt.bookkeeping.account.GDebit"); 
		tokenMap.put("credit", "com.interrupt.bookkeeping.account.GCredit"); 
		tokenMap.put("entry", "com.interrupt.bookkeeping.journal.Entry"); 
		tokenMap.put("entries", "com.interrupt.bookkeeping.journal.Entries"); 
		tokenMap.put("journal", "com.interrupt.bookkeeping.journal.GJournal"); 
		tokenMap.put("account", "com.interrupt.bookkeeping.account.Account"); 
		tokenMap.put("transaction", "com.interrupt.bookkeeping.journal.Transaction"); 
		
		tokenMap.put("system", "com.interrupt.bookkeeping.System"); 
		tokenMap.put("journals", "com.interrupt.bookkeeping.journal.Journals"); 
		tokenMap.put("accounts", "com.interrupt.bookkeeping.account.Accounts"); 
		tokenMap.put("user", "com.interrupt.bookkeeping.users.User"); 
		tokenMap.put("users", "com.interrupt.bookkeeping.users.Users"); 
		tokenMap.put("group", "com.interrupt.bookkeeping.users.Group"); 
		tokenMap.put("groups", "com.interrupt.bookkeeping.users.Groups"); 
		
		_options = new Options(); 
		
	}
	
	
	public Spittoon getSpittoon() { return spittoon; }
	public void setSpittoon(Spittoon spittoon) { this.spittoon = spittoon; }
	public String getContextUrl() { return contextUrl; }
	public void setContextUrl(String contextUrl) { this.contextUrl = contextUrl; }
	
	
	/**
	 * i) there should be 1 token executed for this command 
	 * ii) there should be 1 accompanying list of options supplied to the command 
	 */
	
	// set tokens 
	public void setToken(IToken token) { 
		
		if( !this.validateCommandToken(token) ) { 
			throw new CommandException("Command token is not in the set of possible tokens["+ this.getTokens().toXML() +"]"); 
		}
		this.getTokens().removeAllToken(); 
		this.getTokens().addToken(token); 
		
		_token = token; 
		
	}	
	public IToken getToken() { 
		
		logger.info("this.getTokens(): " + this.getTokens()); 
		logger.info("_token: "+  _token); 
		return this.getTokens().findTokenByName(_token.getName()); 
	}
	
	// set options 
	public void addOption(IOption option) { 
		this._options.addOption(option); 
	}
	public IOptions getOptions() { 
		return this._options; 
	}
	public void setOptions(IOptions options) { 
		this._options = options;
	}
	
	public IOption getOption(String oname) { 
		
		return (IOption)this._options.findOptionByName(oname); 
	}
	
	public String getContextXPath() {
		
		logger.info("AbstractCommand.GETContextXPath CALLED > contextXPath["+contextXPath+"]"); 
		return contextXPath; 
	}
	public void setContextXPath(String contextXPath) {
		
		logger.info("AbstractCommand.SETContextXPath CALLED > contextXPath["+contextXPath+"]"); 
		this.contextXPath = contextXPath;
	}
	
	
	/* there should be 1 set of possible tokens for any given command
	 */
	public ITokens getTokens() { 
		
		List allToken = this.allTokens(); 
		if(allToken.isEmpty()) { 
			return null;
		}
		return (ITokens)this.allTokens().get(0);
		
	}
	public void setTokens(ITokens oset) { 
		
		this.addTokens(oset);
	}
	public void addTokens(ITokens oset) { 
		this.removeAllTokens(); 
		super.addTokens(oset); 
	}
	
	
	public Aauthentication getAauthentication() {
        return aauthentication;
    }
	public void setAauthentication(Aauthentication aauthentication) {
        this.aauthentication = aauthentication;
    }
    
    /* EXECUTE this command
	 */
	public IResult execute(IUser user) { 
		
		// check that the user is authorized to perform this operation
	    
		boolean authorised = this.aauthentication.authorised(user, this); 
		if(!authorised) { 
			throw new AuthorisationException("User is either not Authenticated or Authorised to perform this action"); 
		}
		
		return null; 
		
	}
	
	/** 
	 * a command can only be executed on a given token or set of tokens 
	 * 
	 */
	public boolean validateCommandToken(IToken token) { 
		
		boolean valid = false; 
		String tokenName = token.getName(); 
		
		List possibleTokens = this.getTokens().allToken(); 
		Iterator oiter = possibleTokens.iterator(); 
		Token eacho = null; 
		while( oiter.hasNext() ) { 
			
			eacho = (Token)oiter.next(); 
			String each_n = eacho.getName(); 
			
			if(tokenName.equals(each_n)) { 
				valid = true;
				break;
			}
		}
		
		return valid; 
	}
	
	
	public List isolateSearchList(Attributes attributes, List possibleResults) { 
		
		List isolatedList = possibleResults; 
		Map isolatedMap = new HashMap(); 
		
		
		//** putting a match count for each attribute 
		Iterator iter = possibleResults.iterator(); 
		IBob eachBob = null; 
		while(iter.hasNext()) { 
			
			eachBob = (IBob)iter.next(); 
			//logger.debug("");
			//logger.debug("");
			//logger.debug("NEW Iteration");
			
			// 2. isolate on attributes 
			int matchCount = 0; 
			for( int i=0; i < attributes.getLength(); i++ ) { 	// COMPARE Attributes 
				
				//logger.debug("");
				String compareValue = attributes.getValue(i); 
				if((compareValue != null) && (compareValue.trim().length() > 0)) { 
					
					Attributes eachAttributes = eachBob.getAttributes(); 
					for (int j = 0; j < eachAttributes.getLength(); j++) {	// EACH Attributes 
						
						String bobValue = eachAttributes.getValue(j); 
						logger.debug("compareValue["+compareValue+"] / bobValue["+bobValue+"]"); 
						if(compareValue.equals(bobValue)) { 
							
							// add even if match only only 1 attribute 
							matchCount += 1;
							logger.debug("MATCH!!! count["+matchCount+"]"); 
							
						}
						
					}
					if(matchCount > 0) { 
						isolatedMap.put(eachBob, new Integer(matchCount)); 
					}
					
				}
				
			}
			
			
		}
		
		//return new ArrayList(isolatedMap.keySet()); 
		
		int latestMatch = 0; 
		int highestMatch = 0; 
		
		Set kset = isolatedMap.keySet(); 
		Iterator iter2 = kset.iterator(); 
		IBob eachBob2 = null; 
		Integer eachInteger = null; 
		while(iter2.hasNext()) { 
			
			eachBob2 = (IBob)iter2.next(); 
			eachInteger = (Integer)isolatedMap.get(eachBob2); 
			latestMatch = eachInteger.intValue(); 
			
			//logger.debug("AbstractCommand.isolateSearchList / LatestMatch["+latestMatch+"] / HighestMatch["+highestMatch+"]"); 
			
			if(latestMatch == highestMatch) { 
				isolatedList.add(eachBob2); 
			}
			else if(latestMatch > highestMatch) {
				
				highestMatch = latestMatch; 
				isolatedList = new ArrayList(); 
				isolatedList.add(eachBob2); 
			}
			
		}
		
		
		return isolatedList; 
		
	}
	
	
	public String tokenToFQClassName(String token) { 
		
		
		if(token == null || token.trim().length() == 0) { 
			
			throw new CommandException( "token name is NULL" ); 
		}
		
		String value = null; 
		Set keys = tokenMap.keySet(); 
		if(!keys.contains(token)) { 
			
			throw new CommandException( "token["+token+"] does not exist" ); 
		}
		
		Iterator kiter = keys.iterator(); 
		while(kiter.hasNext()) { 
			
			String each = (String)kiter.next();
			if(token.trim().equals(each)) { 
				
				value = (String)tokenMap.get(each);
				break; 
			}
			
		}
		
		return value; 
	}
	
	
	/* there should be 1 set of possible token.literals for any given command
	 */
	public ITokenLiterals getTokenLiterals() { 
		
		List allTokenLiterals = this.allTokenLiterals(); 
		if(allTokenLiterals.isEmpty()) { 
			return null;
		}
		return (ITokenLiterals)this.allTokenLiterals().get(0);
	}
	public void setTokenLiterals(ITokenLiterals oset) { 
		this.addTokenLiterals(oset);
	}
	public void addTokenLiterals(ITokenLiterals oset) { 
		this.removeAllTokenLiterals(); 
		super.addTokenLiterals(oset); 
	}
	
	
	/* I want to be able to add token.literals from the command itself
	 */
	public void addTokenLiteral(IBob addition) { 
		this.getTokenLiterals().addChild( addition ); 
	}
	
	
	public void removeAllTokenLiteral() { 
		
		ITokenLiterals tokenLiterals = this.getTokenLiterals(); 
		if(tokenLiterals == null) { 
			return; 
		}
		
		tokenLiterals.removeAllChildren(); 
	}
	
	
	/* Add / Remove / Reverse commands have a reciever on which a command applies 
	 * token.literals
	 */
	public IBob getTokenReciever() { 
		
		return this._tokenReciever; 
		
	}
	public void setTokenReciever(IBob treciever) { 
		
		this.removeChild(this._tokenReciever); 
		
		this.addChild(treciever); 
		this._tokenReciever =  treciever; 
		
	}
	
	
}


