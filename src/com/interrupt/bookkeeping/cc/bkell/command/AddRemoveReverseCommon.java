package com.interrupt.bookkeeping.cc.bkell.command; 


import com.interrupt.bookkeeping.exception.CommandException;

public class AddRemoveReverseCommon extends AbstractCommand { 
	
	public void setToken(IToken token) { 
		
		throw new CommandException("Add / Remove / Reverse commands do not allow 'token', only 'token.reciever'"); 
	}
} 


