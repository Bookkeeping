package com.interrupt.bookkeeping.cc.bkell.command;

import com.interrupt.bookkeeping.cc.bkell.aauth.Aauthentication;
import com.interrupt.bookkeeping.exception.AuthorisationException;
import com.interrupt.bookkeeping.users.IUser;

public class LogoutCommand extends AbstractCommand {
	
	
	private Aauthentication aauth = null; 
	
	public LogoutCommand(Aauthentication aauth) { 
		this.aauth = aauth; 
	}
	
	public IResult execute(IUser user) {
		
	    boolean loggedOut = aauth.logout(user); 
		if(loggedOut) { 
			return new GResult(); 
		}
		throw new AuthorisationException("Unable to lgout user ["+user.getId()+"]"); 
		
	}
	
}
