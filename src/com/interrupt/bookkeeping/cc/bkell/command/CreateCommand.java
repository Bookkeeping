package com.interrupt.bookkeeping.cc.bkell.command;


import java.io.File;
import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;

import java.util.List; 
import java.util.Iterator; 
import com.interrupt.bob.base.IBob; 
import com.interrupt.bob.base.Bob; 
import com.interrupt.bob.util.StringUtil;
import com.interrupt.bookkeeping.GSystem;
import com.interrupt.bookkeeping.ISystem;
import com.interrupt.bookkeeping.cc.bkell.IBkell;
import com.interrupt.bookkeeping.system.BookkeepingSystemProperties;
import com.interrupt.bookkeeping.users.IUser;

public class CreateCommand extends AbstractCommand {
	
	
	public CreateCommand() { 
		
		this.setName("create"); 
		
		/** 
		 * get the possible set of 
		 * 	i. tokens for a command & 
		 * 	ii. options for those tokens
		 * 
		 */ 
		//ISystem bsystem = (GSystem)genericBob.load( new File(BookkeepingSystemProperties.instance().getProperty("bob.def")) ); 
		ISystem bsystem = (GSystem)Bob.loadS( CreateCommand.class.getResourceAsStream("/bookkeeping.system.xml"), BookkeepingSystemProperties.instance().getProperty("bob.def") ); 
		
		//logger.debug( "1 > "+ bsystem ); 
		
		IBkell bkell = bsystem.findBkellById("bkell.main"); 
		
		//logger.debug( "2 > "+ bkell ); 
		
		ITokens tokens = bkell.findTokensById("tokens.def"); 
		ICommands commands = bkell.findCommandsById("commands.def"); 
		
		//logger.debug( "3 > "+ tokens ); 
		//logger.debug( "4 > "+ commands ); 
		
		
		ICommand command = commands.findCommandByName("create"); 
		ITokens possibleTokens = (ITokens)command.allTokens().get(0); 
		
		
		// set the list of possible tokens 
		this.setTokens(possibleTokens); 
		
	}
	public IResult execute(IUser user) { 
		
		super.execute(user); 
		
		IResult result = new GResult(); 
		
		
		//** TOKEN LITERAL - this should accept token literals as well as options 
		ITokenLiterals tliterals = this.getTokenLiterals(); 
		List literalList = tliterals.allChildren(); 
		IBob tliteral = null;  
		if( literalList.size() > 0 ) { 
			
			tliteral = (IBob)literalList.get(0);
			result.addChild(tliteral); 
			return result; 
		}
		
		
		//** use TOKEN OPTIONS otherwise 
		IToken token = this.getToken(); 
		//logger.debug( "creating token["+token+"]" ); 
		
		
		String tokenName = token.getName(); 
		
		IOptions options = this.getOptions(); 
		List option_l = options.allOption();
		
		
		// create <bob/> from token 
		String fqClassName = this.tokenToFQClassName(tokenName); 
		IBob createdBob = Bob.make(fqClassName);
		
		// fill with options
		Iterator oiter = option_l.iterator(); 
		IOption each_o = null; 
		while(oiter.hasNext()) { 
			
			each_o = (IOption)oiter.next(); 
			String oname = each_o.getName(); 
			String ovalue = each_o.getValue(); 
			
			// setter method
			Class bobClass = createdBob.getClass(); 
			Class defParams[] = { ovalue.getClass() };
			Object methParams[] = { ovalue }; 
			
    		String setterName = "set" + StringUtil.upperFirstLetter(oname);
			try {   
				
				Method setterMethod = bobClass.getMethod( setterName, defParams );  
				setterMethod.invoke( createdBob, methParams );
				
			}       
			catch(NoSuchMethodException e) {
				e.printStackTrace();
			}       
			catch(IllegalAccessException e) {
				e.printStackTrace();
			}       
			catch(InvocationTargetException e) {
				e.printStackTrace();
			}       
			
		}
		
		result.addChild(createdBob); 
		return result;
		
	}
	
}


