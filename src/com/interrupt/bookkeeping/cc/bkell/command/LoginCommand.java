package com.interrupt.bookkeeping.cc.bkell.command;

import org.apache.log4j.Logger;

import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;
import com.interrupt.bookkeeping.GSystem;
import com.interrupt.bookkeeping.ISystem;
import com.interrupt.bookkeeping.cc.bkell.IBkell;
import com.interrupt.bookkeeping.cc.bkell.aauth.Aauthentication;
import com.interrupt.bookkeeping.http.BookkeepingSystemFacade;
import com.interrupt.bookkeeping.system.BookkeepingSystemProperties;
import com.interrupt.bookkeeping.users.IUser;
import com.interrupt.bookkeeping.users.IUserSession;
import com.interrupt.bookkeeping.users.User;
import com.interrupt.spittoon.Spittoon;

public class LoginCommand extends AbstractCommand {
	
	
    private Logger logger = Logger.getLogger(LoginCommand.class); 
    private Logger algorithm = Logger.getLogger("algorithm"); 
    
	public LoginCommand() { 
		
		this(null); 
	}
	public LoginCommand(Spittoon spittoon) { 
		
		super("login", spittoon); 
		
		ISystem bsystem = (GSystem)Bob.loadS( CreateCommand.class.getResourceAsStream("/bookkeeping.system.xml"), BookkeepingSystemProperties.instance().getProperty("bob.def") ); 
		IBkell bkell = bsystem.findBkellById("bkell.main"); 
		ITokens tokens = bkell.findTokensById("tokens.def"); 
		ICommands commands = bkell.findCommandsById("commands.def"); 
		
		ICommand command = commands.findCommandByName("login"); 
		ITokens possibleTokens = (ITokens)command.allTokens().get(0); 
		
		// set the list of possible tokens 
		this.setTokens(possibleTokens); 
		
	}
	public IResult execute(IUser ignoreThis) { 
		
		IOption groupOption = this.getOption("group"); 
		IOption unameOption = this.getOption("username"); 
		IOption passwdOption = this.getOption("password"); 
		
		String sessionid = null;
		
		Aauthentication aauth = (Aauthentication)this.getSpittoon().
			retrieve(this.getSpittoon().getAauthDbUrl(), 
				"/system[ @id='main.system' ]/aauthentication[ @id='main.authentication' ]", true); 
		logger.debug("Loaded Aauthentication["+ aauth +"]"); 
		
		algorithm.debug(""); algorithm.debug(""); 
		algorithm.debug("BEGIN > LoginCommand.execute > Spittoon CALL"); 
		IBob testing = this.getSpittoon().
			retrieve(this.getSpittoon().getAauthDbUrl(), 
				"/system[ @id='main.system' ]/aauthentication[ @id='main.authentication' ]", true); 
		algorithm.debug("END > LoginCommand.execute > Loaded Aauthentication["+ testing +"]"); 
		//if(true)
		//	throw new RuntimeException("TEST END["+testing+"]");
		
		
		IUser user = (IUser)aauth.getUsers().findUserById(unameOption.getValue()); 
		if(user == null) { 
			return null; 
		}
		IUser userSubmit = new User(); 
		//userSubmit.setAttributes(user.getAttributes()); 
		userSubmit.setId(unameOption.getValue()); 
		userSubmit.setUsername(unameOption.getValue());  
		userSubmit.setPassword(passwdOption.getValue()); 
		userSubmit.setLogintimeout("600000"); 
		userSubmit.setDefaultGroup(user.getDefaultGroup()); 
		
		IUserSession usession = null; 
		if((groupOption != null) && (groupOption.getValue() != null) && (groupOption.getValue().trim().length() > 0)) { 
			
			usession = aauth.authenticate(groupOption.getValue(), userSubmit); 
		}
		else { 
			
			String defaultGroup = userSubmit.getDefaultGroup(); 
			usession = aauth.authenticate(defaultGroup, userSubmit); 
		}
		if(usession == null) { 
			return null; 
		}
		
		//IUserSession usession = aauth.getUserSession(sessionid); 
		//logger.debug("LoginCommand.execute: "+ usession.toXML(false)); 
	    //logger.debug("LoginCommand.execute: ns: "+ usession.getNamespace()); 
	    
		IResult result = new GResult(); 
		result.addChild(aauth); 
		return result; 
		
	}
	
}

