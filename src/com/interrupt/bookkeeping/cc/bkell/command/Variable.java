package com.interrupt.bookkeeping.cc.bkell.command;

import org.apache.log4j.Logger;

import com.interrupt.bob.base.BobSystem;

import com.interrupt.bob.base.IBob;

public class Variable extends GVariable {
	
	
	private Logger logger = Logger.getLogger(Variable.class); 
	
	public Variable() { 
	}
	
	public String getName() { 
		
		String sname = super.getName(); 
		if(sname != null) { 
			sname = sname.trim(); 
		}
		
		//logger.debug("getting varible["+sname+"]"); 
		return sname; 
		
	}
	
	public void setName(String name) { 
		
		//logger.debug("setting variable["+name+"]"); 
		if(name != null) { 
			name = name.trim(); 
		} 
		super.setName(name); 
		
	}
	
	public void addChild(IBob child) { 
		super.addChild(child); 
	}
	
}


