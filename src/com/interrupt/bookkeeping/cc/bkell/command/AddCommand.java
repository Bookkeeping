package com.interrupt.bookkeeping.cc.bkell.command; 


import java.lang.reflect.InvocationTargetException;
import java.util.List; 
import java.util.Iterator; 

import org.apache.log4j.Logger;

import com.interrupt.bookkeeping.journal.Journal;

import com.interrupt.bob.base.IBob;
import com.interrupt.bob.util.Util;
import com.interrupt.bookkeeping.cc.ExpressionVisitor;
import com.interrupt.bookkeeping.cc.bkell.command.ITokenLiterals;
import com.interrupt.bookkeeping.cc.bkell.command.IResult;
import com.interrupt.bookkeeping.cc.bkell.command.GResult;
import com.interrupt.bookkeeping.exception.BkellException;
import com.interrupt.bookkeeping.exception.CommandException; 
import com.interrupt.bookkeeping.journal.IEntry;
import com.interrupt.bookkeeping.journal.IJournal;
import com.interrupt.bookkeeping.users.IUser;

public class AddCommand extends AddRemoveReverseCommon { 
	
	
	private Logger logger = Logger.getLogger(AddCommand.class); 
	
	public AddCommand() { 
		
		super();
		this.setName("add");
	}
	
	
	/* execute this command, add the 'token.literals' to the 'token.reciever'
	 */
	public IResult execute(IUser user) { 
		
		logger.debug("AddCommand.execute: "+ this.toXML(false)); 
		super.execute(user); 
		
		IBob treciever = this.getTokenReciever();
		
		ITokenLiterals tliterals = this.getTokenLiterals(); 
		List allLiterals = tliterals.allChildren();
		Iterator titer = allLiterals.iterator(); 
		IBob eachLiteral = null; 
		while( titer.hasNext() ) { 
			
			eachLiteral = (IBob)titer.next(); 
			
			Class defParams[] = { eachLiteral.getClass() }; 
			IBob methParams[] = { eachLiteral }; 
			
			logger.debug("adding >> treceiver["+treciever.getClass()+" / "+treciever+"] / token["+eachLiteral.getClass()+" / "+eachLiteral.getTagName()
				+"] / defParams["+defParams+"] / methParams["+methParams+"]"); 
			
			try { 
				Util.evalReflectedAdd(treciever, eachLiteral.getTagName(), defParams, methParams); 
			}
			catch(NoSuchMethodException e) {
	    	    BkellException bke = com.interrupt.bookkeeping.util.Util.generateBkellException(e); 
				throw bke; 
	    	}
	    	catch(IllegalAccessException e) {
	    	    BkellException bke = com.interrupt.bookkeeping.util.Util.generateBkellException(e); 
	    	    throw bke; 
	    	}
	    	catch(InvocationTargetException e) {
	    	    BkellException bke = com.interrupt.bookkeeping.util.Util.generateBkellException(e); 
	    	    throw bke; 
	    	}
	    	catch(Throwable e) {
	    		BkellException bke = com.interrupt.bookkeeping.util.Util.generateBkellException(e); 
	    	    throw bke; 
	    	}
			
		}
		
		//** TODO - update treciever into DB 
		
		
		IResult result = new GResult(); 
		result.addChild(treciever); 
		
		return result; 
	}
	
}


