package com.interrupt.bookkeeping.cc.bkell.command; 


import java.util.List;
import java.util.Iterator;
import org.apache.log4j.Logger;
import com.interrupt.bob.base.BobSystem;
import com.interrupt.bookkeeping.exception.CommandException; 

public class Token extends GToken { 
	
	
	private Logger logger = Logger.getLogger(Token.class); 
	private IOptions _options = null; 
	
	
	// set the main options
	public IOptions getOptions() { 
		
		return _options;
	}
	
	public void setOptions(IOptions options) { 

		// ensure 'options' is one of the possible group of options in the set 
		if( !this.validateOptionToSet((Options)options) ) { 
			throw new CommandException( "Input Options do not conform to possible set of options["+this.getOptionSet().toXML()+"]" );
		}
		_options = options; 
	}
	
	
	// ensure that there is only 1 optionSet
	public IOptionSet getOptionSet() { 
		
		List allOptions = this.allOptionSet(); 
		if(allOptions.isEmpty()) { 
			return null;
		}
		return (IOptionSet)allOptions.get(0);
		
	}
	public void setOptionSet(IOptionSet oset) { 
		
		this.addOptionSet(oset);
	}
	public void addOptionSet(IOptionSet oset) { 
		this.removeAllOptionSet(); 
		super.addOptionSet(oset); 
	}


	public boolean validateOptionToSet(Options options) {
		
		
		logger.debug("validateOptionToSet > options > "+ options); 
		logger.debug("validateOptionToSet > optionSet > "+ this.getOptionSet()); 
		
		boolean valid = false; 
		List inputKeys = options.keySet(); 
		
		List possibleOptions = this.getOptionSet().allOptions(); 
		Iterator oiter = possibleOptions.iterator(); 
		Options eacho = null; 
		while( oiter.hasNext() ) { 
			
			eacho = (Options)oiter.next(); 
			List keyset = eacho.keySet(); 
			
			if(inputKeys.containsAll(keyset)) { 
				valid = true;
				break;
			}
		}
		
		return valid; 
	}
	
	
}



