
package com.interrupt.bookkeeping.cc.bkell.command; 


import java.util.List; 
import java.util.Iterator; 
import org.apache.log4j.Logger;
import com.interrupt.bob.base.BobSystem;

import com.interrupt.bob.base.IBob;
import com.interrupt.bookkeeping.cc.bkell.command.ITokenLiterals;
import com.interrupt.bookkeeping.cc.bkell.command.IResult;
import com.interrupt.bookkeeping.cc.bkell.command.GResult;
import com.interrupt.bookkeeping.exception.CommandException; 
import com.interrupt.bookkeeping.users.IUser;

public class RemoveCommand extends AddRemoveReverseCommon { 
	
	
	private Logger logger = Logger.getLogger(RemoveCommand.class); 
	public RemoveCommand() { 
		
		super();
		this.setName("remove");
	}
	
	
	/* execute this command, add the 'token.literals' to the 'token.reciever'
	 */
	public IResult execute(IUser user) { 
		
		super.execute(user); 
		
		IBob treciever = this.getTokenReciever();
		
		ITokenLiterals tliterals = this.getTokenLiterals(); 
		List allLiterals = tliterals.allChildren();
		Iterator titer = allLiterals.iterator(); 
		IBob eachLiteral = null; 
		
		logger.debug("BEFORE > treciever > "+ treciever.toXML()); 
		String path = null; 
		while( titer.hasNext() ) { 
			
			// this will remove the first instance of the child type 
			eachLiteral = (IBob)titer.next(); 
			logger.debug("Memory XML > removing > "+ eachLiteral.toXML()); 
			
			if(treciever.removeChild(eachLiteral)) { 	//** if we successfully remove the child in XML, then remove it from DB 
				
				path = this.getContextXPath() + "/" + eachLiteral.getTagName() + "." + eachLiteral.getAttributeValue("id"); 
				logger.debug("DB XML > removing > context url["+ this.getContextUrl() +"] / path["+ path +"]"); 
				spittoon.removeCollection(this.getContextUrl(), path); 
				
			}
			
		}
		
		IResult result = new GResult(); 
		result.addChild(treciever); 
		
		logger.debug("AFTER > treciever > "+ treciever.toXML()); 
		
		return result; 
	}
	
}


