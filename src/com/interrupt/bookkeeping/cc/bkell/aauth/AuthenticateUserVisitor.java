package com.interrupt.bookkeeping.cc.bkell.aauth;

import org.apache.log4j.Logger;

import com.interrupt.bob.base.BobSystem;

import com.interrupt.bob.base.IBob;
import com.interrupt.bob.base.IVisitor;
import com.interrupt.bookkeeping.users.User;
import com.interrupt.bookkeeping.users.UserSession;


public class AuthenticateUserVisitor implements IVisitor { 
	
	private String uname = null; 
	private String passwd = null; 
	private Logger logger = Logger.getLogger(AuthenticateUserVisitor.class); 
	private UserSession usession = null; 
	
	public void setUname(String un) { 
		uname = un; 
	}
	public String getUname() { 
		return uname; 
	}
	public void setPasswd(String pwd) { 
		passwd = pwd; 
	}
	public String getPasswd() { 
		return passwd; 
	}
	public UserSession getUserSession() {
		return this.usession;
	}
	public void setUserSession(UserSession usession) {
		this.usession = usession;
	}
	public void visit(IBob bob) { 
		
		logger.debug("AuthenticateUserVisitor::visit > tagName["+ bob.getTagName() +"] > id["+ bob.getAttributeValue("id") +"]"); 
		if(	bob.getTagName().equals("user") ) { 
			
			if( ((User)bob).getUsername().equals(uname) && 
				((User)bob).getPassword().equals(passwd) ) { 
				
				((User)bob).setAuthenticated(true); 
				((User)bob).setUserSession(usession); 
				((User)bob).startSession(); 
				
			}
			
			if( bob.getTagName().equals("user") ) { 
				logger.debug(">>>>> Visiting \\ "+ bob.getTagName() +" \\ authenticated \\ "+ ((User)bob).isAuthenticated()); 
			}
			
		}
		
	}
	
}
