package com.interrupt.bookkeeping.cc.bkell.aauth;

import org.xmldb.api.base.XMLDBException;

import com.interrupt.bookkeeping.System;
import com.interrupt.bookkeeping.exception.AuthorisationException;
import com.interrupt.bookkeeping.system.BookkeepingSystemProperties;
import com.interrupt.bookkeeping.users.Group;
import com.interrupt.bookkeeping.users.IGroup;

public class AauthenticationMediator { 
	
	private System system = null; 
	private Aauthentication aauth = null; 
	public System getSystem() {
		return system;
	}
	public void setSystem(System system) {
		this.system = system;
	}
	public Aauthentication getAauth() {
		return aauth;
	}
	public void setAauth(Aauthentication aauth) {
		this.aauth = aauth;
	} 
	
	public void messageMoveGroupBookkeepingToAttic(String groupid) { 
		
		IGroup removedGroup = system.getGroups().removeGroupById(groupid); 
		/*try { 
			//Spittoon.instance().removeDocument(
			//	BookkeepingSystemProperties.instance().getProperty("db.url") + "system/groups/", groupid); 
		}
		catch(XMLDBException e) { 
			throw new AuthorisationException("There was an error removing group["+groupid+"] from the 'main.groups'"); 
		}
		*/
		
		this.system.getGroupAttic().addGroup(removedGroup); 
	}
	
	public void addGroupSetAssociatedBookkeeping(Group group) { 
		
		this.system.addGroupSetAssociatedBookkeeping(group); 
	}
	
}
