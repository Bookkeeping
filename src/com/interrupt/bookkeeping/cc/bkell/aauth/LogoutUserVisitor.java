package com.interrupt.bookkeeping.cc.bkell.aauth;

import org.apache.log4j.Logger;

import com.interrupt.bob.base.BobSystem;

import com.interrupt.bob.base.IBob;
import com.interrupt.bob.base.IVisitor;
import com.interrupt.bookkeeping.users.User;


public class LogoutUserVisitor implements IVisitor { 
	
	String uname = null; 
	String passwd = null; 
	private Logger logger = Logger.getLogger(LogoutUserVisitor.class); 
	
	public void setUname(String un) { 
		uname = un; 
	}
	public String getUname() { 
		return uname; 
	}
	public void setPasswd(String pwd) { 
		passwd = pwd; 
	}
	public String getPasswd() { 
		return passwd; 
	}
	public void visit(IBob bob) { 
		
		if(	bob.getTagName().equals("user") ) { 
			
			if( ((User)bob).getUsername().equals(uname) && 
				((User)bob).getPassword().equals(passwd) ) { 
				
				((User)bob).setAuthenticated(false); 
		}
		
		if( bob.getTagName().equals("user") ) { 
			logger.debug(">>>>> Visiting \\ "+ bob.getTagName() +" \\ authenticated \\ "+ ((User)bob).isAuthenticated()); 
		}
		
	}
}
}