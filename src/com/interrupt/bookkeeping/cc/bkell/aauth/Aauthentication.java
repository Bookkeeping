package com.interrupt.bookkeeping.cc.bkell.aauth;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import java.util.List;

import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;

import org.apache.log4j.Logger;
import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.XMLDBException;

import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;
import com.interrupt.bob.base.IVisitor;
import com.interrupt.bookkeeping.cc.bkell.command.ICommand;
import com.interrupt.bookkeeping.exception.AuthorisationException;
import com.interrupt.bookkeeping.exception.SystemException;
import com.interrupt.bookkeeping.http.BookkeepingSystemFacade;
import com.interrupt.bookkeeping.system.BookkeepingSystem;
import com.interrupt.bookkeeping.system.BookkeepingSystemProperties;
import com.interrupt.bookkeeping.users.GGroups;
import com.interrupt.bookkeeping.users.Group;
import com.interrupt.bookkeeping.users.Groups;
import com.interrupt.bookkeeping.users.IGroup;
import com.interrupt.bookkeeping.users.IGroups;
import com.interrupt.bookkeeping.users.IUser;
import com.interrupt.bookkeeping.users.IUserSession;
import com.interrupt.bookkeeping.users.IUsers;
import com.interrupt.bookkeeping.users.User;
import com.interrupt.bookkeeping.users.UserSession;
import com.interrupt.bookkeeping.users.Users;
import com.interrupt.spittoon.Spittoon;
import com.interrupt.util.IdGenerator;

public class Aauthentication extends GAauthentication implements java.io.Serializable {
	
	
	public static final String AAUTHENTICATION = "aauthentication"; 
    private Logger logger = Logger.getLogger(Aauthentication.class); 
	private static Aauthentication instance = null; 
	
	private ThreadGroup userThreadGroup = new ThreadGroup("UserThreadGroup"); 
	private Spittoon spittoon = null; 
	private AauthenticationMediator aauthenticationMediator = null; 
	private Bob bob = new Bob(); 
	private IBob loadedAuthorise = null;  
	private UserSession userSession = null;  
	//private IUsers aauthUsers = null; 
	//private IGroups aauthGroups = null; 
	
	public Aauthentication() { 
		
		//spittoon = Spittoon.instance(); 
		this.setId("main.authentication"); 
	}
	
	public static synchronized Aauthentication instance() { 
		
		if(instance == null) { 
			instance = new Aauthentication(); 
			instance.initialise(); 
		}
		return instance; 
	}
	
	
	public synchronized void setup() { 
		
		//loadedAuthorise = bob.load(Aauthentication.class.getResourceAsStream("/bookkeeping.system.xml"), BookkeepingSystemProperties.instance().getProperty("bob.def")); 
		//logger.debug("loadedAuthorise:: "+ loadedAuthorise.toXML() ); 
		
		//IUsers userResult = (IUsers)loadedAuthorise.find("users", "aauth.users"); 
		//logger.debug("userResult:: "+ userResult.toXML() ); 
		
		
		//spittoon.addDocument(BookkeepingSystemProperties.instance().getProperty("db.url") + "system/aauthentication/users/", userResult); 
		
		// put a default '/groups/' in the xml database 
		//IGroups groupResult = (IGroups)loadedAuthorise.find("groups", "aauth.groups"); 
		//spittoon.addDocument(BookkeepingSystemProperties.instance().getProperty("db.url") + "system/aauthentication/groups/", groupResult); 
		
	}
	public synchronized void teardown() throws XMLDBException { 
		
		// remove default collections  
		//spittoon.removeCollection(BookkeepingSystemProperties.instance().getProperty("db.url") + "system/aauthentication/"); 
		
	}
	
	public synchronized void initialise() { 
		
		//BookkeepingSystemProperties bsproperties = BookkeepingSystemProperties.instance(); 
		//IUsers rootUsers = (IUsers)spittoon.getDocument(
		//	bsproperties.getProperty("db.url") + "system/aauthentication/users/", "users", "aauth.users"); 
		//IGroups rootGroups = (IGroups)spittoon.getDocument(
		//	bsproperties.getProperty("db.url") + "system/aauthentication/groups/", "groups", "aauth.groups"); 
		//IUsers rootUsers = null;  
		//IGroups rootGroups = null;  
		
		//this.setUsers(rootUsers); 
		//this.setGroups(rootGroups); 
		
	}
	
	public String hashPassword(String passwd) throws SystemException { 
		
		MessageDigest digest = null; 
		byte[] input = null; 
		try { 
			
			digest = MessageDigest.getInstance("SHA");
			digest.reset();
			input = digest.digest(passwd.getBytes("UTF-8"));
		}
		catch(NoSuchAlgorithmException e) { 
			throw new SystemException(e); 
		}
		catch(UnsupportedEncodingException e) { 
			throw new SystemException(e); 
		}
		
		String hashString = new String(input); 
		return hashString; 
		
	}
	public synchronized AauthenticationMediator getAauthenticationMediator() {
		return aauthenticationMediator;
	}
	public synchronized void setAauthenticationMediator(AauthenticationMediator aauthenticationMediator) {
		this.aauthenticationMediator = aauthenticationMediator;
	}
	public UserSession getUserSession() { return userSession; }
	public void setUserSession(UserSession userSession) { this.userSession = userSession; }
	public synchronized IUsers getUsers() { 
		return this.findUsersById("aauth.users"); 
	}
	public synchronized void setUsers(IUsers users) { 
		users.setId("aauth.users"); 
		this.removeAllUsers(); 
		this.addUsers(users); 
	}
	public synchronized IGroups getGroups() { 
		return this.findGroupsById("aauth.groups"); 
	}
	public synchronized void setGroups(IGroups groups) { 
		groups.setId("aauth.groups"); 
		this.removeAllGroups(); 
		this.addGroups(groups); 
	}
	public Spittoon getSpittoon() {
		return spittoon;
	}
	public void setSpittoon(Spittoon spittoon) {
		this.spittoon = spittoon;
	}
	
	
	public synchronized IUserSession authenticate(String groupid, IUser user) { 
		 
		
		logger.debug("Aauthenticate::authenticate > groupid["+groupid+"] / user["+user+"]"); 
		
		//** check if group contains the user 
		IGroup agroup = this.getGroups().findGroupById(groupid); 
		if(agroup == null) { 
			throw new AuthorisationException("This associated group does not exist"); 
		}
		IUser auser = agroup.findUserById(user.getId()); 
		if((auser == null ) && (!agroup.getOwner().equals(user.getId()))) { 
			
			throw new AuthorisationException("The user is not associated with the group"); 
		}
		
		
		IUsers users = this.getUsers(); 
		IUser iuser = users.findUserById(user.getId()); 
		
		//String passwdHash = hashPassword(user.getPassword()); 
		String passwdHash = user.getPassword(); 
		
		
		/*logger.debug("authneticate USER input["+ user.getUsername() +"] / existing["+ 
				iuser.getUsername() +"] > match...["+ user.getUsername().equals(iuser.getUsername()) +"]");  
		logger.debug("authneticate PASSWORD input["+ user.getPassword() +"] / existing["+ 
				iuser.getPassword() +"] > match...["+ passwdHash.equals(iuser.getPassword()) +"]");  
		*/
		//** TODO - come up with a better authentication scheme
		if(	user.getUsername().equals(iuser.getUsername()) && 
			passwdHash.equals(iuser.getPassword())) { 
			
			logger.debug("authenticated ok!!!"); 
			
			this.userSession = new UserSession(); 
			try { 
				this.userSession.setTimeout(Long.parseLong(user.getLogintimeout())); 
			}
			catch(java.lang.NumberFormatException e) { 
				throw new AuthorisationException("Please set a login timeout for user["+user.getId()+"]"); 
			}
			this.userSession.setId(IdGenerator.generateId()); 
			this.userSession.setGroupid(groupid); 
			this.userSession.setUserid(user.getId()); 
			
			//** this visitor will authenticate the use and fire 
			// off the UserSession thread 
			AuthenticateUserVisitor auvisitor = new AuthenticateUserVisitor(); 
			auvisitor.setUname( user.getUsername() ); 
			auvisitor.setPasswd( user.getPassword() ); 
			auvisitor.setUserSession(this.userSession); 
			
			this.accept(auvisitor); 
			return this.userSession; 
		}
		
		return null; 
	}
	public synchronized boolean authenticated(IUser user) { 
		
		IUsers users = this.getUsers(); 
		User iuser = (User)users.findUserById(user.getId()); 
		if(iuser.isAuthenticated()) { 
			return true; 
		}
		return false; 
	}
	public synchronized IUserSession getUserSession(String sessionid) { 
		
		return (IUserSession)this.find("userSession", sessionid); 
		
	}
	
	
	public synchronized boolean logout(IUser user) { 
		
		//IUsers users = this.getUsers(); 
		//IUser iuser = users.findUserById(user.getId()); 
		
		/*IGroup agroup = this.getGroups().findGroupById(groupid); 
		if(agroup == null) { 
			throw new AuthorisationException("This associated group does not exist"); 
		}
		IUser auser = agroup.findUserById(user.getId()); 
		if((auser == null ) && (!agroup.getOwner().equals(user.getId()))) { 
			
			throw new AuthorisationException("The user is not associated with the group"); 
		}
		*/
		
		////** TODO - come up with a better authentication scheme
		//if(	user.getUsername().equals(iuser.getUsername()) && 
		//	user.getPassword().equals(iuser.getPassword())) { 
			
			LogoutUserVisitor auvisitor = new LogoutUserVisitor(); 
			auvisitor.setUname( user.getUsername() ); 
			auvisitor.setPasswd( user.getPassword() ); 
			this.accept(auvisitor); 
			return true; 
		//}
		
	}
	
	public synchronized boolean authorised(IUser user, ICommand command) { 
		
		if(!this.authenticated(user)) { 
			return false; 
		}
		
		IAllowedActions resultAllowed = (IAllowedActions)this.find("allowedActions", user.getId() + ".allowedActions");  
		List commands = resultAllowed.allCommand(); 
		Iterator iter = commands.iterator(); 
		ICommand eachCommand = null; 
		while(iter.hasNext()) { 
			
			eachCommand = (ICommand)iter.next(); 
			//logger.debug(">>> each command: "+ eachCommand.toXML()); 
			
			if( eachCommand.getName().equals(command.getName()) ) { 
				return true; 
			}
			
		}
		return false; 
	}
	
	public synchronized IUser findUser(String userid) { 
		
		return (IUser)this.getUsers().findUserById(userid); 
	}
	public synchronized IGroup findGroup(String groupid) { 
		
		return (IGroup)this.getGroups().findGroupById(groupid); 
	}
	
	public synchronized void addGroup(Group group) throws AuthorisationException { 
		
		// 1. check that there's an owner reference 
		if(group.getOwner() == null || group.getOwner().trim().length() == 0) { 
			throw new AuthorisationException("A group must have an owner (user) that is referenced"); 
		}
		
		// 2. check that there's not a duplicate group 
		Group duplicateGroupCheck = (Group)this.find("group", group.getId()); 
		if(duplicateGroupCheck != null) { 
			throw new AuthorisationException("There is already a group with the id["+group.getId()+"]");
		}
		
		// 3. check that the owner actually exists as a user
		String dburl = spittoon.getAauthDbUrl(); 
		StringBuffer sbuffer = new 
			StringBuffer("/system[ @id='main.system' ]/aauthentication[ @id='main.authentication' ]/users[ @id='aauth.users' ]"); 
		sbuffer.append("/user[ @id='"); 
		sbuffer.append(group.getOwner()); 
		sbuffer.append("']"); 
		String xpath = sbuffer.toString(); 
		
		User aauthUser = (User)spittoon.retrieve(dburl, xpath, false); 
		
		if(aauthUser == null) { 
			throw new AuthorisationException("The referenced owner / user ["+ group.getOwner() +"] does not exist");
		}
		
		//** add the group 
		this.resetPull(); 
		IBob nextPull = this.pullNext(); 
		while( nextPull != null) { 
			
			if(	nextPull.getTagName().equals("groups") && 
				nextPull.getAttributes().getValue("id").equals("aauth.groups")) { 
				
				((GGroups)nextPull).addGroup(group); 
				break; 
			}
			nextPull = this.pullNext(); 
		}
		
	}
	
	public synchronized void removeGroup(String groupId) { 
		
		// 1. ensure there are no more users besides the default user before deleting 
		Group targetGroup = (Group)(this.getGroups().findGroupById(groupId)); 
		if(targetGroup != null) { 
			
			List userList = targetGroup.allUser(); 
			if(userList.size() > 1) { 
				throw new AuthorisationException("There are more users than the default owner"); 
			}
		}
		
		// 2. move group / bookkeeping data to a removed folder for future reference 
		this.aauthenticationMediator.messageMoveGroupBookkeepingToAttic(groupId); 
		
	}
	
	
	public synchronized void addUser(User user) { 
		
		//** 1. check that there's not an existing user 
		//** this assumes that the existing user list is current with what's in the database 
		logger.debug("Aauthentication 0: "+ this.toXML(false)); 
		logger.debug("Aauthentication.addUser 1: "+ (Users)this.getUsers()); 
		logger.debug("Aauthentication.addUser 2: "+ user); 
		logger.debug(""); 
		
		User existingUser = (User)this.getUsers().findUserById(user.getId()); 
		if(existingUser != null) { 
			throw new AuthorisationException("There is already existing user with id["+user.getId()+"]"); 
		}
		
		
		//** 2. now add a default group corresponding to the new user 
		// this is the default group to add 
		InputStream istream = Aauthentication.class.getResourceAsStream("/add.usergroup.xml"); 
		try { 
			logger.debug("Bytes available for read of '/add.usergroup.xml' stream["+ istream.available() +"]"); 
		}
		catch(IOException e) { 
			e.printStackTrace(); 
		}
		
		Group defaultGroup = 
			(Group)Bob.loadS(
				istream, 
					BookkeepingSystemProperties.instance().getProperty("bob.def")); 
		defaultGroup.setId(user.getId() + ".group"); 
		defaultGroup.setName(user.getId() + ".group"); 
		defaultGroup.setOwner(user.getId()); 
		
		//** 3. add to aauth.groups 
		String dburl = spittoon.getAauthDbUrl(); 
		String groupsXpath = 
			"/system[ @id='main.system' ]/aauthentication[ @id='main.authentication' ]/groups[ @id='aauth.groups' ]" +  
				"/group[ @id='"+ defaultGroup.getId() +"' ]"; 
		spittoon.createR(dburl, groupsXpath, defaultGroup.toXML(false)); 
		
		
		//** 4. add the user to the database 
		user.setDefaultGroup(defaultGroup.getId()); // set this user's default group name 
		String authXpath = "/system[ @id='main.system' ]/aauthentication[ @id='main.authentication' ]/users[ @id='aauth.users' ]"; 
		Users aauthUsers = (Users)spittoon.retrieve(dburl, authXpath, false); 
		aauthUsers.addUser(user); 
		this.setUsers( aauthUsers ); 
		
		
		//** 5. add the user to the database 
		spittoon.createR(dburl, authXpath + "/user[ @id='"+user.getId()+"' ]", user.toXML(false)); 
		
		this.aauthenticationMediator.addGroupSetAssociatedBookkeeping(defaultGroup); 
		
	}
	
	public synchronized void removeUser(String userId) { 
		
		// 1. first remove the associated group 
		Group defaultGroup = (Group)this.getGroups().removeGroupByOwner(userId); 
		this.removeGroup(defaultGroup.getId()); 
		
		
		// 2. remove the user 
		class RemoveUserVisitor implements IVisitor { 
			public String userid = null; 
			public void visit(IBob bob) { 
				if(	bob.getTagName().equals("user") && 
					bob.getAttributes().getValue("id").equals(userid)) { 
					
					((Users)bob.getParent()).removeUserById(userid); 
				}
			}
		}
		RemoveUserVisitor ruVisitor = new RemoveUserVisitor(); 
		ruVisitor.userid = userId; 
		this.accept(ruVisitor); 
		
	}
	
	
	public synchronized void addUserToGroup(String groupId, User user) { 
		
		this.getGroups().findGroupById(groupId).addUser(user); 
		
	}
	public synchronized void removeUserFromGroup(String groupid, String userid) { 
		
		// 1. check if group exists
		Group group = (Group)this.getGroups().findGroupById(groupid); 
		if(group == null) { 
			throw new AuthorisationException("There is no group with id '"+groupid+"'"); 
		}
		
		// 2. check if user exists 
		User user = (User)this.getUsers().findUserById(userid); 
		if(user == null) { 
			throw new AuthorisationException("There is no user with id '"+userid+"'");
		}
		
		// 3. check if user is in the group 
		User removedUser = (User)group.removeUserById(user.getId()); 
		if(removedUser == null) { 
			throw new AuthorisationException("The user '"+user.getId()+"' does not exist in the group '"+group.getId()+"'"); 
		}
		
		this.getGroups().findGroupById(groupid).removeUserById(userid); 
		
	}
	
	
	class SavePersistor { 
		
		private String baseDir = null; 
		private String groupDir = "system/aauthentication/groups/"; 
		private String usersDir = "system/aauthentication/users/"; 
		private BookkeepingSystemProperties bsprops = null; 
		private Spittoon spittoon = null; 
		
		public SavePersistor() { 
			bsprops = BookkeepingSystemProperties.instance(); 
		}
		public void setSpittoon(Spittoon spit) { 
			spittoon = spit; 
		}
		public Spittoon getSpittoon() { 
			return spittoon; 
		}
		public void setBaseDir(String bdir) { 
			baseDir = bdir; 
		}
		public String getBaseDir() {
			return baseDir; 
		}
		public void visit(IBob persistable) { 
			
			if(persistable instanceof IGroups && ((IGroups)persistable).getId().equals("aauth.groups")) { 
				
				//** save to system/aauthentication/groups/
				//spittoon.updateDocument(
				//	BookkeepingSystemProperties.instance().getProperty("db.url") + "system/aauthentication/groups/", 
				//		persistable.getTagName(), 
				//			persistable.getAttributes().getValue("id"), 
				//				persistable); 
				
			}
			else if(persistable instanceof IUsers && ((IUsers)persistable).getId().equals("aauth.users")) { 
				
				//** save to system/aauthentication/users/ 
				//spittoon.updateDocument(
				//	BookkeepingSystemProperties.instance().getProperty("db.url") + "system/aauthentication/users/", 
				//		persistable.getTagName(), 
				//			persistable.getAttributes().getValue("id"), 
				//				persistable); 
				
			}
		}
	}
	
	
	public boolean ii = true;
	public void run() { 
		
		//** run until an shutdown interruption 
		//... 
		// 1. how to communicate with running thread -> calling a synchronized method 
		// 2. how to send a shutdown signal -> interrupt(); 
		while(ii) { 
			
			logger.debug("...running"); 
		}
	}
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Logger.getLogger(Aauthentication.class).debug("Starting"); 
		
//		Aauthentication aauth = Aauthentication.instance(); 
//		Thread aauthThread = new Thread(aauth);
//
//		Logger.getLogger(Aauthentication.class).debug("Helloooooooooooooooooooooooooooooooooooooooooo"); 
//		aauthThread.start(); 
		
		/*boolean ii = true;
		while(ii) { 
			//try { aauthThread.join(); } catch(Throwable e) { }
			//aauthThread.interrupt(); 
			//ii = false; 
			for(int j=0; j < 1000; j++) {}
			aauth.ii = false; 
		}*/
//			aauth.ii = false; 
		//Logger.getLogger(Aauthentication.class).debug("Ending"); 
		
	}
	
}

