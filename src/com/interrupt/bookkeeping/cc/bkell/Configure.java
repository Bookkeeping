package com.interrupt.bookkeeping.cc.bkell;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;
import com.interrupt.bob.base.IVisitor;
import com.interrupt.bookkeeping.cc.bkell.aauth.Aauthentication;
import com.interrupt.bookkeeping.exception.SystemException;
import com.interrupt.bookkeeping.users.IUser;
import com.interrupt.spittoon.Spittoon;
import com.interrupt.util.Util;

public class Configure {
	
	
	public void run() { 
		
		Logger logger = Logger.getLogger(Configure.class); 
		logger.debug("Configuring Bookkeeping database..."); 
		
		
		InputStream inStream = ClassLoader.getSystemResourceAsStream("bookkeeping.properties"); 
	    Properties props = new Properties(); 
	    try { 
	    	props.load(inStream); 
	    }
	    catch(IOException e) { 
	    	throw new SystemException(e);
	    }
	    
		/**
		 * 'Bob' setup
		 */
		System.setProperty("bob.home", props.getProperty("bob.home"));
	    System.setProperty("bob.base", props.getProperty("bob.base"));
	    System.setProperty("bob.gen", props.getProperty("bob.gen"));
	    System.setProperty("bob.end", props.getProperty("bob.gen"));
	    System.setProperty("bob.end", props.getProperty("bob.def"));
	    
		
	    /** 
	     * Spittoon initialisation
	     */
		Spittoon spittoon = new Spittoon(); 
		spittoon.initialise(); 
		spittoon.setupRoot(); 
		
		/**
		 * Spittoon DB setup 
		 */
		//String axpath = "/system[ @id='main.system' ]/aauthentication[ @id='main.authentication' ]/users[ @id='aauth.users' ]/user[ @id='root' ]"; 
		String axpath = "/system[ @id='main.system' ]"; 
		String axml = Util.loadTextFile("setup.aauthentication.xml"); 
		String aurl = spittoon.getAauthDbUrl(); 
		logger.debug("Aauth XPath["+ axpath +"]"); 
		logger.debug("Loaded axml["+axml+"]"); 
		
		//String gxpath = "/system[ @id='main.system' ]/groups[ @id='main.groups' ]/bookkeeping[ @id='main.bookkeeping' ]/journals[ @id='main.journals' ]/journal[ @id='generalledger' ]/entries[ @id='main.entries' ]"; 
		String gxpath = "/system[ @id='main.system' ]"; 
		String gxml = Util.loadTextFile("setup.groups.xml"); 
		String gurl = spittoon.getGroupsDbUrl();  
		logger.debug("Groups XPath["+ gxpath +"]"); 
		logger.debug("Loaded gxml["+gxml+"]"); 
		
		//** generating default root password hash  
		String passwd = props.getProperty("defaultRootPasswd"); 
		Aauthentication aauth = new Aauthentication(); 
		String passwdHash = aauth.hashPassword(passwd); 
		logger.debug("Here 1 > defaultRootPasswd["+passwd+"] / passwdHash["+passwdHash+"]"); 
		
		class FindRootUser implements IVisitor { 
			
			private Logger logger = Logger.getLogger(Configure.class); 
			private String phash = null;
			public FindRootUser(String passwdh) { 
				phash = passwdh; 
			}
			public void visit(IBob bob) { 
				
				logger.error("FindRootUser.visit > tag["+bob.getTagName()+"] / id["+bob.getAttributeValue("id")+"]"); 
				if(	bob.getTagName().equals("user") && 
					bob.getAttributeValue("id").equals("root")) { 
					((IUser)bob).setPassword(phash); 
				}
			}
			
		}

		//** setting root passwd 
		FindRootUser fruser = new FindRootUser(passwdHash); 
		
		IBob loadedAauth = com.interrupt.bob.util.Util.loadBobFromXML(axml); 
		logger.debug("Loaded Bob from axml["+loadedAauth.toXML(false)+"]"); 
		
		loadedAauth.accept(fruser); 
		logger.debug("Loaded Bob AFTER FindRootUser / Create Root User ["+loadedAauth.toXML(false)+"]"); 
		
		
		//** creating collections in the XML database 
		logger.debug("Creating database collections for > AAUTH url["+aurl+"] / xpath["+axpath+"] > GROUPS url["+gurl+"] / xpath["+gxpath+"]"); 
		
		logger.debug("Creating aauthentication collections..."); 
		spittoon.createR(aurl, axpath, loadedAauth.toXML(false)); 
		
		logger.debug("Creating groups collections..."); 
		spittoon.createR(gurl, gxpath, gxml); 
		
	}
	
	public static void main(String args[]) { 
		
		Configure configure = new Configure(); 
		configure.run(); 
	}
	
}
