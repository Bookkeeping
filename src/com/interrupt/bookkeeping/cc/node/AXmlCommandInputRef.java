package com.interrupt.bookkeeping.cc.node;

import com.interrupt.bob.base.IBob;

public class AXmlCommandInputRef {
	
	
	private IBob reference = null; 
	private AXmlCommandInput xmlCommandInput = null; 
	
    
	public IBob getReference() {
		return this.reference;
	}
	public void setReference(IBob ref) {
		this.reference = ref;
	}
	
	
	public AXmlCommandInput getXmlCommandInput() {
		return xmlCommandInput;
	}
	public void setXmlCommandInput(AXmlCommandInput xmlCommandInput) {
		this.xmlCommandInput = xmlCommandInput;
	}
    
	public String toXML() { 
	    return this.reference.toXML(); 
	}
	public String toXML(boolean inline) { 
        return this.reference.toXML(inline); 
    }
    
}


