package com.interrupt.bookkeeping.exception;

import java.io.IOException;
import java.lang.Thread.UncaughtExceptionHandler;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import com.interrupt.bookkeeping.cc.bkell.Bkell;

public class BkellUncaughtExceptionHandler implements UncaughtExceptionHandler { 
	
	private Logger logger = Logger.getLogger(Bkell.class); 
	private HttpServletResponse resp = null; 
	
	public BkellUncaughtExceptionHandler() {}
	public void uncaughtException(Thread t, Throwable e) { 
		
		BkellException bke = (BkellException)e; 
		logger.error("[CAUGHT] BkellUncaughtExceptionHandler.uncaughtException ["+ bke +"]"); 
		
		try { 
			this.handleException(e); 
		}
		catch(IOException ee) { 
			ee.printStackTrace(); 
		}
	}
	
	public void handleException(Throwable e) throws IOException {}
	
	public HttpServletResponse getResp() { return resp; }
	public void setResp(HttpServletResponse resp) { this.resp = resp; } 
	
}
