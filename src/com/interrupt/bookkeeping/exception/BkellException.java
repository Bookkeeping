package com.interrupt.bookkeeping.exception;

import com.interrupt.logs.ILogs;

public class BkellException extends RuntimeException {
	
	
	private ILogs logMessages = null; 
	
    public BkellException() { 
    	super(); 
    }
    public BkellException(String message) { 
    	super(message); 
    }
    public BkellException(String message, Throwable cause) {
    	super(message,cause); 
    }
    public BkellException(Throwable cause) {
    	super(cause); 
    }
	
    
    public ILogs getLogMessages() {
		return logMessages;
	}
	public void setLogMessages(ILogs logMessages) {
		this.logMessages = logMessages;
	}
    
    
    
}
