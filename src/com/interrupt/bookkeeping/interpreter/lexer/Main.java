
package com.interrupt.bookkeeping.interpreter.lexer; 


import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.interrupt.bob.base.BobSystem;

import com.interrupt.bob.util.Util;
import com.interrupt.bookkeeping.cc.bkell.Bkell;


public class Main {
	
	private Logger logger = Logger.getLogger(Main.class); 
	
	/*
		Problem 1: This synax is not recursive. Thus one cannot put commands into commands 
		Problem 2: Extracting raw XML from command line is very difficult to
		
		-entry [A-Z-az0-9]*
		-account [A-Za-z0-9]*
		-journal [A-Za-z0-9]*
		-name [A-Za-z0-9]*
		-type [A-Za-z0-9]*
		-counterWeight [[a-z]{5,6}]*
		-amount [[0-9]+\.[0-9]+]*
		-id [A-Z-az0-9]*
		-entrynum [0-9]*
		-date [[0-9]{1,2}/[0-9]{1,2}/[0-9]{4}]*
		
		(debit[,]?)*
		(credit[,]?)*
		(entry[,]?)*
		
		create      (token -opts(token.literal,...))
		add         ((token) token.literal,...)
		remove      ((token) token.literal,...)
		reverse     ((token) token.literal,...)
		find        (token -opts)
		load        (token -opts)
		list        (token -opts)
		login
		logout
		exit 
		
		
		** After reading user input, the Lexer must pass the below parameters to the Parser.
		Each command will be processed iteratively, starting from each leaf node to the root command: 
		i) commad ii) token iii) options iv) token.literals 
		
	 */
	
	public Map symbolMap = new HashMap(); 
	public Map commandMap = new HashMap();
	
	
	public static void main(String args[]) { 
		
		Logger.getLogger(Main.class).debug( "Main Input: "+ args[0] ); 
		
		String opt_entry = "-entry [A-Za-z0-9]*";				// opt.entry
		String opt_account = "-account [A-Za-z0-9]*";			// opt.account
		String opt_journal = "-journal [A-Za-z0-9]*";			// opt.journal
		String opt_name = "-name [A-Za-z0-9]*";					// opt.name 
		String opt_type = "-type [A-Za-z0-9]*";					// opt.type
		String opt_cweight = "-counterWeight [[a-z]{5,6}]*";	// opt.cweight
		String opt_amount = "-amount ([0-9]+\\.[0-9]+)*";		// opt.amount 
		String opt_id = "-id [A-Za-z0-9]*";						// opt.id
		String opt_entrynum = "-entrynum [0-9]*";				// opt.entrynum
		String opt_date = "-date [[0-9]{1,2}/[0-9]{1,2}/[0-9]{4}]*";	// opt.date
		String opt_file = "-F [A-Za-z0-9]*"; 					// opt.file
		
		String tok_debit_in = "(debit[,]?)*";		// token.in.debit
		String tok_credit_in = "(credit[,]?)*";		// token.in.credit
		String tok_entry_in = "(entry[,]?)*";		// token.in.entry
		String tok_debit = "debit";					// token.debit
		String tok_credit = "credit";				// token.credit
		String tok_entry = "entry";					// token.entry
		String tok_transaction = "transaction";					// token.transaction
		String tok_journal = "journal";							// token.journal
		String tok_account = "account";							// token.account 
		
		// TODO
		// we also have to represent XML token literals 
		
		
		Main main = new Main(); 
		main.symbolMap.put("opt.entry", opt_entry); 
		main.symbolMap.put("opt.account", opt_account); 
		main.symbolMap.put("opt.journal", opt_journal); 
		main.symbolMap.put("opt.name", opt_name); 
		main.symbolMap.put("opt.type", opt_type); 
		main.symbolMap.put("opt.cweight", opt_cweight); 
		main.symbolMap.put("opt.amount", opt_amount); 
		main.symbolMap.put("opt.id", opt_id); 
		main.symbolMap.put("opt.entrynum", opt_entrynum); 
		main.symbolMap.put("opt.date", opt_date); 
		main.symbolMap.put("opt.file", opt_file); 
		
		main.symbolMap.put("token.debit.in", tok_debit_in); 
		main.symbolMap.put("token.credit.in", tok_credit_in); 
		main.symbolMap.put("token.entry.in", tok_entry_in); 
		main.symbolMap.put("token.debit", tok_debit); 
		main.symbolMap.put("token.credit", tok_credit); 
		main.symbolMap.put("token.entry", tok_entry); 
		main.symbolMap.put("token.transaction", tok_transaction); 
		main.symbolMap.put("token.journal", tok_journal); 
		main.symbolMap.put("token.account", tok_account); 
		
		
		
		/* CREATE Commands 
		 */
		String cmd_create_debit = "create \\((${token.debit}){1} " + 
			"(((${opt.entry} ${opt.account} ${opt.amount}){1}|" + 
			"(${opt.id}){1})\\){1}"; 
		
		String cmd_create_credit = "create \\((${token.credit}){1} " + 
			"(((${opt.entry} ${opt.account} ${opt.amount}){1}|" + 
			"(${opt.id}){1})\\){1}"; 
		
		String cmd_create_entry = "create \\((${token.entry}){1} " + 
			"((${opt.entrynum} ${opt.journal} ${opt.date}){1}|" + 
			"(${opt.id}){1}){1} " + 
			"\\(${token.debit.in}|${token.credit.in}\\)\\)"; 
		
		String cmd_create_transaction = "create \\((${token.transaction}){1} " + 
			"((${opt.name}){1}|" + 
			"(${opt.id}){1}){1} " + 
			"\\(${token.entry.in}\\)\\)"; 
		
		String cmd_create_journal = "create \\((${token.journal})* " + 
			"((${opt.name}{1}|" + 
			"(${opt.id}){1}){1} " + 
			"\\(${token}[,]?)*\\)\\)"; 
		
		/*String cmd_create_account = "create \\((${token.account})* " + 
			"((${opt.name}{1}|" + 
			"((${opt.type}{1}|" + 
			"(${opt.cweight}){1}){1} " + 
			"\\(${token}[,]?)*\\)\\)"; 
		*/
		
		String cmd_create_account = "create \\(${token.account} "+ 
			"((${opt.name}){1} (${opt.type}){1} (${opt.cweight}){1} "+ 
			"\\((${token}[,]?)*\\)\\)"; 
		
		
		String cmd_add = "add \\(\\(\\$\\{token\\})*\\) \\$\\{token\\}[,]?)*\\)"; 
		String cmd_remove = "remove \\(\\(\\$\\{token\\})*\\) \\$\\{token\\}[,]?)*\\)"; 
		String cmd_reverse = "reverse \\(\\(\\$\\{token\\})*) \\$\\{token\\}[,]?)*\\)"; 
		String cmd_find = "find \\(\\$\\{token\\})* \\$\\{opts\\})*\\)"; 
		String cmd_load = "load \\(\\$\\{token\\})* \\$\\{opts\\})*\\)"; 
		String cmd_list = "list \\(\\$\\{token\\})* \\$\\{opts\\})*\\)"; 
		String cmd_login = "login"; 
		String cmd_logout = "logout"; 
		String cmd_exit = "exit"; 
		
		main.commandMap.put("cmd.create.debit", cmd_create_debit); 
		main.commandMap.put("cmd.create.credit", cmd_create_credit); 
		main.commandMap.put("cmd.create.entry", cmd_create_entry); 
		main.commandMap.put("cmd.create.transaction", cmd_create_transaction); 
		//main.commandMap.put("cmd.create.journal", cmd_create_journal); 
		//main.commandMap.put("cmd.create.account", cmd_create_account); 
		
		
		String substitution = "\\$\\{[A-Z-a-z0-9\\.]+\\}";
		
		main.testCreateDebit(args[0]);
		
	}
	
	public void testCreateDebit(String input) { 
		
		
		/*String regex = "create \\((${token.debit}){1} " + 
			"((${opt.entry} ${opt.account} ${opt.amount}){1}|" + 
			"(${opt.id}){1})\\){1}"; 
		*/
		String regex = null; 
		Set keyset = this.commandMap.keySet(); 
		Iterator keyiterator = keyset.iterator(); 
		while(keyiterator.hasNext()) { 
		
			String rkey = (String)keyiterator.next(); 
			regex = (String)this.commandMap.get(rkey);
			logger.debug("");
			logger.debug("");
			logger.debug("NEXT Key["+rkey+"] Pattern["+regex+"]"); 
			
			
			String regex_sub = "\\$\\{[A-Z-a-z0-9\\.]+\\}"; 
			//boolean matches_sub = Pattern.matches( regex_sub, regex ); 
			boolean hasMatch = false; 
			Pattern pattern = Pattern.compile(regex_sub); 
			Matcher matcher = pattern.matcher(regex); 
			
			// find token & option substitutions 
			//logger.debug( "Substitution Matches / count["+matcher.groupCount()+"]: "+ matches_sub ); 
			int lastIndex = 0; 
			StringBuffer newRegex = new StringBuffer(); 
			int new_start_index = -1; 
			int new_end_index = -1; 
			while(matcher.find()) { 
				
				hasMatch = true; 
				
				//logger.debug(""); 
				//logger.debug("Next Match / index["+matcher.start()+"]: "+ matcher.group()); 
				
				String interpretable = regex.substring(matcher.start(), matcher.end()); 
				String interpreted = Util.getInterpretableName(interpretable); 
				String interpretedPattern = (String)symbolMap.get(interpreted); 
				//logger.debug("Interpreted["+interpreted+"] / InterpretedPattern["+interpretedPattern+"]"); 
				
				// match patterns on token & substitutions 
				Pattern pattern_in = null;
				Matcher matcher_in = null; 
				if(interpretedPattern != null) { 
					
					pattern_in = Pattern.compile(interpretedPattern); 
					matcher_in = pattern_in.matcher(input); 
					
					if(matcher_in.find()) { 
						
						
						logger.debug(">>> INPUT["+interpretedPattern+"] / ["+interpretable+"] > Next Match["+matcher_in.group()+"]"); 
						//logger.debug("Hmmm ["+regex.substring(matcher.start(), matcher.end())+"] / "+ !")*".equals(regex.substring(matcher.end(), matcher.end() + 2)));
						if( !")*".equals(interpretedPattern.substring(interpretedPattern.length() - 2, interpretedPattern.length()) )){ 
							
							// start index 
							if( '(' == regex.charAt(matcher.start() - 1) ) { 
								new_start_index = matcher.start() - 1; 
							}
							else { 
								new_start_index = matcher.start();
							}
							
							// end index 
							if( "){1}".equals(regex.substring(matcher.end(), matcher.end() + 4))) { 
								new_end_index = matcher.end() + 4;
							}
							else { 
								new_end_index = matcher.end(); 
							}
							
						}
						else { 
							
							new_start_index = matcher.start(); 
							new_end_index = matcher.end(); 
						}
						
						newRegex.append(regex.substring(lastIndex, new_start_index)); 
						newRegex.append(interpretedPattern); 
						lastIndex = new_end_index; 
						//logger.debug(">>> REPLACING in regex["+ newRegex.toString() +"]"); 
						
					}
					else { 
						
						
						logger.debug(">>> INPUT ["+interpretedPattern+"] / ["+interpretable+"] > NO MATCH "); 
						//logger.debug("Hmmm ["+regex.substring(matcher.start(), matcher.end())+"] / "+ !")*".equals(regex.substring(matcher.end(), matcher.end() + 2)));
						if( !")*".equals(interpretedPattern.substring(interpretedPattern.length() - 2, interpretedPattern.length()) )){ 
							
							// start index 
							if( '(' == regex.charAt(matcher.start() - 1) ) { 
								new_start_index = matcher.start() - 1; 
							}
							else { 
								new_start_index = matcher.start();
							}
							
							// end index 
							if( 
								"){1}".equals(regex.substring(matcher.end(), matcher.end() + 4)) 
								) { 
								new_end_index = matcher.end() + 4;
							}
							else { 
								new_end_index = matcher.end(); 
							}
							
						}
						else { 
							
							new_start_index = matcher.start(); 
							new_end_index = matcher.end(); 
						}
						
						newRegex.append(regex.substring(lastIndex, new_start_index)); 
						newRegex.append("__no_pattern__"); 
						lastIndex = new_end_index; 
						//logger.debug(">>> REPLACING in regex["+ newRegex.toString() +"]"); 
						
					}
					
				}
			
			}
			
			if(hasMatch) { 
				
				newRegex.append(regex.substring(new_end_index));
				String regexFinal = newRegex.toString(); 
				logger.debug("	>>> FINAL regex["+ regexFinal +"]"); 
				logger.debug("	>>> FINAL input["+input+"]"); 
				
				Pattern patternFinal = Pattern.compile(regexFinal); 
				Matcher matcherFinal = patternFinal.matcher(input); 
				logger.debug("	>>> FINAL Matches["+ matcherFinal.matches()+"]"); 
			}
			
			
			// TODO 
			// now collect i) token ii) options iii) token.literals 
			
			
		}
		
	}
	
	
}

