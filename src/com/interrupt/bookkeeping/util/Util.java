
package com.interrupt.bookkeeping.util;

import org.apache.log4j.Logger;

import com.interrupt.bob.base.IBob;
import com.interrupt.bob.base.IVisitor;
import com.interrupt.bookkeeping.cc.bkell.Bkell;
import com.interrupt.bookkeeping.exception.BkellException;
import com.interrupt.logs.GLog;
import com.interrupt.logs.GLogMessage;
import com.interrupt.logs.GLogMessages;
import com.interrupt.logs.GLogs;
import com.interrupt.logs.ILog;
import com.interrupt.logs.ILogMessage;
import com.interrupt.logs.ILogMessages;
import com.interrupt.logs.ILogs;
import com.interrupt.util.IdGenerator;

public class Util {
	
	
    public static String OPEN_STATE = "open";
    public static String CLOSED_STATE = "closed";
    public static String REVERSED_STATE = "reversed";
    public static String REVERSE_CLOSED_STATE = "reverse_closed";
    public static String REMOVED_STATE = "removed";
    
    private static Logger logger = Logger.getLogger(Util.class); 
	
    
    /** filter XML to get rid of spaces 
	 */
	public static String filterSpacesFromXML(String cinput_s) { 
    	
    	String input_1 = cinput_s.replaceAll("< ", "<");  
    	
    	String input_11 = input_1.replaceAll(" : ", ":");  
    	String input_2 = input_11.replaceAll("\\/ >", " />"); 
		String input_x = input_2.replaceAll("< \\/", "</"); 
		String input_y = input_x.replaceAll("<\\/ ", "</"); 
		String input_xx = input_y.replaceAll(" \\/ ", "/"); 
		String input_3 = input_xx.replaceAll(" = ", "="); 
		
		//String input_4 = input_3.replaceAll("\" ", "\""); 
		//String input_5 = input_4.replaceAll(" \"", "\""); 
		//String input_6 = input_5.replaceAll("\' ", "\'"); 
		//String input_7 = input_6.replaceAll(" \'", "\'"); 
		
		return input_3; 
    }
    
	public static IBob populateEmptyIDs(String inputXML) { 
		
		IBob wrapper = com.interrupt.bob.util.Util.loadBobFromXML(inputXML); 
        return Util.populateEmptyIDs(wrapper); 
	}
	public static IBob populateEmptyIDs(IBob input) { 
			
		/**
		 * make sure each bob has an ID 
		 */
        class IdPopulator implements IVisitor { 
        	private Logger logger = null;  
        	public IdPopulator() { 
        		logger = Logger.getLogger(com.interrupt.bookkeeping.util.Util.class); 
        		logger.debug("IdPopulator > Populating IDs"); 
        	} 
        	public void visit(IBob bob) { 
        		if(bob.getAttributeValue("id") == null || bob.getAttributeValue("id").trim().length() < 1) { 
        			bob.setProperty(IBob.XML_DEPTH, "1"); //** just print out the parent document 
        			logger.debug("found empty id for["+ bob.getTagName() +"]"); 
        			bob.setAttributeValue("id", IdGenerator.generateId()); 
        			logger.debug("tag AFTER ID population["+ bob.toXML(false) +"]"); 
        		}
        	}
        }
        input.acceptFirst(new IdPopulator()); 
        
        return input; 
	}
	
	public static Throwable getRootCause(Throwable e) { 
		
		Throwable cause = e.getCause(); 
		if( cause != null) { 
			
			logger.error("next cause message["+ cause.getMessage() +"]"); 
			return cause; 
		}
		return e; 
	}
	public static BkellException generateBkellException(Throwable e) { 
		
		
		logger.error("generateBkellException CALLED"); 
		
		Throwable roote = Util.getRootCause(e); 
		String topMessage_S = e.getMessage(); 
		String rootMessage_S = roote.getMessage(); 
		
		logger.error("TOP error message["+ topMessage_S +"]"); 
		logger.error("ROOT error message["+ rootMessage_S +"]"); 
		
		ILogMessages messages = new GLogMessages(); 
		if( topMessage_S != null ) { 
			ILogMessage tmessage = new GLogMessage(); 
			tmessage.setContent(topMessage_S); 
			
			messages.addLogMessage(tmessage); 
		}
		ILogMessage rmessage = null; 
		if(rootMessage_S != null) { 
			
			if(topMessage_S != null) { 
					
				if(topMessage_S != rootMessage_S) { 
					
					rmessage = new GLogMessage(); 
					rmessage.setContent(rootMessage_S); 
					messages.addLogMessage(rmessage); 
				}
			}
			else if(rootMessage_S != null) { 

				rmessage = new GLogMessage(); 
				rmessage.setContent(rootMessage_S); 
				messages.addLogMessage(rmessage);
			}
		}
		
		ILog log = new GLog(); 
		log.setLevel("ERROR"); 
		log.addLogMessages(messages); 
		
		ILogs logs = new GLogs(); 
		logs.addLog(log); 
		
		BkellException bke = new BkellException(e.getMessage(),e); 
		bke.setLogMessages(logs); 
		
		return bke; 
	}
	
	public static void main(String args[]) { 
	}
    
}

