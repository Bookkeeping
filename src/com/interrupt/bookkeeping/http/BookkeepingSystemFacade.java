package com.interrupt.bookkeeping.http;


import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException; 
import java.io.InputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintWriter;
import java.io.StringBufferInputStream;
import java.io.StringReader;

import javax.servlet.ServletException; 
import javax.servlet.ServletRequest;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest; 
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse; 
import javax.servlet.http.HttpSession;

import org.apache.commons.httpclient.util.URIUtil;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue; 
import java.util.concurrent.BlockingQueue; 
import java.util.concurrent.LinkedBlockingQueue;

import com.interrupt.bob.base.Bob; 
import com.interrupt.bob.base.IBob; 
import com.interrupt.bookkeeping.ISystem; 

import com.interrupt.bookkeeping.account.IAccount; 
import com.interrupt.bookkeeping.account.IAccounts; 

import com.interrupt.bookkeeping.journal.IJournal; 
import com.interrupt.bookkeeping.journal.IJournals; 
import com.interrupt.bookkeeping.journal.IEntry; 
import com.interrupt.bookkeeping.journal.IEntries; 

import com.interrupt.bookkeeping.cc.bkell.Bkell;
import com.interrupt.bookkeeping.cc.bkell.command.AddCommand; 
import com.interrupt.bookkeeping.cc.bkell.command.GOption; 
import com.interrupt.bookkeeping.cc.bkell.command.GOptions; 
import com.interrupt.bookkeeping.cc.bkell.command.GResult; 
import com.interrupt.bookkeeping.cc.bkell.command.GToken; 
import com.interrupt.bookkeeping.cc.bkell.command.ICommand; 
import com.interrupt.bookkeeping.cc.bkell.command.IResult; 
import com.interrupt.bookkeeping.cc.bkell.command.ITokens; 
import com.interrupt.bookkeeping.cc.bkell.command.ListCommand; 
import com.interrupt.bookkeeping.cc.node.PInputOption;
import com.interrupt.bookkeeping.exception.BkellException;
import com.interrupt.bookkeeping.exception.BkellUncaughtExceptionHandler;
import com.interrupt.bookkeeping.http.ServletInputParams; 
import com.interrupt.bookkeeping.system.BookkeepingSystem;  
import com.interrupt.bookkeeping.system.BookkeepingSystemProperties;
import com.interrupt.logs.ILogs;
import com.interrupt.util.IdGenerator; 

public class BookkeepingSystemFacade { 
	
	
    public static final String FACADE_NAME = "system.facade"; 
	private Logger logger = Logger.getLogger(BookkeepingSystemFacade.class); 
	private BookkeepingSystem bsystem = null; 
    private static BookkeepingSystemFacade instance = null; 
	
	private Thread bkellThread = null; 
	private Bkell bkell = null; 
	
	private InputStream inputStream = null;  
	private PipedOutputStream poutputStream = null; 
	
	
	public BookkeepingSystemFacade() { 
		
		//** initialise the Bookkeeping system including Spittoon 
		bsystem = BookkeepingSystem.instance(); 
		
		poutputStream = new PipedOutputStream(); 
		inputStream = new PipedInputStream(); 
		
	}
	
	public Bkell getBkell() {
		return bkell;
	}
	public void setBkell(Bkell bkell) {
		this.bkell = bkell;
	}
	
	
	/*public static BookkeepingSystemFacade instance() { 
		
		if(instance == null) { 
			
			instance = new BookkeepingSystemFacade(); 
			instance.initialise(); 
		}
		return instance; 
	}
	*/
	
	
	public void initialise() { 
		
		logger.debug("BookkeepingSystemFacade initialise CALLED"); 
		
		System.getProperties().setProperty(com.interrupt.bob.util.Util.DEF, BookkeepingSystemProperties.instance().getProperty("bob.def")); 
		System.getProperties().setProperty(com.interrupt.bob.util.Util.BASE, "."); 
		System.getProperties().setProperty(com.interrupt.bob.util.Util.END, ".xml"); 
		
		ISystem bkellSystem = 
			(ISystem)Bob.loadS( 
				Bkell.class.getResourceAsStream("/bookkeeping.system.xml"), 
					BookkeepingSystemProperties.instance().getProperty("bob.def") ); 
		
		
		//try { 
			
			//** loading bkell state 
			bkell = (Bkell)bkellSystem.findBkellById("bkell.main"); 
			bkell.initialise(); 
			bkell.setInputStream(inputStream); 
	    	try { bkell.connectPipe(poutputStream); } 
	    	catch(IOException e) { 
	    		logger.error("IOException["+ e.getMessage() +"]... continuing"); 
	    	}
	    	
			//** 
			/* BkellUncaughtExceptionHandler ehandler = new BkellUncaughtExceptionHandler() {
				public void handleException(Throwable e) throws IOException {
					
					logs = ((BkellException)e).getLogMessages(); 
					
					this.getLogger().debug("DONE BookkeepingSystemFacade > [Exception] BkellUncaughtExceptionHandler"); 
		    		this.getLogger().debug(logs.toXML(false)); 
		    		
		    		this.setLogs(logs); 
		    		
		    		//** force system to garbage collect 
		    		//System.gc(); 
		    		
				}
			}; */
			
			
			bkellThread = new Thread(bkell);
			//bkellThread.setDefaultUncaughtExceptionHandler(ehandler); 
			bkellThread.setDaemon(true); 
			bkellThread.start(); 
			
			
			logger.info(">>> Printing ISystem"); 
			logger.info(bkellSystem.toXML()); 
			
			logger.info(">>> Printing IBkell"); 
            logger.info(bkell.toXML()); 
			
            logger.debug("END BookkeepingSystemFacade initialise"); 
    		
		//}
		//catch(IOException e) { 
		//	e.printStackTrace();
		//}
		
	}
	
	public void shutdown() throws IOException, InterruptedException { 
	    
		logger.debug("BookkeepingSystemFacade shutdown CALLED"); 
		//try { 
			
			
			logger.debug("1"); 
			poutputStream.write("exit;".getBytes()); 
			
			synchronized(bkellThread) { 
				bkellThread.wait(); 
			}
			
			
			//** close output stream 
			logger.debug("2"); 
			poutputStream.close(); 
			bkell.getInputStream().close(); 
			
			
			logger.debug("3"); 
			bkell.shutdown(); 
	        
			
			logger.debug("bkellThread.isAlive["+ bkellThread.isAlive() +"]"); 
			
		//}
		//catch(Exception e) { 
		//	logger.error("Exception during shutdown ["+ e.getMessage() +"]", e); 
		//}
		
        logger.debug("BookkeepingSystemFacade shutdown END"); 
	}
	
	
	/** 
	 * 
	 * Some logging information 
	 */
	private synchronized void preamble(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException, InterruptedException {
		
		logger.debug(""); 
    	logger.debug(">> session > " + req.getSession()); 
    	logger.debug(">> timout > " + req.getSession().getMaxInactiveInterval()); 
    	logger.debug(" >> Session Validity > sessionFromCookie: "+ req.isRequestedSessionIdFromCookie() + " / " + 
    	    "sessionIdValue: "+ req.getRequestedSessionId() + " / " + 
    	    "sessionFromURL: "+ req.isRequestedSessionIdFromURL() + " / " + 
    	    "sessionIdValid: "+ req.isRequestedSessionIdValid()); 
    	
	}
	public synchronized void performAsRoot(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException, InterruptedException {
	    
		
		logger.debug("performAsRoot CALLED"); 
		this.preamble(req, resp); 
		
		
		//** LOGIN as root 
		String loginString = "login(user -username root -password password);";     	
       	try { poutputStream.write(loginString.getBytes()); } 
    	catch(IOException e) { 
    		logger.error("IOException["+ e.getMessage() +"]... reconnecting pipe", e); 
    	}
    	
    	synchronized(bkell) { bkell.wait(); //** wait for a notification from this thread. 
    	}
    	
    	
		//** CALL out to Bkell
    	this.__core(req, resp); 
		

		//** LOGOUT from root 
		String logoutString = "logout;";     	
		
		try { poutputStream.write(logoutString.getBytes()); } 
    	catch(IOException e) { 
    		logger.error("IOException["+ e.getMessage() +"]... reconnecting pipe", e); 
    	}
    	
    	synchronized(bkell) { bkell.wait(); //** wait for a notification from this thread. 
    	}
    	
    	
    	//** return 
    	String result = null; 
		try { 
			result = bkell.getPreviousCommandResult().toXML(false); 
		}
		catch(NullPointerException e) { 
			
			logger.error("NullPointerException while referencing 'bkell.getPreviousCommandResult()' > ERROR while processing"); 
			resp.setContentType("text/xml"); 
    		
			//ILogs logs = ((BkellUncaughtExceptionHandler)this.bkellThread.getDefaultUncaughtExceptionHandler()).getLogs(); 
			ILogs logs = this.bkell.getBkellException().getLogMessages(); 
			logger.debug("DONE BookkeepingSystemFacade > [Exception] BkellUncaughtExceptionHandler"); 
    		logger.debug(logs.toXML(false)); 
    		
    		PrintWriter pwriter = resp.getWriter(); 
    		pwriter.println(logs.toXML(false)); 
    		pwriter.flush(); 
    		
			return;		//** there must be an error if there's an empty previous command 
		}
		resp.setContentType("text/xml"); 
		
		logger.debug("DONE BookkeepingSystemFacade"); 
		logger.debug(result); 
		
		PrintWriter pwriter = new PrintWriter(resp.getOutputStream()); 
		pwriter.println(result); 
		pwriter.flush();
		
		//** force system to garbage collect 
		System.gc(); 
		
	}

    public synchronized void perform(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException, InterruptedException {
        
    	
    	logger.debug("perform CALLED"); 
    	this.preamble(req, resp); 
    	
    	
    	//** make call to Bkell 
    	this.__core(req, resp); 
    	
    	
    	//** return 
		String result = null; 
		try { 
			result = bkell.getPreviousCommandResult().toXML(false); 
		}
		catch(NullPointerException e) { 
			
			// ... wait 1/2 second for 'BkellUncaughtExceptionHandler' to complete execution !! 
			//this.wait(500); 
			
			logger.error("NullPointerException while referencing 'bkell.getPreviousCommandResult()' > ERROR while processing"); 
			resp.setContentType("text/xml"); 
    		
			//ILogs logs = ((BkellUncaughtExceptionHandler)this.bkellThread.getDefaultUncaughtExceptionHandler()).getLogs(); 
			ILogs logs = this.bkell.getBkellException().getLogMessages(); 
			logger.debug("DONE BookkeepingSystemFacade > [Exception] BkellUncaughtExceptionHandler"); 
    		logger.debug(logs.toXML(false)); 
    		
    		PrintWriter pwriter = resp.getWriter(); 
    		pwriter.println(logs.toXML(false)); 
    		pwriter.flush(); 
    		
			return;		//** there must be an error if there's an empty previous command 
		}
		resp.setContentType("text/xml"); 
		
		logger.debug("DONE BookkeepingSystemFacade"); 
		logger.debug(result); 
		
		PrintWriter pwriter = new PrintWriter(resp.getOutputStream()); 
		pwriter.println(result); 
		pwriter.flush();
		
		//** force system to garbage collect 
		System.gc(); 
		
    }
    
    
    /***
     * 
     * The actual call to the bookkeeping shell 
     */
    public synchronized void __core(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException, InterruptedException {
    	
    	
    	logger.debug("__core CALLED"); 
    	
    	// ** A session id should correspond to a user/group. This is better than 
    	// explicitly specifying the user / group in the http request because 
    	// a session id indicates that this person has been authenticated. 
    	
    	// ** If the session id is null and the bkexpr is to 'register' or 'login', 
    	// then that is allowed 
    	String bkexpr = req.getParameter("bkexpr"); 
    	
    	logger.debug(""); 
        logger.debug(">> [encoded] bkexpr > " + bkexpr); 
    	
        bkexpr = URIUtil.decode(bkexpr); 
    	
        logger.debug(">> [DECODED] bkexpr > " + bkexpr); 
    	
    	try { 
    		poutputStream.write(bkexpr.getBytes()); 
    	} 
    	catch(IOException e) { 
    		logger.error("IOException["+ e.getMessage() +"]... reconnecting pipe", e); 
    	}
    	
    	synchronized(bkell) { bkell.wait(); //** wait for a notification from this thread. 
    	}
    	
    }
    
}


