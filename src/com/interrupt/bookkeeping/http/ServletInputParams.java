package com.interrupt.bookkeeping.http;

import java.io.PrintWriter; 
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * a simple typed bag to hold http servlet request and response parameters; 
 * i suppose enums might be a better choice for this, but I haven't gone 
 * into Java 5 syntax 
 */
public class ServletInputParams {
	
	public HttpServletRequest req;
	public HttpServletResponse res; 
	public PrintWriter pwriter; 
	
}


