package com.interrupt.bookkeeping.http;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.interrupt.bookkeeping.cc.ExpressionVisitor;
import com.interrupt.bookkeeping.cc.bkell.Bkell;
import com.interrupt.bookkeeping.cc.bkell.aauth.Aauthentication;
import com.interrupt.bookkeeping.system.BookkeepingSystemProperties;

public class EchoServlet extends HttpServlet {
    
    private Logger logger = Logger.getLogger(EchoServlet.class); 
    
    public void init() throws ServletException {
    }
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException { 
        this.process(req, resp);
    }
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException { 
        this.process(req, resp); 
    }
    
    public void process(HttpServletRequest req, HttpServletResponse resp) throws ServletException { 
        
        
        com.interrupt.bookkeeping.ISystem system = (com.interrupt.bookkeeping.ISystem)req.getSession().getAttribute("system"); 
        PrintWriter pwriter = null; 
        try { 
            
            if(system != null) { 
                System.out.println( "EchoServlet system: " + system.toXML(false) ); 
            }
            else { 
                System.out.println( "EchoServlet system: " + null );
            }
            
            
            pwriter = new PrintWriter(resp.getOutputStream()); 
            if(system == null) { 
                
                pwriter.println("system NULL"); 
                system = (com.interrupt.bookkeeping.ISystem)com.interrupt.bob.base.Bob.loadS( 
                            Bkell.class.getResourceAsStream("/bookkeeping.system.xml"), 
                                BookkeepingSystemProperties.instance().getProperty("bob.def") ); 
            }
            else { 
                pwriter.println(system.toXML(false)); 
            }
            req.getSession().setAttribute("system", system); 
            
            pwriter.close(); 
            
        }
        catch(IOException e) { 
            e.printStackTrace(); 
        }
        
    }
    
}
