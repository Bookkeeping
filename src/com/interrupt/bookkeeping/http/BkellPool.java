package com.interrupt.bookkeeping.http;

import java.util.HashMap;

import com.interrupt.bookkeeping.cc.bkell.Bkell;

/**
 * HttpSession kills the bkell thread instances in the BookkeepingSystemFacade. Let's see if this does the trick 
 * @author twashing
 *
 */
public class BkellPool {
    
    
    private HashMap pool = null; 
    private static BkellPool instance = null; 
    
    private BkellPool() { 
        
        pool = new HashMap(); 
    }
    
    public static BkellPool instance() { 
        
        if(instance == null) { 
            instance = new BkellPool();
        }
        return instance; 
    }
    
    public synchronized void put(String name, Thread bkell) { 
        
        synchronized(pool) { 
            pool.put(name, bkell); 
        }
    }
    
    public synchronized Thread get(String name) { 
        
        synchronized(pool) { 
            return (Thread)pool.get(name); 
        }
    }
    
    
}

