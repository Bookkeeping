package com.interrupt.bookkeeping.http;

import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;

import org.apache.log4j.Logger;

import com.interrupt.bookkeeping.cc.bkell.command.LoginCommand;

public class BookkeepingAttributeListener implements HttpSessionAttributeListener {
    
    
    private Logger logger = Logger.getLogger(BookkeepingAttributeListener.class); 
    
    public void attributeAdded(HttpSessionBindingEvent bevent) { 
        
        logger.debug("attributeAdded CALLED: name["+bevent.getName()+"] / source["+bevent.getSource()+"] / toString["+bevent.toString()+"]"); 
    }
    public void attributeRemoved(HttpSessionBindingEvent bevent) { 
        
        logger.debug("attributeRemoved CALLED: name["+bevent.getName()+"] / source["+bevent.getSource()+"] / toString["+bevent.toString()+"]");
    }
    public void attributeReplaced(HttpSessionBindingEvent bevent) { 
        
        logger.debug("attributeReplaced CALLED: name["+bevent.getName()+"] / source["+bevent.getSource()+"] / toString["+bevent.toString()+"]");
    }
    
}
