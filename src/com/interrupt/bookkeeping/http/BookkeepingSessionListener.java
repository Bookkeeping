package com.interrupt.bookkeeping.http;

import java.io.IOException;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.log4j.Logger;

import com.interrupt.bookkeeping.exception.SystemException;
import com.interrupt.bookkeeping.system.BookkeepingSystemProperties;

public class BookkeepingSessionListener implements HttpSessionListener {
    
    
	
    private Logger logger = Logger.getLogger(BookkeepingSystemFacade.class); 
    private BookkeepingSystemFacade bookkeepingSystemFacade = null; 
    
    public void sessionCreated(HttpSessionEvent sevent) {
        
    	logger.debug("SESSION CREATED [BookkeepingSessionListener.sessionCreated]"); 
    	System.out.println("SESSION CREATED [BookkeepingSessionListener.sessionCreated]"); 
    	
    	/*System.setProperty( "java.class.path", 
        	System.getProperty("java.class.path") + 
        		":/Users/timothyw/Tools/apache-tomcat-5.5.27/webapps/webkell/WEB-INF/lib/" 
        );
    	logger.error( "java.class.path: "+ System.getProperty("java.class.path") ); 
    	//System.getProperties().list(System.out); 
    	java.io.InputStream is = this.getClass().getResourceAsStream("bookkeeping.properties"); 
    	logger.error("try 1: "+ is); 
    	
    	Class created = null; 
    	try { created = Class.forName("com.interrupt.bookkeeping.http.BookkeepingSessionListener"); }
    	catch(ClassNotFoundException e) { e.printStackTrace(); }
    	logger.error("try 2: created class: "+ created); 
    	
    	String bktext = com.interrupt.util.Util.loadTextFile("bookkeeping.system.xml"); 
    	logger.error( "try 3: bktext: "+ bktext ); 
    	*/
    	
    	
        //** initialising the sytem facade - let the servlet handle this. we're duplicating exist (spittoon) DB instances
        bookkeepingSystemFacade = new BookkeepingSystemFacade(); 
        bookkeepingSystemFacade.initialise(); 
        
        sevent.getSession().setAttribute(BookkeepingSystemFacade.FACADE_NAME, bookkeepingSystemFacade); 
        
        logger.debug(">>> BookkeepingSystemFacade created in SESSION > "+ sevent.getSession().getAttribute(BookkeepingSystemFacade.FACADE_NAME)); 
        System.out.println(">>> BookkeepingSystemFacade created in SESSION > "+ sevent.getSession().getAttribute(BookkeepingSystemFacade.FACADE_NAME)); 
        
    }
    
    public void sessionDestroyed(HttpSessionEvent sevent) {
        
    	logger.debug("SESSION DESTROYED [BookkeepingSessionListener.sessionDestroyed]"); 
    	System.out.println("SESSION DESTROYED [BookkeepingSessionListener.sessionDestroyed]"); 
    	
        try { 
            
            if(bookkeepingSystemFacade != null) { 
                bookkeepingSystemFacade.shutdown(); 
            }
        }
        catch(IOException e) { 
            logger.error("Error shutting down the BookkeepingSystemFacade", e); 
        }
        catch(InterruptedException e) { 
            logger.error("Error shutting down the BookkeepingSystemFacade", e); 
        }
        
    }

}
