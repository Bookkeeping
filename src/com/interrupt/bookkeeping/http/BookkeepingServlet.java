package com.interrupt.bookkeeping.http;

import javax.servlet.ServletException; 
import javax.servlet.SingleThreadModel; 
import javax.servlet.http.HttpServlet; 
import javax.servlet.http.HttpServletRequest; 
import javax.servlet.http.HttpServletResponse; 

import org.apache.log4j.Logger;
import org.exist.EXistException;
import org.exist.storage.BrokerPool;
import org.exist.util.Configuration;
import org.exist.util.DatabaseConfigurationException;

import java.io.File; 
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.IOException;
import java.util.List; 
import java.util.Map;
import java.util.Set;
import java.util.Iterator;

import com.interrupt.bob.base.Bob; 
import com.interrupt.bob.base.IBob; 
import com.interrupt.bookkeeping.ISystem;
import com.interrupt.bookkeeping.account.IAccount;
import com.interrupt.bookkeeping.account.IAccounts;
import com.interrupt.bookkeeping.cc.ExpressionVisitor;
import com.interrupt.bookkeeping.cc.ReplaceVisitor;
import com.interrupt.bookkeeping.cc.analysis.DepthFirstAdapter;
import com.interrupt.bookkeeping.cc.bkell.Bkell;
import com.interrupt.bookkeeping.cc.bkell.aauth.Aauthentication;
import com.interrupt.bookkeeping.cc.bkell.command.AddCommand;
import com.interrupt.bookkeeping.cc.bkell.command.GOption;
import com.interrupt.bookkeeping.cc.bkell.command.GOptions;
import com.interrupt.bookkeeping.cc.bkell.command.GResult;
import com.interrupt.bookkeeping.cc.bkell.command.GToken;
import com.interrupt.bookkeeping.cc.bkell.command.ICommand;
import com.interrupt.bookkeeping.cc.bkell.command.IResult;
import com.interrupt.bookkeeping.cc.bkell.command.ITokens;
import com.interrupt.bookkeeping.cc.bkell.command.ListCommand;
import com.interrupt.bookkeeping.cc.bkell.command.RemoveCommand;
import com.interrupt.bookkeeping.http.BookkeepingSystemFacade;
import com.interrupt.bookkeeping.http.ServletInputParams;
import com.interrupt.bookkeeping.journal.IEntries;
import com.interrupt.bookkeeping.journal.IEntry;
import com.interrupt.bookkeeping.journal.IJournal;
import com.interrupt.bookkeeping.journal.IJournals;
import com.interrupt.bookkeeping.system.BookkeepingSystem;
import com.interrupt.bookkeeping.users.IUserSession;
import com.interrupt.bookkeeping.users.UserSession;
import com.interrupt.util.IdGenerator;


public class BookkeepingServlet extends HttpServlet {
    
	
	private Logger logger = Logger.getLogger(BookkeepingServlet.class); 
	BookkeepingSystemFacade bsfacade = null; 
	
    /** 
     * Initialise the bookkeeping system 
     */
    public void init() throws ServletException {
    	
    	
    	//** initialising the servlet 
    	String valueDBUrl = getServletConfig().getInitParameter("db.url"); 
    	String valueExistInit = getServletConfig().getInitParameter("exist.initdb"); 
    	String valueExistHome = getServletConfig().getInitParameter("exist.home"); 
    	String valueSaxValidation = getServletConfig().getInitParameter("org.xml.sax.features.validation"); 
    	String valueEndorsedDirs = getServletConfig().getInitParameter("java.endorsed.dirs"); 
    	String valueConfFile = getServletConfig().getInitParameter("configuration");
    	
    	logger.debug(""); 
    	logger.debug("db.url > "+ valueDBUrl); 
    	logger.debug("exist.initdb > "+ valueExistInit); 
    	logger.debug("exist.home > "+ valueExistHome); 
    	logger.debug("org.xml.sax.features.validation > "+ valueSaxValidation); 
    	logger.debug("java.endorsed.dirs > "+ valueEndorsedDirs); 
    	
    	if(valueDBUrl != null)
    		System.getProperties().setProperty("db.url", valueDBUrl); 
    	
    	if(valueExistInit != null)
    		System.getProperties().setProperty("exist.initdb", valueExistInit); 
    	
    	if(valueExistHome != null)
    		System.getProperties().setProperty("exist.home", valueExistHome); 
    	
    	if(valueSaxValidation != null)
    		System.getProperties().setProperty("org.xml.sax.features.validation", valueSaxValidation); 
    	
    	if(valueEndorsedDirs != null)
    		System.getProperties().setProperty("java.endorsed.dirs", valueEndorsedDirs); 
    	
    }
    
    
    public void doGet(HttpServletRequest req, HttpServletResponse resp) 
		throws ServletException, IOException {
		
		logger.debug( "doGet CALLED >>> " );
		
    }
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {
    	
		logger.debug( "doPost CALLED >>> " );
		//ServletInputParams siparams = new ServletInputParams(); 
		//siparams.req = req; 
		//siparams.res = resp; 
		
		try {
		    
		    if(req.getSession(false) == null) { 
		        
		        logger.debug( "CREATING a new session" ); 
		    }
		    bsfacade = (BookkeepingSystemFacade)(req.getSession(true).getAttribute(BookkeepingSystemFacade.FACADE_NAME)); 
            if(bsfacade == null) { 
		        
                logger.debug( "CREATING a new bsfacade" );
		        bsfacade = new BookkeepingSystemFacade(); 
		        bsfacade.initialise(); 
		    }
            
		    bsfacade.perform(req, resp); 
		    String xmlResult = null; 
		    try { 
		    	xmlResult = bsfacade.getBkell().getPreviousCommandResult().toXML(false); 
		    }
		    catch(NullPointerException e) { 
				
				logger.error("BookkeepingServlet.doPost > NullPointerException while referencing 'bkell.getPreviousCommandResult()' > ERROR while processing"); 
				return;		//** there must be an error if there's an empty previous command
			}
			
		    logger.debug( "END BookkeepingServlet RESULT: " + xmlResult ); 
		    
		    req.getSession().setAttribute(BookkeepingSystemFacade.FACADE_NAME, bsfacade);
           	
		}
		catch(InterruptedException e) { 
			throw new ServletException(e); 
		}
		
    }
    
    public void destroy() { 
    	
    	if(bsfacade != null) { 
	        
            logger.debug( "BookkeepingServlet.destroy > CALLED" ); 
	        try { 
            	
	        	bsfacade.shutdown(); 
	        	
            }
            catch(IOException e) { 
            	logger.error("IOException THROWN > ["+e.getMessage()+"]", e); 
            }
            catch(InterruptedException e) { 
            	logger.error("InterruptedException THROWN > ["+e.getMessage()+"]", e);
            }
	        
            logger.debug( "BookkeepingServlet.destroy > END" );
	    }
        
    }
}


