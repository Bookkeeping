package com.interrupt.bookkeeping;

import java.util.List;

import com.interrupt.bookkeeping.journal.IJournals;


public class Bookkeeping extends GBookkeeping {
	
	
	private com.interrupt.bookkeeping.account.IAccounts accounts = null;
	private com.interrupt.bookkeeping.journal.IJournals journals = null; 
	private com.interrupt.bookkeeping.journal.ITransactions transactions = null; 
	
	public Bookkeeping() { 
		
		accounts = new com.interrupt.bookkeeping.account.Accounts();
		accounts.setParent(this); 
		
		journals = new com.interrupt.bookkeeping.journal.Journals(); 
		journals.setParent(this); 
		
		transactions = new com.interrupt.bookkeeping.journal.Transactions(); 
		transactions.setParent(this); 
		
	}
	
	// Allow 1 Accounts, Journals, Transaction per 'Bookkeeping' 
	// As well, the 'remove' methods are not meant to be used 
	
	// Accounts 
	public com.interrupt.bookkeeping.account.IAccounts findAccountsById(String value) {
	    return accounts;
	}
	public com.interrupt.bookkeeping.account.IAccounts getAccounts() {
		return accounts;
	}
	
	
	public void addAccounts(com.interrupt.bookkeeping.account.IAccounts accounts) { 
		this.setAccounts(accounts); 
	}
	public void setAccounts(com.interrupt.bookkeeping.account.IAccounts accounts) {
		
		this.accounts = accounts;
		this.accounts.setParent(this); 
		
		this.removeAllAccounts();  
		super.addAccounts(accounts); 
	}
	
	
	// Journals 
	public com.interrupt.bookkeeping.journal.IJournals findJournalsById(String value) {
	    return this.getJournals();
	}
	public com.interrupt.bookkeeping.journal.IJournals getJournals() {
		List allJournals = this.allJournals();
		if(allJournals.size() > 0) { 
			return (com.interrupt.bookkeeping.journal.IJournals)allJournals.get(0); 
		}
		return null; 
	}
	
	
	public void addJournals(com.interrupt.bookkeeping.journal.IJournals journals) { 
		this.setJournals(journals); 
	}
	public void setJournals(com.interrupt.bookkeeping.journal.IJournals journals) {
		
		this.journals = journals;
		this.journals.setParent(this); 
		
		this.removeAllJournals(); 
		super.addJournals(journals); 
	}
	
	
	// Transactions 
	public com.interrupt.bookkeeping.journal.ITransactions findTransactionsById(String value) {
		return transactions;
	}
	public com.interrupt.bookkeeping.journal.ITransactions getTransactions() {
		return transactions;
	}
	
	
	public void setTransactions(
			com.interrupt.bookkeeping.journal.ITransactions transactions) {
		this.transactions = transactions;
		this.transactions.setParent(this); 
	}
	
	
}


