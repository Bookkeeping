
package com.interrupt.bookkeeping.workflow;

import com.interrupt.bob.base.IBob;
import com.interrupt.bookkeeping.cc.bkell.Bkell;
import com.interrupt.bookkeeping.exception.WorkflowException;
import com.interrupt.bookkeeping.util.Util;

import org.apache.log4j.Logger;
import org.xml.sax.helpers.AttributesImpl;


public class StateManager {
    
	private Logger logger = Logger.getLogger(StateManager.class); 
	public StateManager() {}

    /* transition to the appripriate state
     * if state is 1) null or 2) non of these, it goes to the 'open' state
     */
    public void transition(IBob ibob, String destination) throws WorkflowException {
	
	// getting the 'state' value of the IBob object
	String sstate = (ibob.getAttributes()).getValue("state");
	String estate = null;
	
	if(sstate == null) {
	    //throw new WorkflowException("Cannot find attribute 'state'");
	    return;
	}
	if(destination == null) {
	    throw new WorkflowException("destination is NULL");
	}
	logger.debug("-- transition > Class["+ibob.getClass()+"] > sstate["+sstate+"] > destination["+destination+"]");
	
	// transition
	if(sstate.equals(Util.OPEN_STATE) &&
	    destination.equals(Util.CLOSED_STATE)) {
	    
	    estate = Util.CLOSED_STATE;
	}
	else if(sstate.equals(Util.CLOSED_STATE) &&
		destination.equals(Util.REVERSED_STATE)) {
	    
	    estate = Util.REVERSED_STATE;
	}
	else if(sstate.equals(Util.CLOSED_STATE) &&
		destination.equals(Util.REVERSE_CLOSED_STATE)) {
	    
	    estate = Util.REVERSE_CLOSED_STATE;
	}
	else if(sstate.equals(Util.CLOSED_STATE) &&
		destination.equals(Util.REMOVED_STATE)) {
	    
	    estate = Util.REMOVED_STATE;
	}
	else if(sstate.equals(Util.REVERSED_STATE)) {
	    // if this has been removed, do nothing
	    estate = Util.REVERSED_STATE;
	}
	else if(sstate.equals(Util.REMOVED_STATE)) {
	    // if this has been removed, do nothing
	    estate = Util.REMOVED_STATE;
	}
	else {
	    estate = Util.OPEN_STATE;
	}
	
	int index = (ibob.getAttributes()).getIndex("state");
	((AttributesImpl)ibob.getAttributes()).setValue(index,estate);
    }
}


