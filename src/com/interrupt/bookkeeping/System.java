package com.interrupt.bookkeeping;

import java.util.List;

import org.apache.log4j.Logger;

import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.BobException;
import com.interrupt.bob.base.IBob;
import com.interrupt.bob.base.IVisitor;
import com.interrupt.bookkeeping.cc.bkell.aauth.Aauthentication;
import com.interrupt.bookkeeping.cc.bkell.aauth.AauthenticationMediator;
import com.interrupt.bookkeeping.cc.bkell.aauth.IAauthentication;
import com.interrupt.bookkeeping.cc.bkell.command.IResult;
import com.interrupt.bookkeeping.exception.AuthorisationException;
import com.interrupt.bookkeeping.system.BookkeepingSystemProperties;
import com.interrupt.bookkeeping.users.Group;
import com.interrupt.bookkeeping.users.IGroup;
import com.interrupt.bookkeeping.users.IGroups;
import com.interrupt.bookkeeping.users.Groups;
import com.interrupt.bookkeeping.users.IUser;
import com.interrupt.bookkeeping.users.IUserSession;
import com.interrupt.bookkeeping.users.User;
import com.interrupt.spittoon.Spittoon;


public class System extends com.interrupt.bookkeeping.GSystem {
	
	
	private Logger logger = Logger.getLogger(com.interrupt.bookkeeping.System.class); 
	private AauthenticationMediator aauthenticationMediator = null; 
	private Spittoon spittoon = null; 
	
	public System() {}
	
	public Spittoon getSpittoon() {
		return spittoon;
	}
	public void setSpittoon(Spittoon spittoon) {
		this.spittoon = spittoon;
	}
	
	
	public void initialise() { 
		
		if(spittoon == null) { 
			
			spittoon = new Spittoon(); 
			spittoon.initialise(); 
		}
		//1. initiliase and set your aauthentication 
	    //Aauthentication.instance().initialise(); 
		//this.setAauthentication(Aauthentication.instance()); 
		String dburl = spittoon.getAauthDbUrl(); 
		String authXpath = "/system[ @id='main.system' ]/aauthentication[ @id='main.authentication' ]"; 
		Aauthentication aauthentication = (Aauthentication)spittoon.retrieve(dburl, authXpath, true); 
		aauthentication.setSpittoon(spittoon); 
		this.setAauthentication(aauthentication); 
		logger.debug("System.initialise:: Adding aauthentication["+aauthentication.getId()+"] to System"); 
		
		
		//1.5 setup AauthenticationMediator 
		aauthenticationMediator = new AauthenticationMediator(); 
		this.setAauthenticationMediator(aauthenticationMediator); 
		
		//BookkeepingSystemProperties bsproperties = BookkeepingSystemProperties.instance(); 
		
		//2. initialise and set your main groups
		String gdburl = spittoon.getGroupsDbUrl();  
		String groupsXpath = "/system[ @id='main.system' ]/groups[ @id='main.groups' ]"; 
		Groups mgroups = (Groups)spittoon.retrieve(gdburl, groupsXpath, false); 
		
		this.setGroups(mgroups); 
		
		
		//3. initialise the group attic 
		/*groupAttic = (Groups)Spittoon.instance().getDocument(
			bsproperties.getProperty("db.url") + "system/groups/", 
				"groups", 
					"group.attic"); 
		
		IResult groupAtticList = null; //Spittoon.instance().getCollectionBobs(bsproperties.getProperty("db.url") + "system/groups.attic/"); 
		Groups groupAttic = new Groups(); 
		groupAttic.setId("group.attic"); 
		groupAttic.setChildren(groupAtticList.allChildren()); 
		this.setGroupAttic(groupAttic); 
		*/
	}
	
	
	public AauthenticationMediator getAauthenticationMediator() {
		return aauthenticationMediator;
	}
	public void setAauthenticationMediator(AauthenticationMediator aauthenticationMediator) {
		
		this.aauthenticationMediator = aauthenticationMediator;
		this.aauthenticationMediator.setAauth(this.getAauthentication()); 
		this.aauthenticationMediator.setSystem(this); 
		this.getAauthentication().setAauthenticationMediator(aauthenticationMediator); 
	}
	
	public String authenticate(String groupid, IUser user) { 
		
		IUserSession usession = this.getAauthentication().authenticate(groupid, user); 
		return usession.getId(); 
	}
	public boolean authenticated(IUser user) { 
		
		return this.getAauthentication().authenticated(user); 
	}
	
	
	// find 'User' 
	public IUser findUser(String sessionid) { 
		
		IUserSession usession = (IUserSession)this.getAauthentication().getUsers().find("userSession", sessionid); 
		if(usession == null) { 
			throw new AuthorisationException("There is no session associated with id["+sessionid+"]"); 
		}
		logger.debug("System.findUser:: usession: "+ usession.toXML()); 
		
		IUser userResult = (IUser)usession.getParent();  
		logger.debug("System.findUser:: userResult: "+ userResult.toXML()); 
		
		return userResult; 
	}
	public IUser findUser(IUser user) { 
		return this.getAauthentication().findUser(user.getId()); 
	}
	
	
	// find 'Group' 
	public IGroup findGroup(String sessionid) { 
		
		IUserSession usession = (IUserSession)this.getAauthentication().getUsers().find("userSession", sessionid); 
		if(usession == null) { 
			throw new AuthorisationException("There is no session associated with id["+sessionid+"]"); 
		}
		logger.debug("System.findGroup:: usession: "+ usession.toXML()); 
		
		String gid = usession.getGroupid(); 
		IGroup groupResult = this.getAauthentication().getGroups().findGroupById(gid); 
		logger.debug("System.findGroup:: groupResult: "+ groupResult.toXML()); 
		
		return groupResult; 
	}
	public IGroup findGroup(IGroup group) { 
		return this.getAauthentication().findGroup(group.getId()); 
	}
	
	
	// find 'Bookkeeping' 
	public IBookkeeping findBookkeeping(String sessionid) { 
		
		IUserSession usession = (IUserSession)this.getAauthentication().getUsers().find("userSession", sessionid); 
		if(usession == null) { 
			throw new AuthorisationException("There is no session associated with id["+sessionid+"]"); 
		}
		String groupid = usession.getGroupid();
		return this._findBookkeeping(groupid); 
	}
	public IBookkeeping findBookkeeping(IGroup group) {
		return this._findBookkeeping(group.getId()); 
	}
	private IBookkeeping _findBookkeeping(String groupid) { 
		
		return this.getGroups().findGroupById(groupid).findBookkeepingById("main.bookkeeping"); 
	}
	
	
	// Aauthentication 
	public Aauthentication getAauthentication() { 
		return (Aauthentication)this.findAauthenticationById("main.authentication"); 
	}
	public void setAauthentication(Aauthentication aauth) {
		
		aauth.setId("main.authentication"); 
		this.removeAllAauthentication(); 
		super.addAauthentication(aauth); 
	}
	public void addAauthentication(com.interrupt.bookkeeping.cc.bkell.aauth.Aauthentication aauth) { 
		
		this.removeAllAauthentication(); 
		super.addAauthentication(aauth);
	}
	
	
	// Groups 
	public Groups getGroups() { 
		return (Groups)this.findGroupsById("main.groups"); 
	}
	public void setGroups(Groups groups) { 
		
		groups.setId("main.groups"); 
		this.removeGroupsById(groups.getId()); 
		super.addGroups(groups); 
	}
	public void addGroups(Groups groups) {
		
		if(spittoon == null) 
			this.initialise(); 
		this.removeGroupsById(groups.getId()); 
		super.addGroups(groups); 
	}
	
	
	// Group Attic 
	public Groups getGroupAttic() { 
		return (Groups)this.findGroupsById("group.attic"); 
	}
	public void setGroupAttic(Groups groups) {
		
		groups.setId("group.attic"); 
		this.removeGroupsById(groups.getId()); 
		super.addGroups(groups); 
	}
	public void addGroupAttic(Groups groups) { 
		this.removeGroupsById(groups.getId()); 
		super.addGroups(groups); 
	}
	
	
	public void addGroup(Group group) { 
		this.getAauthentication().addGroup(group); 
	}
	public void addUser(User user) {
		
		if(spittoon == null) 
			this.initialise(); 
		
		try { 
			if( this.aauthenticationMediator == null ) { 
				
				aauthenticationMediator = new AauthenticationMediator(); 
				this.setAauthenticationMediator(aauthenticationMediator); 
				
			}
			logger.debug("System.addUser: "+ user); 
			logger.debug("System.addUser USERS: "+ 
				((Aauthentication)this.findAauthenticationById("main.authentication")).getUsers() ); 
	    	this.getAauthentication().addUser(user); 
		}
		catch(RuntimeException t) {
			
			//Throwable cause = this.getRootCause(t); 
			//cause.printStackTrace();
			t.printStackTrace(); 
		}
	}
	
	private Throwable getRootCause(Throwable t) { 
		
		Throwable cause = t.getCause(); 
		if(cause != null) { 
			return this.getRootCause(t); 
		}
		return t;
		 
	}
	public void addGroupSetAssociatedBookkeeping(Group newGroup) { 
		
		logger.debug("System.addGroupSetAssociatedBookkeeping: "+ newGroup); 
		this.getAauthentication().addGroup(newGroup); 
		
		//** create a new 'bookkeeping' entity
		Bookkeeping defaultBookkeeping = 
			(Bookkeeping)Bob.loadS(System.class.getResourceAsStream("/default.bookkeeping.xml"), 
				BookkeepingSystemProperties.instance().getProperty("bob.def")); 
		
		Group correspondingbk = newGroup; 
		correspondingbk.removeAllUser(); 
		correspondingbk.addBookkeeping(defaultBookkeeping); 
		this.getGroups().addGroup(correspondingbk); 
		
		String gurl = spittoon.getGroupsDbUrl(); 
		logger.debug("BEFORE group add and associated bookkeeping > gurl["+gurl+"]"); 
		
		spittoon.createR(gurl, 
			"/system[ @id='main.system' ]/groups[ @id='main.groups' ]/group[ @id='"+correspondingbk.getId()+"' ]", 
				correspondingbk.toXML(false)); 
		
		logger.debug("AFTER group add and associated bookkeeping"); 
		
	}
	
	
	public void accept(IVisitor visitor) throws BobException { 
		
		/*if(visitor instanceof PersistableVisitor) { 
			
			// let Aauthentication save individually 
			PersistableVisitor aauthSaveVisitor = this.getAauthentication().getSavePersistableVisitor(); 
			aauthSaveVisitor.setSpittoon(((PersistableVisitor)visitor).getSpittoon()); 
			aauthSaveVisitor.setBaseDir(((PersistableVisitor)visitor).getBaseDir()); 
			this.getAauthentication().accept(aauthSaveVisitor); 
			
			// let Groups save individually 
			PersistableVisitor groupsSaveVisitor = this.getGroups().getSavePersistableVisitor(); 
			groupsSaveVisitor.setSpittoon(((PersistableVisitor)visitor).getSpittoon()); 
			groupsSaveVisitor.setBaseDir(((PersistableVisitor)visitor).getBaseDir()); 
			this.getGroups().accept(groupsSaveVisitor); 
			
		}
		else { 
			
			// otherwise, roll the 'accept' as normal 
			super.accept(visitor); 
		}
		*/
		
	}
	
	class SavePersistor { 
		
		private String baseDir = null; 
		private String groupDir = "system/aauthentication/groups/"; 
		private String usersDir = "system/aauthentication/users/"; 
		private BookkeepingSystemProperties bsprops = null; 
		private Spittoon spittoon = null; 
		
		public SavePersistor() { 
			bsprops = BookkeepingSystemProperties.instance(); 
		}
		public void setSpittoon(Spittoon spit) { 
			spittoon = spit; 
		}
		public Spittoon getSpittoon() { 
			return spittoon; 
		}
		public void setBaseDir(String bdir) { 
			baseDir = bdir; 
		}
		public String getBaseDir() {
			return baseDir; 
		}
		public void visit(IBob bob) {
			
			//** just set off the aauthentication and groups visitors 
			if(bob instanceof Aauthentication) { 
				
				/*PersistableVisitor pv = ((Aauthentication)bob).getSavePersistableVisitor(); 
				pv.setBaseDir(baseDir); 
				pv.setSpittoon(spittoon); 
				
				bob.accept(pv); 
				*/
				
			}
			else if(bob instanceof Groups && 
						(((Groups)bob).getId().equals("main.groups") || 
						((Groups)bob).getId().equals("group.attic"))  
					) { 
				
				/*PersistableVisitor pv = ((Aauthentication)bob).getSavePersistableVisitor(); 
				pv.setBaseDir(baseDir); 
				pv.setSpittoon(spittoon); 
				
				bob.accept(pv); 
				*/
				
			}
		} 
		
	}
	
	
	
	/**
	 * ** DELETE: the code below is deprecated and should be deleted after the 
	 * rest of the system is working 
	 */
	// Bkell 
	public com.interrupt.bookkeeping.cc.bkell.IBkell getBkell() {
		List allBkell = this.allBkell();
		if(allBkell.size() > 0) { 
			return (com.interrupt.bookkeeping.cc.bkell.IBkell)allBkell.get(0); 
		}
		return null; 
	}
	public void setBkell(com.interrupt.bookkeeping.cc.bkell.IBkell bkell) {
		this.addBkell(bkell); 
	}
	public void addBkell(com.interrupt.bookkeeping.cc.bkell.Bkell bkell) { 
		this.removeAllBkell(); 
		super.addBkell(bkell);
	}
	public com.interrupt.bookkeeping.cc.bkell.IBkell findBkellById(String value) {
		return super.findBkellById(value); 
	}

	// Users 
	public com.interrupt.bookkeeping.users.IUsers getUsers() {
		List allUsers = this.allUsers();
		if(allUsers.size() > 0) { 
			return (com.interrupt.bookkeeping.users.IUsers)allUsers.get(0); 
		}
		return null; 
	}
	public void setUsers(com.interrupt.bookkeeping.users.IUsers users) {
		this.addUsers(users); 
	}
	public void addUsers(com.interrupt.bookkeeping.users.IUsers users) { 
		this.removeAllUsers(); 
		super.addUsers(users);
	}
	

	// Bookkeeping 
	public com.interrupt.bookkeeping.IBookkeeping getBookkeeping() {
		List allBookkeeping = this.allBookkeeping();
		if(allBookkeeping.size() > 0) { 
			return (com.interrupt.bookkeeping.IBookkeeping)allBookkeeping.get(0); 
		}
		return null; 
	}
	public void setBookkeeping(com.interrupt.bookkeeping.IBookkeeping bookkeeping) {
		this.addBookkeeping(bookkeeping); 
	}
	public void addBookkeeping(com.interrupt.bookkeeping.IBookkeeping bookkeeping) { 
		this.removeAllBookkeeping(); 
		super.addBookkeeping(bookkeeping);
	}
	
}

