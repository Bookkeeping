package com.interrupt.bookkeeping;

import org.apache.log4j.Logger;

import com.interrupt.bookkeeping.cc.bkell.Bkell;

public class TrialTiming {
	
	
	private Logger logger = Logger.getLogger(TrialTiming.class); 
	public static void main(String args[]) { 
		
		Logger.getLogger(TrialTiming.class).debug("Going to sleep"); 
		
		Thread timingThread = new Thread(); 
		try { 
			timingThread.sleep(6000); 
		}
		catch(InterruptedException e) { 
			e.printStackTrace(); 
		}
		
		Logger.getLogger(TrialTiming.class).debug("Done sleeping"); 
		
	}
	
}

