package com.interrupt.bookkeeping.currency;

import com.interrupt.bookkeeping.exception.CurrencyException;

public class Currencies extends GCurrencies {
	
	
	private static Currencies instance = null; 
	public Currencies() {} 
	
	public static Currencies instance() { 
		
		if(instance == null) { 
			instance = new Currencies(); 
		}
		return instance; 
	}
	
	
	/**
	 * currency ids must be unique 
	 */
	public void addCurrency(ICurrency currency) { 
		
		ICurrency existingc = this.findCurrencyById(currency.getId()); 
		if(existingc != null) { 
			throw new CurrencyException("There is already a currency with the id["+currency.getId()+"]"); 
		}
		
		super.addCurrency(currency); 
		
	}
	
}

