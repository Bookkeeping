
package com.interrupt.bookkeeping.journal;

import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.interrupt.bookkeeping.Bookkeeping;
import com.interrupt.bookkeeping.IBookkeeping;
import com.interrupt.bookkeeping.journal.Entries;
import com.interrupt.bookkeeping.journal.Entry;
import com.interrupt.bookkeeping.cc.bkell.Bkell;
import com.interrupt.bookkeeping.exception.EntryException;

import com.interrupt.bookkeeping.util.Util;


public class Transaction extends GTransaction {
	
	
	private Logger logger = Logger.getLogger(Transaction.class); 
	public boolean balances() {
		
        boolean balances = true;
        
        List entries = this.allEntries();
        Iterator eiter = entries.iterator();
        Entries eachEntries = null;
        while( eiter.hasNext() ) {
        	
            eachEntries = (Entries)eiter.next();
            if( !eachEntries.balances() ) {

                //throw new EntryException( "Entry id["+ eachEntries.getId() +"]" ); 
                balances = false;
                break;
            }       
                    
        }       
                
        return balances;
    }
	
    public void close() {
		
		
		/* Now close the 'transaction'
		 */
		logger.debug("Closing Transaction id["+ this.getId() +"]");
		this.setState(Util.CLOSED_STATE);
		
		
		
		/* Put the <entry/> in thier corresponding journal 
		 */
		Entries entries = (Entries)this.allEntries().get(0); 
		
		// check null & bad 'journalid'
		Journals journals = (Journals)((Bookkeeping)this.getParent().getParent()).getJournals(); 
		GJournal foundJournal = null; 
		
		List allEntries = entries.allEntry(); 
		Iterator entryIter = allEntries.iterator(); 
		Entry eachEntry = null;
		while( entryIter.hasNext() ) { 
			
			eachEntry = (Entry)entryIter.next(); 
			String jid = eachEntry.getJournalid(); 
			
			if( jid == null || jid.trim().length() < 1 ) { 
				throw new EntryException( "Entry id["+eachEntry.getId()+"] doesn't have a 'journalid' set" ); 
			}
			
			foundJournal = (GJournal)journals.findJournalById(jid);
			if(foundJournal == null) { 
				throw new EntryException( "Cannot find Journal with id["+jid+"] / Entry id["+eachEntry.getId()+"]" ); 
			}
			((Entries)(foundJournal.allEntries().get(0))).addEntry(eachEntry);
		}
		
		
    }
	
}


