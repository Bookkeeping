
package com.interrupt.bookkeeping.journal;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Iterator;

import org.apache.log4j.Logger;

import com.interrupt.bookkeeping.account.GCredit;
import com.interrupt.bookkeeping.account.GDebit;
import com.interrupt.bookkeeping.cc.bkell.Bkell;
import com.interrupt.bookkeeping.exception.EntryException;
import com.interrupt.bookkeeping.util.Util;

import com.interrupt.bob.base.IBob;
import com.interrupt.bob.base.IVisitor;
import com.interrupt.util.DateUtil;

public class Entries extends GEntries {
	
    
	private Logger logger = Logger.getLogger(Entries.class); 
	
	public Entries() {
		
		super(); 
	}
	public Entries(String id) {
		
		super(); 
		this.setId(id); 
	}
	
	public void addEntry(Entry entry) {
	    this.addEntry((IEntry)entry); 
	}
	public void addEntry(IEntry entry) {
		
		if( !((Entry)entry).balances() ) { 
			
			logger.debug("Entries.addEntry: " + entry.toXML()); 
		    throw new EntryException("Debits must equal Credits");
		}
		
		super.addEntry(entry);
	}
	    
	public boolean balances() {
		
		List entryList = this.allEntry();
		Iterator iter = entryList.iterator();
		Entry next = null;
		while(iter.hasNext()) {
		    
		    next = (Entry)iter.next();
		    if(!next.balances()) {
			return false;
		    }
		}
		return true;
    }
	
    
    
	/**
	 * Date Pattern of:
	 * "MM/dd/yyyy HH:mm:ss z"
	 * 
	 */
	public com.interrupt.bookkeeping.journal.IEntry findEntryByDate(String value) {
	    
		
        java.util.Iterator iter = _children.iterator();
	    com.interrupt.bookkeeping.journal.IEntry each = null;
	    while(iter.hasNext()) {
			
			Object next = iter.next();
			if(next instanceof com.interrupt.bookkeeping.journal.IEntry) {
				
				each = (com.interrupt.bookkeeping.journal.IEntry)next;
				
				String comparator = each.getDate();
				
				
				if(DateUtil.equals(comparator, value)) {
					
				    //break;
				    return each;
				}
			}
		}
		return null;
	}
	
	
	public List listEntryBeforeDate(String value) {
	    
		
		List dateList = new ArrayList(); 
        java.util.Iterator iter = _children.iterator();
	    com.interrupt.bookkeeping.journal.IEntry each = null;
	    while(iter.hasNext()) {
			
			Object next = iter.next();
			if(next instanceof com.interrupt.bookkeeping.journal.IEntry) {
				
				each = (com.interrupt.bookkeeping.journal.IEntry)next;
				String comparator = each.getDate();
				if(DateUtil.before(comparator, value)) {
					
					dateList.add(each);
				}
			}
		}
		return dateList;
	}
	
	public List listEntryAfterDate(String value) {
	    
		
		List dateList = new ArrayList(); 
	    java.util.Iterator iter = _children.iterator();
	    com.interrupt.bookkeeping.journal.IEntry each = null;
	    while(iter.hasNext()) {
			
			Object next = iter.next();
			if(next instanceof com.interrupt.bookkeeping.journal.IEntry) {
				
				each = (com.interrupt.bookkeeping.journal.IEntry)next;
				
				String comparator = each.getDate();
				
				
				if(DateUtil.after(comparator, value)) {
					
					dateList.add(each);
				}
			}
		}
		return dateList;
	}
	
	
	public static void main( String srgs[] ) { 
		
		
		String format_s = "MM/dd/yyyy HH:mm:ss z"; 
		SimpleDateFormat dformat = new SimpleDateFormat( format_s ); 
		
		Date date1 = null; 
		Date date2 = null; 
		try { 
			
			date1 = dformat.parse("08/15/2005 00:00:00 EST"); 
			date2 = dformat.parse("09/15/2005 00:00:00 EST"); 
			
			Logger.getLogger(Entries.class).debug( date1.before(date2) ); 
			
		}
		catch( ParseException e ) { 
			
			e.printStackTrace(); 
		}
		
	}
	
}


