package com.interrupt.bookkeeping.journal;


import java.util.List; 
import com.interrupt.bookkeeping.journal.Entries;
import com.interrupt.bookkeeping.journal.GJournal;
import com.interrupt.bookkeeping.journal.IEntries;
import com.interrupt.bookkeeping.journal.IEntry;

public class Journal extends GJournal { 
	
	public Journal() {} 
	
	/**
	 * in case you just want to add an entry to the journal 
	 */
	public void addEntry(IEntry entry) { 
		
		List entries = this.allEntries(); 
		
		IEntries mainEntries = null; 
		if(entries.isEmpty()) { 
			
			mainEntries = new Entries(); 
			this.addEntries(mainEntries); 
		}
		else { 
			
			mainEntries = (IEntries)entries.get(0);
		}
		
		mainEntries.addEntry(entry); 
	}
	
}
