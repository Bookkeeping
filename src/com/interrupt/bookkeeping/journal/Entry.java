
package com.interrupt.bookkeeping.journal;

import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.interrupt.bob.base.IBob;
import com.interrupt.bookkeeping.Bookkeeping;
import com.interrupt.bookkeeping.journal.GEntry;
import com.interrupt.bookkeeping.account.CreditPointer;
import com.interrupt.bookkeeping.account.DebitPointer;
import com.interrupt.bookkeeping.account.IDebit;
import com.interrupt.bookkeeping.account.GDebit;
import com.interrupt.bookkeeping.account.ICredit;
import com.interrupt.bookkeeping.account.GCredit;
import com.interrupt.bookkeeping.account.IAccount;
import com.interrupt.bookkeeping.account.GAccount;
import com.interrupt.bookkeeping.account.Accounts;
import com.interrupt.bookkeeping.cc.bkell.Bkell;
import com.interrupt.bookkeeping.exception.CurrencyException;
import com.interrupt.bookkeeping.exception.EntryException;
import com.interrupt.bookkeeping.exception.AccountException;

import com.interrupt.bookkeeping.util.Util;
import com.interrupt.util.IdGenerator;


public class Entry extends GEntry {
	
	
	public Entry() { 
		
		super(); 
		this.setId(IdGenerator.generateId()); 
	}
	private Logger logger = Logger.getLogger(Entry.class); 
	public boolean balances() {
		
		List credits = this.allCredit();
		Iterator citer = credits.iterator();
		
		List debits = this.allDebit();
		Iterator diter = debits.iterator();
		
		double csize = 0;
		double dsize = 0;
	
		while(citer.hasNext()) {
		    GCredit next = (GCredit)citer.next();
		    csize += Double.parseDouble(next.getAmount());
		}
		
		while(diter.hasNext()) {
		    GDebit next = (GDebit)diter.next();
		    dsize += Double.parseDouble(next.getAmount());
		}
		
		logger.debug("Entry["+ this.toXML(false) +"] > Credit Size["+csize+"] > Debit Size["+dsize+"] > balances["+ (csize == dsize) +"]"); 
		if(csize != dsize) {
		    return false;
		}
		
		return true;
    }
	
	
	/** 
	 * does this Entry have a currency defined 
	 */
	private void _checkCurrencyExists() { 
		
		String currencye = this.getCurrency(); 
		if(currencye == null || currencye.trim().length() == 0) { 
			throw new CurrencyException("This entry doesn't have a currency defined"); 
		}
	}
	
	public void addDebit(com.interrupt.bookkeeping.account.IDebit debit) { 
		
		this._checkCurrencyExists(); 
		if(!this.getCurrency().equals(debit.getCurrency())) { 
			throw new CurrencyException("Debit["+debit.getCurrency()+"] has a different currency than Entry["+this.getCurrency()+"]"); 
		}
		
		debit.setEntryid(this.getId()); 
		super.addDebit(debit); 
		
	}
	public void addCredit(com.interrupt.bookkeeping.account.ICredit credit) { 

		this._checkCurrencyExists(); 
		if(!this.getCurrency().equals(credit.getCurrency())) { 
			throw new CurrencyException("Credit["+credit.getCurrency()+"] has a different currency than Entry["+this.getCurrency()+"]"); 
		}
		
		credit.setEntryid(this.getId()); 
		super.addCredit(credit); 
		
	}
	
	
    public void close() {
    	
	    
	    logger.debug("Closing Entry["+ this.getId() +"]");
	    
		// ensure this entry balances
		if( !this.balances() ) {
		    throw new EntryException("entry [id: "+this.getId()+"] does not balance");
		}
	    
		// put all debits/credits in appropriate 'Account'
		List debits = this.allDebit();
		Iterator diter = debits.iterator();
		List credits = this.allCredit();
		Iterator citer = credits.iterator();
		
		
		// the grandparent SHOULD be 'Bookkeeping' 
		String emessage = null;
		if( this.getParent() == null ) { 
			emessage = "Entry["+ this.getId() +"] does not have 'Entries' / parent"; 
		}
		else if( this.getParent().getParent() == null ) { 
			emessage = "Entry["+ this.getId() +"] does not have parent.parent"; 
		}
		else if( this.getParent().getParent().getParent() == null ) { 
			emessage = "Entry["+ this.getId() +"] does not have parent.parent.parent"; 
		}
		else if( this.getParent().getParent().getParent().getParent() == null ) { 
			emessage = "Entry["+ this.getId() +"] does not have 'Bookkeeping'  / parent.parent.parent.parent"; 
		}
		
		if(emessage != null) { 
			throw new EntryException( emessage ); 
		}
		
		Bookkeeping bkeeping = (Bookkeeping)this.getParent().getParent().getParent().getParent();
		Accounts accountsInstance = (Accounts)bkeeping.getAccounts();
		
		logger.debug( "Entry.close >>>>>>>>>>>>> "+ this.getParent().getParent().getParent().getParent() ); 
		
		GDebit nextd = null;
		
		String accountid = null;
		String accountDName = null; 
		IAccount rdAccount = null;
		
		DebitPointer dpointer = null; 
		while(diter.hasNext()) {
		    
			nextd = (GDebit)diter.next();
			dpointer = new DebitPointer(nextd); //** new  
			
			this._checkDebitAccountLink(nextd); 
		    accountid = nextd.getAccountid(); 
		    
		    // try id first, otherwise go for the name 
		    if(accountid == null || accountid.trim().length() < 1) { 
		    	
		    	accountDName = nextd.getAccount(); 
		    	rdAccount = accountsInstance.findAccountByName(accountDName);
		    }
		    else { 
		    	rdAccount = accountsInstance.findAccountById(accountid);
		    }
		    
		    rdAccount.addDebit(nextd);
		    rdAccount.addDebitPointer(dpointer); //** new 
		    
		}
		
		GCredit nextc = null;
		String accountcid = null;
		String accountCName = null; 
		IAccount rcAccount = null; 
		
		CreditPointer cpointer = null; 
		while(citer.hasNext()) {
		    
			nextc = (GCredit)citer.next();
			cpointer = new CreditPointer(nextc); //** new  
			
		    this._checkCreditAccountLink(nextc);
		    accountcid = nextc.getAccountid();

		    // try id first, otherwise go for the name 
		    if(accountid == null || accountid.trim().length() < 1) { 
		    	
		    	accountCName = nextd.getAccount(); 
		    	rcAccount = accountsInstance.findAccountByName(accountCName);
		    }
		    else { 
		    	rcAccount = accountsInstance.findAccountById(accountcid);
		    }
		    
		    rcAccount.addCredit(nextc); 
		    rcAccount.addCreditPointer(cpointer); //** new 
		    
		}
		
		// close this entry
		this.setState(Util.CLOSED_STATE);

    }


    private void _checkDebitAccountLink(IDebit debit) {
		
		String aid = debit.getAccountid();
		if(aid == null) {
		    throw new AccountException("debit does not have an 'accountid'");
		}
		
		
		// the grandparent SHOULD be 'Bookkeeping' 
		String emessage = null;
		if( this.getParent() == null ) { 
			emessage = "Entry["+ this.getId() +"] does not have 'Entries' / parent"; 
		}
		else if( this.getParent().getParent() == null ) { 
			emessage = "Entry["+ this.getId() +"] does not have parent.parent"; 
		}
		else if( this.getParent().getParent().getParent() == null ) { 
			emessage = "Entry["+ this.getId() +"] does not have parent.parent.parent"; 
		}
		else if( this.getParent().getParent().getParent().getParent() == null ) { 
			emessage = "Entry["+ this.getId() +"] does not have 'Bookkeeping'  / parent.parent.parent.parent"; 
		}
		
		if(emessage != null) { 
			throw new EntryException( emessage ); 
		}
		
		Bookkeeping bkeeping = (Bookkeeping)this.getParent().getParent().getParent().getParent();
		Accounts ainstance = (Accounts)bkeeping.getAccounts();
		
		
		IAccount rAccount = ainstance.findAccountById(aid);
		if(rAccount == null) {
		    throw new AccountException("no Account with id["+aid+"]");
		}
    }
    
    private void _checkCreditAccountLink(ICredit credit) {
		
		String aid = credit.getAccountid();
		if(aid == null) {
		    throw new AccountException("credit does not have an 'accountid'");
		}
		
		
		// the grandparent SHOULD be 'Bookkeeping' 
		String emessage = null;
		if( this.getParent() == null ) { 
			emessage = "Entry["+ this.getId() +"] does not have 'Entries' / parent"; 
		}
		else if( this.getParent().getParent() == null ) { 
			emessage = "Entry["+ this.getId() +"] does not have parent.parent"; 
		}
		else if( this.getParent().getParent().getParent() == null ) { 
			emessage = "Entry["+ this.getId() +"] does not have parent.parent.parent"; 
		}
		else if( this.getParent().getParent().getParent().getParent() == null ) { 
			emessage = "Entry["+ this.getId() +"] does not have 'Bookkeeping'  / parent.parent.parent.parent"; 
		}
		
		if(emessage != null) { 
			throw new EntryException( emessage ); 
		}
		
		// the grandparent SHOULD be 'Bookkeeping' 
		Bookkeeping bkeeping = (Bookkeeping)this.getParent().getParent().getParent().getParent();
		Accounts ainstance = (Accounts)bkeeping.getAccounts();
		
		IAccount rAccount = ainstance.findAccountById(aid);
		if(rAccount == null) {
		    throw new AccountException("no Account with id["+aid+"]");
		}
    }
    
}


