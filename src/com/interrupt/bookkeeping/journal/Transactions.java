
package com.interrupt.bookkeeping.journal;


import java.util.List;
import java.util.Iterator;

public class Transactions extends GTransactions {
    
    
    public boolean balances() {
		
		List entryList = this.allTransaction();
		Iterator iter = entryList.iterator();
		Transaction next = null;
		while(iter.hasNext()) {
		    
		    next = (Transaction)iter.next();
		    if(!next.balances()) {
			return false;
		    }
		}
		return true;
    }

}


