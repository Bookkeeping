package com.interrupt.bookkeeping.action;

import com.interrupt.bob.base.IBob;
import com.interrupt.bookkeeping.cc.bkell.aauth.Aauthentication;
import com.interrupt.bookkeeping.exception.AuthorisationException;
import com.interrupt.bookkeeping.users.User;
import com.interrupt.bookkeeping.users.Users;

public abstract class AbstractAction implements Action {
	
	
	public void execute(IBob bob, User user) {
		
		
		// There is 1 set of 'Users' in the system
		Users users = Users.getInstance(); 
		
		// check that the user is authorized to perform this operation
		try { 
			
			//users.authorize( this, user ); 
			//Aauthentication.instance().authorised(user, command); 
			
			
		}
		catch( AuthorisationException e ) { 
			
			//** handle un-authorized actions at a later point 
			e.printStackTrace();
			
		}
		
	}

}
