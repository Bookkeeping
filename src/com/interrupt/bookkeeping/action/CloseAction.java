
package com.interrupt.bookkeeping.action;

import java.util.List;
import java.util.Iterator;

import org.apache.log4j.Logger;

import com.interrupt.bob.base.BobSystem;

import com.interrupt.bookkeeping.account.IDebit;
import com.interrupt.bookkeeping.account.GDebit;
import com.interrupt.bookkeeping.account.ICredit;
import com.interrupt.bookkeeping.account.GCredit;
import com.interrupt.bookkeeping.account.IAccount;
import com.interrupt.bookkeeping.account.IAccounts;
import com.interrupt.bookkeeping.account.Accounts;
import com.interrupt.bookkeeping.journal.Transaction;
import com.interrupt.bookkeeping.journal.Entry;
import com.interrupt.bookkeeping.exception.EntryException;
import com.interrupt.bookkeeping.exception.AccountException;
import com.interrupt.bookkeeping.users.User;
import com.interrupt.bookkeeping.util.Util;
import com.interrupt.bob.base.IBob;
import com.interrupt.bob.base.IVisitor;

public class CloseAction extends AbstractAction implements IVisitor {
    
	
	private Logger logger = Logger.getLogger(CloseAction.class); 
	public void execute(IBob ibob, User user) {
		
    	super.execute(ibob, user);
		ibob.accept(this);
    }
    
    public void visit(IBob bob) {
		
    	// TODO CloseAction will set off
    	//	1. CloseEntryVisior 
    	// 	2. CloseTransactionVisitor 
    	//
    	// ** this will happen only if User has been authenticated & authorized 
    	// 
    	// public void execute(IBob, IUser); 
		/*if(bob instanceof Transaction) {
		    
		    logger.debug("CloseAction:visiting: Transaction["+((Transaction)bob).getId()+"]");
		    ((Transaction)bob).close(); 
			
		}
		else if(bob instanceof Entry) {
		*/
		if(bob instanceof Entry) {
			
		    logger.debug("CloseAction:visiting:	Entry["+((Entry)bob).getId()+"]");
		    ((Entry)bob).close(); 
		    
		}
    }
}



