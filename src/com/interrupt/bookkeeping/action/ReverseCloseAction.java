
package com.interrupt.bookkeeping.action;

import java.util.List;
import java.util.Iterator;

import org.apache.log4j.Logger;

import com.interrupt.bob.base.BobSystem;

import com.interrupt.bookkeeping.account.IDebit;
import com.interrupt.bookkeeping.account.GDebit;
import com.interrupt.bookkeeping.account.ICredit;
import com.interrupt.bookkeeping.account.GCredit;
import com.interrupt.bookkeeping.account.IAccount;
import com.interrupt.bookkeeping.account.IAccounts;
import com.interrupt.bookkeeping.account.Accounts;
import com.interrupt.bookkeeping.journal.IEntry;
import com.interrupt.bookkeeping.journal.Transaction;
import com.interrupt.bookkeeping.journal.Entry;
import com.interrupt.bookkeeping.exception.EntryException;
import com.interrupt.bookkeeping.exception.AccountException;
import com.interrupt.bookkeeping.exception.WorkflowException;
import com.interrupt.bookkeeping.users.User;
import com.interrupt.bookkeeping.util.Util;
import com.interrupt.bookkeeping.workflow.StateManager;
import com.interrupt.bob.base.IBob;
import com.interrupt.bob.base.IVisitor;

public class ReverseCloseAction extends AbstractAction implements IVisitor {
    
	
	private Logger logger = Logger.getLogger(ReverseCloseAction.class); 
	public void execute(IBob ibob, User user) {
    	
    	super.execute(ibob, user); 
    	ibob.accept(this);
    }
    
    public void visit(IBob bob) throws WorkflowException {
    	
		logger.debug("ReverseCloseAction:visiting: ["+bob.getClass()+"]");
		IEntry entry = null; 
		try { 
			
			entry = (IEntry)bob; 
			String svalue = entry.getState();
			if(svalue == null) {
			    throw new WorkflowException("'state' value is NULL");
			}
			if(!svalue.equals(Util.CLOSED_STATE)) {
			    throw new WorkflowException("state of Entry["+entry.getId()+"] must be 'CLOSED' / ["+entry.getState()+"]");
			}
			
			StateManager smanager = new StateManager();
			smanager.transition(entry,Util.REVERSE_CLOSED_STATE);
			
		}
		catch(ClassCastException e) { 
			
			// not a real error; we can only reverse <entry/>
			//throw e; 
		}
    }

}


