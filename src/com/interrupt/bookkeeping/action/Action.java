package com.interrupt.bookkeeping.action;


import com.interrupt.bob.base.IBob;
import com.interrupt.bookkeeping.exception.AuthorisationException;
import com.interrupt.bookkeeping.users.User;

public interface Action {

	public void execute( IBob bob, User user ); 
	
}
