package com.interrupt.bookkeeping.users;

import java.util.Iterator; 

import org.apache.log4j.Logger; 

import com.interrupt.bob.base.IBob; 
import com.interrupt.bookkeeping.system.BookkeepingSystemProperties; 
import com.interrupt.spittoon.Spittoon; 

public class Groups extends GGroups {
	
	
	private Logger logger = Logger.getLogger(Groups.class); 
	private Spittoon spittoon = null; 
	public Groups() { 
		//spittoon = Spittoon.instance(); 
	}
	
	
	/** 
	 * Persistable methods 
	 */
	class SavePersistor { 
		
		private String baseDir = null; 
		private String groupsDir = "system/groups/"; 
		private BookkeepingSystemProperties bsprops = null; 
		private Spittoon spittoon = null; 
		
		public SavePersistor() { 
			bsprops = BookkeepingSystemProperties.instance(); 
		}
		public void setSpittoon(Spittoon spit) { 
			spittoon = spit; 
		}
		public Spittoon getSpittoon() { 
			return spittoon; 
		}
		public void setBaseDir(String bdir) { 
			baseDir = bdir; 
		}
		public String getBaseDir() {
			return baseDir; 
		}
		/*public void visit(IBob persistable) { 
			
			if(persistable instanceof IGroups && ((IGroups)persistable).getId().equals("main.groups")) { 
				
				Iterator groupIter = ((IGroups)persistable).allGroup().iterator(); 
				IGroup eachGroup = null; 
				while(groupIter.hasNext()) { 
					
					eachGroup = (IGroup)groupIter.next(); 
					
					//** save to system/groups/
					spittoon.updateDocument(
						BookkeepingSystemProperties.instance().getProperty("db.url") + "system/groups/" + eachGroup.getId(), 
							eachGroup.getTagName(), 
								eachGroup.getAttributes().getValue("id"), 
									eachGroup); 
					
				}
			}
		}
		*/
		
	}
	
}

