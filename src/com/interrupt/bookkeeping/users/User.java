package com.interrupt.bookkeeping.users;

import org.apache.log4j.Logger;

import com.interrupt.bookkeeping.users.GUser;


public class User extends GUser implements IUser {
	
	
	private Logger logger = Logger.getLogger(User.class); 
	//private boolean authenticated = false; 
	private Thread sessionThread = null; 
	private IUserSession usession = null; 
	
	public boolean isAuthenticated() {
		
	    if(this.getAuthenticated().equals("true")) { 
	        return true;
	    }
	    return false; 
	}
	public void setAuthenticated(boolean authenticated) {
		
		logger.debug("User:: setAuthenticated["+ authenticated +"]"); 
		
	    if(authenticated) { 
	        this.setAuthenticated("true"); 
	    }
	    else { 
	        this.setAuthenticated("false");
	    }
	    
	} 
	
	
	public void setUserSession(IUserSession usession) { 
		this.usession = usession; 
		this.removeAllUserSession(); 
		this.addUserSession(usession); 
	}
	public IUserSession getUserSession() { 
		return this.usession; 
	}
	
	public void startSession() { 
		
		//((UserSession)usession).setStartTime(java.lang.System.currentTimeMillis()); 
		sessionThread = new Thread((UserSession)usession); 
		sessionThread.start(); 
		
	}
	
}

