package com.interrupt.bookkeeping.users;

import org.apache.log4j.Logger;

public class UserSession extends GUserSession implements Runnable {
	
	
	private Logger logger = Logger.getLogger(UserSession.class); 
	private long timeout = 0l; 
	
	public long getTimeout() {
		return this.timeout;
	}
	public void setTimeout(long timeout) {
		this.timeout = timeout;
	}
	
	public void run() { 
		
		//** keep running until we have reached the timeout threshold 
		logger.debug("UserSession:: BEGIN"); 
		
		long runTime = 0l; 
		long beginTime = java.lang.System.currentTimeMillis(); 
		long currentTime = 0l; 
		
		while(runTime < timeout) { 
			
			currentTime = java.lang.System.currentTimeMillis(); 
			runTime = currentTime - beginTime; 
			
			//logger.debug("UserSession:: runTime["+runTime+"] / timeout["+timeout+"]"); 
		}
		
		logger.debug("UserSession:: END"); 
		((User)this.getParent()).setAuthenticated(false); 
		
	}
	
}

