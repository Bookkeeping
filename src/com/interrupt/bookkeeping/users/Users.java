package com.interrupt.bookkeeping.users;

import com.interrupt.bookkeeping.action.Action;
import com.interrupt.bookkeeping.exception.AuthorisationException;


public class Users extends GUsers {
	
	
	private static Users instance = null; 
	public Users() {
	}
	
	public static Users getInstance() { 
		
		if( instance == null ) { 
			instance = new Users(); 
		}
		return instance;
	}
	
	public void authorize( Action action, User user ) throws AuthorisationException { 
		
		// leave Authorization for later on
		//** any user is authorized to perform any action 
	}
	
	/* this is kind of a hack because java's reflection mechanism doesn't 
	 * seem to recognize the fact that super-interfaces 
	 */
	public void addUser(com.interrupt.bookkeeping.users.User addition) { 
	    super.addUser((com.interrupt.bookkeeping.users.IUser)addition); 
	}
}
