package com.interrupt.bookkeeping.account;

import com.interrupt.bookkeeping.journal.Entry;
import com.interrupt.bookkeeping.journal.Journal;

public class DebitPointer extends GDebitPointer { 
	
	public DebitPointer() {
		
		super(); 
	} 
	public DebitPointer(GDebit debit) { 
		
		super(); 
		this.setAccountid(debit.getAccountid()); 
		this.setEntryid(debit.getEntryid()); 
		this.setJournalid(((Entry)debit.getParent()).getJournalid()); 
		this.setRefid(debit.getId()); 
	}
	
}

