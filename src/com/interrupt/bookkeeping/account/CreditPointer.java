package com.interrupt.bookkeeping.account;

import com.interrupt.bookkeeping.journal.Entry;
import com.interrupt.bookkeeping.journal.Journal;

public class CreditPointer extends GCreditPointer {
	
	public CreditPointer() {
		
		super(); 
	}
	public CreditPointer(GCredit credit) { 
		
		super(); 
		this.setAccountid(credit.getAccountid()); 
		this.setEntryid(credit.getEntryid()); 
		this.setJournalid(((Entry)credit.getParent()).getJournalid()); 
		this.setRefid(credit.getId()); 
	}
	
}

