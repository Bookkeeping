
package com.interrupt.bookkeeping.account;


import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.log4j.Logger;

import com.interrupt.bob.base.BobSystem;

import com.interrupt.bob.base.IBob;
import com.interrupt.bookkeeping.exception.AccountException;
import com.interrupt.bookkeeping.exception.CurrencyException;

/* - asset/expense         ([DEBIT] outflow of cash -> paying for an exp or buying an asset)
 * - liability/revenue     ([CREDIT] inflow of cash -> sold something or borrowing $)
 */
public class Accounts extends GAccounts {
	
    
	private Logger logger = Logger.getLogger(Accounts.class); 
	
    //private Accounts instance = null;
    
    /* figure out a way to make Singleton work
     */
    public Accounts() {
		super();
    }
    public Accounts(String namespace, String tagName) {
		super(namespace,tagName);
    }
	
	
    
    public IBob make() {
		return new Accounts();
    }
    
    /*public IAccounts getInstance() {
		
		if(instance == null) {
			instance = new Accounts();
		}
		return instance;
    }
    */
    
    
    /* remove all accounts from system - DANGEROUS
     */
    public void purge() {
	
	// >> replace this code with 'Accounts.removeAllAccount()' when ready <<
	this.removeAllAccount();
	
	/*List accountList = this.allAccount();
	Iterator iter = accountList.iterator();

	IAccount nextAccount = null;
	String nextid = null;
	while(iter.hasNext()) {
	    
	    nextAccount = (IAccount)iter.next();
	    nextid = nextAccount.getId();
	    this.removeAccountById(nextid);
	}
	*/
    }
    
    
    public void addAccount(Account account) {
        this.addAccount((IAccount)account); 
    }
    public void addAccount(IAccount account) {
		
		//ensure there are no duplicate IDs
		List accounts = this.allAccount();
		Iterator iter = accounts.iterator();
		IAccount next = null;
		while(iter.hasNext()) {
		    next = (IAccount)iter.next();
		    String nextid = next.getId();
		    
		    if(nextid.equals(account.getId())) {
				
				logger.debug("Accounts.addAccount: " + account.toXML()); 
				throw new AccountException("duplicate id");
		    }
		}	
		super.addAccount(account);
    }
    
    
    /* 1. account types are: asset,liability,expense,revenue,capital
     *	  - depending on the type of account, counterWeight is (debit or credit)
     */
    public boolean balances() {
    	
    	List accounts = this.allAccount();
		Iterator iter = accounts.iterator();
		IAccount nextAccount = null;
		
		double d_dcount = 0;
		double d_ccount = 0;
		double c_dcount = 0;
		double c_ccount = 0;
		while(iter.hasNext()) {
		    
			nextAccount = (IAccount)iter.next();
		    if((nextAccount.getCounterWeight()).equals(Account.DEBIT)) {
		    	
				List alldebit = ((Account)nextAccount).allDebitLookup();
				Iterator diter = alldebit.iterator();
				while(diter.hasNext()) {
				    
					IDebit nextDebit = (IDebit)diter.next(); 
					if(!nextAccount.getCurrency().equals(nextDebit.getCurrency())) { 
						throw new CurrencyException("Debit currency["+nextDebit.getCurrency()+"] mismatch with Account currency["+nextAccount.getCurrency()+"]"); 
					}
				    d_dcount += Double.parseDouble(nextDebit.getAmount());
				}
				
				List allcredit = ((Account)nextAccount).allCreditLookup();
				Iterator citer = allcredit.iterator();
				while(citer.hasNext()) {
				    
					ICredit nextCredit = (ICredit)citer.next(); 
					if(!nextAccount.getCurrency().equals(nextCredit.getCurrency())) { 
						throw new CurrencyException("Credit currency["+nextCredit.getCurrency()+"] mismatch with Account currency["+nextAccount.getCurrency()+"]"); 
					}
				    d_ccount += Double.parseDouble(nextCredit.getAmount()); 
				}
		    }
		    else if((nextAccount.getCounterWeight()).equals(Account.CREDIT)) {
				
				List allCredit = ((Account)nextAccount).allCreditLookup();
				Iterator diter = allCredit.iterator();
				while(diter.hasNext()) {
				    
					IDebit nextDebit = (IDebit)diter.next(); 
					if(!nextAccount.getCurrency().equals(nextDebit.getCurrency())) { 
						throw new CurrencyException("Debit currency["+nextDebit.getCurrency()+"] mismatch with Account currency["+nextAccount.getCurrency()+"]"); 
					}
				    c_dcount += Double.parseDouble(nextDebit.getAmount());
				}
				
				List allcredit = ((Account)nextAccount).allCreditLookup();
				Iterator citer = allcredit.iterator();
				while(citer.hasNext()) {
				    
					ICredit nextCredit = (ICredit)citer.next(); 
					if(!nextAccount.getCurrency().equals(nextCredit.getCurrency())) { 
						throw new CurrencyException("Credit currency["+nextCredit.getCurrency()+"] mismatch with Account currency["+nextAccount.getCurrency()+"]"); 
					}
				    c_ccount += Double.parseDouble(nextCredit.getAmount());
				}
		    }
		}
		double totalLeft = d_dcount + c_ccount;
		double totalRight = d_ccount + c_dcount;
		
		if(totalLeft == totalRight) {
		    return true;
		}
		return false;
		
    }
    
}


