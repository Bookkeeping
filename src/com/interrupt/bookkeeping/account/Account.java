
package com.interrupt.bookkeeping.account;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.interrupt.bookkeeping.Bookkeeping;
import com.interrupt.bookkeeping.exception.AccountException;
import com.interrupt.bookkeeping.journal.Entries;
import com.interrupt.bookkeeping.journal.Entry;
import com.interrupt.bookkeeping.journal.Journal;
import com.interrupt.bookkeeping.journal.Journals;

/* - asset/expense         ([DEBIT] outflow of cash -> paying for an exp or buying an asset)
 * - liability/revenue     ([CREDIT] inflow of cash -> sold something or borrowing $)
 */ 
public class Account extends GAccount {
    
    public static String DEBIT = "debit";
    public static String CREDIT = "credit";
    
    public static String ASSET = "asset";
    public static String EXPENSE = "expense";
    public static String LIABILITY = "liability";
    public static String REVENUE = "revenue";
    
    public void setType(String _type) {
	
	if(_type.equals(Account.ASSET)) {
	    this.setCounterWeight(Account.DEBIT);
	}
	else if(_type.equals(Account.EXPENSE)) {
	    this.setCounterWeight(Account.DEBIT);
	}
	else if(_type.equals(Account.LIABILITY)) {
	    this.setCounterWeight(Account.CREDIT);
	}
	else if(_type.equals(Account.REVENUE)) {
	    this.setCounterWeight(Account.CREDIT);
	}
	else {
	    throw new AccountException("Account must be of type 'Account.ASSET', 'Account.EXPENSE' , 'Account.LIABILITY', 'Account.REVENUE'");
	}
	super.setType(_type);
    }
    public String getType() {
	return super.getType();
    }

    
    public  void setCounterWeight(String cweight) {
	
	if(cweight.equals(Account.DEBIT)) {
	    super.setCounterWeight(Account.DEBIT);
	}
	else if(cweight.equals(Account.CREDIT)) {
	    super.setCounterWeight(Account.CREDIT);
	}
	else {
	    throw new AccountException("Account counterWeight must be of type 'Account.DEBIT', 'Account.CREDIT'");
	}
	//super.setCounterWeight(cweight);
    }
    public  String getCounterWeight() {
    	return super.getCounterWeight();
    }
    
    /** 
     * TODO - this will need o be made more performant later on 
     */
    public List allDebitLookup() { 
    	
    	List debitList = new ArrayList(); 
    	Bookkeeping bookkeeping = (Bookkeeping)this.getParent().getParent(); 
    	Journals mainJournals = (Journals)bookkeeping.findJournalsById("main.journals"); 
    	
    	Iterator dpIter = this.allDebitPointer().iterator(); 
    	DebitPointer eachdp = null; 
    	
    	Journal journal = null;  
		Entries entries = null;  
    	Entry entry = null; 
    	GDebit debit = null; 
    	while(dpIter.hasNext()) { 
    		
    		eachdp = (DebitPointer)dpIter.next(); 
    		String refid = eachdp.getRefid(); 
    		String entryid = eachdp.getEntryid(); 
    		String journalid = eachdp.getJournalid(); 
    		
    		journal = (Journal)mainJournals.findJournalById(journalid); 
    		entries = (Entries)journal.findEntriesById("main.entries"); 
        	entry = (Entry)entries.findEntryById(entryid); 
        	debit = (GDebit)entry.findDebitById(refid); 
        	debitList.add(debit); 
    	}
    	
    	return debitList; 
    }
    
    public List allCreditLookup() { 
    	
    	List creditList = new ArrayList(); 
    	Bookkeeping bookkeeping = (Bookkeeping)this.getParent().getParent(); 
    	Journals mainJournals = (Journals)bookkeeping.findJournalsById("main.journals"); 
    	
    	Iterator cpIter = this.allCreditPointer().iterator(); 
    	CreditPointer eachdp = null; 
    	
    	Journal journal = null; 
		Entries entries = null;  
    	Entry entry = null; 
    	GCredit credit = null; 
    	while(cpIter.hasNext()) { 
    		
    		eachdp = (CreditPointer)cpIter.next(); 
    		String refid = eachdp.getRefid(); 
    		String entryid = eachdp.getEntryid(); 
    		String journalid = eachdp.getJournalid(); 
    		
    		journal = (Journal)mainJournals.findJournalById(journalid); 
    		entries = (Entries)journal.findEntriesById("main.entries"); 
        	entry = (Entry)entries.findEntryById(entryid); 
        	credit = (GCredit)entry.findCreditById(refid); 
        	creditList.add(credit); 
    	}
    	
    	return creditList; 
    }
    
}


