
package com.interrupt.bookkeeping.system;


import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.io.File;
import java.io.FileWriter;

import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.Database;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.XMLResource;

import com.interrupt.bob.base.IBob;
import com.interrupt.bob.base.BobSystem;

//import com.interrupt.bob.core.Main;

//import com.interrupt.bob.core.IXmlProcessor;
//import com.interrupt.bob.core.XmlProcessor;
//import com.interrupt.bob.core.IQueue;
import com.interrupt.bob.processor.ProcessorException;
import com.interrupt.bookkeeping.ISystem;
import com.interrupt.spittoon.Spittoon;

public class BookkeepingSystem {
	
	
	private static BookkeepingSystem _instance = null; 
	
	private ISystem MODEL = null; 
	private Spittoon spittoon = null; 
	
	
	private BookkeepingSystem() {} 

	
	public static void initialise() { 
		BookkeepingSystem.instance(); 
	}
	
	
	// First time calling an instance will initialise the system 
	public static BookkeepingSystem instance() { 
		
		if(_instance == null) { 
			_instance = new BookkeepingSystem(); 
			_instance._initialise(); 
		}
		return _instance; 
		
	}
	
	public void _initialise() { 
		
		System.getProperties().setProperty(com.interrupt.bob.util.Util.DEF, BookkeepingSystemProperties.instance().getProperty("bob.def")); 
		System.getProperties().setProperty(com.interrupt.bob.util.Util.BASE, "."); 
		System.getProperties().setProperty(com.interrupt.bob.util.Util.END, ".xml"); 
		
		// initialise BookkeepingSystemProperties
		BookkeepingSystemProperties.instance(); 
		
		// initialise Spittoon 
		//spittoon = Spittoon.instance(); 
		//MODEL = spittoon.load(); 
		
	}
	public void shutdown() { 
		//spittoon.shutdown(); 
	}
	
	public ISystem getMODEL() {
		return MODEL;
	}
	public void setMODEL(ISystem model) {
		MODEL = model;
	}
	
	public void saveModel() { 
		//spittoon.save(MODEL); 
	}
	
	
	public void updateModel() { 
		//spittoon.updateSystem(MODEL); 
	}
	
	
	public void deleteModel() { 
		//spittoon.delete(); 
	}
	
}


