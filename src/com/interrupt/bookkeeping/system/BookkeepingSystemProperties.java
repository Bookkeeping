package com.interrupt.bookkeeping.system;


import java.io.File;
import java.io.FileReader;
import java.io.InputStream; 
import java.io.IOException; 

import com.interrupt.bookkeeping.exception.SystemException;
public class BookkeepingSystemProperties extends java.util.Properties { 
	
	
	private static BookkeepingSystemProperties instance = null; 
	
	private org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(BookkeepingSystemProperties.class); 
	
	private BookkeepingSystemProperties() { 
		
		//ClassLoader cloader = this.getClass().getClassLoader(); 
		//InputStream istream = cloader.getResourceAsStream("bookkeeping.properties"); 
		
		//logger.debug(""); 
		//logger.debug("BookkeepingSystemProperties:: LISTING SYSTEM PROPERTIES"); 
		//logger.debug("java.class.path: "+ System.getProperty("java.class.path")); 
		//System.getProperties().list(System.out); 
		try { 
			load(BookkeepingSystemProperties.class.getResourceAsStream("/bookkeeping.properties"));
		}
		catch(IOException e) { 
			throw new SystemException("Problem retrieving resource 'bookkeeping.properties'", e); 
		}
		
	}
	
	public static BookkeepingSystemProperties instance() { 
		
		if(instance == null) { 
			instance = new BookkeepingSystemProperties(); 
		}
		return instance; 
	}
	
}

