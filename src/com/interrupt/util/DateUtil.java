package com.interrupt.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
	
	
	private static String format_s = null; 
	private static SimpleDateFormat dformat = null; 
	static { 
		
		format_s = "MM/dd/yyyy HH:mm:ss z"; 
		dformat = new SimpleDateFormat( format_s ); 
		
	}
	
	public static boolean before( String before_s, String after_s ) { 
		
		
		boolean isbefore = false; 
		Date date1 = null; 
		Date date2 = null; 
		try { 
			
			date1 = dformat.parse( before_s ); 
			date2 = dformat.parse( after_s ); 
			
			isbefore = date1.before(date2); 
			
		}
		catch( ParseException e ) { 
			
			e.printStackTrace(); 
		}
		return isbefore; 
		
	}
	
	public static boolean after( String after_s, String before_s ) { 
		
		
		boolean isbefore = false; 
		Date date1 = null; 
		Date date2 = null; 
		try { 
			
			date1 = dformat.parse( after_s ); 
			date2 = dformat.parse( before_s ); 
			
			isbefore = date1.after(date2); 
			
		}
		catch( ParseException e ) { 
			
			e.printStackTrace(); 
		}
		return isbefore; 
		
	}
	

	public static boolean equals( String date_1, String date_2 ) { 
		
		
		boolean isequals = false; 
		Date date1 = null; 
		Date date2 = null; 
		try { 
			
			date1 = dformat.parse( date_1 ); 
			date2 = dformat.parse( date_2 ); 
			
			isequals = date1.equals(date2); 
			
		}
		catch( ParseException e ) { 
			
			e.printStackTrace(); 
		}
		return isequals; 
		
	}
	
	
	
}

