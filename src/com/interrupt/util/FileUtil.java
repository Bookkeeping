package com.interrupt.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class FileUtil {
	
	public static String readTextFile(String fileName) throws FileNotFoundException, IOException { 
		
		FileReader myFile = new FileReader( fileName ); 
		return FileUtil._readTextFile(myFile); 
	}
	public static String readTextFileAsStream(String fileName) throws FileNotFoundException, IOException { 
		
		InputStream istream = FileUtil.class.getResourceAsStream(fileName); 
		InputStreamReader isreader = new InputStreamReader(istream); 
		return FileUtil._readTextFile(isreader); 
	}
	private static String _readTextFile(Reader fileReader) throws FileNotFoundException, IOException { 
		
		StringBuffer sbuffer = new StringBuffer(); 
		BufferedReader buff = new BufferedReader(fileReader);
		boolean eof = false;
		String eachLine = null; 
		while (!eof) {
			
			eachLine = buff.readLine();
			if (eachLine == null) { 
				eof = true; 
			}
			else {
				sbuffer.append(eachLine); 
			}
		}
		return sbuffer.toString(); 
	}
}

