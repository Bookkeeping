
package com.interrupt.util;

/* Uses RMI's VMID in 'distributed garbage collection' package (java.rmi.dgc.VMID)
 * to generate Unique IDs accross virtual machines
 */

public class IdGenerator {

    public static String generateId() {
    	
    	java.rmi.dgc.VMID iid = new java.rmi.dgc.VMID(); 
		//String id = iid.toString().replaceAll(":", ""); 
		return iid.toString().replaceAll(":", "");
    }
}

