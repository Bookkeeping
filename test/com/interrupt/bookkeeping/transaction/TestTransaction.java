
package com.interrupt.bookkeeping.transaction;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import java.util.List;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.AttributesImpl;

import com.interrupt.bookkeeping.Bookkeeping;
import com.interrupt.bookkeeping.action.CloseAction;
import com.interrupt.bookkeeping.account.GDebit;
import com.interrupt.bookkeeping.account.GCredit;
import com.interrupt.bookkeeping.account.IAccount;
import com.interrupt.bookkeeping.account.GAccount;
import com.interrupt.bookkeeping.account.IAccounts;
import com.interrupt.bookkeeping.account.Accounts;

import com.interrupt.bookkeeping.journal.Journals;
import com.interrupt.bookkeeping.journal.GJournal;
import com.interrupt.bookkeeping.journal.IEntry;
import com.interrupt.bookkeeping.journal.Entry;
import com.interrupt.bookkeeping.journal.Entries;
import com.interrupt.bookkeeping.journal.Transaction;
import com.interrupt.bookkeeping.journal.Transactions;
import com.interrupt.bookkeeping.exception.EntryException;
import com.interrupt.bookkeeping.util.Util;

public class TestTransaction extends TestCase {
    
    
	
	private IAccount cashAccount = null; 
	private IAccount salesAccount = null; 
	private IAccount taxAccount = null; 
	private Accounts ainstance = null; 
	
	private GJournal journal1 = null; 
	private GJournal journal2 = null; 
	private Journals journals = null; 
	
	private GDebit cashDebit = null; 
	private GCredit salesCredit = null; 
	private GCredit taxCredit = null; 
	
	private Entry entry = null; 
	private Entry entry1 = null; 
	
    public TestTransaction() {
	}
    public TestTransaction(String name) {
		super(name);
    }
    

    /* SETUP / TEARDOWN
     */
    public void setUp() {
		
		
		// setting up Accounts
		cashAccount = new GAccount();
		cashAccount.setId("100");
		salesAccount = new GAccount();
		salesAccount.setId("101");
		taxAccount = new GAccount();
		taxAccount.setId("102");
		
		ainstance = new Accounts();
		ainstance.purge();
		ainstance.addAccount(cashAccount);
		ainstance.addAccount(salesAccount);
		ainstance.addAccount(taxAccount);
		
		
		// Setup the journals
		journal1 = new GJournal(); 
		journal1.setId("j1");
		journal1.addEntries( new Entries() );
		
		journal2 = new GJournal(); 
		journal2.setId("j2");
		journal2.addEntries( new Entries() );
		
		journals = new Journals(); 
		journals.addJournal(journal1); 
		journals.addJournal(journal2); 
		
		
		// setting up transaction
		cashDebit = new GDebit();
		cashDebit.setAccountid("100");
		cashDebit.setAmount("11.50");
		
		salesCredit = new GCredit();
		salesCredit.setAccountid("101");
		salesCredit.setAmount("10.00");
		
		taxCredit = new GCredit();
		taxCredit.setAccountid("102");
		taxCredit.setAmount("1.50");
	
		entry = new Entry();
		entry.setId("01");
		entry.addDebit(cashDebit);
		entry.addCredit(salesCredit);
		entry.addCredit(taxCredit);
		
		entry1 = new Entry();
		entry1.setId("02");
		entry1.addDebit(cashDebit);
		entry1.addCredit(salesCredit);
		entry1.addCredit(taxCredit);
	
	}
    public void tearDown() {
		
		journal1 = null;
		journal2 = null; 
		journals = null; 
		
		cashAccount = null; 
		salesAccount = null; 
		taxAccount = null; 
		
		cashDebit = null; 
		salesCredit = null; 
		taxCredit = null; 
		
		entry = null; 
		entry1 = null; 
	}
    
    public static Test suite() {
		
		TestSuite suite = new TestSuite();
		suite.addTest( new TestTransaction("testCloseTransaction") );
		suite.addTest( new TestTransaction("testBalancingEntries") );
		suite.addTest( new TestTransaction("testBalancingTransactions") );
		suite.addTest( new TestTransaction("testCorrespondingJournal") );
		
		return suite;
    }
	
	
	/* when adding <entry/> to a <transaction/>, the corresponding journal 
	 * must be affected as well
	 */
	public void testCorrespondingJournal() {
		
		
		// 1. Create & close entries - entries without a 'journalid' should throw an error 
		Entries entries = new Entries();
		entries.addEntry(entry);
		entries.addEntry(entry1);
		
		Transaction transaction = new Transaction();
		transaction.addEntries(entries);
		
		// setup ancestry
		Transactions transactions = new Transactions(); 
		transactions.addTransaction(transaction); 
		
		Bookkeeping bookkeeping = new Bookkeeping(); 
		bookkeeping.setAccounts(ainstance); 
		bookkeeping.setJournals(journals); 
		bookkeeping.setTransactions(transactions); 
		
		
		CloseAction caction = new CloseAction();
		EntryException eexception = null; 
		try { 
			transaction.accept(caction);
		}
		catch( EntryException e ) { 
			eexception = e;
		}
		assertNotNull( "There SHOULD be an EntryException due to because 1 or more entries do not correspond with a journalid", eexception ); 
		
		
		// 2. Create & close entries -  the 'journalid' on each entry means that that journal should now have that entry
		eexception = null; 
		entry.setJournalid("j1");
		entry1.setJournalid("j2");
		try { 
			
			transaction.accept(caction);
		}
		catch( EntryException e ) { 
			eexception = e;
		}
		assertNull( "EntryException  SHOULD be NULL as entry 'journalid's have been set", eexception ); 
		
		
		// ** journals should now have the corresponding <entry/>
		journals = (Journals)bookkeeping.getJournals(); 
		
		GJournal resultj1 = (GJournal)journals.findJournalById( "j1" ); 
		Entry resultEntry1 = (Entry)((Entries)resultj1.allEntries().get(0)).findEntryById("01");
		
		GJournal resultj2 = (GJournal)journals.findJournalById( "j2" ); 
		Entry resultEntry2 = (Entry)((Entries)resultj2.allEntries().get(0)).findEntryById("02");
		
		
		assertNotNull("Entry id[01] CANNOT be found in journal id[j1]", resultEntry1); 
		assertNotNull("Entry id[02] CANNOT be found in journal id[j2]", resultEntry2); 
		
	}
	
    public void testCloseTransaction() {
		
		
		// set the 'journalid's 
		entry.setJournalid("j1");
		entry1.setJournalid("j2");
		
		Entries entries = new Entries();
		entries.addEntry(entry);
		entries.addEntry(entry1);
			
		Transaction transaction = new Transaction();
		transaction.addEntries(entries);
		
		// setup ancestry 
		Transactions transactions = new Transactions(); 
		transactions.addTransaction(transaction); 
		
		Bookkeeping bookkeeping = new Bookkeeping(); 
		bookkeeping.setAccounts(ainstance); 
		bookkeeping.setJournals(journals); 
		bookkeeping.setTransactions(transactions); 
		
		
		// do it...
		CloseAction caction = new CloseAction();
		transaction.accept(caction);
		
		// ensure transaction & all entries are in 'closed' state
		assertEquals("transaction SHOULD be 'closed'", Util.CLOSED_STATE, transaction.getState());
	
		IEntry rentry1 = entries.findEntryById("01");
		IEntry rentry2 = entries.findEntryById("02");
		assertEquals("Entry1 SHOULD be 'closed'", Util.CLOSED_STATE, rentry1.getState());
		assertEquals("Entry2 SHOULD be 'closed'", Util.CLOSED_STATE, rentry2.getState());
    }
    
    /* only balancing 'entry' objects added to a 'entries'
     */ 
    public void testBalancingEntries() {
		
		GDebit cashDebit = new GDebit();
		cashDebit.setAmount("11.50");
	
		GCredit salesCredit = new GCredit();
		salesCredit.setAmount("10.00");
		
		GCredit taxCredit = new GCredit();
		taxCredit.setAmount("1.00");
	
		Entry entry = new Entry();
		entry.addDebit(cashDebit);
		entry.addCredit(salesCredit);
		entry.addCredit(taxCredit);
		
		Entries entries = new Entries();
		EntryException texception = null;
		
		try {
		    entries.addEntry(entry);
		}
		catch(EntryException e) {
		    texception = e;
		}
	
		List tentries = entries.allEntry();
		assertNotNull("Should be a 'EntryException' with bad balance", texception);
		assertEquals("There should be no entries", 0, tentries.size());
    }
    
    /* test working code 
     */ 
    public void testBalancingTransactions() {
		
		GDebit cashDebit = new GDebit();
		cashDebit.setAmount("11.50");
	
		GCredit salesCredit = new GCredit();
		salesCredit.setAmount("10.00");
		
		GCredit taxCredit = new GCredit();
		taxCredit.setAmount("1.50");
	
		Entry entry = new Entry();
		entry.addDebit(cashDebit);
		entry.addCredit(salesCredit);
		entry.addCredit(taxCredit);
		
		Entries entries = new Entries();
		entries.addEntry(entry); 
			
		Transaction transaction1 = new Transaction();
		Transaction transaction2 = new Transaction();
		Transaction transaction3 = new Transaction();
		EntryException texception = null;
		
		Transactions transactions = new Transactions();
		try {
		    transaction1.addEntries(entries);
		    transaction2.addEntries(entries);
		    transaction3.addEntries(entries);
	
		    transactions.addTransaction(transaction1);
		    transactions.addTransaction(transaction2);
		    transactions.addTransaction(transaction3);
		}
		catch(EntryException e) {
		    texception = e;
		}
	
		assertNull("'EntryException' SHOULD be null", texception);
		assertTrue("'Transactions' DOES NOT balance", transactions.balances());
    }

	
}


