package com.interrupt.bookkeeping.currency;

import com.interrupt.bob.base.Bob;
import com.interrupt.bookkeeping.Bookkeeping;
import com.interrupt.bookkeeping.account.Accounts;
import com.interrupt.bookkeeping.account.GCredit;
import com.interrupt.bookkeeping.account.GDebit;
import com.interrupt.bookkeeping.account.TestAccounts;
import com.interrupt.bookkeeping.exception.CurrencyException;
import com.interrupt.bookkeeping.journal.Entry;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class TestCurrencies extends TestCase {
	
	
    public TestCurrencies() {
	}
    public TestCurrencies(String name) {
		super(name);
    }
    
	public static Test suite() {
		
		TestSuite suite = new TestSuite(); 
		suite.addTest( new TestCurrencies("testUniqueCurrencies") ); 
		suite.addTest( new TestCurrencies("testDtCtAndEntryHaveSameCurrency") ); 
		suite.addTest( new TestCurrencies("testPointersCorrespondToCorrectEntryCurrencies") ); 
		return suite;
    }
    
	public void testUniqueCurrencies() { 
		
		GCurrency currency1 = new GCurrency(); 
		currency1.setId("CDN"); 
		GCurrency currency2 = new GCurrency(); 
		currency2.setId("CDN"); 
		
		Currencies currencies = Currencies.instance(); 
		currencies.addCurrency(currency1); 
		
		CurrencyException ce = null; 
		try { 
			currencies.addCurrency(currency2); 
		}
		catch(CurrencyException e) { 
			ce = e; 
		}
		
		assertNotNull("Trying to add 2 currencies of the same id SHOULD give an error", ce); 
		
	}
	
	public void testDtCtAndEntryHaveSameCurrency() { 
		
		GDebit debit = new GDebit(); 
		debit.setCurrency("CDN"); 
		
		
		
		GDebit debitX = new GDebit(); 
		debitX.setCurrency("USD"); 
		GCredit creditX = new GCredit(); 
		creditX.setCurrency("USD"); 
		
		
		Entry entry = new Entry(); 
		entry.setCurrency("CDN"); 
		entry.addDebit(debit); 
		
		CurrencyException ce = null; 
		try { 
			entry.addDebit(debitX); 
			entry.addCredit(creditX); 
		}
		catch(CurrencyException e) { 
			ce = e; 
		}
		assertNotNull("There SHOULD be an exception when trying to add debit/credit to an entry with mismatched currencies", ce); 
		
	}
	
	public void testPointersCorrespondToCorrectEntryCurrencies() { 
		
		System.getProperties().setProperty(com.interrupt.bob.util.Util.DEF, "xml/bookkeeping.system.xml test/xml/test.currencies.badpointers.xml"); 
		System.getProperties().setProperty(com.interrupt.bob.util.Util.BASE, "."); 
		System.getProperties().setProperty(com.interrupt.bob.util.Util.END, ".xml"); 
		
		
		Bookkeeping bkeeping = (Bookkeeping)Bob.loadS(TestCurrencies.class.getResourceAsStream("/test.currencies.badpointers.xml"), "xml/bookkeeping.system.xml test/xml/test.currencies.badpointers.xml"); 
		Accounts accounts = (Accounts)bkeeping.getAccounts(); 
		
		CurrencyException ce = null; 
		try { 
			accounts.balances(); 
		}
		catch(CurrencyException e) { 
			ce = e; 
		}
		assertNotNull("There SHOULD be an error as a debitPointer and creditPointer are pointing to a debit/credit of different currency", ce); 
		
	}
	
	
}


