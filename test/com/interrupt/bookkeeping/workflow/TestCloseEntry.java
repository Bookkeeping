
package com.interrupt.bookkeeping.workflow;

/*import com.bigbross.bossa.Bossa;
import com.bigbross.bossa.BossaFactory;
import com.bigbross.bossa.PersistenceException;
import com.bigbross.bossa.BossaException;
import com.bigbross.bossa.BossaTransaction;
import com.bigbross.bossa.work.WorkManager;
import com.bigbross.bossa.wfnet.CaseType;
import com.bigbross.bossa.wfnet.CaseTypeManager;
import com.bigbross.bossa.wfnet.Transition;
import com.bigbross.bossa.wfnet.Place;
import com.bigbross.bossa.resource.Resource;
import com.bigbross.bossa.resource.ResourceManager;
*/

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.List;
import java.util.Iterator;

import org.apache.log4j.Logger;

import com.interrupt.bookkeeping.journal.Entry;
import com.interrupt.bookkeeping.account.GDebit;
import com.interrupt.bookkeeping.account.GCredit;
import com.interrupt.bookkeeping.cc.bkell.Bkell;
import com.interrupt.bookkeeping.util.Util;
import com.interrupt.bookkeeping.exception.WorkflowException;

public class TestCloseEntry extends TestCase {
    
    
	private Logger logger = Logger.getLogger(TestCloseEntry.class); 
	public TestCloseEntry() {}
    public TestCloseEntry(String name) {
	super(name);
    }
    
    
    /* SETUP / TEARDOWN
     */
    public void setUp() {}
    public void tearDown() {}
    
    public static Test suite() {
	
	TestSuite suite = new TestSuite();
	//suite.addTest( new TestCloseEntry("testBossa") );
	suite.addTest( new TestCloseEntry("testCloseTransition") );
	suite.addTest( new TestCloseEntry("testReverseTransition") );
	suite.addTest( new TestCloseEntry("testRemoveTransition") );
	suite.addTest( new TestCloseEntry("testEmptyState") );
	suite.addTest( new TestCloseEntry("testNullDestination") );
	suite.addTest( new TestCloseEntry("testAtEnd") );
	return suite;
    }
    
    /*public void testBossa() {
	
	BossaFactory factory = new BossaFactory();
	factory.setStateDir("/Users/timothyw/projects/software/bookkeeping/data/bossa");

	Bossa bossa = null;
	PersistenceException ex = null;
	try {
	    bossa = factory.createBossa();
	}
	catch(PersistenceException e) {
	    ex = e;
	}
	
	// create 'places' and transitions
	CaseType casetype = new CaseType("TestCaseType");
	Place placeOpen = casetype.registerPlace("open");
	Place placeClosed = casetype.registerPlace("closed");
	Transition transb = casetype.registerTransition("close","tim");
	transb.input(placeOpen,"1");
	transb.output(placeClosed,"1");
	
	// register casetype
	BossaException ex2 = null;
	CaseTypeManager caseTypeManager = bossa.getCaseTypeManager();
	String emessage = null;
	try {
	    caseTypeManager.registerCaseType(casetype);
	}
	catch(BossaException e) {
	    ex2 = e;
	    emessage = ex2.getMessage();
	}
	assertNull("Registering casetype FAILED: " + emessage, ex2);
	
	// create resources 'tim'
	ResourceManager resourceManager = bossa.getResourceManager();
        Resource tim = null;
	try {
	    tim = resourceManager.createResource("tim");
	}
	catch(BossaException e) {
	}
	assertNotNull("resource 'tim' was NOT created", tim);
	
	Resource resource = resourceManager.getResource("tim");
	assertNotNull("resource 'resource' was NOT retrieved", resource);
	
	// get work item list
	//WorkManager workManager = bossa.getWorkManager();
	//List wiList = workManager.getWorkItems(resource);
	//assertNotNull("work item list is NULL", wiList);
	
	//logger.debug("Work Item List: " + wiList);
	//Iterator iter = wiList.iterator();
	//while(iter.hasNext()) {
	//    Object next  = iter.next();
	//    logger.debug("next: " + next);
	//}
	
	
    }
	*/
	
	
    public void testCloseTransition() {
		
		Entry entry = new Entry();
		entry.setState(Util.OPEN_STATE);
		
		StateManager smanager = new StateManager();
		smanager.transition( entry, Util.CLOSED_STATE );
	
		assertEquals("entry state SHOULD be 'closed'", Util.CLOSED_STATE, entry.getState());
    }
    public void testReverseTransition() {
		
		Entry entry = new Entry();
		entry.setState(Util.CLOSED_STATE);
		
		StateManager smanager = new StateManager();
		smanager.transition( entry, Util.REVERSED_STATE );
	
		assertEquals("entry state SHOULD be 'reversed'", Util.REVERSED_STATE, entry.getState());
    }
    public void testRemoveTransition() {
		
		Entry entry = new Entry();
		entry.setState(Util.CLOSED_STATE);
		
		StateManager smanager = new StateManager();
		smanager.transition( entry, Util.REMOVED_STATE );
	
		assertEquals("entry state SHOULD be 'removed'", Util.REMOVED_STATE, entry.getState());
    }
    public void testEmptyState() {
	
		// if state is not, got OPEN
		Entry entry = new Entry();
		
		StateManager smanager = new StateManager();
		smanager.transition( entry, Util.REMOVED_STATE );
	
		assertEquals("entry state SHOULD be 'open'", Util.OPEN_STATE, entry.getState());
    }
    public void testNullDestination() {
		
		Entry entry = new Entry();
		entry.setState(Util.CLOSED_STATE);
		
		StateManager smanager = new StateManager();
		WorkflowException ex = null;
		try {
		    smanager.transition( entry, null );
		}
		catch(WorkflowException e) {
		    ex = e;
		}
		assertNotNull("null destination: an exception SHOULD have been thrown", ex);
    }
    public void testAtEnd() {
		
		// cannot change from the REMOVED state
		Entry entry = new Entry();
		entry.setState(Util.REMOVED_STATE);
		
		StateManager smanager = new StateManager();
		smanager.transition( entry, Util.OPEN_STATE );
	
		assertEquals("entry state SHOULD STILL be 'removed'", Util.REMOVED_STATE, entry.getState());
    }
    
}


