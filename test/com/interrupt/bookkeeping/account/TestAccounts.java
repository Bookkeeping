
package com.interrupt.bookkeeping.account;

import java.util.List;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;
import com.interrupt.bookkeeping.Bookkeeping;
import com.interrupt.bookkeeping.action.CloseAction;
import com.interrupt.bookkeeping.exception.AccountException;
import com.interrupt.bookkeeping.journal.Journals;
import com.interrupt.bookkeeping.journal.Journal;
import com.interrupt.bookkeeping.journal.IEntry;
import com.interrupt.bookkeeping.journal.Entries;
import com.interrupt.bookkeeping.journal.Entry;
import com.interrupt.bookkeeping.journal.Transaction;
import com.interrupt.bookkeeping.journal.Transactions;
import com.interrupt.util.IdGenerator;

public class TestAccounts extends TestCase {
    
    
	private Journal journal1 = null; 
	private Journal journal2 = null; 
	private Journals journals = null; 
	
    public TestAccounts() {
	}
    public TestAccounts(String name) {
		super(name);
    }
    
    
    /* SETUP / TEARDOWN
     */
    public void setUp() {
	
		
		// Setup the journals
		journal1 = new Journal(); 
		journal1.setId("j1");
		journal1.addEntries( new Entries("main.entries") );
		
		journal2 = new Journal(); 
		journal2.setId("j2");
		journal2.addEntries( new Entries("main.entries") );
		
		journals = new Journals(); 
		journals.addJournal(journal1); 
		journals.addJournal(journal2); 
		
	}
    public void tearDown() {
		
		//((Accounts)Accounts.getInstance()).purge();
		
		journal1 = null;
		journal2 = null; 
		journals = null; 
		
    }
    
    public static Test suite() {
		
		TestSuite suite = new TestSuite();
		//suite.addTest( new TestAccounts("testPurge") );
		//suite.addTest( new TestAccounts("testUniqueAccount") );
		
		suite.addTest( new TestAccounts("testBalanceAccounts") );
		//suite.addTest( new TestAccounts("testValidDebitCreditPointers") );
		
		return suite;
    }
    
    
    /* remove all accounts from system
     */
    public void testPurge() {
		
		IAccount account1 = new GAccount();
		account1.setId("001");
		IAccount account2 = new GAccount();
		account2.setId("002");
		IAccount account3 = new GAccount();
		account3.setId("01");
		IAccount account4 = new GAccount();
		account4.setId("02");
		
		Accounts accounts = new Accounts();
		accounts.addAccount(account1);
		accounts.addAccount(account2);
		accounts.addAccount(account3);
		accounts.addAccount(account4);
		assertEquals("'Accounts' should have 4 accounts", 4, (accounts.allAccount()).size());
	
		accounts.purge();
		assertEquals("'Accounts' should have 0 accounts", 0, (accounts.allAccount()).size());
    }
    
    /* ensure all accounts in system are unique (a unique 'id')
     */
    public void testUniqueAccount() {
		
		IAccount account1 = new GAccount();
		account1.setId("001");
		
		IAccount account2 = new GAccount();
		account2.setId("002");
	
		IAccount account3 = new GAccount();
		account3.setId("003");
	
		IAccount account4 = new GAccount();
		account4.setId("001");
		
		IAccounts accounts = new Accounts(); 
		
		// all unique accounts
		AccountException aexception = null;
		try {
		    accounts.addAccount(account1);
		    accounts.addAccount(account2);
		    accounts.addAccount(account3);
		}
		catch(AccountException e) {
		    aexception = e;
		}
		assertNull("'AccountException' should NOT have been thrown", aexception);
	
		// a duplicate id here
		aexception = null;
		try {
		    accounts.addAccount(account4);
		}
		catch(AccountException e) {
		    aexception = e;
		}
		assertNotNull("duplicate id -> 'AccountException' SHOULD be thrown", aexception);
		
    }

    public void testBalanceAccounts() {
		
		System.getProperties().setProperty(com.interrupt.bob.util.Util.DEF, "xml/bookkeeping.system.xml xml/bookkeeping.debitPointer.xml"); 
		System.getProperties().setProperty(com.interrupt.bob.util.Util.BASE, "."); 
		System.getProperties().setProperty(com.interrupt.bob.util.Util.END, ".xml"); 
		
		Bookkeeping bookkeeping = 
			(Bookkeeping)Bob.loadS(TestAccounts.class.getResourceAsStream("/test.balanceAccounts.xml"), "xml/bookkeeping.system.xml xml/bookkeeping.debitPointers.xml"); 
		
		CloseAction closeAction = new CloseAction();
		bookkeeping.accept(closeAction);
		
		boolean balances = ((Accounts)bookkeeping.getAccounts()).balances();
		assertTrue("'Accounts' does not balance", balances);
    }
    
    
}


