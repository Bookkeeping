package com.interrupt.bookkeeping.http;

import org.apache.cactus.*; 
import junit.framework.*; 
import java.net.HttpURLConnection; 

public class CactusTests extends ServletTestCase {
	
	
	public void setUp() { 
	}
	public void tearDown() { 
	}
	
	/**
	 * executed on the client side
	 */
	public void beginTestMethod(WebRequest theRequest) {
	}
	public void testMethod() { 
	}
	/**
	 * executed on the client side
	 */
	public void endTestMethod(HttpURLConnection theConnection) {
	}
}
