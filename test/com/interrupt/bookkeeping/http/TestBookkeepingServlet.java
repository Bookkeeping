package com.interrupt.bookkeeping.http;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.cactus.ServletTestCase;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;
import org.xmldb.api.base.XMLDBException;

import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;
import com.interrupt.bookkeeping.cc.kkell.aauth.TestAAuthentication;
import com.interrupt.bookkeeping.exception.AuthorisationException;
import com.interrupt.bookkeeping.users.User;
import com.interrupt.spittoon.Spittoon;
import com.interrupt.util.FileUtil;
import com.interrupt.util.Util;
import com.meterware.httpunit.PostMethodWebRequest;
import com.meterware.httpunit.WebRequest;
import com.meterware.httpunit.WebResponse;
import com.meterware.servletunit.ServletRunner;
import com.meterware.servletunit.ServletUnitClient;

import junit.framework.*;

public class TestBookkeepingServlet extends ServletTestCase { 
	
	
	private Logger logger = Logger.getLogger(TestBookkeepingServlet.class); 
	private Spittoon spittoon = null;  
	
	public TestBookkeepingServlet() { 
		
		super(); 
		
	}
	public TestBookkeepingServlet(String name) {
		
		super(name);
		
		//spittoon = Spittoon.instance();  
		
    }
    
	public static Test suite() {
		
		TestSuite suite = new TestSuite(); 
		/*
		suite.addTest( new TestBookkeepingServlet("testAddEntryBadInput") );
		suite.addTest( new TestBookkeepingServlet("testAddEntryGoodInput") );
		suite.addTest( new TestBookkeepingServlet("testSimultaneousAddRead") );
		suite.addTest( new TestBookkeepingServlet("testSimultaneousAddDifferentUser") );
		*/
		suite.addTest( new TestBookkeepingServlet("testInitialDBState") );
		//suite.addTest( new TestBookkeepingServlet("testAddUserGroup") ); 
		//suite.addTest( new TestBookkeepingServlet("testSimultaneousAddSameUser") );
		
		return suite;
	}
	
	
	public void setUp() { 
		
		logger.debug("TestBookkeepingServlet.setUp CALLED"); 
		
		//logger.debug("System Properties...");
		//java.lang.System.getProperties().list(System.out); 
		
		// BEGIN
		System.out.println("java.class.path["+ java.lang.System.getProperty("java.class.path") +"]"); 
		
		String resourceName = "setup.aauthentication.xml"; 
		InputStream istream = this.getClass().getResourceAsStream(resourceName);
		System.out.println("1. (\"setup.aauthentication.xml\") resoruce stream["+ istream +"]"); 
		
		String resourceName2 = "/setup.aauthentication.xml"; 
		InputStream istream2 = this.getClass().getResourceAsStream(resourceName2);
		System.out.println("2. (\"/setup.aauthentication.xml\") resoruce stream["+ istream2 +"]"); 
		
		String resourceName3 = "/setup.aauthentication.xml"; 
		ClassLoader parent = this.getClass().getClassLoader().getParent();
		System.out.println("3. PARENT["+ parent +"]"); 
		InputStream istream3 = parent.getResourceAsStream(resourceName3);
		System.out.println("3. PARENT > resoruce stream["+ istream3 +"]"); 
		
		
		InputStream is1 = this.getClass().getClassLoader().getResourceAsStream("setup.aauthentication.xml"); 
		InputStream is2 = this.getClass().getClassLoader().getResourceAsStream("/setup.aauthentication.xml"); 
		InputStream is3 = this.getClass().getClassLoader().getSystemResourceAsStream("setup.aauthentication.xml"); 
		InputStream is4 = this.getClass().getClassLoader().getSystemResourceAsStream("/setup.aauthentication.xml"); 
		
		System.out.println("is1 ["+ is1 +"]"); 
		System.out.println("is2 ["+ is2 +"]"); 
		System.out.println("is3 ["+ is3 +"]"); 
		System.out.println("is4 ["+ is4 +"]"); 
		
		
		ClassLoader cloader = this.getClass().getClassLoader(); 
		System.out.println("ClassLoader["+ cloader.toString() +"]"); 
		
		ClassLoader sloader = session.getClass().getClassLoader();  
		System.out.println("Session ClassLoader["+ sloader.toString() +"]"); 
		
		InputStream istream4 = session.getClass().getResourceAsStream(resourceName2);
		System.out.println("4. (/session) resoruce stream["+ istream4 +"]"); 

		istream4 = session.getClass().getResourceAsStream(resourceName);
		System.out.println("4. (session) resoruce stream["+ istream4 +"]"); 
		
		
		String textString = Util.loadTextFile("setup.aauthentication.xml"); 
		System.out.println("RESULT [ "+ textString +" ]"); 
		
		// END 
		
		
		//** setup 
		spittoon = new Spittoon(); 
		spittoon.initialise(); 
		spittoon.setupRoot(); 
		
		InputStream istreamAauth = session.getClass().getResourceAsStream("setup.aauthentication.xml"); 
		InputStream istreamGroups = session.getClass().getResourceAsStream("setup.groups.xml"); 
		
		String axml = Util.loadTextFile(istreamAauth); 
		String gxml = Util.loadTextFile(istreamGroups); 
		
		String axpath = "/system[ @id='main.system' ]"; 
		logger.debug("Aauthentication XPath["+ axpath +"]"); 
		String aurl = spittoon.getAauthDbUrl();  
		spittoon.createR(aurl, axpath, axml); 
		
		String gxpath = "/system[ @id='main.system' ]"; 
		logger.debug("Groups XPath["+ gxpath +"]"); 
		String gurl = spittoon.getGroupsDbUrl(); 
		spittoon.createR(gurl, gxpath, gxml); 
		
	}
	public void tearDown() { 
		
		logger.debug("TestBookkeepingServlet.tearDown CALLED"); 
		
		//** clean slate 
		spittoon.tearDownRoot(); 
		
	}
	
	
	/**
	 * our tests will start with a blank state (not in 'bookkeeping.system.xml') 
	 */
	public void testInitialDBState() { 
		
		logger.debug("TestBookkeepingServlet.testInitialDBState CALLED"); 
		
		//** load added user and group
		IBob aauthGDoc = null;	
			spittoon.retrieve("xmldb:exist://localhost:8080/exist/xmlrpc/db/", 
				"/system[ @id='main.system' ]/aauthentication[ @id='main.aauthentication' ]/groups[ @id='aauth.groups' ]", false); 
		
		assertNotNull("CANNOT find aauthGDoc in DB", aauthGDoc); 
		assertEquals("we do not have the groups doc with real id", "main.groups", aauthGDoc.getAttributeValue("id")); 
		
		IBob groupsDoc = null;	
		spittoon.retrieve("xmldb:exist://localhost:8080/exist/xmlrpc/db/", 
			"/system[ @id='main.system' ]/groups[ @id='main.groups' ]", false); 
		
		assertNotNull("CANNOT find groupsDoc in DB", groupsDoc); 
		assertEquals("we do not have the groups doc with real id", "main.groups", groupsDoc.getAttributeValue("id")); 
		
		logger.debug("TestBookkeepingServlet.testInitialDBState FINISHED"); 
		
	}
	
	
	/** 
	 * When adding a user, there will be a default group associated with it. 
	 */
	public void testAddUserGroup() throws FileNotFoundException, IOException, ServletException { 
		
		
		String fileContents = FileUtil.readTextFileAsStream("/add.usergroup.xml"); 
		request.setAttribute("data", fileContents); 
		request.setAttribute("cmd", "register"); 
		request.setAttribute("token", "group"); 
		
    	config.setInitParameter("db.url", "xmldb:exist://localhost:8090/exist/xmlrpc/db/bookkeeping/"); 
    	config.setInitParameter("exist.initdb", "false"); 
    	config.setInitParameter("exist.home", "C:\\Program Files\\eXist"); 
    	config.setInitParameter("org.xml.sax.features.validation", "false"); 
    	config.setInitParameter("java.endorsed.dirs", "WEB-INF\\lib\\endorsed"); 
    	
    	//** bring Spittoon into an initial state 
    	Spittoon spittoon = null; //Spittoon.instance();  
    	//spittoon.installInitialise();  
    	//spittoon.installCreate(); 
    	
		//** request will have <command/> payload in it 
		BookkeepingServlet bservlet = new BookkeepingServlet(); 
		bservlet.init(config); 
		bservlet.service(request, response); 
		
		
		
		//IBob groupDoc = groupsDoc.find("groups", "testuser.group"); 
		//assertNotNull("CANNOT find groupDoc in groups", groupDoc); 
		//assertEquals("do not have the group doc with specified id", "testuser.group"); 
		
	}
	
	
	/**
	 * A user has to come in a default group. Otherwise throw an error. 
	 */
	public void testAddUserWithoutGroup() { 
		
	}
	
	
	/**
	 * tests an invalid addition of a journal entry that does not balance 
	 * 
	 *	<journal:entry id="" entrynum="" state="" journalid="" date="">
	 *		<account:debit id="" amount="45.00" entryid="" accountid="1" account="office equipment"/>
	 *		<account:credit id="" amount="50.00" entryid="" accountid="3" account="bank"/>
	 *	</journal:entry>
	 */
	public void testAddEntryBadInput() { 
		
		//1. specify the correct user/group 
		
	}
	
	/**
	 * tests a valid entry that does balance 
	 * 		--> also ensure that Bookkeeping MODEL is reloaded 
	 * 
	 *	<journal:entry id="" entrynum="" state="" journalid="" date="">
	 *		<account:debit id="" amount="50.00" entryid="" accountid="1" account="office equipment"/>
	 *		<account:credit id="" amount="50.00" entryid="" accountid="3" account="bank"/>
	 *	</journal:entry>
	 */
	public void testAddEntryGoodInput() { 
		
	}
	
	/** 
	 * tests simultaneous http requests by same user to add valid entries. both requests should be handled one at a time
	 * 		--> also ensure that Bookkeeping MODEL is reloaded 
	 * 
	 * 	<journal:entry id="" entrynum="" state="" journalid="" date="">
	 *		<account:debit id="" amount="50.00" entryid="" accountid="1" account="office equipment"/>
	 *		<account:credit id="" amount="50.00" entryid="" accountid="3" account="bank"/>
	 *	</journal:entry>
	 *		
	 *	<journal:entry id="" entrynum="" state="" journalid="" date="">
	 *		<account:debit id="" amount="70.00" entryid="" accountid="1" account="office equipment"/>
	 *		<account:credit id="" amount="70.00" entryid="1" accountid="3" account="bank"/>
	 *	</journal:entry>
	 * 
	 */
	public void testSimultaneousAddSameUser() {
		
		
		//** clean slate 
		spittoon.tearDownRoot(); 
		
		
		//** setup 
		spittoon = new Spittoon(); 
		spittoon.initialise(); 
		spittoon.setupRoot(); 
		
		String axml = Util.loadTextFile("setup.aauthentication.xml"); 
		String gxml = Util.loadTextFile("setup.groups.xml"); 
		
		String axpath = "/system[ @id='main.system' ]"; 
		logger.debug("Aauthentication XPath["+ axpath +"]"); 
		String aurl = spittoon.getAauthDbUrl();  
		spittoon.createR(aurl, axpath, axml); 
		
		String gxpath = "/system[ @id='main.system' ]"; 
		logger.debug("Groups XPath["+ gxpath +"]"); 
		String gurl = spittoon.getGroupsDbUrl(); 
		spittoon.createR(gurl, gxpath, gxml); 
		
		
		//** create 2nd user
		User userStartingPoint = 
			(User)Bob.loadS(
				TestAAuthentication.class.getResourceAsStream("/add.user.xml"), 
					BookkeepingSystemProperties.instance().getProperty("bob.def")); 
		com.interrupt.bookkeeping.System system = new com.interrupt.bookkeeping.System(); 
		system.setSpittoon(spittoon); 
		system.initialise(); 
		try { 
			system.getAauthentication().addUser(userStartingPoint); 
		}
		catch(AuthorisationException e) { 
			e.printStackTrace(); 
		}
		
		
		
		//** http client calls i) add and ii) read 
		
		
	    
	}
	
	/**
	 * tests simultaneous http requests to add and read valid entries. both requests can be handled at the same time. 
	 *  
	 *  add: 
	 * 	<journal:entry id="" entrynum="" state="" journalid="" date="">
	 *		<account:debit id="" amount="50.00" entryid="" accountid="1" account="office equipment"/>
	 *		<account:credit id="" amount="50.00" entryid="" accountid="3" account="bank"/>
	 *	</journal:entry>
	 *
	 *	read: 
	 *	<journal:entry id='e1' entrynum='' state='' journalid='' date='' >  
     *		<account:debit id='abc' amount='10.00' entryid='e1' accountid='1' account="office equipment" />  
     *		<account:debit id='def' amount='1.50' entryid='e1' accountid='2' account="tax" />  
     *		<account:credit id='ghi' amount='11.50' entryid='e1' accountid='3' account="bank" />
     * </journal:entry>   
	 */
	public void testSimultaneousAddRead() { 
		
	}
	
	/**
	 * tests simultaneous http requests by different user to add valid entries. both requests should be handled one at a time
	 * 		--> also ensure that Bookkeeping MODEL is reloaded 
	 * 	
	 * 	'root' user:
	 * 	<journal:entry id="" entrynum="" state="" journalid="" date="">
	 *		<account:debit id="" amount="50.00" entryid="" accountid="1" account="office equipment"/>
	 *		<account:credit id="" amount="50.00" entryid="" accountid="3" account="bank"/>
	 *	</journal:entry>
	 *	
	 *	'tim' user: 	
	 *	<journal:entry id="" entrynum="" state="" journalid="" date="">
	 *		<account:debit id="" amount="70.00" entryid="" accountid="1" account="office equipment"/>
	 *		<account:credit id="" amount="70.00" entryid="1" accountid="3" account="bank"/>
	 *	</journal:entry>
	 */
	public void testSimultaneousAddDifferentUser() { 
		
	}
	
}
