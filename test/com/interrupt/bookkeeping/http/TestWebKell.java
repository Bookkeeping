package com.interrupt.bookkeeping.http;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import junit.framework.TestCase;

public class TestWebKell extends TestCase {
	
	public void testHomePage() throws Exception { 
		
	    final WebClient webClient = new WebClient();
	    final HtmlPage page = (HtmlPage) webClient.getPage("http://htmlunit.sourceforge.net");
	    assertEquals( "htmlunit - Welcome to HtmlUnit", page.getTitleText() );
	}
	
}
