package com.interrupt.bookkeeping;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import java.util.List;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.AttributesImpl;

import com.interrupt.bob.base.Bob;
import com.interrupt.bookkeeping.account.ICreditPointer;
import com.interrupt.bookkeeping.account.IDebit;
import com.interrupt.bookkeeping.account.GDebit;
import com.interrupt.bookkeeping.account.ICredit;
import com.interrupt.bookkeeping.account.GCredit;
import com.interrupt.bookkeeping.account.IAccount;
import com.interrupt.bookkeeping.account.GAccount;
import com.interrupt.bookkeeping.account.IAccounts;
import com.interrupt.bookkeeping.account.Accounts;
import com.interrupt.bookkeeping.account.IDebitPointer;
import com.interrupt.bookkeeping.cc.bkell.Bkell;
import com.interrupt.bookkeeping.cc.bkell.aauth.Aauthentication;
import com.interrupt.bookkeeping.cc.bkell.command.AbstractCommand;
import com.interrupt.bookkeeping.cc.bkell.command.FindCommand;
import com.interrupt.bookkeeping.cc.bkell.command.GCommand;
import com.interrupt.bookkeeping.cc.bkell.command.GOption;
import com.interrupt.bookkeeping.cc.bkell.command.IOption;
import com.interrupt.bookkeeping.cc.bkell.command.IOptions;
import com.interrupt.bookkeeping.cc.bkell.command.IResult;
import com.interrupt.bookkeeping.cc.bkell.command.IToken;
import com.interrupt.bookkeeping.cc.bkell.command.Token;
import com.interrupt.bookkeeping.cc.kkell.aauth.TestAAuthentication;

import com.interrupt.bookkeeping.journal.GJournal;
import com.interrupt.bookkeeping.journal.Journals;
import com.interrupt.bookkeeping.journal.IEntry;
import com.interrupt.bookkeeping.journal.GEntry;
import com.interrupt.bookkeeping.journal.Entry;
import com.interrupt.bookkeeping.journal.Entries;
import com.interrupt.bookkeeping.journal.ITransaction;
import com.interrupt.bookkeeping.journal.GTransaction;
import com.interrupt.bookkeeping.journal.Transaction;
import com.interrupt.bookkeeping.users.Group;
import com.interrupt.bookkeeping.users.IGroup;
import com.interrupt.bookkeeping.users.IUser;
import com.interrupt.bookkeeping.users.User;
import com.interrupt.bookkeeping.exception.AuthorisationException;
import com.interrupt.bookkeeping.exception.EntryException;
import com.interrupt.util.IdGenerator;

public class TestGeneral extends TestCase {
    
    
	private Logger logger = Logger.getLogger(TestGeneral.class); 
	private Bookkeeping bookkeeping = null; 
	
	private IAccount officeEquipAccount = null;
	private IAccount taxAccount = null;
	private IAccount bankAccount = null;
	private IAccount loanAccount = null;
	private Accounts accountsInstance = null;
	
	private GJournal journal1 = null; 
	private GJournal journal2 = null; 
	private Journals journals = null; 
	
	
    public TestGeneral() {
	}
    public TestGeneral(String name) {
		super(name);
    }
    
    
    /* SETUP / TEARDOWN
     */
    public void setUp() {
		
		
    	// setup the 'Accounts' object
    	officeEquipAccount = new GAccount();
    	officeEquipAccount.setType("asset");
    	officeEquipAccount.setId("01");
    	officeEquipAccount.setName("officeEquipAccount");
    	officeEquipAccount.setCounterWeight("debit");
    	
    	taxAccount = new GAccount();
    	taxAccount.setType("expense");
    	taxAccount.setId("02");
    	taxAccount.setName("taxAccount");
    	taxAccount.setCounterWeight("debit");
    	
    	bankAccount = new GAccount();
    	bankAccount.setType("asset");
    	bankAccount.setId("03");
    	bankAccount.setName("bankAccount");
    	bankAccount.setCounterWeight("debit");
    	
    	loanAccount = new GAccount();
    	loanAccount.setType("expense");
    	loanAccount.setId("04");
    	loanAccount.setName("loanAccount");
    	loanAccount.setCounterWeight("debit");

    	accountsInstance = new Accounts();
    	accountsInstance.purge();
    	accountsInstance.addAccount(officeEquipAccount);
    	accountsInstance.addAccount(taxAccount);
    	accountsInstance.addAccount(bankAccount);
    	accountsInstance.addAccount(loanAccount);
    	
    	
		// Setup the journals
		journal1 = new GJournal(); 
		journal1.setId("j1");
		journal1.addEntries( new Entries() );
		
		journal2 = new GJournal(); 
		journal2.setId("j2");
		journal2.addEntries( new Entries() );
		
		journals = new Journals(); 
		journals.addJournal(journal1); 
		journals.addJournal(journal2); 
		
		
		bookkeeping = new Bookkeeping(); 
		bookkeeping.setAccounts( accountsInstance ); 
		bookkeeping.setJournals(journals); 
		
	}
    public void tearDown() {}
    
    public static Test suite() {
		
		TestSuite suite = new TestSuite();
		//suite.addTest( new TestGeneral("testBalancingEntries") );
		suite.addTest( new TestGeneral("testDebitCreditPointersToAccount") );
		suite.addTest( new TestGeneral("testLoadMainGroups") );
		suite.addTest( new TestGeneral("testUnAuthorisedCommand") );
		
		return suite;
    }
    
    
    /* each debit/credit goes to an account
     */
    public void testDebitCreditPointersToAccount() {
		
    	
		logger.debug("testDebitCreditsToAccount >");
		
		// add & close transaction/entry
			
			// create debits/credits for officeEquipEntry
			IDebit oeDebit = new GDebit();
			oeDebit.setId(IdGenerator.generateId());
			oeDebit.setAccountid("01");
			oeDebit.setAmount("10.00");
			oeDebit.setCurrency("CDN"); 
			
			IDebit tDebit = new GDebit();
			tDebit.setId(IdGenerator.generateId());
			tDebit.setAccountid("02");
			tDebit.setAmount("1.50");
			tDebit.setCurrency("CDN"); 
			
			ICredit bCredit = new GCredit();
			bCredit.setId(IdGenerator.generateId());
			bCredit.setAccountid("03");
			bCredit.setAmount("11.50");
			bCredit.setCurrency("CDN"); 
			
		IEntry officeEquipEntry = new Entry();
		officeEquipEntry.setJournalid("j1");
		officeEquipEntry.setId(IdGenerator.generateId());
		officeEquipEntry.setCurrency("CDN"); 
		officeEquipEntry.addDebit(oeDebit);
		officeEquipEntry.addDebit(tDebit);
		officeEquipEntry.addCredit(bCredit);
		
		
		    // create debits/credits for loanEntry
		    IDebit lDebit = new GDebit();
		    lDebit.setId(IdGenerator.generateId());
		    lDebit.setAccountid("04");
		    lDebit.setAmount("1500.00");
		    lDebit.setCurrency("CDN"); 
			
		    ICredit bCredit2 = new GCredit();
		    bCredit2.setId(IdGenerator.generateId());
		    bCredit2.setAccountid("03");
		    bCredit2.setAmount("1500.00");
		    bCredit2.setCurrency("CDN"); 
			
		IEntry loanEntry = new Entry();
		loanEntry.setJournalid("j2");
		loanEntry.setId(IdGenerator.generateId());
		loanEntry.setCurrency("CDN"); 
		loanEntry.addDebit(lDebit);
		loanEntry.addCredit(bCredit2);
		
		
		// add entries
		Entries entries = new Entries();
		entries.addEntry(officeEquipEntry);
		entries.addEntry(loanEntry);
			
		Transaction transaction = new Transaction();
		bookkeeping.getTransactions().addTransaction(transaction); 
		
		transaction.setId(IdGenerator.generateId());
		transaction.addEntries(entries); 
		
		com.interrupt.bookkeeping.action.CloseAction closeAction = 
		    new com.interrupt.bookkeeping.action.CloseAction();
		transaction.accept(closeAction);
		
		// find resulting debits/credits
		IAccount oAccount = accountsInstance.findAccountById("01");
		IAccount bAccount = accountsInstance.findAccountById("02");
		IAccount lAccount = accountsInstance.findAccountById("03");
		IAccount aAccount = accountsInstance.findAccountById("04");
		
		IDebitPointer roDebit = oAccount.findDebitPointerByRefid(oeDebit.getId());
		IDebitPointer rbDebit = bAccount.findDebitPointerByRefid(tDebit.getId());
		ICreditPointer rlCredit = lAccount.findCreditPointerByRefid(bCredit.getId());
	
		IDebitPointer raDebit = aAccount.findDebitPointerByRefid(lDebit.getId());
		ICreditPointer raCredit = lAccount.findCreditPointerByRefid(bCredit2.getId());
	
		assertNotNull("roDebit should NOT be null", roDebit);
		assertNotNull("rbDebit should NOT be null", rbDebit);
		assertNotNull("rlCredit should NOT be null", rlCredit);
		assertNotNull("raDebit should NOT be null", raDebit);
		assertNotNull("raCredit should NOT be null", raCredit);
	
		assertEquals("oegen & result debit NOT same", oeDebit.getId(), roDebit.getRefid());
		assertEquals("tgen & result debit NOT same", tDebit.getId(), rbDebit.getRefid());
		assertEquals("bgen & result credit NOT same", bCredit.getId(), rlCredit.getRefid());
		assertEquals("lgen & result debit NOT same", lDebit.getId(), raDebit.getRefid());
		assertEquals("bgen2 & result credit NOT same", bCredit2.getId(), raCredit.getRefid());
		
    }
    
    public void testLoadMainGroups() { 
    	
    	
    	//Spittoon.instance().installInitialise(); 
		//Spittoon.instance().installDelete(); 
		//Spittoon.instance().installCreate(); 
		
		
    	// 1. load the system 
		com.interrupt.bookkeeping.System system = new com.interrupt.bookkeeping.System(); 
		system.initialise(); 
		
		IUser user = new User(); 
		user.setId("root"); 
		user.setUsername("root"); 
		user.setPassword("password"); 
		user.setLogintimeout("60000"); 
		
		
    	// 2. login group/user 
		String groupid = "webkell"; 
		String sessionid = system.authenticate(groupid, user); 
		
    	
    	// 3. retrieve group/bookkeeping data for that user 
    	IGroup bkGroup = system.findGroup(sessionid); 
    	assertNotNull("There SHOULD be a related group returned based on our sessionid", bkGroup); 
    	assertEquals("Found a different group than was expected via sessionid",groupid, bkGroup.getId()); 
    	
    	
    	// 4. trying retrieving group/bookkeping data for a group not logged in under(with error) 
    	IGroup bkGroupBadsession = null; 
    	AuthorisationException ae = null; 
    	try { 
    		bkGroupBadsession = system.findGroup("nullSessionId"); 
    	}
    	catch(AuthorisationException e) { 
    		ae = e; 
    	}
    	assertNotNull("There should be an exception thrown when trying to find a group with a bad session id", ae); 
    	assertNull("There should be no associated group with a null session", bkGroupBadsession); 
    	
    	/*
    	// 5. trying persisting group/bookkeping data for a group not logged in under(with error)
    	Group groupStartingPoint = 
			(Group)Bob.loadS(
				TestAAuthentication.class.getResourceAsStream("/add.group.xml"), BookkeepingSystemProperties.instance().getProperty("bob.def")); 
		system.addGroup(groupStartingPoint); 
		
		// now add root user to group 
		system.getAauthentication().addUserToGroup(groupStartingPoint.getId(), (User)user); 
		Spittoon.instance().update(system); 
		*/
    	
    }
    
    public void testUnAuthorisedCommand() { 

    	//Spittoon.instance().installInitialise(); 
		//Spittoon.instance().installDelete(); 
		//Spittoon.instance().installCreate(); 
		
    	// 1. load the system 
		com.interrupt.bookkeeping.System system = new com.interrupt.bookkeeping.System(); 
		system.initialise(); 
		
		
    	// 2. login group/user 
		IUser user = new User(); 
		user.setId("root"); 
		user.setUsername("root"); 
		user.setPassword("password"); 
		user.setLogintimeout("60000"); 
		
		
    	// 3. find (AUTHORISED) group/bookkeeping data for that user
    	IToken entryToken = new Token(); 
		entryToken.setName("entry"); 
		String optionsString = 
	        "<?xml version='1.0' encoding='UTF-8'?>" + 
			"<options xmlns='com/interrupt/bookkeeping/cc/bkell/command' id='entry.option' >" + 
	        "	<option name='entryid' value='' />" + 
	        //"	<option name='accountid' value='' />" + 
	        //"	<option name='amount' value='' />" + 
	        "</options>"; 
	    IOptions entryOptions = (IOptions)Bob.loadS(optionsString, BookkeepingSystemProperties.instance().getProperty("bob.def")); 
	    
	    
		GCommand gcommand = (GCommand)Bob.loadS(TestAAuthentication.class.getResourceAsStream("/test.command.find.xml"), BookkeepingSystemProperties.instance().getProperty("bob.def")); 
		FindCommand fcommand = new FindCommand(gcommand); 
    	
		logger.debug("system.getGroups() "+ system.getGroups().toXML()); 
    	
		fcommand.setToken(entryToken); 
    	fcommand.setTokenReciever(system.getGroups()); 
    	fcommand.setOptions(entryOptions); 
    	
    	logger.debug("FindCommand > "+ fcommand.toXML()); 
    	
    	Aauthentication aauth = Aauthentication.instance(); 
    	aauth.authenticate("webkell", user); 
    	IResult result = fcommand.execute(user); 
    	//logger.debug("FindCommand > result > "+ result.toXML()); 
    	
    	assertNotNull("We SHOULD have a result from the find command", result); 
    	
    	
    	// 4. try listing (UNAUTHORISED) group/bookkeeping data for that user
    	class StubCommand extends AbstractCommand {}
    	StubCommand scommand = new StubCommand(); 
    	AuthorisationException ae = null; 
    	try { 
    		scommand.execute(user); 
    	}
    	catch(AuthorisationException e) { 
    		ae = e; 
    	}
    	assertNotNull("There SHOULD be an exception for a command that root is not authorised", ae); 
    	
    }
    
}


