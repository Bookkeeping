
package com.interrupt.bookkeeping.journal;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import com.interrupt.bookkeeping.account.GDebit;
import com.interrupt.bookkeeping.account.GCredit;

public class TestEntry extends TestCase {
    
    
    public TestEntry() {}
    public TestEntry(String name) {
		super(name);
    }
    
    
    /* SETUP / TEARDOWN
     */
    public void setUp() {}
    public void tearDown() {}
    
    public static Test suite() {
		
		TestSuite suite = new TestSuite();
		suite.addTest( new TestEntry("testBalances") );
		return suite;
    }
    
    public void testBalances() {
		
		GDebit cashDebit = new GDebit();
		cashDebit.setAmount("11.50");
	
		GCredit salesCredit = new GCredit();
		salesCredit.setAmount("10.00");
		
		GCredit taxCredit = new GCredit();
		taxCredit.setAmount("1.50");
	
		Entry entry = new Entry();
		entry.addDebit(cashDebit);
		entry.addCredit(salesCredit);
		entry.addCredit(taxCredit);
		
		assertTrue("entry.balances() SHOULD return 'true'", entry.balances());
	
    }
	
	/* See 'testCloseEntry' in TestAction.java 
	 */
	
	
}


