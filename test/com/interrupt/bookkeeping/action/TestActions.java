
package com.interrupt.bookkeeping.action;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.io.File;
import java.util.List;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.AttributesImpl;

import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;
import com.interrupt.bookkeeping.account.IDebit;
import com.interrupt.bookkeeping.account.GDebit;
import com.interrupt.bookkeeping.account.ICredit;
import com.interrupt.bookkeeping.account.GCredit;
import com.interrupt.bookkeeping.account.IAccount;
import com.interrupt.bookkeeping.account.GAccount;
import com.interrupt.bookkeeping.account.IAccounts;
import com.interrupt.bookkeeping.account.Accounts;
import com.interrupt.bookkeeping.journal.IEntries;
import com.interrupt.bookkeeping.journal.Journals;
import com.interrupt.bookkeeping.journal.GJournal;
import com.interrupt.bookkeeping.journal.Entries;
import com.interrupt.bookkeeping.journal.IEntry;
import com.interrupt.bookkeeping.journal.GEntry;
import com.interrupt.bookkeeping.journal.Entry;
import com.interrupt.bookkeeping.journal.ITransaction;
import com.interrupt.bookkeeping.journal.GTransaction;
import com.interrupt.bookkeeping.journal.Transaction;
import com.interrupt.bookkeeping.exception.EntryException;
import com.interrupt.util.IdGenerator;

import com.interrupt.bookkeeping.action.CloseAction;
import com.interrupt.bookkeeping.action.RemoveAction;
import com.interrupt.bookkeeping.exception.WorkflowException;
import com.interrupt.bookkeeping.util.Util;


public class TestActions extends TestCase {
	
    
    Logger logger = Logger.getLogger(TestActions.class);
	
    private Accounts accountsInstance = null;  
    private IEntry officeEquipEntry = new Entry();
    private IEntry loanEntry = new Entry();
	
	private GJournal journal1 = null; 
	private GJournal journal2 = null; 
	private Journals journals = null; 
    
    private IDebit oeDebit = null;
    private IDebit tDebit = null;
    private ICredit bCredit = null;
    
    private IDebit lDebit = null;
    private ICredit bCredit2 = null;
    
    public TestActions() {}
    public TestActions(String name) {
		super(name);
    }
    

    /* SETUP / TEARDOWN
     */
    public void setUp() {
		
		
		// Setup the journals
		journal1 = new GJournal(); 
		journal1.setId("j1");
		journal1.addEntries( new Entries() );
		
		journal2 = new GJournal(); 
		journal2.setId("j2");
		journal2.addEntries( new Entries() );
		
		journals = new Journals(); 
		journals.addJournal(journal1); 
		journals.addJournal(journal2); 
		
		//** setup the DEFALT 'Accounts' object
		IAccount officeEquipAccount = new GAccount();
		officeEquipAccount.setType("asset");
		officeEquipAccount.setId("01");
		officeEquipAccount.setName("officeEquipAccount");
		officeEquipAccount.setCounterWeight("debit");
	
		IAccount taxAccount = new GAccount();
		taxAccount.setType("expense");
		taxAccount.setId("02");
		taxAccount.setName("taxAccount");
		taxAccount.setCounterWeight("debit");
	
		IAccount bankAccount = new GAccount();
		bankAccount.setType("asset");
		bankAccount.setId("03");
		bankAccount.setName("bankAccount");
		bankAccount.setCounterWeight("debit");
	
		IAccount loanAccount = new GAccount();
		loanAccount.setType("expense");
		loanAccount.setId("04");
		loanAccount.setName("loanAccount");
		loanAccount.setCounterWeight("debit");
	
		accountsInstance = new Accounts(); 
		accountsInstance.purge();
		//accountsInstance.setNamespace("");
		//accountsInstance.setName("accounts");
		accountsInstance.addAccount(officeEquipAccount);
		accountsInstance.addAccount(taxAccount);
		accountsInstance.addAccount(bankAccount);
		accountsInstance.addAccount(loanAccount);
		
		
		    //** create DEFAULT debits/credits for officeEquipEntry
		    oeDebit = new GDebit();
		    oeDebit.setId(IdGenerator.generateId());
		    oeDebit.setAccountid("01");
		    oeDebit.setAmount("10.00");
		
		    tDebit = new GDebit();
		    tDebit.setId(IdGenerator.generateId());
		    tDebit.setAccountid("02");
		    tDebit.setAmount("1.50");
		
		    bCredit = new GCredit();
		    bCredit.setId(IdGenerator.generateId());
		    bCredit.setAccountid("03");
		    bCredit.setAmount("11.50");
			
		officeEquipEntry = new Entry();
		officeEquipEntry.setId(IdGenerator.generateId());
		officeEquipEntry.addDebit(oeDebit);
		officeEquipEntry.addDebit(tDebit);
		officeEquipEntry.addCredit(bCredit);
		
		    // create debits/credits for loanEntry
		    lDebit = new GDebit();
		    lDebit.setId(IdGenerator.generateId());
		    lDebit.setAccountid("04");
		    lDebit.setAmount("1500.00");
	
		    bCredit2 = new GCredit();
		    bCredit2.setId(IdGenerator.generateId());
		    bCredit2.setAccountid("03");
		    bCredit2.setAmount("1500.00");
		    
		loanEntry = new Entry();
		loanEntry.setId(IdGenerator.generateId());
		loanEntry.addDebit(lDebit);
		loanEntry.addCredit(bCredit2);
    }
    public void tearDown() {
		
		journal1 = null;
		journal2 = null; 
		journals = null; 
		
		accountsInstance = null;
    }
    
    public static Test suite() {
		
		TestSuite suite = new TestSuite();
		/*suite.addTest( new TestActions("testCloseEntry") );
		suite.addTest( new TestActions("testRemoveEntry") );
		suite.addTest( new TestActions("testReverseEntry") );
		suite.addTest( new TestActions("testCloseEntryFromXMLString") );
		suite.addTest( new TestActions("testCloseEntryFromFile") );
		*/
		suite.addTest( new TestActions("testFindEntryById") );
		suite.addTest( new TestActions("testFindEntryByDate") );
		suite.addTest( new TestActions("testListEntryByDate") );
		
		suite.addTest( new TestActions("testListEntryBeforeDate") );
		suite.addTest( new TestActions("testListEntryAfterDate") );
		
		return suite;
    }


    /* test adding an entry; which means it's in a 'CLOSED' state 
     * Here, we are just adding objects. Below, find loading from 
     * 	i) XML 
     * 	ii) File 
     */
    public void testCloseEntry() {
		
		logger.debug("testAddEntry");
		
		officeEquipEntry.setJournalid("j1");
		loanEntry.setJournalid("j2"); 
		
		// add & close transaction/entry
		Entries entries = new Entries();
		entries.addEntry(officeEquipEntry);
		entries.addEntry(loanEntry);
		
		Transaction transaction = new Transaction();
		transaction.setId(IdGenerator.generateId());
		transaction.addEntries(entries); 
		
		CloseAction closeAction = new CloseAction();
		//closeAction.execute(transaction);
		
		
		// find resulting debits/credits in corresponding accounts - a repeat of 'TestGeneral'
		IAccount oAccount = accountsInstance.findAccountById("01");
		IAccount bAccount = accountsInstance.findAccountById("02");
		IAccount lAccount = accountsInstance.findAccountById("03");
		IAccount aAccount = accountsInstance.findAccountById("04");
		
		IDebit roDebit = oAccount.findDebitById(oeDebit.getId());
		IDebit rbDebit = bAccount.findDebitById(tDebit.getId());
		ICredit rlCredit = lAccount.findCreditById(bCredit.getId());
		
		IDebit raDebit = aAccount.findDebitById(lDebit.getId());
		ICredit raCredit = lAccount.findCreditById(bCredit2.getId());
		
		assertNotNull("roDebit should NOT be null", roDebit);
		assertNotNull("rbDebit should NOT be null", rbDebit);
		assertNotNull("rlCredit should NOT be null", rlCredit);
		assertNotNull("raDebit should NOT be null", raDebit);
		assertNotNull("raCredit should NOT be null", raCredit);
		
		// determine states of entries
		assertEquals("state in officeEquipEntry is NOT 'CLOSED'", Util.CLOSED_STATE, officeEquipEntry.getState());
		assertEquals("state in loanEntry is NOT 'CLOSED'", Util.CLOSED_STATE, loanEntry.getState());
		assertEquals("state in transaction is NOT 'CLOSED'", Util.CLOSED_STATE, transaction.getState());
		
	}
    public void testCloseEntryFromXMLString() { 
    	
    	
    	System.getProperties().setProperty(com.interrupt.bob.util.Util.BASE, "."); 
        System.getProperties().setProperty(com.interrupt.bob.util.Util.END, ".xml"); 
        System.getProperties().setProperty(com.interrupt.bob.util.Util.DEF, "xml/bookkeeping.2.bookkeeping.xml xml/bookkeeping.2.transactions.xml"); 
        
        
    	String xmlString = "<entries id='' xmlns='com/interrupt/bookkeeping/journal' >  " + 
    		"	<entry id='e1' entrynum='' state='' journalid='' date='' >  " + 
    		"	    <debit xmlns='com/interrupt/bookkeeping/account' id='d1' amount='10.00' entryid='' accountid='01' />  " + 
    		"	    <debit xmlns='com/interrupt/bookkeeping/account' id='d2' amount='1.50' entryid='' accountid='02' />  " + 
    		"	    <credit xmlns='com/interrupt/bookkeeping/account' id='c1' amount='11.50' entryid='' accountid='03' />  " + 
    		"	</entry>  " + 
    		"</entries> "; 
    	
    	
    	IBob rootBob = new Bob(); 
    	IEntries jentries = (IEntries)rootBob.load(xmlString, "xml/bookkeeping.2.bookkeeping.xml xml/bookkeeping.2.transactions.xml"); 
    	assertNotNull("loaded 'IEntries' is NULL", jentries); 
    	
    	CloseAction closeAction = new CloseAction();
		//closeAction.execute(jentries);
		
		IAccount oAccount = accountsInstance.findAccountById("01");
		IAccount bAccount = accountsInstance.findAccountById("02");
		IAccount lAccount = accountsInstance.findAccountById("03");
		
		IDebit roDebit = oAccount.findDebitById("d1");
		IDebit rbDebit = bAccount.findDebitById("d2");
		ICredit rlCredit = lAccount.findCreditById("c1");
		
		assertNotNull("roDebit should NOT be null", roDebit);
		assertNotNull("rbDebit should NOT be null", rbDebit);
		assertNotNull("rlCredit should NOT be null", rlCredit);
		
    }
    public void testCloseEntryFromFile() { 
    	
    	
    	System.getProperties().setProperty(com.interrupt.bob.util.Util.BASE, "."); 
        System.getProperties().setProperty(com.interrupt.bob.util.Util.END, ".xml"); 
        System.getProperties().setProperty(com.interrupt.bob.util.Util.DEF, "xml/bookkeeping.2.bookkeeping.xml xml/bookkeeping.2.transactions.xml"); 
        
        
    	IBob rootBob = new Bob(); 
    	IEntries jentries = (IEntries)rootBob.load(new File("test/xml/entries.testclosefromfile.xml"), "xml/bookkeeping.2.bookkeeping.xml xml/bookkeeping.2.transactions.xml"); 
    	assertNotNull("loaded 'IEntries' is NULL", jentries); 
    	
    	CloseAction closeAction = new CloseAction();
		//closeAction.execute(jentries);
		
		IAccount oAccount = accountsInstance.findAccountById("01");
		IAccount bAccount = accountsInstance.findAccountById("02");
		IAccount lAccount = accountsInstance.findAccountById("03");
		
		IDebit roDebit = oAccount.findDebitById("d1");
		IDebit rbDebit = bAccount.findDebitById("d2");
		ICredit rlCredit = lAccount.findCreditById("c1");
		
		assertNotNull("roDebit should NOT be null", roDebit);
		assertNotNull("rbDebit should NOT be null", rbDebit);
		assertNotNull("rlCredit should NOT be null", rlCredit);
		
    }
    public void testRemoveEntry() {
		
		Entries entries = new Entries();
		entries.addEntry(officeEquipEntry);
		entries.addEntry(loanEntry);
		
		Transaction transaction = new Transaction();
		transaction.setId(IdGenerator.generateId());
		transaction.addEntries(entries);
		
		RemoveAction removeAction = new RemoveAction();
		WorkflowException we = null;
		try {
		    //removeAction.execute(transaction);
		}
		catch(WorkflowException e) {
		    we = e;
		    logger.debug(">>> WorkflowException thrown: " + we.getMessage());
		}
		assertNotNull("There SHOULD be a WorkflowException - state is not CLOSED", we);
		
		
		we = null;
		officeEquipEntry.setState(Util.CLOSED_STATE);
		loanEntry.setState(Util.CLOSED_STATE);
		transaction.setState(Util.CLOSED_STATE);
		try {
		    //removeAction.execute(transaction);
		}
		catch(WorkflowException e) {
		    we = e;
		}
		assertNull("WorkflowException SHOULD be NULL", we);
		
		assertEquals("officeEquipEntry SHOULD be 'REMOVED'", Util.REMOVED_STATE, officeEquipEntry.getState());
		assertEquals("loanEntry SHOULD be 'REMOVED'", Util.REMOVED_STATE, loanEntry.getState());
		assertEquals("transaction SHOULD be 'REMOVED'", Util.REMOVED_STATE, transaction.getState());
    }
	
    public void testReverseEntry() {
		
		Entries entries = new Entries();
		entries.addEntry(officeEquipEntry);
		entries.addEntry(loanEntry);
	
		Transaction transaction = new Transaction();
		transaction.setId(IdGenerator.generateId());
		transaction.addEntries(entries);
		
		ReverseAction reverseAction = new ReverseAction();
		WorkflowException we = null;
		try {
		    //reverseAction.execute(transaction);
		}
		catch(WorkflowException e) {
		    we = e;
		    logger.debug(">>> WorkflowException thrown: " + we.getMessage());
		}
		assertNotNull("There SHOULD be a WorkflowException - state is not CLOSED", we);
		
		
		we = null;
		officeEquipEntry.setState(Util.CLOSED_STATE);
		loanEntry.setState(Util.CLOSED_STATE);
		transaction.setState(Util.CLOSED_STATE);
		try {
		    //reverseAction.execute(transaction);
		}
		catch(WorkflowException e) {
		    we = e;
		}
		assertNull("WorkflowException SHOULD be NULL", we);
		
		assertEquals("officeEquipEntry SHOULD be 'REVERSED'", Util.REVERSED_STATE, officeEquipEntry.getState());
		assertEquals("loanEntry SHOULD be 'REVERSED'", Util.REVERSED_STATE, loanEntry.getState());
		assertEquals("transaction SHOULD be 'REVERSED'", Util.REVERSED_STATE, transaction.getState());
		
    }
	
    public void testFindEntryById() { 
    	

        System.getProperties().setProperty( "bob.base", "." ); 
        System.getProperties().setProperty( "bob.end", ".xml" ); 
        
    	IBob rootBob = new Bob(); 
    	Entries jentries = (Entries)rootBob.load(new File("test/xml/entries.testclosefromfile.xml"), "xml/bookkeeping.2.bookkeeping.xml xml/bookkeeping.2.transactions.xml"); 
    	assertNotNull("loaded 'IEntries' is NULL", jentries); 
    	
    	Entry foundEntry = (Entry)jentries.findEntryById("e1"); 
    	assertNotNull("Cannot find Entry with id[e1]", foundEntry); 
    	
    	String id_s = foundEntry.getId(); 
    	assertEquals( "Entry ids are not equal", "e1", id_s ); 
    	
    }
    
    public void testFindEntryByDate() { 
    	
    	
        System.getProperties().setProperty( "bob.base", "." ); 
        System.getProperties().setProperty( "bob.end", ".xml" ); 
        
    	IBob rootBob = new Bob(); 
    	Entries jentries = (Entries)rootBob.load(new File("test/xml/entries.testclosefromfile.xml"), "xml/bookkeeping.2.bookkeeping.xml xml/bookkeeping.2.transactions.xml"); 
    	assertNotNull("loaded 'IEntries' is NULL", jentries); 
    	
    	Entry foundEntry = (Entry)jentries.findEntryByDate("08/15/2006 00:00:00 EST"); 
    	assertNotNull( "Cannot find Entry with date[08/15/2006 00:00:00 EST]", foundEntry ); 
    	
    	String result_s = foundEntry.getDate(); 
    	assertEquals( "Entry dates are not equal", "08/15/2006 00:00:00 EST", result_s ); 
    	
    }
    
    public void testListEntryByDate() { 
    	
    	
        System.getProperties().setProperty( "bob.base", "." ); 
        System.getProperties().setProperty( "bob.end", ".xml" ); 
        
    	IBob rootBob = new Bob(); 
    	Entries jentries = (Entries)rootBob.load(new File("test/xml/entries.testclosefromfile.xml"), "xml/bookkeeping.2.bookkeeping.xml xml/bookkeeping.2.transactions.xml"); 
    	assertNotNull("loaded 'IEntries' is NULL", jentries); 
    	
    	List entryList = jentries.listEntryByDate("08/15/2006 00:00:00 EST"); 
    	
    	assertNotNull( "NULL Entry List with date[08/15/2006 00:00:00 EST]", entryList ); 
    	assertEquals( "Cannot find 2 Entries with date[08/15/2006 00:00:00 EST]", 2, entryList.size() ); 
    	
    }
    
    
    public void testListEntryBeforeDate() { 
    	
    	
        System.getProperties().setProperty( "bob.base", "." ); 
        System.getProperties().setProperty( "bob.end", ".xml" ); 
        
    	IBob rootBob = new Bob(); 
    	Entries jentries = (Entries)rootBob.load(new File("test/xml/entries.testclosefromfile.xml"), "xml/bookkeeping.2.bookkeeping.xml xml/bookkeeping.2.transactions.xml"); 
    	assertNotNull("loaded 'IEntries' is NULL", jentries); 
    	
    	List entryList = jentries.listEntryBeforeDate("08/15/2006 00:00:00 EST"); 
    	
    	assertNotNull( "NULL Entry List with date BEFORE[08/15/2006 00:00:00 EST]", entryList ); 
    	assertEquals( "Cannot find 2 Entries with date BEFORE[08/15/2006 00:00:00 EST]", 2, entryList.size() ); 
    	
    }
    
    public void testListEntryAfterDate() { 
    	
    	
        System.getProperties().setProperty( "bob.base", "." ); 
        System.getProperties().setProperty( "bob.end", ".xml" ); 
        
    	IBob rootBob = new Bob(); 
    	Entries jentries = (Entries)rootBob.load(new File("test/xml/entries.testclosefromfile.xml"), "xml/bookkeeping.2.bookkeeping.xml xml/bookkeeping.2.transactions.xml"); 
    	assertNotNull("loaded 'IEntries' is NULL", jentries); 
    	
    	List entryList = jentries.listEntryAfterDate("08/15/2006 00:00:00 EST"); 
    	
    	assertNotNull( "NULL Entry List with date AFTER[08/15/2006 00:00:00 EST]", entryList ); 
    	assertEquals( "Cannot find 3 Entries with date AFTER[08/15/2006 00:00:00 EST]", 3, entryList.size() ); 
    	
    }
    
    
}


