package com.interrupt.bookkeeping;

import java.io.File;

import junit.framework.TestCase;

import org.xmldb.api.base.XMLDBException;

import com.interrupt.bookkeeping.system.BookkeepingSystem;

public class AbstractBookkeepingTestCase extends TestCase {
	
	public AbstractBookkeepingTestCase() { 
		
		super();
	}
	public AbstractBookkeepingTestCase(String name) {
		
		super(name); 
		BookkeepingSystem.instance(); 
		
    }
    
}
