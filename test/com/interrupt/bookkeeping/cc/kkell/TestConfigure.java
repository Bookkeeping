package com.interrupt.bookkeeping.cc.kkell;

import org.xmldb.api.base.XMLDBException;

import com.interrupt.bob.base.IBob;
import com.interrupt.bookkeeping.cc.bkell.Configure;
import com.interrupt.bookkeeping.cc.bkell.aauth.IAauthentication;
import com.interrupt.bookkeeping.cc.kkell.aauth.TestAAuthentication;
import com.interrupt.bookkeeping.users.IGroups;
import com.interrupt.spittoon.Spittoon;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class TestConfigure extends TestCase {
	
	
	private Spittoon spittoon = null; 
	
	public TestConfigure(String name) { 
		super(name); 
	}
	
	public void tearDown() { 
		
		spittoon.tearDownRoot(); 
	}
	
	public void testConfigure() { 
		
		
		//** this should setup everything 
		Configure.main(new String[]{}); 
		
		spittoon = new Spittoon(); 
		spittoon.initialise(); 
		
		//** see if /system[ @id='main.system' ]/aauthentication[ @id='main.authentication' ] was created in aauth DB 
		IBob loadedAauth = spittoon.retrieve(spittoon.getAauthDbUrl(), 
			"/system[ @id='main.system' ]/aauthentication[ @id='main.authentication' ]", true); 
		
		
		//** see if /system[ @id='main.system' ]/groups[ @id='main.groups' ] was created in groups DB 
		IBob loadedGroups = spittoon.retrieve(spittoon.getGroupsDbUrl(), 
				"/system[ @id='main.system' ]/groups[ @id='main.groups' ]", true); 
		
		
		assertNotNull("loadedAauth is NULL", loadedAauth); 
		assertNotNull("loadedGroups is NULL", loadedGroups); 
		
		assertTrue("loadedAauth is NOT an 'IAauthentication'", (loadedAauth instanceof IAauthentication)); 
		assertTrue("loadedGroups is NOT an 'IGroups'", (loadedGroups instanceof IGroups)); 
				
	}
	
	public static Test suite() throws XMLDBException {
		
		TestSuite suite = new TestSuite(); 
		suite.addTest( new TestConfigure("testConfigure") ); 
		return suite;
	}
	
}
