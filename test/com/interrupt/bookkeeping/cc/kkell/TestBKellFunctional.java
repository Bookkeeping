package com.interrupt.bookkeeping.cc.kkell;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.StringReader;

import org.apache.log4j.Logger;

import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;
import com.interrupt.bookkeeping.ISystem;
import com.interrupt.bookkeeping.cc.bkell.Bkell;
import com.interrupt.bookkeeping.exception.BkellException;
import com.interrupt.bookkeeping.system.BookkeepingSystem;
import com.interrupt.bookkeeping.users.IUserSession;

import junit.framework.TestCase;

public class TestBKellFunctional extends TestCase {
	
	private Bkell bkell = null; 
	
	public TestBKellFunctional() { 
		
		System.getProperties().setProperty(com.interrupt.bob.util.Util.DEF, BookkeepingSystemProperties.instance().getProperty("bob.def")); 
		System.getProperties().setProperty(com.interrupt.bob.util.Util.BASE, "."); 
		System.getProperties().setProperty(com.interrupt.bob.util.Util.END, ".xml"); 
		
	}
	
	protected void setUp() throws Exception {
		
		super.setUp(); 
		
		/*ISystem bkellSystem = 
			(ISystem)Bob.loadS( 
				Bkell.class.getResourceAsStream("/bookkeeping.system.xml"), 
					BookkeepingSystemProperties.instance().getProperty("bob.def") ); 
		bkell = (Bkell)bkellSystem.findBkellById("bkell.main"); 
		Thread bkellThread = new Thread(bkell); 
		*/
		
		/*bkellThread.start(); 
		try { 
			
			// 'join()' method is a block method for the thread
			bkellThread.join();
		}
		catch(InterruptedException e) { 
			e.printStackTrace();
		}
		*/
	}
	
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	
	// login ( user -username root -password password );	<bkell.functional.login.bk> 
	// exit; 
	
	// load ( users -id aauth.users ); 						<bkell.functional.load.bk>
	
	// create ( debit -id erfg -amount 123.33 ); 			<bkell.functional.create1.bk>
	// create ( <debit xmlns='com/interrupt/bookkeeping/account' id='def' amount='1.50' /> ); 
    
    // load ( users -id aauth.users ); 						<bkell.functional.load1.bk>
    // load ( debit -amount 10.00 ); 						<bkell.functional.load2.bk>
    // load ( debit -id abc -amount 10.00 ); 				<bkell.functional.load3.bk>
    // load ( <debit xmlns='com/interrupt/bookkeeping/account' id='def' amount='1.50' /> ); 
    
	public synchronized void testLogin() { 
		
		
		//com.interrupt.bookkeeping.System system = new com.interrupt.bookkeeping.System(); 
		//system.initialise(); 
		
		//BookkeepingSystem bsystem = BookkeepingSystem.instance(); 
		//bsystem.setMODEL(system); //TODO - refactor how BookkeepingSystem gets a MODEL 
		
		
		InputStream istream = TestBKellFunctional.class.getResourceAsStream("/bkell.functional.login.bk"); 
		//String bkellString = "login ( user -username root -password password );"; 
		//InputStream istream = new java.io.StringBufferInputStream(bkellString); 
		
		ISystem bkellSystem = 
			(ISystem)Bob.loadS( 
				Bkell.class.getResourceAsStream("/bookkeeping.system.xml"), 
					BookkeepingSystemProperties.instance().getProperty("bob.def") ); 
		Bkell bkell = (Bkell)bkellSystem.findBkellById("bkell.main"); 
		
		//Bkell bkell = new Bkell(); 
		bkell.setInputStream(istream); 
		
		
		BkellException ee = null; 
		try { 
			bkell.run(); 
		}
		catch(BkellException e) { 
			
			//e.printStackTrace(); 
			ee = e; 
		}
		//assertNull("There should be no exception when running the 'bkell.functional.login.bk' script", ee); 
		
		IBob commandResult = bkell.getPreviousCommandResult(); 
		assertNotNull("There should be a command result after logging in", commandResult); 
		assertTrue("The command result SHOULD be an IUserSession", (commandResult instanceof IUserSession) ); 
		
	}
	
}

