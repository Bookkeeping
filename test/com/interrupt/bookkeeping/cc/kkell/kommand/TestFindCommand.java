
package com.interrupt.bookkeeping.cc.kkell.kommand;

import java.io.File;
import java.util.List; 
import org.apache.log4j.Logger;

import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;
import com.interrupt.bookkeeping.account.GDebit; 
import com.interrupt.bookkeeping.account.GCredit; 
import com.interrupt.bookkeeping.account.IDebit;
import com.interrupt.bookkeeping.journal.IEntry;
import com.interrupt.bookkeeping.journal.IJournal;

import com.interrupt.bookkeeping.Bookkeeping;
import com.interrupt.bookkeeping.GSystem;
import com.interrupt.bookkeeping.ISystem;
import com.interrupt.bookkeeping.TestGeneral;
import com.interrupt.bookkeeping.cc.bkell.Bkell;
import com.interrupt.bookkeeping.cc.bkell.IBkell;

import com.interrupt.bookkeeping.cc.bkell.*;
import com.interrupt.bookkeeping.cc.bkell.command.*;

import com.interrupt.bookkeeping.journal.Entry;
import com.interrupt.bookkeeping.exception.CommandException; 

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class TestFindCommand extends TestCase {
	
	
	private Logger logger = Logger.getLogger(TestFindCommand.class); 
	private ISystem bsystem = null; 
	private ITokens tokens = null; 
	private ICommands commands = null; 
	
    public TestFindCommand() {
	}
    public TestFindCommand(String name) {
		super(name);
    }
    
	protected void setUp() throws Exception {
		
		
		super.setUp(); 
		
		System.getProperties().setProperty(com.interrupt.bob.util.Util.DEF, "xml/"); 
		System.getProperties().setProperty(com.interrupt.bob.util.Util.BASE, "."); 
		System.getProperties().setProperty(com.interrupt.bob.util.Util.END, ".xml"); 
		
		
		Bob genericBob = new Bob(); 
		bsystem = (GSystem)genericBob.load( new File("xml/bookkeeping.2.users.xml") ); 
		
		IBkell bkell = bsystem.findBkellById("bkell.main"); 
		tokens = bkell.findTokensById("tokens.def"); 
		commands = bkell.findCommandsById("commands.def"); 
		
		logger.debug( "TestFindCommand > tokens.def > "+ tokens ); 
		logger.debug( "TestFindCommand > commands.def > "+ commands ); 
		
		logger.debug( "tokens list > "+ bkell.allTokens().size() ); 
		
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	
    public static Test suite() {
		
		TestSuite suite = new TestSuite();
		suite.addTest( new TestFindCommand("test001") );
		
		return suite;
    }
    
	
	
	public void test001() { 
		
		
		IBob bobby = new Bob(); 
		
		// Testing the supposed command:
		// find ((token.literal)debit -entry 32 -account expenses -amount '10.00') 
		
		logger.debug( "test001 > tokens > "+ tokens ); 
		logger.debug( "test001 > tokens.children > "+ tokens.allChildren() ); 
		
		
		FindCommand find_cmd = (com.interrupt.bookkeeping.cc.bkell.command.FindCommand)
		Bob.make( "com.interrupt.bookkeeping.cc.bkell.command.FindCommand" ); 
		
		
		// set the possible tokens 
		find_cmd.setTokens(commands.findCommandByName("find").findTokensById("find.tokens")); 
		
		
		// set the token we are looking for 
		Token token = (Token)tokens.findTokenByName("debit"); 
		find_cmd.setToken(token); 
		
		
		// from the set of possible tokens, there should be 1 set of options for a given input token
		IOptionSet optionSet = token.getOptionSet(); 
		optionSet.removeOptionsById("entry.option"); 
		token.setOptionSet(optionSet); 
		
		
		// set the token.reciever 
		Bookkeeping bookkeeping = (Bookkeeping)bobby.load( new File("xml/bookkeeping.2.bookkeeping.xml") ); 
		
		IJournal journalReciever = bookkeeping.getJournals().findJournalByName("generalledger"); 
		logger.debug("ZZzzzzz["+bookkeeping.getJournals().toXML()+"]"); 
		
		find_cmd.setTokenReciever(journalReciever); 
		
		// add the option (only 1)
		IOption idOption = new GOption(); 
		idOption.setName("id"); 
		idOption.setValue("def"); 
		
		find_cmd.addOption(idOption);
		
		
		// execute the command 
		logger.debug("FIND command right before execute["+find_cmd.toXML()+"]"); 
		
	}
	
}



