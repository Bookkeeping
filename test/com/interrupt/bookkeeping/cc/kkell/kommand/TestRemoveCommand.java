package com.interrupt.bookkeeping.cc.kkell.kommand;

import java.io.File;
import java.util.List; 
import org.apache.log4j.Logger;

import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;
import com.interrupt.bookkeeping.account.GDebit; 
import com.interrupt.bookkeeping.account.GCredit; 
import com.interrupt.bookkeeping.journal.IEntry;

import com.interrupt.bookkeeping.GSystem;
import com.interrupt.bookkeeping.ISystem;
import com.interrupt.bookkeeping.TestGeneral;
import com.interrupt.bookkeeping.cc.bkell.Bkell;
import com.interrupt.bookkeeping.cc.bkell.IBkell;
import com.interrupt.bookkeeping.cc.bkell.aauth.Aauthentication;
import com.interrupt.bookkeeping.cc.bkell.command.RemoveCommand;

import com.interrupt.bookkeeping.cc.bkell.*;
import com.interrupt.bookkeeping.cc.bkell.command.*;

import com.interrupt.bookkeeping.journal.Entry;
import com.interrupt.bookkeeping.users.IUser;
import com.interrupt.bookkeeping.users.User;
import com.interrupt.bookkeeping.exception.CommandException; 

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class TestRemoveCommand extends TestCase {
	
	
	private Logger logger = Logger.getLogger(TestRemoveCommand.class); 
	private ISystem bsystem = null; 
	private ITokens tokens = null; 
	private ICommands commands = null; 
	
    public TestRemoveCommand() {
	}
    public TestRemoveCommand(String name) {
		super(name);
    }
    
	protected void setUp() throws Exception {
		
		
		super.setUp(); 
		
		System.getProperties().setProperty(com.interrupt.bob.util.Util.DEF, "xml/"); 
		
		
		Bob genericBob = new Bob(); 
		bsystem = (GSystem)genericBob.load( new File("xml/bookkeeping.2.users.xml") ); 
		
		logger.debug( "1 > "+ bsystem ); 
		
		IBkell bkell = bsystem.findBkellById("bkell.main"); 
		
		logger.debug( "2 > "+ bkell ); 
		
		tokens = bkell.findTokensById("tokens.def"); 
		commands = bkell.findCommandsById("commands.def"); 
		
		logger.debug( "3 > "+ tokens ); 
		logger.debug( "4 > "+ commands ); 
		
		logger.debug( "tokens list > "+ bkell.allTokens().size() ); 
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	
    public static Test suite() {
		
		TestSuite suite = new TestSuite();
		suite.addTest( new TestRemoveCommand("test001") );
		
		return suite;
    }
    
	
	
	public void test001() {
		
		
		// Testing the supposed command:
		// create (debit -entry 32 -account expenses -amount '10.00') 
		logger.debug( "test001 > tokens > "+ tokens ); 
		logger.debug( "test001 > tokens.children > "+ tokens.allChildren() ); 
		
		
		// from the set of possible tokens, there should be 1 set of options for a given input token
		Token token = (Token)tokens.findTokenByName("entry"); 
		IOptionSet optionSet = token.getOptionSet(); 
		IOptions options = optionSet.findOptionsById("entrynum.option"); 
		token.setOptions(options); 
		
		
		RemoveCommand remove_cmd = (com.interrupt.bookkeeping.cc.bkell.command.RemoveCommand)
			Bob.make( "com.interrupt.bookkeeping.cc.bkell.command.RemoveCommand" ); 
		
		
		// there should only be 1 token for a given command 
		remove_cmd.addTokens(tokens); // all possible tokens that can have stuff added 
		CommandException ee = null; 
		try { 
			
			remove_cmd.setToken(token); 
		}
		catch(CommandException e) { 
			ee = e; 
		}
		assertNotNull( "the add command should not allow a token, only a token.reciever["+ee+"]", ee ); 
		
		
		// set a token.reciever
		IBob bobby = new Bob();
		Entry entry = (Entry)bobby.load(
"<entry xmlns='com/interrupt/bookkeeping/journal' id='e1' entrynum='' state='' journalid='' date='' >" + 
	"<debit xmlns='com/interrupt/bookkeeping/account' id='' amount='10.00' entryid='' accountid='1' />" + 
	"<debit xmlns='com/interrupt/bookkeeping/account' id='234' amount='1.50' entryid='e1' accountid='2' />" + 
	"<credit xmlns='com/interrupt/bookkeeping/account' id='456' amount='11.50' entryid='e1' accountid='3' />" + 
"</entry>" ); 
		
		
		remove_cmd.setTokenReciever(entry); 
		
		// add debits, credits to token.literals
		GDebit debit = new GDebit();
		debit = 
			(GDebit)debit.load("<debit xmlns='com/interrupt/bookkeeping/account' id='234' amount='1.50' entryid='e1' accountid='2' />"); 

		GCredit credit = new GCredit(); 
		credit = 
			(GCredit)credit.load("<credit xmlns='com/interrupt/bookkeeping/account' id='456' amount='11.50' entryid='e1' accountid='3' />");
		
		
		// ******
		com.interrupt.bookkeeping.account.IDebit foundDebit = entry.findDebitById("234");
		logger.debug( "TESTING 123 ["+ foundDebit.toXML() +"]" ); 
		logger.debug( "TESTING 123 ["+ debit.toXML() +"]" ); 
		logger.debug( "TESTING 123 ["+ foundDebit.equals(debit) +"]" ); 
		// ******
		
		
		ITokenLiterals tliterals = new GTokenLiterals(); 
		tliterals.addChild(debit);
		tliterals.addChild(credit);
		remove_cmd.setTokenLiterals(tliterals);
		
		logger.debug( "TestRemoveCommand > XML Out ["+ remove_cmd.toXML() +"]" ); 
		
		// each command should return an XML or a list of XML
		// TODO - (for now) in an AbstractCommand class, put the 'execute()' method in Bob definition 
		IUser user = new User(); 
		user.setId("root"); 
		user.setUsername("root"); 
		Aauthentication aauth = Aauthentication.instance(); 
		aauth.authenticate("webkell", user); 
		//result = rcommand.execute(user); 
		IResult result = remove_cmd.execute(user); 
		
		logger.debug( "TestRemoveCommand > XML Result ["+ result.toXML()+"]" ); 
		
		assertNotNull( "IResult should not be null(perhaps empty)", result ); 
		assertEquals( "IResult SHOULD have 1 result", 1, result.allChildren().size() );
		
		
		IBob entry_r = (IBob)result.allChildren().get(0);
		assertTrue("the 1 result SHOULD be an instance of 'IResult'", (entry_r instanceof IEntry) );
		assertEquals("the entry SHOULD have 1 children", 1, entry_r.allChildren().size());
		assertEquals("there SHOULD be 1 debit", 1, ((IEntry)entry_r).allDebit().size()); 
		assertEquals("there SHOULD be 0 credit", 0, ((IEntry)entry_r).allCredit().size()); 
		
	}
	
}



