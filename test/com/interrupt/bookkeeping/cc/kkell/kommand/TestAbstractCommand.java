package com.interrupt.bookkeeping.cc.kkell.kommand;

import java.io.File;
import java.util.List; 
import org.apache.log4j.Logger;

import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;
import com.interrupt.bookkeeping.account.GDebit; 
import com.interrupt.bookkeeping.account.GCredit; 
import com.interrupt.bookkeeping.journal.IEntry;

import com.interrupt.bookkeeping.GSystem;
import com.interrupt.bookkeeping.ISystem;
import com.interrupt.bookkeeping.TestGeneral;
import com.interrupt.bookkeeping.cc.bkell.Bkell;
import com.interrupt.bookkeeping.cc.bkell.IBkell;

import com.interrupt.bookkeeping.cc.bkell.*;
import com.interrupt.bookkeeping.cc.bkell.command.*;

import com.interrupt.bookkeeping.journal.Entry;
import com.interrupt.bookkeeping.exception.CommandException; 

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class TestAbstractCommand extends TestCase {
	
	
	private Logger logger = Logger.getLogger(TestAbstractCommand.class); 
	private ISystem bsystem = null; 
	private ITokens tokens = null; 
	private ICommands commands = null; 
	
    public TestAbstractCommand() {
	}
    public TestAbstractCommand(String name) {
		super(name);
    }
    
	protected void setUp() throws Exception {
		
		
		super.setUp(); 
		
		System.getProperties().setProperty(com.interrupt.bob.util.Util.DEF, "xml/"); 
		
		
		Bob genericBob = new Bob(); 
		bsystem = (GSystem)genericBob.load( new File("xml/bookkeeping.2.users.xml") ); 
		
		logger.debug( "1 > "+ bsystem ); 
		
		IBkell bkell = bsystem.findBkellById("bkell.main"); 
		
		logger.debug( "2 > "+ bkell ); 
		
		tokens = bkell.findTokensById("tokens.def"); 
		commands = bkell.findCommandsById("commands.def"); 
		
		logger.debug( "3 > "+ tokens ); 
		logger.debug( "4 > "+ commands ); 
		
		logger.debug( "tokens list > "+ bkell.allTokens().size() ); 
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	
    public static Test suite() {
		
		TestSuite suite = new TestSuite();
		suite.addTest( new TestAbstractCommand("testTokenLiterals") );
		suite.addTest( new TestAbstractCommand("testAddTokenLiterals") );
		suite.addTest( new TestAbstractCommand("testAddOption") );
		
		return suite;
    }
    
	
	/* AbstractCommand should have only 1 'tokenLiterals'
	 */
	public void testTokenLiterals() { 
		
		AddCommand acmd = new AddCommand(); 
		List allTokenLiterals = acmd.allTokenLiterals(); 
		
		assertEquals("There SHOULD be only 1 tokenLiterals in a command", 1, allTokenLiterals.size() ); 
	}
	public void testAddTokenLiterals() { 
		
		
		Entry entry = new Entry();
		entry.setId("01"); 
		Entry entry1 = new Entry();
		entry1.setId("02"); 
		
		AddCommand acmd = new AddCommand(); 
		acmd.addTokenLiteral(entry);
		acmd.addTokenLiteral(entry1);
		
		
		ITokenLiterals tliterals = acmd.getTokenLiterals(); 
		assertNotNull("the token.literals container is NULL", tliterals); 
		
		List allChildren = tliterals.allChildren(); 
		assertEquals("There SHOULD be 2 token.literals in the AddCommand", 2, allChildren.size()); 

	}
	
	public void testAddOption() { 
		
		IOption option1 = new GOption(); 
		IOption option2 = new GOption(); 
		
		
		FindCommand fcommand = new FindCommand(); 
		fcommand.addOption(option1); 
		fcommand.addOption(option2); 
		
		IOptions options = fcommand.getOptions(); 
		List allOs = options.allOption(); 
		
		assertEquals("There SHOULD be 2 options in the FindCommand", 2, allOs.size()); 
		
	}
	
}



