package com.interrupt.bookkeeping.cc.kkell.kommand;

import java.io.File;

import org.apache.log4j.Logger;

import com.interrupt.bob.base.Bob;
import com.interrupt.bookkeeping.GSystem;
import com.interrupt.bookkeeping.ISystem;
import com.interrupt.bookkeeping.TestGeneral;
import com.interrupt.bookkeeping.cc.bkell.Bkell;
import com.interrupt.bookkeeping.cc.bkell.IBkell;

import com.interrupt.bookkeeping.cc.bkell.*;
import com.interrupt.bookkeeping.cc.bkell.aauth.Aauthentication;
import com.interrupt.bookkeeping.cc.bkell.command.*;
import com.interrupt.bookkeeping.users.IUser;
import com.interrupt.bookkeeping.users.User;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class TestCreateCommand extends TestCase {
	
	
	private Logger logger = Logger.getLogger(TestCreateCommand.class); 
	private ISystem bsystem = null; 
	private ITokens tokens = null; 
	private ICommands commands = null; 
	
    public TestCreateCommand() {
	}
    public TestCreateCommand(String name) {
		super(name);
    }
    
	protected void setUp() throws Exception {
		
		
		super.setUp(); 
		
		System.getProperties().setProperty(com.interrupt.bob.util.Util.DEF, "xml/"); 
		
		
		Bob genericBob = new Bob(); 
		bsystem = (GSystem)genericBob.load( new File("xml/bookkeeping.2.users.xml") ); 
		
		logger.debug( "1 > "+ bsystem ); 
		
		IBkell bkell = bsystem.findBkellById("bkell.main"); 
		
		logger.debug( "2 > "+ bkell ); 
		
		tokens = bkell.findTokensById("tokens.def"); 
		commands = bkell.findCommandsById("commands.def"); 
		
		logger.debug( "3 > "+ tokens ); 
		logger.debug( "4 > "+ commands ); 
		
		logger.debug( "tokens list > "+ bkell.allTokens().size() ); 
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	
    public static Test suite() {
		
		TestSuite suite = new TestSuite();
		suite.addTest( new TestCreateCommand("test001") );
		
		return suite;
    }
    
	
	public void test001() { 
		
		
		// Testing the supposed command:
		// create (debit -entry 32 -account expenses -amount '10.00') 
		
		logger.debug( "test001 > tokens > "+ tokens ); 
		logger.debug( "test001 > tokens.children > "+ tokens.allChildren() ); 
		
		//ITokens cmdTokens = (com.interrupt.bookkeeping.cc.cc.bkell.command.GTokens)Bob.make("com.interrupt.bookkeeping.bkell.command.GTokens"); 

		// from the set of possible tokens, there should be 1 set of options for a given input token
		Token token = (Token)tokens.findTokenByName("debit"); 
		IOptionSet optionSet = token.getOptionSet(); 
		IOptions options = optionSet.findOptionsById("entry.option"); 
		token.setOptions(options); 
		
		
		//ICommand create_cmd = commands.findCommandByName("create");
		CreateCommand create_cmd = (com.interrupt.bookkeeping.cc.bkell.command.CreateCommand)
			Bob.make( "com.interrupt.bookkeeping.cc.bkell.command.CreateCommand" ); 
		
		
		// there should only be 1 token for a given command 
		create_cmd.addTokens(tokens); 
		create_cmd.setToken(token); 
		
		
		// each command should return an XML or a list of XML
		IUser user = new User(); 
		user.setId("root"); 
		user.setUsername("root"); 
		Aauthentication aauth = Aauthentication.instance(); 
		aauth.authenticate("webkell", user); 
		//result = rcommand.execute(user); 
		IResult result = create_cmd.execute(user); // TODO - (for now) in an AbstractCommand class, put the 'execute()' method 
		
		assertNotNull( "IResult should not be null(perhaps empty)", result ); 
		assertEquals( "IResult SHOULD have 1 result", 1, result.allChildren().size() );

		logger.debug( "XML Result ["+ result.toXML()+"]" ); 
		
	}
}



