
package com.interrupt.bookkeeping.cc.kkell.kommand;

import java.io.File;
import java.util.List; 
import org.apache.log4j.Logger;

import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;
import com.interrupt.bookkeeping.account.GDebit; 
import com.interrupt.bookkeeping.account.GCredit; 
import com.interrupt.bookkeeping.account.IDebit;
import com.interrupt.bookkeeping.journal.IEntry;
import com.interrupt.bookkeeping.journal.IJournal;

import com.interrupt.bookkeeping.Bookkeeping;
import com.interrupt.bookkeeping.GSystem;
import com.interrupt.bookkeeping.ISystem;
import com.interrupt.bookkeeping.TestGeneral;
import com.interrupt.bookkeeping.cc.bkell.Bkell;
import com.interrupt.bookkeeping.cc.bkell.IBkell;

import com.interrupt.bookkeeping.cc.bkell.*;
import com.interrupt.bookkeeping.cc.bkell.aauth.Aauthentication;
import com.interrupt.bookkeeping.cc.bkell.command.*;

import com.interrupt.bookkeeping.journal.Entry;
import com.interrupt.bookkeeping.users.IUser;
import com.interrupt.bookkeeping.users.User;
import com.interrupt.bookkeeping.exception.CommandException; 

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class TestListCommand extends TestCase {
	
	
	private Logger logger = Logger.getLogger(TestListCommand.class); 
	private ISystem bsystem = null; 
	private ITokens tokens = null; 
	private ICommands commands = null; 
	
    public TestListCommand() {
	}
    public TestListCommand(String name) {
		super(name);
    }
    
	protected void setUp() throws Exception {
		
		
		super.setUp(); 
		
		System.getProperties().setProperty(com.interrupt.bob.util.Util.DEF, "xml/"); 
		System.getProperties().setProperty(com.interrupt.bob.util.Util.BASE, "."); 
		System.getProperties().setProperty(com.interrupt.bob.util.Util.END, ".xml"); 
		
		
		Bob genericBob = new Bob(); 
		bsystem = (GSystem)genericBob.load( new File("xml/bookkeeping.2.users.xml") ); 
		
		IBkell bkell = bsystem.findBkellById("bkell.main"); 
		tokens = bkell.findTokensById("tokens.def"); 
		commands = bkell.findCommandsById("commands.def"); 
		
		logger.debug( "TestListCommand > tokens.def > "+ tokens ); 
		logger.debug( "TestListCommand > commands.def > "+ commands ); 
		
		logger.debug( "tokens list > "+ bkell.allTokens().size() ); 
		
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	
    public static Test suite() {
		
		TestSuite suite = new TestSuite();
		suite.addTest( new TestListCommand("test001") );
		
		return suite;
    }
    
	
	
	public void test001() { 
		
		
		IBob bobby = new Bob(); 
		
		// Testing the supposed command:
		// list ((token.literal)debit -entry 32 -account expenses -amount '10.00') 
		
		logger.debug( "test001 > tokens > "+ tokens ); 
		logger.debug( "test001 > tokens.children > "+ tokens.allChildren() ); 
		
		
		ListCommand list_cmd = (com.interrupt.bookkeeping.cc.bkell.command.ListCommand)
		Bob.make( "com.interrupt.bookkeeping.cc.bkell.command.ListCommand" ); 
		
		
		// set the possible tokens 
		list_cmd.setTokens(commands.findCommandByName("find").findTokensById("find.tokens")); 
		
		
		// set the token we are looking for 
		Token token = (Token)tokens.findTokenByName("debit"); 
		list_cmd.setToken(token); 
		
		
		// from the set of possible tokens, there should be 1 set of options for a given input token
		IOptionSet optionSet = token.getOptionSet(); 
		optionSet.removeOptionsById("entry.option"); 
		token.setOptionSet(optionSet); 
		
		
		// set the token.reciever 
		Bookkeeping bookkeeping = (Bookkeeping)bobby.load( new File("xml/bookkeeping.2.bookkeeping.xml") ); 
		
		IJournal journalReciever = bookkeeping.getJournals().findJournalByName("generalledger"); 
		logger.debug("ZZzzzzz["+bookkeeping.getJournals().toXML()+"]"); 
		
		list_cmd.setTokenReciever(journalReciever); 
		
		// add the option (only 1)
		IOption idOption = new GOption(); 
		idOption.setName("entryid"); 
		idOption.setValue("e1"); 
		
		/*IOption amountOption = new GOption(); 
		amountOption.setName("amount"); 
		amountOption.setValue("11.50"); 
		
		IOption accountOption = new GOption(); 
		accountOption.setName("accountid"); 
		accountOption.setValue("1"); 
		*/
		list_cmd.addOption(idOption);
		
		
		// execute the command 
		logger.debug("LIST command right before execute["+list_cmd.toXML()+"]"); 
		
		
		IUser user = new User(); 
		user.setId("root"); 
		user.setUsername("root"); 
		Aauthentication aauth = Aauthentication.instance();
		aauth.authenticate("webkell", user); 
		//result = rcommand.execute(user); 
		IResult result = list_cmd.execute(user); 
		
		List allChildren = result.allChildren(); 
		
		assertEquals("Result should have 2 objects", 2, allChildren.size()); 
		
		
		IBob debitResult0 = (IBob)allChildren.get(0); 
		boolean correctType0 = debitResult0 instanceof IDebit; 
		
		IBob debitResult1 = (IBob)allChildren.get(1); 
		boolean correctType1 = debitResult1 instanceof IDebit; 
		
		assertTrue("the first result should be of type <debit>", correctType0); 
		assertTrue("the second result should be of type <debit>", correctType1); 
		
		assertEquals( "the 'id' of result entry SHOULD be 'abc'", "abc", ((IDebit)debitResult0).getId() ); 
		assertEquals( "the 'amount' of result entry SHOULD be '10.00'", "10.00", ((IDebit)debitResult0).getAmount() ); 
		
	}
	
}



