package com.interrupt.bookkeeping.cc.kkell.kommand;

import java.io.File;
import java.util.List; 
import org.apache.log4j.Logger;

import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;
import com.interrupt.bookkeeping.account.GAccount;
import com.interrupt.bookkeeping.account.GDebit; 
import com.interrupt.bookkeeping.account.GCredit; 
import com.interrupt.bookkeeping.account.IAccount;
import com.interrupt.bookkeeping.journal.GEntries;
import com.interrupt.bookkeeping.journal.GEntry;
import com.interrupt.bookkeeping.journal.GJournal;
import com.interrupt.bookkeeping.journal.IEntries;
import com.interrupt.bookkeeping.journal.IEntry;
import com.interrupt.bookkeeping.journal.IJournal;

import com.interrupt.bookkeeping.GSystem;
import com.interrupt.bookkeeping.ISystem;
import com.interrupt.bookkeeping.TestGeneral;
import com.interrupt.bookkeeping.cc.bkell.Bkell;
import com.interrupt.bookkeeping.cc.bkell.IBkell;
import com.interrupt.bookkeeping.cc.bkell.aauth.Aauthentication;
import com.interrupt.bookkeeping.cc.bkell.command.RemoveCommand;

import com.interrupt.bookkeeping.cc.bkell.*;
import com.interrupt.bookkeeping.cc.bkell.command.*;

import com.interrupt.bookkeeping.journal.Entry;
import com.interrupt.bookkeeping.users.IUser;
import com.interrupt.bookkeeping.users.User;
import com.interrupt.bookkeeping.util.Util;
import com.interrupt.bookkeeping.exception.CommandException; 

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class TestReverseCommand extends TestCase {
	
	
	private Logger logger = Logger.getLogger(TestReverseCommand.class); 
	private ISystem bsystem = null; 
	private ITokens tokens = null; 
	private ICommands commands = null; 
	
    public TestReverseCommand() {
	}
    public TestReverseCommand(String name) {
		super(name);
    }
    
	protected void setUp() throws Exception {
		
		
		super.setUp(); 
		
		System.getProperties().setProperty(com.interrupt.bob.util.Util.DEF, "xml/"); 
		System.getProperties().setProperty(com.interrupt.bob.util.Util.BASE, "."); 
		System.getProperties().setProperty(com.interrupt.bob.util.Util.END, ".xml"); 
		
		
		Bob genericBob = new Bob(); 
		bsystem = (GSystem)genericBob.load( new File("xml/bookkeeping.2.users.xml") ); 
		
		logger.debug( "1 > "+ bsystem ); 
		
		IBkell bkell = bsystem.findBkellById("bkell.main"); 
		
		logger.debug( "2 > "+ bkell ); 
		
		tokens = bkell.findTokensById("tokens.def"); 
		commands = bkell.findCommandsById("commands.def"); 
		
		logger.debug( "3 > "+ tokens ); 
		logger.debug( "4 > "+ commands ); 
		
		logger.debug( "tokens list > "+ bkell.allTokens().size() ); 
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	
    public static Test suite() {
		
		TestSuite suite = new TestSuite();
		//suite.addTest( new TestReverseCommand("test001") );
		
		suite.addTest( new TestReverseCommand("testReceverEntryType") );
		suite.addTest( new TestReverseCommand("testTokenLiteralSizeAndType") );
		suite.addTest( new TestReverseCommand("testReversal") );
		
		return suite;
    }
    
	
	public void testReceverEntryType() { 
		
		
		IJournal journal = new GJournal();
		
		ReverseCommand rcommand = new ReverseCommand(); 
		rcommand.setTokenReciever(journal); 
		//rcommand.setTokenLiterals(tliterals); 
		
		CommandException ee = null; 
		try { 
			
			IUser user = new User(); 
    		user.setId("root"); 
    		user.setUsername("root"); 
    		Aauthentication aauth = Aauthentication.instance(); 
    		aauth.authenticate("webkell", user); 
    		//result = rcommand.execute(user); 
    		rcommand.execute(user); 
		}
		catch(CommandException e) { 
			
			ee = e;
		}
		
		assertNotNull("there SHOULD be a CommandException - only entry can be a reciever", ee); 
		
	}
	

	public void testTokenLiteralSizeAndType() { 
		
		
		IEntry entry = new GEntry();
		entry.setState(Util.CLOSED_STATE); 
		
		IAccount account1 = new GAccount(); 
		IAccount account2 = new GAccount(); 
		ITokenLiterals tliterals = new GTokenLiterals();
		tliterals.addChild(account1); 
		tliterals.addChild(account2); 
		
		
		// there must be 1 token literal of type <entry/>
		ReverseCommand rcommand = new ReverseCommand(); 
		rcommand.setTokenReciever(entry); 
		rcommand.setTokenLiterals(tliterals); 
		
		CommandException ee = null; 
		try { 
			
			IUser user = new User(); 
    		user.setId("root"); 
    		user.setUsername("root"); 
    		Aauthentication aauth = Aauthentication.instance(); 
    		aauth.authenticate("webkell", user); 
    		//result = rcommand.execute(user); 
    		rcommand.execute(user); 
		}
		catch(CommandException e) { 
			
			ee = e;
		}
		assertNotNull("there SHOULD be a CommandException - only entry can be a reciever", ee); 
		
		// 2. >>>>>>>>>>
		ee = null; 
		IEntries entries = new GEntries(); 
		entry.setParent(entries); 
		rcommand.setTokenReciever(entry); 
		
		IEntry entryLiteral = new GEntry(); 
		entryLiteral.setState(Util.CLOSED_STATE); 
		tliterals = new GTokenLiterals();
		tliterals.addChild(entryLiteral); 
		
		rcommand.removeAllTokenLiterals(); 
		rcommand.addTokenLiterals(tliterals); 
		IResult reverseResult = null; 
		try { 
			IUser user = new User(); 
    		user.setId("root"); 
    		user.setUsername("root"); 
    		Aauthentication aauth = Aauthentication.instance(); 
    		aauth.authenticate("webkell", user); 
    		//result = rcommand.execute(user); 
    		reverseResult = (IResult)rcommand.execute(user); 
		}
		catch(CommandException e) { 
			ee = e;
		}
		
		String emessage = null; 
		if(ee != null) { 
			emessage = ee.getMessage(); 
		}
		assertNull("There should be NO CommandException here / message["+emessage+"]",ee); 
		assertNotNull("There SHOULD be a reversal result",reverseResult); 
		
	}
	
	public void testReversal() {
		
		
		// Testing the supposed command:
		// reverse((token.literal) token.literal,...) 
		
		
		ReverseCommand reverse_cmd = (com.interrupt.bookkeeping.cc.bkell.command.ReverseCommand)
			Bob.make( "com.interrupt.bookkeeping.cc.bkell.command.ReverseCommand" ); 
		reverse_cmd.addTokens(tokens); // all possible tokens that can have stuff added 
		
		
		// set a token.reciever
		IEntries entries = new GEntries(); 
		
		IBob bobby = new Bob();
		Entry entry = (Entry)bobby.load(
"<entry xmlns='com/interrupt/bookkeeping/journal' id='entry1' entrynum='' state='closed' journalid='' date='' >" + 
	"<debit xmlns='com/interrupt/bookkeeping/account' id='012' amount='10.00' entryid='' accountid='1' />" + 
	"<debit xmlns='com/interrupt/bookkeeping/account' id='234' amount='1.50' entryid='e1' accountid='2' />" + 
	"<credit xmlns='com/interrupt/bookkeeping/account' id='456' amount='11.50' entryid='e1' accountid='3' />" + 
"</entry>" ); 
		entry.setState(Util.CLOSED_STATE); 
		entries.addEntry(entry); 
		reverse_cmd.setTokenReciever(entry); 
		
		
		// set the literals 
		Entry theReversal = (Entry)bobby.load(
"<entry xmlns='com/interrupt/bookkeeping/journal' id='reverse1' entrynum='' state='closed' journalid='' date='' >" + 
	"<credit xmlns='com/interrupt/bookkeeping/account' id='012' amount='10.00' entryid='' accountid='1' />" + 
	"<credit xmlns='com/interrupt/bookkeeping/account' id='234' amount='1.50' entryid='e1' accountid='2' />" + 
	"<debit xmlns='com/interrupt/bookkeeping/account' id='456' amount='11.50' entryid='e1' accountid='3' />" + 
"</entry>" ); 
		theReversal.setState(Util.CLOSED_STATE); 
		
		ITokenLiterals tliterals = new GTokenLiterals(); 
		tliterals.addChild(theReversal);
		reverse_cmd.setTokenLiterals(tliterals);
		
		logger.debug( "TestReverseCommand > XML Out ["+ reverse_cmd.toXML() +"]" ); 
		
		
		
		/** each command should return an XML or a list of XML
		 * TODO - (for now) in an AbstractCommand class, put the 'execute()' method in Bob definition 
		 */
		IUser user = new User(); 
		user.setId("root"); 
		user.setUsername("root"); 
		Aauthentication aauth = Aauthentication.instance(); 
		aauth.authenticate("webkell", user); 
		//result = rcommand.execute(user); 
		IResult result = reverse_cmd.execute(user); 
		logger.debug( "TestReverseCommand > XML Result ["+ result.toXML()+"]" ); 
		
		
		assertEquals( "IResult SHOULD have 1 result", 1, result.allChildren().size() );
		
		IBob entry_r = (IBob)result.allChildren().get(0);
		assertTrue("the 1 result SHOULD be an instance of 'IEntries'", (entry_r instanceof IEntries) );
		assertEquals("the entry SHOULD have 2 children", 2, entry_r.allChildren().size()); 
		
		IEntry entry_e = ((IEntries)entry_r).findEntryById("entry1"); 
		IEntry reverse_e = ((IEntries)entry_r).findEntryById("reverse1"); 
		
		assertNotNull("CANNOT find entry with id[entry1]", entry_e); 
		assertNotNull("CANNOT find entry with id[reverse1]", reverse_e); 
		
		assertEquals("entry1 SHOULD have a state of ["+Util.REVERSED_STATE+"]", 
			Util.REVERSED_STATE, entry_e.getState()); 
		
		assertEquals("reverse1 SHOULD have a state of ["+Util.REVERSE_CLOSED_STATE+"]", 
			Util.REVERSE_CLOSED_STATE, reverse_e.getState()); 
			
	}
	
}



