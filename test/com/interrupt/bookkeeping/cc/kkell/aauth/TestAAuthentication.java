package com.interrupt.bookkeeping.cc.kkell.aauth;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.List;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.log4j.Logger;
import org.jgroups.util.Command;
import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.Database;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.CollectionManagementService;

import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;
import com.interrupt.bob.base.IVisitor;
import com.interrupt.bookkeeping.AbstractBookkeepingTestCase;
import com.interrupt.bookkeeping.Bookkeeping;
import com.interrupt.bookkeeping.TestGeneral;
import com.interrupt.bookkeeping.cc.bkell.aauth.Aauthentication;
import com.interrupt.bookkeeping.cc.bkell.aauth.GAllowedActions;
import com.interrupt.bookkeeping.cc.bkell.aauth.IAllowedActions;
import com.interrupt.bookkeeping.cc.bkell.command.CreateCommand;
import com.interrupt.bookkeeping.cc.bkell.command.FindVisitor;
import com.interrupt.bookkeeping.cc.bkell.command.GCommand;
import com.interrupt.bookkeeping.exception.AuthorisationException;
import com.interrupt.bookkeeping.exception.SystemException;
import com.interrupt.bookkeeping.system.BookkeepingSystemProperties;
import com.interrupt.bookkeeping.users.GGroup;
import com.interrupt.bookkeeping.users.GGroups;
import com.interrupt.bookkeeping.users.Group;
import com.interrupt.bookkeeping.users.IGroup;
import com.interrupt.bookkeeping.users.IUser;
import com.interrupt.bookkeeping.users.IUserSession;
import com.interrupt.bookkeeping.users.IUsers;
import com.interrupt.bookkeeping.users.User;
import com.interrupt.bookkeeping.users.Users;
import com.interrupt.spittoon.Spittoon;
import com.interrupt.util.TestIdGenerator;
import com.interrupt.util.Util;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class TestAAuthentication extends TestCase {
	
	
	Logger logger = Logger.getLogger(TestAAuthentication.class); 
	private Spittoon spittoon = null; 
	private Bob bob = new Bob(); 
	private IBob loadedAuthorise = null;  
	private BookkeepingSystemProperties bsproperties = null; 
	public TestAAuthentication() throws XMLDBException { 
		
		//super(); 
		this(null); 
	}
	public TestAAuthentication(String name) throws XMLDBException {
		
		super(name); 
		
		System.getProperties().setProperty(com.interrupt.bob.util.Util.DEF, BookkeepingSystemProperties.instance().getProperty("bob.def")); 
		System.getProperties().setProperty(com.interrupt.bob.util.Util.BASE, "."); 
		System.getProperties().setProperty(com.interrupt.bob.util.Util.END, ".xml"); 
		
		bsproperties = BookkeepingSystemProperties.instance(); 
		
		
		//loadedAuthorise = bob.load(new File("xml/bookkeeping.authorise.xml"), BookkeepingSystemProperties.instance().getProperty("bob.def")); 
		loadedAuthorise = bob.load(TestAAuthentication.class.getResourceAsStream("/bookkeeping.authorise.xml"), BookkeepingSystemProperties.instance().getProperty("bob.def")); 
		
    }
    
	public void setUp() { 
		
		// xml database will be running locally as part of the unit test suite 
		spittoon = new Spittoon(); 
		spittoon.initialise(); 
		spittoon.setupRoot(); 
		
		/**
		 * for some reason the Configure class isn't working 
		 */
		String axml = Util.loadTextFile("setup.aauthentication.xml"); 
		String gxml = Util.loadTextFile("setup.groups.xml"); 
		
		String axpath = "/system[ @id='main.system' ]"; 
		logger.debug("Aauthentication XPath["+ axpath +"]"); 
		String aurl = spittoon.getAauthDbUrl();  
		spittoon.createR(aurl, axpath, axml); 
		
		String gxpath = "/system[ @id='main.system' ]"; 
		logger.debug("Groups XPath["+ gxpath +"]"); 
		String gurl = spittoon.getGroupsDbUrl(); 
		spittoon.createR(gurl, gxpath, gxml); 
		
		
	}
	public void tearDown() { 
		
		//spittoon.tearDownRoot(); 
	}
	
	public static Test suite() throws XMLDBException {
		
		TestSuite suite = new TestSuite(); 
		
		//** basic authentication functionality 
		/*suite.addTest( new TestAAuthentication("testDefaultRootUser") );
		suite.addTest( new TestAAuthentication("testDefaultAllowedActions") );
		
		suite.addTest( new TestAAuthentication("testAuthenticated") );
		suite.addTest( new TestAAuthentication("testNotAuthenticated") );
		suite.addTest( new TestAAuthentication("testAuthorised") );
		suite.addTest( new TestAAuthentication("testNotAuthorised") );
		suite.addTest( new TestAAuthentication("testAuthorisedButNotAuthenticated") );
		
		suite.addTest( new TestAAuthentication("testLoadFromDB") ); 
		
		suite.addTest( new TestAAuthentication("testAddGroup") ); 
		suite.addTest( new TestAAuthentication("testAddGroupSetAssociatedBookkeeping") ); 
		suite.addTest( new TestAAuthentication("testRemoveGroup") ); 
		
		
		suite.addTest( new TestAAuthentication("testAddUserDefaultGroup") ); 
		suite.addTest( new TestAAuthentication("testRemoveUserDefaultGroup") ); 
		
		//** a secondary user tries to join a group 
		suite.addTest( new TestAAuthentication("testAddUserToGroup") ); 
		suite.addTest( new TestAAuthentication("testRemoveUserFromGroup") ); 
		*/
		
		/*
		//** the default group owner accepts another into their group
		suite.addTest( new TestAAuthentication("testDefaultUserAcceptUserIntoGroup") ); 
		suite.addTest( new TestAAuthentication("testDefaultUserRejectUserIntoGroup") ); 
		
		//** Deals with the actual thread that maintains the sessions 
		suite.addTest( new TestAAuthentication("testSessionAuthenticate") ); 
		suite.addTest( new TestAAuthentication("testSessionTimeout") ); 
		*/
		
		suite.addTest( new TestAAuthentication("testAddUserDefaultGroup") ); 
		/*suite.addTest( new TestAAuthentication("testGeneratePasswordHash") ); 
		suite.addTest( new TestAAuthentication("testAathenticateGeneratePasswordHash") ); 
		suite.addTest( new TestAAuthentication("testGoodLogin") ); 
		suite.addTest( new TestAAuthentication("testSessionTimeout") ); 
		suite.addTest( new TestAAuthentication("testBadLogin") ); 
		*/
		return suite;
	}
	
	
	/********************
	 * 
	 ********************/
	public void testGeneratePasswordHash() throws NoSuchAlgorithmException, UnsupportedEncodingException { 
		
		String password = "sdfib0876-df"; 
		String passwordComparator = "sdfib0876-df"; 
		
		MessageDigest digest = MessageDigest.getInstance("SHA-1");
		
		digest.reset();
		byte[] input = digest.digest(password.getBytes("UTF-8"));
		
		digest.reset();
		byte[] inputComparator = digest.digest(passwordComparator.getBytes("UTF-8"));
		
		String hashString = new String(input); 
		String hashStringComparator = new String(inputComparator); 
		
		logger.debug("testGeneratePasswordHash:: hash1["+hashString+"] > hash2["+hashStringComparator+"]"); 
		assertEquals("The password hashes are generating different values > exp["+hashString+"] / actual["+ 
				hashStringComparator+"]", hashString, hashStringComparator); 
		
	}
	public void testAathenticateGeneratePasswordHash() { 
		
		String password = "sdfib0876-df"; 
		Aauthentication aauth = new Aauthentication(); 
		String hash1 = aauth.hashPassword(password); 
		String hash2 = aauth.hashPassword(password); 
		String hashDiff = aauth.hashPassword("zinger"); 
		
		logger.debug("testAathenticateGeneratePasswordHash > hash1["+hash1+"] > hash2["+hash2+"] > hashDiff["+hashDiff+"]"); 
		assertEquals("Aauthentication is generating DIFFERENT password hashes > exp["+hash1+"] / actual["+ 
				hash2+"]", hash1, hash2); 
		
		assertTrue("Aauthentication is generating SAME password hashes > exp["+hash1+"] / actual["+ 
				hashDiff+"]", !hash1.equals(hashDiff)); 
	}
	public void testGoodLogin() throws XMLDBException, InterruptedException { 
		
		
		Aauthentication aauthentication = (Aauthentication)spittoon.
			retrieve(spittoon.getAauthDbUrl(), 
					"/system[ @id='main.system' ]/aauthentication[ @id='main.authentication' ]", true); 
		
		logger.debug("loaded Aauthentication ["+ aauthentication +"]"); 
		
		IUser user = new User(); 
		user.setId("root"); 
		user.setUsername("root"); 
		user.setPassword("password"); 
		user.setLogintimeout("2500"); 
		
		String authResult = null; 
		SystemException ee = null; 
		try { 
			IUserSession usession = aauthentication.authenticate("webkell", user); 
			authResult = usession.getId(); 
		}
		catch(SystemException e) { 
			e.printStackTrace(); 
			ee = e; 
		}
		
		assertNull("There was a SystemException while authenticating", ee); 
		assertNotNull("This should give us a POSITIVE login result", authResult); 
		
	}
	public void testSessionTimeout() throws XMLDBException, InterruptedException { 
		
		
		Aauthentication aauthentication = (Aauthentication)spittoon.
			retrieve(spittoon.getAauthDbUrl(), 
					"/system[ @id='main.system' ]/aauthentication[ @id='main.authentication' ]", true); 
		
		logger.debug("loaded Aauthentication ["+ aauthentication +"]"); 
		
		IUser user = new User(); 
		user.setId("root"); 
		user.setUsername("root"); 
		user.setPassword("password"); 
		user.setLogintimeout("2500"); 
		
		String authResult = null; 
		try { 
			
			IUserSession usession = aauthentication.authenticate("webkell", user); 
			authResult = usession.getId(); 
		}
		catch(SystemException e) { 
			e.printStackTrace(); 
		}
		
		
		boolean auth1 = aauthentication.authenticated(user); 
		assertTrue("We should still be authenticated if less than 2.5 seconds", auth1); 
		
		/** 
		 * crude way of waiting for more than 5 seconds for the session to invalidate
		 * this object probably won't control the thread monitor, so I need to control time somehow 
		 */
		for (int i = 0; i < 4000; i++) {
			logger.debug("Waiting for session end...["+i+"]"); 
		}
		
		
		boolean auth2 = aauthentication.authenticated(user); 
		assertFalse("We should NOT be authenticated after 2.5 seconds", auth2); 
		
	}
	public void testBadLogin() throws XMLDBException { 
		
		Aauthentication aauthentication = (Aauthentication)spittoon.
			retrieve(spittoon.getAauthDbUrl(), 
					"/system[ @id='main.system' ]/aauthentication[ @id='main.authentication' ]", true); 
		
		IUser user = new User(); 
		user.setId("root"); 
		user.setUsername("root"); 
		user.setPassword("wrong.password"); 
		
		IUserSession usession = aauthentication.authenticate("webkell", user); 
		String authResult = usession.getId(); 
		assertNull("This should give us a NEGATIVE login result",authResult); 
		
		aauthentication.teardown(); 
		
	}
	
	/********************
	 * 
	 ********************/
	
	
	/**
	 * ensure there is a default 'root' user in the system 
	 */
	public void testDefaultRootUser() throws XMLDBException { 
		
		Aauthentication aauthentication = Aauthentication.instance(); 
		aauthentication.setup(); 
		
		String collectionString = bsproperties.getProperty("db.url") + "system/aauthentication/users/"; 
		logger.debug("TestAauthentication.testDefaultRootUser: collectionString:: " + collectionString); 
		
		User rootUser = null; //(User)spittoon.getDocument(collectionString, "user", "root"); 
		
		assertNotNull("rootUser is null", rootUser); 
		assertEquals("rootUser id is WRONG", rootUser.getId(), "root"); 
		
		//** TEARDOWN 
		//spittoon.removeCollection("xmldb:exist://localhost:8090/exist/xmlrpc/db/bookkeeping/system/users/"); 
		aauthentication.teardown(); 
		
	}
	
	public void testDefaultAllowedActions() throws XMLDBException { 
		
		//** SETUP 
		// put a default '/aauthentication/users/user/allowedActions' in the xml database 
		Aauthentication aauthentication = Aauthentication.instance(); 
		aauthentication.setup(); 
		
		String collectionString = bsproperties.getProperty("db.url") + "system/aauthentication/users/"; 
		
		IUsers rootUsers = null;	//(IUsers)spittoon.getDocument(collectionString, "users", "aauth.users");  
		IAllowedActions rootAllowedActions = (IAllowedActions)rootUsers.find("allowedActions", "root.allowedActions"); 
		
		assertNotNull("allowedActions is null", rootAllowedActions); 
		assertEquals("allowedActions id is WRONG", rootAllowedActions.getId(), "root.allowedActions"); 
		
		//** TEARDOWN 
		aauthentication.teardown(); 
		
	}
	
	/**
	 * 1. login 
	 * 2. tests that after a good login, that the user's session is maintained 
	 */
	public void testAuthenticated() throws XMLDBException { 
		
		Aauthentication aauthentication = Aauthentication.instance(); 
		aauthentication.setup(); 
		aauthentication.initialise(); 
		
		IUser user = new User(); 
		user.setId("root"); 
		user.setUsername("root"); 
		user.setPassword("password"); 
		user.setLogintimeout("60000"); 
		
		aauthentication.authenticate("webkell", user); 
		boolean authResult = aauthentication.authenticated(user); 
		assertTrue("We SHOULD be authenticated after logging in", authResult); 
		
		aauthentication.teardown(); 
		
	}
	
	/**
	 * 1. login
	 * 2. test logged in 
	 * 3. log a person out and then test if they are authenticated 
	 */
	public void testNotAuthenticated() throws XMLDBException { 
		
		Aauthentication aauthentication = Aauthentication.instance(); 
		aauthentication.setup(); 
		aauthentication.initialise(); 
		
		IUser user = new User(); 
		user.setId("root"); 
		user.setUsername("root"); 
		user.setPassword("password"); 
		user.setLogintimeout("60000"); 
		
		aauthentication.authenticate("webkell", user); 
		boolean authResult = aauthentication.authenticated(user); 
		assertTrue("Part 1. We SHOULD be authenticated after logging in", authResult); 
		
		aauthentication.logout(user); 
		boolean authResult2 = aauthentication.authenticated(user); 
		assertFalse("Part 2. We SHOULD NOT be authenticated at this point", authResult2); 
		
		aauthentication.teardown(); 
		
	}
	
	
	/** 
	 * 1. login 
	 * 2. make the user perform an action for which they are authorised  
	 */
	public void testAuthorised() { 
		
		Aauthentication aauthentication = Aauthentication.instance(); 
		aauthentication.setup(); 
		aauthentication.initialise(); 
		
		IUser user = new User(); 
		user.setId("root"); 
		user.setUsername("root"); 
		user.setPassword("password"); 
		user.setLogintimeout("60000"); 
		
		aauthentication.authenticate("webkell", user); 
		boolean authResult = aauthentication.authenticated(user); 
		assertTrue("Part 1. We SHOULD be authenticated after logging in", authResult); 
		
		CreateCommand ccommand = new CreateCommand(); 
		boolean authorised = aauthentication.authorised(user, ccommand); 
		assertTrue("we SHOULD be authorised at this point", authorised); 
		
	}
	
		
	/** 
	 * 1. login  
	 * 2. make the user perform an action for which they are NOT authorised 
	 */
	public void testNotAuthorised() { 
		
		Aauthentication aauthentication = Aauthentication.instance(); 
		aauthentication.setup(); 
		aauthentication.initialise(); 
		
		IUser user = new User(); 
		user.setId("root"); 
		user.setUsername("root"); 
		user.setPassword("password"); 
		user.setLogintimeout("60000"); 
		
		aauthentication.authenticate("webkell", user); 
		boolean authResult = aauthentication.authenticated(user); 
		assertTrue("Part 1. We SHOULD be authenticated after logging in", authResult); 
		
		class StubCommand extends GCommand { 
			public StubCommand() { 
				this.setName("stub"); 
			}
		}
		
		StubCommand scommand = new StubCommand(); 
		boolean authorised = aauthentication.authorised(user, scommand); 
		assertFalse("we SHOULD NOT be authorised", authorised); 
		
	}
	
	
	/**
	 * 1. login
	 * 2. test logged in
	 * 3. logout 
	 * 4. make a user perform and action for which they are authorised  
	 */
	public void testAuthorisedButNotAuthenticated() { 
		
		Aauthentication aauthentication = Aauthentication.instance(); 
		aauthentication.setup(); 
		aauthentication.initialise(); 
		
		IUser user = new User(); 
		user.setId("root"); 
		user.setUsername("root"); 
		user.setPassword("password"); 
		user.setLogintimeout("60000"); 
		
		aauthentication.authenticate("webkell", user); 
		boolean authResult = aauthentication.authenticated(user); 
		assertTrue("Part 1. We SHOULD be authenticated after logging in", authResult); 
		
		aauthentication.logout(user); 
		boolean afterLogout = aauthentication.authenticated(user); 
		assertFalse("Part 2. We SHOULD NOT be authenticated here", afterLogout ); 
		
		CreateCommand ccommand = new CreateCommand(); 
		boolean authorised = aauthentication.authorised(user, ccommand); 
		assertFalse("we SHOULD be authorised at this point", authorised); 
		
	}
	
	
	public void testLoadFromDB() { 
		
		//Spittoon.instance().installDelete(); 
		//Spittoon.instance().installCreate(); 
		
		Aauthentication aauth = Aauthentication.instance(); 
		aauth.initialise(); 
		
		GGroups agroups = (GGroups)aauth.find("groups", "aauth.groups"); 
		Users ausers = (Users)aauth.find("users", "aauth.users"); 
		
		assertNotNull("'aauth.groups' SHOULD have been loaded from the DB", agroups); 
		assertNotNull("'aauth.users' SHOULD have been loaded from the DB", ausers); 
		
	}
	
	public void testAddGroup() { 
		
		Aauthentication aauth = Aauthentication.instance();
		Group groupStartingPoint = 
			(Group)bob.load(
				TestAAuthentication.class.getResourceAsStream("/add.usergroup.xml"), BookkeepingSystemProperties.instance().getProperty("bob.def")); 
		
		
		// 1. ensure there is an owner reference 
		Group groupTrial1 = groupStartingPoint; 
		groupTrial1.setOwner(null); 
		AuthorisationException e1 = null; 
		try { aauth.addGroup(groupTrial1); }
		catch(AuthorisationException e) { e1 = e; }
		assertNotNull("an AuthorisationException SHOULD be thrown when there's no 'owner' reference to a user in the group", e1); 
		
		
		// 2. ensure there is not another group with the same name 
		Group duplicateGroup = new Group(); 
		duplicateGroup.setId("testuser.group"); 
		duplicateGroup.setName("testuser.group"); 
		duplicateGroup.setOwner("root"); 
		aauth.addGroup(duplicateGroup); 
		
		Group groupTrial2 = groupStartingPoint; 
		AuthorisationException e2 = null; 
		try { aauth.addGroup(groupTrial2); } 
		catch(AuthorisationException e) { e2 = e; } 
		assertNotNull("an AuthorisationException SHOULD be thrown when there's a duplicate group", e2); 
		
		
		// 3. ensure that owner reference user in the group actually exists as a regular user 
		Group groupTrial3 = groupStartingPoint; 
		groupTrial3.setOwner("nonexistant.user"); 
		AuthorisationException e3 = null; 
		try { aauth.addGroup(groupTrial3); } 
		catch(AuthorisationException e) { e3 = e; } 
		assertNotNull("an AuthorisationException SHOULD be thrown when the owner is set to  non-existant user", e3); 
		
	}
	
	public void testAddGroupSetAssociatedBookkeeping() { 
		
		// 1. ** add a 'group' entry in the 'groups' directory with an empty 'bookkeeping' 
		//Spittoon.instance().installInitialise(); 
		//Spittoon.instance().installDelete(); 
		//Spittoon.instance().installCreate(); 
		
		
		// 2. load the group to add 
		Group groupStartingPoint = 
			(Group)bob.load(
				TestAAuthentication.class.getResourceAsStream("/add.usergroup.xml"), 
					BookkeepingSystemProperties.instance().getProperty("bob.def")); 
		
		com.interrupt.bookkeeping.System system = new com.interrupt.bookkeeping.System(); 
		system.initialise(); 
		
		
		this.setupTestUser(); 
		
		// 3. make the call to add the group 
		system.addGroupSetAssociatedBookkeeping(groupStartingPoint); 
		
		Group newGroup = (Group)system.getGroups().find("group", groupStartingPoint.getId()); 
		assertNotNull("There SHOULD be a new group 'testuser.group'", newGroup); 
		assertNotNull("There new group SHOULD have an id of 'testuser.group'", "testuser.group"); 
		
		Bookkeeping bookkeeping = (Bookkeeping)newGroup.findBookkeepingById("main.bookkeeping"); 
		assertNotNull("There SHOULD be the default set of books 'main.bookkeeping'", bookkeeping); 
		
		
		//** Spittoon should take a random Bob object and persist it. 
		// The said object should instruct Spittoon how to persist 
		//Spittoon.instance().update(system); 
		
		String dburl = BookkeepingSystemProperties.instance().getProperty("db.url"); 
		Group aauthGroup = null; //(Group)Spittoon.instance().getDocument(dburl + "system/aauthentication/groups", "group", groupStartingPoint.getId()); 
		Group mainGroup = null; //(Group)Spittoon.instance().getDocument(dburl + "system/groups", "group", groupStartingPoint.getId()); 
		assertNotNull("new group in aauthentication collection is NULL", aauthGroup); 
		//assertNotNull("new group in groups collection is NULL", mainGroup); 
		/**/
		
	}
	
	public void testRemoveGroup() { 
		
		// initialise Spittoon and System 
		//Spittoon.instance().installInitialise(); 
		//Spittoon.instance().installDelete(); 
		//Spittoon.instance().installCreate(); 
		com.interrupt.bookkeeping.System system = new com.interrupt.bookkeeping.System(); 
		system.initialise(); 
		
		
		// load and add the test group 
		this.setupTestUser(); 
		Group groupStartingPoint = 
			(Group)bob.load(
				TestAAuthentication.class.getResourceAsStream("/add.usergroup.xml"), 
					BookkeepingSystemProperties.instance().getProperty("bob.def")); 
		system.addGroupSetAssociatedBookkeeping(groupStartingPoint); 
		
		
		User testUser1 = (User)Bob.loadS(
			"<user xmlns='com/interrupt/bookkeeping/users' id='testuser1' username='testuser1' password='password' />", 
				BookkeepingSystemProperties.instance().getProperty("bob.def")); 
		User testUser2 = (User)Bob.loadS(
			"<user xmlns='com/interrupt/bookkeeping/users' id='testuser2' username='testuser2' password='password' />", 
				BookkeepingSystemProperties.instance().getProperty("bob.def")); 
		String testGroupId = "testuser.group"; 
		
		
		// add user(s) to the system 
		AddUserToGroupVisitor auvisitor = new AddUserToGroupVisitor(); 
		auvisitor.groupid = testGroupId; 
		auvisitor.userToAdd = testUser1; 
		system.getAauthentication().accept(auvisitor); 
		
		auvisitor.done = false; 
		auvisitor.userToAdd = testUser2; 
		system.getAauthentication().accept(auvisitor); 
		
		
		// 1. ensure there are no more users besides the default user before deleting 
		// 2. move group / bookkeeping data to a removed folder for future reference 
		AuthorisationException ee = null; 
		try { 
			system.getAauthentication().removeGroup(testGroupId); 
		}
		catch(AuthorisationException e) { 
			ee = e; 
		}
		assertNotNull("more than 1 default user in group - There SHOULD have been an 'AuthorisationException'", ee); 
		
		
		// remove the extra test user 
		RemoveUserFromGroupVisitor ruvisitor = new RemoveUserFromGroupVisitor(); 
		ruvisitor.groupid = testGroupId; 
		ruvisitor.userToRemove = testUser2; 
		system.getAauthentication().accept(ruvisitor); 
		
		// now removing groups should work 
		AuthorisationException e2 = null; 
		try { 
			system.getAauthentication().removeGroup(testGroupId); 
		}
		catch(AuthorisationException e) { 
			e2 = e; 
		}
		assertNull("1 default user in group - There should be NO 'AuthorisationException'", e2); 
		
	}
	
	
	/**
	 * These methods will pretty much take the abouve methods and wrap adding a user around them. 
	 * The group id, name and owner should correspond with the user's 
	 */
	public void testAddUserDefaultGroup() { 
		
		com.interrupt.bookkeeping.System system = new com.interrupt.bookkeeping.System(); 
		system.setSpittoon(spittoon); 
		system.initialise(); 
		
		User userStartingPoint = 
			(User)bob.load(
				TestAAuthentication.class.getResourceAsStream("/add.user.xml"), 
					BookkeepingSystemProperties.instance().getProperty("bob.def")); 
		
		
		//** 1. ensure that there's not duplicate user id 
		User userTry1 = userStartingPoint; 
		userTry1.setId("root"); 
		AuthorisationException ee = null; 
		try { 
			system.getAauthentication().addUser(userTry1); 
		}
		catch(AuthorisationException e) { 
			ee = e; 
		}
		assertNotNull("There SHOULD be an exception if there's a duplicate User 'id' or 'name'", ee); 
		
		
		//** 2. test that the user was added to the XML DB 
		userStartingPoint.setId("testuser"); 
		AuthorisationException e2 = null; 
		try { 
			
			system.getAauthentication().addUser(userStartingPoint); 
		}
		catch(AuthorisationException e) { 
			e.printStackTrace(); 
			e2 = e; 
		}
		
		String adburl = spittoon.getAauthDbUrl(); 
		String axpath = "/system[ @id='main.system' ]/aauthentication[ @id='main.authentication' ]/users[ @id='aauth.users' ]/user[ @id='testuser' ]"; 
		User aauthUser = (User)spittoon.retrieve(adburl, axpath, false); 
		
		String message = null;
		if(e2 != null) 
			message = e2.getMessage(); 
		assertNull("There should NOT be any exception when adding a unique user / message["+ message +"]", e2); 
		assertNotNull("The test user was NOT added to the database", aauthUser); 
		
		
		//** 3. test that there's a default group associated with new user 
		String gxpath = "/system[ @id='main.system' ]/aauthentication[ @id='main.authentication' ]/groups[ @id='aauth.groups' ]/group[ @id='testuser.group' ]"; 
		Group aauthGroup = (Group)spittoon.retrieve(adburl, gxpath, false); 
		
		assertNotNull("There SHOULD be an associated group with this new user", aauthGroup); 
		assertEquals("The group id SHOULD be 'testuser.group'", "testuser.group", aauthGroup.getId()); 
		
		// 4 test that there's a default associated group with with associated bookkeeping 
		Group aauthGroupBK = null; 
			//(Group)Spittoon.instance().getDocument(dburl + "system/groups", 
				//"group", userStartingPoint.getId() + ".group"); 
		assertNotNull("There SHOULD be an associated group/bookkeeping with this new user", aauthGroup); 
		assertEquals("The group/bookkeeping id SHOULD be 'testuser.group'", "testuser.group", aauthGroup.getId()); 
		
	}
	
	
	/**
	 * should be a fairly easy job of i) removing the group (with its checks and balances), 
	 * then ii) removing the owning user. 
	 */
	public void testRemoveUserDefaultGroup() { 
		
		//Spittoon.instance().installInitialise(); 
		//Spittoon.instance().installDelete(); 
		//Spittoon.instance().installCreate(); 
		
		com.interrupt.bookkeeping.System system = new com.interrupt.bookkeeping.System(); 
		system.initialise(); 
		
		User userStartingPoint = 
			(User)bob.load(
				TestAAuthentication.class.getResourceAsStream("/add.user.xml"), 
					BookkeepingSystemProperties.instance().getProperty("bob.def")); 
		system.getAauthentication().addUser(userStartingPoint); 
		//Spittoon.instance().update(system); 
		
		
		system.getAauthentication().removeUser(userStartingPoint.getId()); 
		//Spittoon.instance().update(system); 
		
		
		String dburl = BookkeepingSystemProperties.instance().getProperty("db.url"); 
		String testGroupName = userStartingPoint.getId() + ".group"; 
		
		User testUser = null; 
			//(User)Spittoon.instance().getDocument(dburl + "system/aauthentication/users", 
				//"user", userStartingPoint.getId()); 
		Group testGroup = null; 
			//(Group)Spittoon.instance().getDocument(dburl + "system/aauthentication/groups", 
				//"group", testGroupName); 
		Group testGroupBK = null; 
			//(Group)Spittoon.instance().getDocument(dburl + "system/groups", 
				//"group", testGroupName); 
		
		assertNull("There should be NO user 'testuser' in system/aauthentication/users/", testUser); 
		assertNull("There should be NO group 'testuser.group' in system/aauthentication/groups/", testGroup); 
		assertNull("There should be NO group 'testuser.group' in system/groups/", testGroupBK); 
		
	}
	
	
	public void testAddUserToGroup() { 
		
		//Spittoon.instance().installInitialise(); 
		//Spittoon.instance().installDelete(); 
		//Spittoon.instance().installCreate(); 
		
		com.interrupt.bookkeeping.System system = new com.interrupt.bookkeeping.System(); 
		system.initialise(); 
		
		
		// 1. add a second user 
		User userStartingPoint = 
			(User)bob.load(
				TestAAuthentication.class.getResourceAsStream("/add.user.xml"), 
					BookkeepingSystemProperties.instance().getProperty("bob.def")); 
		system.addUser(userStartingPoint); 
		//Spittoon.instance().update(system); 
		
		
		// 2. add an alternative group 
		Group groupStartingPoint = 
			(Group)bob.load(
				TestAAuthentication.class.getResourceAsStream("/add.group.xml"), BookkeepingSystemProperties.instance().getProperty("bob.def")); 
		system.addGroup(groupStartingPoint); 
		
		
		// 3. add the user to the group 
		system.getAauthentication().addUserToGroup(groupStartingPoint.getId(), userStartingPoint); 
		//Spittoon.instance().update(system); 
		
		
		//** assert user is in group, in DB 
		String dburl = BookkeepingSystemProperties.instance().getProperty("db.url"); 
		User resultUser = null; 
			//(User)Spittoon.instance().getDocument(dburl + "system/aauthentication/groups", 
				//"user", userStartingPoint.getId()); 
		assertNotNull("The user SHOULD exist in the group", resultUser); 
		assertEquals("The user id SHOULD be 'testuser'", userStartingPoint.getId(), resultUser.getId()); 
		
		
		//** assert user is in group, in memory 
		Group memGroup = (Group)system.getAauthentication().getGroups().findGroupById(groupStartingPoint.getId()); 
		User memUser = (User)memGroup.findUserById(userStartingPoint.getId()); 
		assertNotNull("The user (in memory) SHOULD exist in the group", memUser); 
		assertEquals("The user id (in memory) SHOULD be 'testuser'", userStartingPoint.getId(), memUser.getId()); 
		
	}
	public void testRemoveUserFromGroup() { 
		
		//Spittoon.instance().installInitialise(); 
		//Spittoon.instance().installDelete(); 
		//Spittoon.instance().installCreate(); 
		
		com.interrupt.bookkeeping.System system = new com.interrupt.bookkeeping.System(); 
		system.initialise(); 
		
		// 1. add a second user 
		User userStartingPoint = 
			(User)bob.load(
				TestAAuthentication.class.getResourceAsStream("/add.user.xml"), 
					BookkeepingSystemProperties.instance().getProperty("bob.def")); 
		system.addUser(userStartingPoint); 
		//Spittoon.instance().update(system); 
		
		// 2. add an alternative group 
		Group groupStartingPoint = 
			(Group)bob.load(
				TestAAuthentication.class.getResourceAsStream("/add.group.xml"), BookkeepingSystemProperties.instance().getProperty("bob.def")); 
		system.addGroup(groupStartingPoint); 
		//Spittoon.instance().update(system); 
		
		
		
		// TESTs 
		//** passing in a non-existant groupid  
		AuthorisationException e1 = null; 
		try { 
			system.getAauthentication().removeUserFromGroup("non.existent.group", userStartingPoint.getId()); 
		}
		catch(AuthorisationException e) { 
			e1 = e; 
		}
		assertNotNull("There SHOULD be an error here as there is no 'non.existent.group'", e1); 
		
		
		//** passing in a non-existant userid 
		AuthorisationException e2 = null; 
		try { 
			system.getAauthentication().removeUserFromGroup(groupStartingPoint.getId(), "non.existent.user"); 
		}
		catch(AuthorisationException e) { 
			e2 = e; 
		}
		assertNotNull("There SHOULD be an error here as there is no 'non.existent.user'", e2); 
		
		
		//** trying to remove a user not in the group, should throw an exception 
		AuthorisationException e3 = null; 
		try { 
			system.getAauthentication().removeUserFromGroup(groupStartingPoint.getId(), userStartingPoint.getId()); 
		}
		catch(AuthorisationException e) { 
			e3 = e; 
		}
		assertNotNull("There SHOULD be an error here as the 'testuser' isn't yet in the 'alternative.group'", e3); 
		
		
		// 3. add the user to the group 
		system.getAauthentication().addUserToGroup(groupStartingPoint.getId(), userStartingPoint); 
		//Spittoon.instance().update(system); 
		
		
		system.getAauthentication().removeUserFromGroup(groupStartingPoint.getId(), userStartingPoint.getId());
		User finalUser = (User)system.getAauthentication().getGroups().findGroupById(groupStartingPoint.getId()).findUserById(userStartingPoint.getId()); 
		assertNull("The removed user should NOT be in the group", finalUser); 
		
	}
	
	
	/**
	 * a utility method to add a test user to the system 
	 */
	public void setupTestUser() { 

		User testUser = (User)Bob.loadS(
			"<user xmlns='com/interrupt/bookkeeping/users' id='testuser' username='testuser' password='password' />", 
				BookkeepingSystemProperties.instance().getProperty("bob.def")); 
		
		// 3. i. get aauth users from DB, ii. add test user and iii. persist to DB 
		String dburl = BookkeepingSystemProperties.instance().getProperty("db.url"); 
		Users aauthUsers = null; //(Users)Spittoon.instance().getDocument(dburl + "system/aauthentication/users/", "users", "aauth.users"); 
		aauthUsers.addUser(testUser); 
		//Spittoon.instance().updateDocument(
			//dburl + "system/aauthentication/users/", 
				//aauthUsers.getTagName(), aauthUsers.getId(), aauthUsers); 
		
	}
	
	/** 
	 * utility classes to add and remove user(s) to/from a group 
	 * they are meant to work at depth 
	 */
	class AddUserToGroupVisitor implements IVisitor { 
		public String groupid = ""; 
		public User userToAdd = null; 
		public boolean done = false; 
		public void visit(IBob bob) { 
			
			if(!done) { 
				if(	bob.getTagName().equals("group") && 
					bob.getAttributes().getValue("id").equals(groupid)) { 
					((Group)bob).addUser(userToAdd); 
					done = true; 
				}
			}
		}
	}
	class RemoveUserFromGroupVisitor implements IVisitor { 
		public String groupid = ""; 
		public User userToRemove = null; 
		public boolean done = false; 
		public void visit(IBob bob) { 
			
			if(!done) { 
				if(	bob.getTagName().equals("group") && 
					bob.getAttributes().getValue("id").equals(groupid)) { 
					((Group)bob).removeUserById(userToRemove.getId()); 
					done = true; 
				}
			}
		}
	}
	
}


