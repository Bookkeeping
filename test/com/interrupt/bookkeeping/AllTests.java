
package com.interrupt.bookkeeping;

import org.xmldb.api.base.XMLDBException;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

public class AllTests extends TestCase {
    
    public static Test suite() throws XMLDBException, ClassNotFoundException, IllegalAccessException, InstantiationException {
		
		TestSuite suite = new TestSuite();
		/*
		suite.addTest( com.interrupt.bookkeeping.action.TestActions.suite() );
		suite.addTest( com.interrupt.bookkeeping.cc.kkell.kommand.TestAbstractCommand.suite() );
		suite.addTest( com.interrupt.bookkeeping.cc.kkell.kommand.TestAddCommand.suite() );
		suite.addTest( com.interrupt.bookkeeping.cc.kkell.kommand.TestCreateCommand.suite() );
		suite.addTest( com.interrupt.bookkeeping.cc.kkell.kommand.TestRemoveCommand.suite() );
		suite.addTest( com.interrupt.bookkeeping.cc.kkell.kommand.TestReverseCommand.suite() );
		suite.addTest( com.interrupt.bookkeeping.cc.kkell.kommand.TestFindCommand.suite() );
		suite.addTest( com.interrupt.bookkeeping.cc.kkell.kommand.TestListCommand.suite() );
		
		//suite.addTest( com.interrupt.bookkeeping.cc.kkell.TestBkell.suite() );
		suite.addTest( com.interrupt.util.TestDateUtil.suite() );
		suite.addTest( com.interrupt.bookkeeping.journal.TestEntry.suite() );
		
		//suite.addTest( com.interrupt.bookkeeping.transaction.TestTransaction.suite() );
		suite.addTest( com.interrupt.bookkeeping.workflow.TestCloseEntry.suite() );
		suite.addTest( com.interrupt.persistence.TestPersistence.suite() );
		suite.addTest( com.interrupt.util.TestIdGenerator.suite() );
		*/
		
		/*suite.addTest( com.interrupt.bookkeeping.currency.TestCurrencies.suite() ); 
		suite.addTest( com.interrupt.bookkeeping.TestGeneral.suite() ); 
		suite.addTest( com.interrupt.bookkeeping.account.TestAccounts.suite() ); 
		*/
		
		suite.addTest( com.interrupt.bookkeeping.cc.kkell.aauth.TestAAuthentication.suite() ); 
		suite.addTest( com.interrupt.bookkeeping.http.TestBookkeepingServlet.suite() ); 
		//suite.addTest( com.interrupt.bookkeeping.cc.kkell.TestConfigure.suite() ); 
		
		return suite;
		
    }
    
}

