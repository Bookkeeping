
package com.interrupt.util;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.Set;
import java.util.TreeSet;

public class TestIdGenerator extends TestCase {
    
    
    public TestIdGenerator() {}
    public TestIdGenerator(String name) {
	super(name);
    }
    

    /* SETUP / TEARDOWN
     */
    public void setUp() {}
    public void tearDown() {}
    
    public static Test suite() {
	
	TestSuite suite = new TestSuite();
	suite.addTest( new TestIdGenerator("testUniqueID1") );
	suite.addTest( new TestIdGenerator("testUniqueID2") );
	
	return suite;
    }


    /* make sure set only accepts unique 'String' objects
     */
    public void testUniqueID1() {
	
	String id1 = IdGenerator.generateId();
	String id2 = id1;

	Set idSet = new TreeSet();
	idSet.add(id1);
	idSet.add(id2);
	
	assertEquals("Set size should be 1", 1, idSet.size());
    }

    /* make sure IDs are unique up to 100'000
     */
    public void testUniqueID2() {
	
	String nextid = null;
	Set idSet = new TreeSet();
	for(int i = 0; i < 100000; i++) {
	    nextid = IdGenerator.generateId();
	    idSet.add(nextid);
	}
	assertEquals("the Set SHOULD be 100'000", 100000, idSet.size());
    }
    
}


