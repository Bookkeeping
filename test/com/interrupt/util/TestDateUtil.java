package com.interrupt.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.interrupt.bookkeeping.TestGeneral;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class TestDateUtil extends TestCase {
	
	
	
	public TestDateUtil() {
	}
    public TestDateUtil(String name) {
		super(name);
    }
    
    
    public static Test suite() {
		
		TestSuite suite = new TestSuite();
		suite.addTest( new TestDateUtil("testBefore") );
		suite.addTest( new TestDateUtil("testAfter") );
		suite.addTest( new TestDateUtil("testEquals") );
		
		return suite;
    }
    
	public void testBefore() { 
		
		
		String date_1 = "08/15/2005 00:00:00 EST"; 
		String date_2 = "09/15/2005 00:00:00 EST";
		
		boolean isbefore = DateUtil.before( date_1, date_2 ); 
		
		assertTrue( "date_1 SHOULD be before date_2", isbefore ); 
		
	}
	
	public void testAfter() { 
		
		
		String date_1 = "11/15/2005 00:00:00 EST"; 
		String date_2 = "09/15/2005 00:00:00 EST";
		boolean isafter = DateUtil.after( date_1, date_2 ); 
		assertTrue( "date_1 SHOULD be after date_2", isafter ); 
		
		
		date_1 = "09/15/2005 04:30:25 EST"; 
		date_2 = "09/15/2005 04:30:00 EST";
		isafter = DateUtil.after( date_1, date_2 ); 
		assertTrue( "date_1 SHOULD be after date_2 - down to the second", isafter ); 
		
		
	}
	
	
	public void testEquals() { 
		
		
		String date_1 = "09/15/2005 12:35:43 EST"; 
		String date_2 = "09/15/2005 12:35:43 EST";
		
		boolean isequals = DateUtil.equals( date_1, date_2 ); 
		
		assertTrue( "date_1 SHOULD be equal to date_2 - down to the second", isequals ); 
		
	}
	
	
}


