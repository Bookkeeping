
package com.interrupt.persistence;


import org.apache.log4j.Logger;
import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.Database;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.XMLResource;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.Set;
import java.util.TreeSet;

import com.interrupt.bob.base.IBob;
import com.interrupt.bookkeeping.cc.bkell.Bkell;
import com.interrupt.bookkeeping.journal.IEntry;
import com.interrupt.bookkeeping.journal.Entry;
import com.interrupt.bookkeeping.system.BookkeepingSystem;
//import com.interrupt.persistence.or.ORPersistenceManager;
//import com.interrupt.persistence.xml.XMLPersistenceManager;


public class TestPersistence extends TestCase {
    
    
	private Logger logger = Logger.getLogger(TestPersistence.class); 
	private String URI = "xmldb:exist://localhost:8090/exist/xmlrpc/db/bookkeeping";
    private Collection col = null;
    private String testResourceName = "bookkeeping.test.xml";
    
    
    public TestPersistence() {
	
	this._initialiseDriver();
    }
	
    public TestPersistence(String name) {
	
	super(name);
	this._initialiseDriver();
    }
    
    private void _initialiseDriver() {

	try {
	    // initialize driver
	    String driver = "org.exist.xmldb.DatabaseImpl";
	    Class cl = Class.forName(driver);
	    Database database = (Database)cl.newInstance();
	    DatabaseManager.registerDatabase(database);
	}
	catch(Exception e) {
	    e.printStackTrace();
	}
    }
    

    /* SETUP / TEARDOWN
     *
    protected void setUp() {
		
		logger.debug("SETUP Called");
		
		// create initial 'bookkeeping.xml'
		try {
		    
		    //Collection col = DatabaseManager.getCollection(URI);
		    col = DatabaseManager.getCollection(URI);
		    XMLResource document = (XMLResource)col.createResource( testResourceName, "XMLResource" );
		    String file = "/Users/timothyw/projects/software/bookkeeping/xml/bookkeeping.xml";
		    File f = new File(file);
		    if(!f.canRead()) {
			logger.debug("cannot read file " + file);
			return;
		    }
		    document.setContent(f);
		    System.out.print("storing document " + document.getId() + "...");
		    col.storeResource(document);
		    logger.debug("ok.");
		    
		}
		catch(XMLDBException e) {
		    e.printStackTrace();
		}
		
    }
	*/
	
    protected void tearDown() {}
    
    public static Test suite() {
		
		TestSuite suite = new TestSuite();
		//suite.addTest( new TestPersistence("testAddEntry") );
		//suite.addTest( new TestPersistence("testRetrieveEntry") );
		//suite.addTest( new TestPersistence("testUpdateEntry") );
		//suite.addTest( new TestPersistence("testBobRetrieve") );
		//suite.addTest( new TestPersistence("testBobUpdate") );
		
		// system ensures that all entries -> link to debit/credits in account
		// add an entry
		
		return suite;
    }

    public void testAddEntry() {
	
	IEntry entry = new Entry();
	//PersistenceManager pmanager = new XMLPersistenceManager();
	
	XMLResource xres = null;
	try {
		xres = (XMLResource)col.getResource( testResourceName );
		String xcontent = (String)xres.getContent();

		logger.debug("");
		logger.debug("=========== testAddEntry ============");
		logger.debug(xcontent);

	}
	catch(org.xmldb.api.base.XMLDBException e) {
		e.printStackTrace();
	}
	
    }

    public void testRetrieveEntry() {
	
	IEntry entry = new Entry();
	//PersistenceManager pmanager = new XMLPersistenceManager();
	
	XMLResource xres = null;
	try {
		xres = (XMLResource)col.getResource( testResourceName );
		String xcontent = (String)xres.getContent();

		logger.debug("");
		logger.debug("============ testRetrieveEntry ============");
		logger.debug(xcontent);

	}
	catch(org.xmldb.api.base.XMLDBException e) {
		e.printStackTrace();
	}
	
    }

    public void testUpdateEntry() {
	
String newXML = "<?xml version='1.0' encoding='UTF-8'?> <bookkeeping xmlns:account='com/interrupt/bookkeeping/account' xmlns:journal='com/interrupt/bookkeeping/journal' xmlns='com/interrupt/bookkeeping' id='newid'> <account:accounts id=''><!-- 1. account types are: asset,liability,expense,revenue,capital 2. each account has a given counterWeight(debit or credit) --> <account:account type='' id='' name='' counterWeight=''> <account:debit/> <account:credit/> </account:account> </account:accounts> <journal:journal id='' name='' type='' balance=''> <journal:transactions id=''><!-- ** state can be 'open' or 'closed' --> <journal:transaction id='' state='' date=''><!-- 1. each entry should have an 'entryid' 2. each debit/credit object has a reference to an 'entryid' 3. each debit/credit object has a reference to an 'accountid' ** state can be 'open', 'closed', or 'reversed' --> <journal:entryid='' entrynum='' state='' date=''> <account:debit id='' amount='' entryid='' accountid=''/> <account:credit id='' amount='' entryid='' accountid=''/> </journal:entry> </journal:transaction> </journal:transactions> </journal:journal> </bookkeeping>";


	IEntry entry = new Entry();
	//PersistenceManager pmanager = new XMLPersistenceManager();
	
	// 1. create BobHandler w/ definitions
	//BobHandler bhandler = new BobHandler();
	
	// 2. get the content as SAX
	XMLResource xres = null;
	try {
	    xres = (XMLResource)col.getResource( testResourceName );
	    //String xcontent = (String)xres.getContentAsSax( bhandler );
	    
	    xres.setContent( newXML );
	    String xcontent = (String)xres.getContent();
	    
	    logger.debug("");
	    logger.debug("============ testUpdateEntry ============");
	    logger.debug(xcontent);

	}
	catch(org.xmldb.api.base.XMLDBException e) {
	    e.printStackTrace();
	}

    }
    
    public void testBobRetrieve() {
	
    	/* 
	//String tempFileName = "/Users/timothyw/temp/temp.bookkeeping.system.xml";
	
	//BookkeepingSystem bsystem = new BookkeepingSystem();
	//IBob result = bsystem.process( tempFileName );
	BookkeepingSystem bsystem = new BookkeepingSystem();
	String results = bsystem.retrieve();
	
	logger.debug( "ZZzzz >> test002 > " + results );
    }
    public void testBobUpdate() {
	
	File xmlf = new File("/Users/timothyw/projects/software/bookkeeping/xml/bookkeeping.system.2.xml");
	FileReader xmlr = null;
	BufferedReader breader = null;
	String line = null;
	StringBuffer sbuffer = new StringBuffer();
	try {
	    
	    xmlr = new FileReader( xmlf );
	    breader = new BufferedReader( xmlr );
	    while( (line = breader.readLine()) != null ) {
		sbuffer.append( line );
	    }
	}
	catch(FileNotFoundException e) {
	    e.printStackTrace();
	}
	catch(IOException e) {
	    e.printStackTrace();
	}

	logger.debug( "ZZzzz >> test003 > " + sbuffer.toString() );   //xmls );
	
	BookkeepingSystem bsystem = new BookkeepingSystem();
	bsystem.update( sbuffer.toString(), "bookkeeping.system.1.xml" );
	*/
	
    }
}


