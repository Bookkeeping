package com.interrupt.persistence;

import org.apache.log4j.Logger;
import org.exist.EXistException;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.XMLDBException;

import com.interrupt.bookkeeping.cc.kkell.aauth.TestAAuthentication;
import com.interrupt.bookkeeping.http.TestBookkeepingServlet;
import com.interrupt.bookkeeping.users.IGroup;
import com.interrupt.bookkeeping.users.IGroups;
import com.interrupt.bookkeeping.users.IUsers;
import com.interrupt.spittoon.Spittoon;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class TestSpittoon extends TestCase {
	
	
	private Logger logger = Logger.getLogger(TestSpittoon.class); 
	private Spittoon spittoon = null; 
	private String baseURL = null; 
	public TestSpittoon() {
		
		super(); 
		baseURL = "xmldb:exist://localhost:8090/exist/xmlrpc/db/bookkeeping/"; 
	}
	public TestSpittoon(String name) {
		
		super(name); 
		baseURL = "xmldb:exist://localhost:8090/exist/xmlrpc/db/bookkeeping/"; 
	}
	
	protected void setUp() {
		
		try { 
			super.setUp(); 
		}
		catch(Exception e) { 
			logger.error("General Exception",e);
		}
		
		//spittoon = Spittoon.instance();  
		
	}
	protected void tearDown() {
		
		/*try { 
			super.tearDown(); 
			 
		}
		catch(XMLDBException e) { 
			logger.error("Exception removing collection ["+baseURL+"]",e); 
		}
		catch(Exception e) { 
			logger.error("General Exception",e);
		}
		*/
		
	}
	public static Test suite() {
		
		TestSuite suite = new TestSuite(); 
		suite.addTest( new TestSpittoon("testInstallInitialise") );
		suite.addTest( new TestSpittoon("testInstallCreate") );
		//suite.addTest( new TestSpittoon("testInstallDelete") );
		return suite; 
		
	}
	
	
	/**
	 * tests the database initialisation and setup of Spittoon 
	 */
	public void testInstallInitialise() { 
		
		//1. assert there is no bookkeeping collection to begin with 
		//Collection baseBookeepingCol = spittoon.getCollection(baseURL); 
		//assertNull("There should be NO base bookkeeping collection to begin with", baseBookeepingCol); 
		
		//spittoon.installInitialise(); 
		
		Collection collSys = null;	//spittoon.getCollection(baseURL + "system/"); 
		Collection collGroups = null;	//spittoon.getCollection(baseURL + "system/groups/"); 
		Collection collAAGroups = null;	//spittoon.getCollection(baseURL + "system/aauthentication/groups/"); 
		Collection collAAUsers = null;	//spittoon.getCollection(baseURL + "system/aauthentication/users/"); 
		
		assertNotNull("collSys is NULL", collSys); 
		assertNotNull("collGroups is NULL", collGroups); 
		assertNotNull("collAAGroups is NULL", collAAGroups); 
		assertNotNull("collAAUsers is NULL", collAAUsers); 
		
	}
	
	public void testInstallCreate() { 
		
		//spittoon.installInitialise(); 
		//spittoon.installCreate(); 
		
		IGroup webkellGroup = null;	//(IGroup)spittoon.getDocument(baseURL + "system/groups/", "group", "webkell"); 
		assertNotNull("Expecting to see the 'webkell' group", webkellGroup); 
		
		IGroups mainGroups = null;	//(IGroups)spittoon.getDocument(baseURL + "system/aauthentication/groups/", "groups", "main.groups"); 
		assertNotNull("Expecting to see the 'main.groups' groups", mainGroups); 
		
		IUsers mainUsers = null;	//(IUsers)spittoon.getDocument(baseURL + "system/aauthentication/users/", "users", "main.users"); 
		assertNotNull("Expecting to see the 'main.users' users", mainUsers); 
		
	}
	public void testInstallDelete() throws XMLDBException { 
		
		//spittoon.installInitialise(); 
		//spittoon.installCreate(); 
		Exception ee = null; 
		//try { 
		//	spittoon.removeCollection(baseURL);
		//}
		//catch(Exception e) { 
		//	ee = e; 
		//}
		assertNotNull("There SHOULD be an EXistException when trying to get '"+ baseURL +"' collection", ee); 
		//assertTrue("Exception thrown SHOULD an 'EXistException'", ee instanceof EXistException);
		
	}
	
}
