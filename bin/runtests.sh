#!/bin/bash

export BK_HOME=/Users/timothyw/projects/software/bookkeeping
export CP="\
.:\
build/gen:\
build/src:\
build/test:\
$BK_HOME/lib/bob.jar:\
$BK_HOME/lib/Piccolo.jar:\
$BK_HOME/lib/commons-cli-1.0.jar:\
$BK_HOME/lib/commons-logging-api.jar:\
$BK_HOME/lib/commons-logging.jar:\
$BK_HOME/lib/junit.jar:\
$BK_HOME/lib/log4j-1.2.7.jar:\
$BK_HOME/lib/velocity-1.3.1.jar:\
$BK_HOME/lib/velocity-dep-1.3.1.jar"


#./rundocs.sh
java -classpath $CP junit.textui.TestRunner com.interrupt.bookkeeping.AllTests


