
echo off
set CP=".;xml/;xml/http/;build/gen;build/src;lib/;lib/antlr.jar;lib/commons-pool-1.2.jar;lib/jgroups-all.jar;lib/resolver.jar;lib/sunxacml.jar;lib/Piccolo.jar;lib/commons-cli-1.0.jar;lib/commons-logging-api.jar;lib/javafind.jar;lib/commons-logging.jar;lib/bob.jar;lib/sablecc.jar;lib/log4j-1.2.9.jar;lib/velocity-1.3.1.jar;lib/velocity-dep-1.3.1.jar;lib/exist-optional.jar;lib/exist-modules.jar;lib/exist.jar;lib/xmldb.jar;lib/xmlrpc-1.2-patched.jar;build/bookkeeping.jar" 

rem set DBURL="xmldb:exist://localhost:8080/exist/xmlrpc/db/bookkeeping/"
set DBURL="xmldb:exist:///db/bookkeeping/"


rem embed the exist DB 
java -classpath %CP% -Ddb.url=%DBURL% -Dexist.initdb="true" -Dexist.home="." -Dorg.xml.sax.features.validation="false" -Djava.endorsed.dirs="lib/endorsed" com.interrupt.bookkeeping.cc.bkell.Bkell 

