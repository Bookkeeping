#!/bin/bash

export BK_HOME=/Users/timothyw/projects/software/Bob/BOBJava
export CP="\
$BK_HOME/build/gen:\
$BK_HOME/build/src:\
$BK_HOME/build/test:\
$BK_HOME/lib/bob.jar"

export PKGS="\
org.shields.junit \
com.interrupt \
com.interrupt.persistence \
com.interrupt.persistence.or \
com.interrupt.persistence.xml \
com.interrupt.util \
com.interrupt.bookkeeping \
com.interrupt.bookkeeping.account \
com.interrupt.bookkeeping.action \
com.interrupt.bookkeeping.exception \
com.interrupt.bookkeeping.exception \
com.interrupt.bookkeeping.journal \
com.interrupt.bookkeeping.util \
com.interrupt.bookkeeping.workflow"

javadoc -classpath $CP -d gen/doc/ -sourcepath src/:test/:gen/ $PKGS


