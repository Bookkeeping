#!/bin/bash

export BK_HOME=.
export CP="\
.:\
$BK_HOME/xml/:\
$BK_HOME/xml/http/:\
$BK_HOME/build/gen:\
$BK_HOME/build/src:\
$BK_HOME/build/test:\
$BK_HOME/lib/spittoon/spittoon.jar:\
$BK_HOME/lib/zob/AntelopeTasks_3.2.19.jar:\
$BK_HOME/lib/zob/bob.jar:\
$BK_HOME/lib/zob/commons-cli-1.0.jar:\
$BK_HOME/lib/zob/commons-logging-api.jar:\
$BK_HOME/lib/zob/commons-logging.jar:\
$BK_HOME/lib/zob/javafind.jar:\
$BK_HOME/lib/zob/junit.jar:\
$BK_HOME/lib/zob/Piccolo-1.0.4.jar:\
$BK_HOME/lib/zob/sablecc.jar:\
$BK_HOME/lib/zob/velocity-1.3.1.jar:\
$BK_HOME/lib/zob/velocity-dep-1.3.1.jar:\
$BK_HOME/lib/:\
$BK_HOME/lib/cactus/:\
$BK_HOME/lib/cactus/aspectjrt-1.2.1.jar:\
$BK_HOME/lib/cactus/cactus-1.7.2.jar:\
$BK_HOME/lib/cactus/cactus-ant-1.7.2.jar:\
$BK_HOME/lib/cactus/cargo-0.5.jar:\
$BK_HOME/lib/cactus/commons-httpclient-2.0.2.jar:\
$BK_HOME/lib/cactus/commons-logging-1.0.4.jar:\
$BK_HOME/lib/cactus/httpunit-1.6.jar:\
$BK_HOME/lib/cactus/junit-3.8.1.jar:\
$BK_HOME/lib/cactus/nekohtml-0.9.1.jar:\
$BK_HOME/lib/cactus/servletapi-2.2.jar:\
$BK_HOME/lib/cactus/servletapi-2.3.jar:\
$BK_HOME/lib/endorsed/resolver-1.2.jar:\
$BK_HOME/lib/endorsed/serializer-2.9.1.jar:\
$BK_HOME/lib/endorsed/xalan-2.7.1.jar:\
$BK_HOME/lib/endorsed/xercesImpl-2.9.1.jar:\
$BK_HOME/lib/endorsed/xml-apis.jar:\
$BK_HOME/lib/exist/core/antlr-2.7.6.jar:\
$BK_HOME/lib/exist/core/commons-collections-3.1.jar:\
$BK_HOME/lib/exist/core/commons-logging-1.0.4.jar:\
$BK_HOME/lib/exist/core/commons-pool-1.4.jar:\
$BK_HOME/lib/exist/core/excalibur-cli-1.0.jar:\
$BK_HOME/lib/exist/core/javax.servlet-1.4.jar:\
$BK_HOME/lib/exist/core/jEdit-syntax.jar:\
$BK_HOME/lib/exist/core/jgroups-all.jar:\
$BK_HOME/lib/exist/core/jline-0_9_5.jar:\
$BK_HOME/lib/exist/core/jta.jar:\
$BK_HOME/lib/exist/core/log4j-1.2.15.jar:\
$BK_HOME/lib/exist/core/quartz-1.6.0.jar:\
$BK_HOME/lib/exist/core/stax-api-1.0.1.jar:\
$BK_HOME/lib/exist/core/sunxacml.jar:\
$BK_HOME/lib/exist/core/xmldb.jar:\
$BK_HOME/lib/exist/core/xmlrpc-1.2-patched.jar:\
$BK_HOME/lib/exist/exist-optional.jar:\
$BK_HOME/lib/exist/exist.jar:\
$BK_HOME/lib/exist/extensions/exist-jmx.jar:\
$BK_HOME/lib/exist/extensions/exist-modules.jar:\
$BK_HOME/lib/exist/extensions/exist-ngram-module.jar:\
$BK_HOME/lib/exist/optional/axis-ant.jar:\
$BK_HOME/lib/exist/optional/axis-schema.jar:\
$BK_HOME/lib/exist/optional/axis.jar:\
$BK_HOME/lib/exist/optional/commons-codec-1.3.jar:\
$BK_HOME/lib/exist/optional/commons-discovery-0.4.jar:\
$BK_HOME/lib/exist/optional/commons-fileupload-1.1.1.jar:\
$BK_HOME/lib/exist/optional/commons-httpclient-3.0.1.jar:\
$BK_HOME/lib/exist/optional/commons-io-1.3.jar:\
$BK_HOME/lib/exist/optional/jaxrpc.jar:\
$BK_HOME/lib/exist/optional/saaj.jar:\
$BK_HOME/lib/exist/optional/wsdl4j-1.5.1.jar:\
$BK_HOME/lib/exist/start.jar:\
$BK_HOME/lib/exist/user/nekohtml-0.9.5.jar:\
$BK_HOME/lib/htmlunit/commons-codec-1.3.jar:\
$BK_HOME/lib/htmlunit/commons-collections-3.2.jar:\
$BK_HOME/lib/htmlunit/commons-httpclient-3.0.1.jar:\
$BK_HOME/lib/htmlunit/commons-io-1.3.1.jar:\
$BK_HOME/lib/htmlunit/commons-lang-2.3.jar:\
$BK_HOME/lib/htmlunit/commons-logging-1.1.jar:\
$BK_HOME/lib/htmlunit/htmlunit-1.13.jar:\
$BK_HOME/lib/htmlunit/jaxen-1.1.1.jar:\
$BK_HOME/lib/htmlunit/js-1.6R5.jar:\
$BK_HOME/lib/htmlunit/nekohtml-0.9.5.jar:\
$BK_HOME/lib/htmlunit/xercesImpl-2.6.2.jar:\
$BK_HOME/lib/htmlunit/xmlParserAPIs-2.6.2.jar:\
$BK_HOME/lib/log4j-1.2.7.jar:\
$BK_HOME/lib/servlet-api-2.5-6.0.0rc0.jar:\
$BK_HOME/lib/xpath20/sablecc-anttask.jar:\
$BK_HOME/lib/xpath20/sablecc.jar"

#export DBURL="xmldb:exist://localhost:8080/exist/xmlrpc/db/bookkeeping/"
export DBURL="xmldb:exist:///db/"


# embed the exist DB 
java -classpath $CP -Ddb.url=$DBURL -Dexist.initdb="true" -Dexist.home="." -Dorg.xml.sax.features.validation="false" -Djava.endorsed.dirs="lib/endorsed" -d32 -client  com.interrupt.bookkeeping.cc.bkell.Bkell 


# $BK_HOME/build/bookkeeping.jar"
# $BK_HOME/build/gen:\
# $BK_HOME/build/src:\
# $BK_HOME/build/test"
#java -classpath $CP -Ddb.url=$DBURL -Dexist.initdb="true" -Dexist.home="." -Dorg.xml.sax.features.validation="false" -Djava.endorsed.dirs="lib/endorsed" com.interrupt.bookkeeping.Deleteme 


