header {
    // import org.xml.sax.helpers.*;
}
class BookkeepingLexer extends Lexer;

options {
	k=10;
	charVocabulary='\u0000'..'\u007F';
	exportVocab=BookkeepingLexer;
	caseSensitive=true;
	importVocab=XMLLexer;
}

LEFT_PAREN :'('	{ System.out.println("LEFT PARENTHESESE '('"); };

RIGHT_PAREN :')' { System.out.println("RIGHT PARENTHESESE ')'"); };

DELIMITER :',' { System.out.println("DELIMITER ','"); };

OPT_ENTRY :"-entry" WHITESPACE( 'A'..'Z' | 'a'..'z' | '0'..'9' )*	{ System.out.println("-entry"); };

OPT_ACCOUNT :"-account" WHITESPACE( 'A'..'Z' | 'a'..'z' | '0'..'9' )*;

OPT_JOURNAL :"-journal" WHITESPACE( 'A'..'Z' | 'a'..'z' | '0'..'9' )*;

OPT_NAME :"-name" WHITESPACE( 'A'..'Z' | 'a'..'z' | '0'..'9' )*;

OPT_TYPE :"-type" WHITESPACE( 'A'..'Z' | 'a'..'z' | '0'..'9' )*;

OPT_CWEIGHT :"-counterWeight" WHITESPACE("debit"|"credit");

OPT_AMOUNT :"-amount" WHITESPACE(('0'..'9')+'.'('0'..'9')('0'..'9'))*;

OPT_ID :"-id" WHITESPACE( 'A'..'Z' | 'a'..'z' | '0'..'9' )*;

OPT_ENTRYNUM :"-entrynum" WHITESPACE('0'..'9')*;

OPT_DATE :"-date" WHITESPACE(('0'..'9')('0'..'9')'/'('0'..'9')('0'..'9')'/'('0'..'9')('0'..'9')('0'..'9')('0'..'9'))*;

OPT_FILE :"-F" WHITESPACE( 'A'..'Z' | 'a'..'z' | '0'..'9' )*;

COMMAND_CREATE :("create")+		{ System.out.println("COMMAND_CREATE"); };

COMMAND_ADD! :("add")+		{ System.out.println("COMMAND_ADD"); };

COMMAND_REMOVE :("remove")+		{ System.out.println("COMMAND_REMOVE"); };

COMMAND_REVERSE :("reverse")+		{ System.out.println("COMMAND_REVERSE"); };

COMMAND_FIND :("find")+			{ System.out.println("COMMAND_FIND"); };

COMMAND_LOAD :("load")+			{ System.out.println("COMMAND_LOAD"); };

COMMAND_LIST :("list")+			{ System.out.println("COMMAND_LIST"); };

COMMAND_LOGIN :("login")+			{ System.out.println("COMMAND_LOGIN"); };

COMMAND_LOGOUT :("logout")+		{ System.out.println("COMMAND_LOGOUT"); };

COMMAND_EXIT :("exit")+		{ System.out.println("COMMAND_EXIT"); };

END_COMMAND :';'	{ System.out.println("END COMMAND ';'"); };

WHITESPACE :( ' ' | '\r' | '\n' | '\t' ) {
		System.out.println("");
		System.out.println("WHITE SPACE ' '"); 
		$setType(Token.SKIP);
	};

// inherited from grammar XMLLexer
ZZZ :"<xml/>" ;

// inherited from grammar XMLLexer
DOCTYPE! :"<!DOCTYPE" WS rootElementName:NAME 
	        { System.out.println("ROOTELEMENT: "+rootElementName.getText()); }   
	        WS
	        ( 
	            ( "SYSTEM" WS sys1:STRING
	                { System.out.println("SYSTEM: "+sys1.getText()); }   

	            | "PUBLIC" WS pub:STRING WS sys2:STRING
	                { System.out.println("PUBLIC: "+pub.getText()); }   
	                { System.out.println("SYSTEM: "+sys2.getText()); }   
	            )
	            ( WS )?
	        )?
	        ( dtd:INTERNAL_DTD ( WS )? 
	            { System.out.println("DTD: "+dtd.getText()); }   

	        )?
			'>'
		;

// inherited from grammar XMLLexer
protected INTERNAL_DTD :'['!
	        // reports warning, but is absolutely ok (checked generated code)
	        // besides this warning was not generated with k=1 which is 
	        // enough for this rule...
	        ( options {greedy=false;} : NL
	        | STRING // handle string specially to avoid to mistake ']' in string for end dtd
	        | .
	        )*
	        ']'!
	    ;

// inherited from grammar XMLLexer
PI! :// { AttributesImpl attributes = new AttributesImpl(); }
	        "<?" 
	        target:NAME
	        ( WS )?
			( ATTR /*[attributes]*/ ( WS )? )*
	        {
	            if (target.getText().equalsIgnoreCase("xml")) {
	                // this is the xml declaration, handle it
	                System.out.println("XMLDECL: "+target.getText());
	            } else {
	                System.out.println("PI: "+target.getText());
	            }
	        }
			"?>"
		;

// inherited from grammar XMLLexer
COMMENT! :"<!--" c:COMMENT_DATA "-->"
	        { System.out.println("COMMENT: "+c.getText()); }
		;

// inherited from grammar XMLLexer
protected COMMENT_DATA :( options {greedy=false;} : NL
	        | .
	        )*
	    ;

// inherited from grammar XMLLexer
ENDTAG! :"</" g:NAME ( WS )? '>'
	        { System.out.println("ENDTAG: "+g.getText()); }
		;

// inherited from grammar XMLLexer
STARTTAG! :// XXX should org.xml.sax.AttributesImpl be replaced by something else?
	        // { AttributesImpl attributes = new AttributesImpl(); }
	        '<' 
	        g:NAME
	        ( WS )?
			( ATTR /*[attributes]*/ ( WS )? )*
			( "/>"
	            { System.out.println("EMTYTAG: "+g.getText()); }
			| '>'
	            { System.out.println("STARTTAG: "+g.getText()); }
			)
		;

// inherited from grammar XMLLexer
PCDATA! :p:PCDATA_DATA
	        { System.out.println("PCDATA: "+p.getText()); }
		;

// inherited from grammar XMLLexer
protected PCDATA_DATA :( options {greedy=true;} : NL
	        | ~( '<' | '\n' | '\r' )
	        )+
	    ;

// inherited from grammar XMLLexer
CDATABLOCK! :"<![CDATA[" p:CDATA_DATA "]]>"
	        { System.out.println("CDATABLOCK: "+p.getText()); }
		;

// inherited from grammar XMLLexer
protected CDATA_DATA :( options {greedy=false;} : NL
	        | .
	        )*
	    ;

// inherited from grammar XMLLexer
protected ATTR :name:NAME ( WS )? '=' ( WS )? value:STRING_NO_QUOTE
	        /*
			{ attributes.addAttribute("", "", name.getText(), "CDATA", 
	                value.getText()); 
	        }
	        */
	        { System.out.println("ATTRIBUTE: "+name.getText()+"="+value.getText()); }
		;

// inherited from grammar XMLLexer
protected STRING_NO_QUOTE :'"'! (~'"')* '"'!
		|	'\''! (~'\'')* '\''!
		;

// inherited from grammar XMLLexer
protected STRING :'"' (~'"')* '"'
		|	'\'' (~'\'')* '\''
		;

// inherited from grammar XMLLexer
protected NAME :( LETTER | '_' | ':') ( options {greedy=true;} : NAMECHAR )*
		;

// inherited from grammar XMLLexer
protected NAMECHAR :LETTER | DIGIT | '.' | '-' | '_' | ':'
		;

// inherited from grammar XMLLexer
protected DIGIT :'0'..'9'
		;

// inherited from grammar XMLLexer
protected LETTER :'a'..'z' 
		| 'A'..'Z'
		;

// inherited from grammar XMLLexer
protected WS :(	options {
	                greedy = true;
				}
			:	' '
			|	ESC
			)+
		;

// inherited from grammar XMLLexer
protected ESC :( '\t'
		 	|	NL
			)
		;

// inherited from grammar XMLLexer
protected NL :(	options {
		generateAmbigWarnings=false;
		greedy = true;
	    }
			: '\n'
			|	"\r\n"
			|	'\r'
			)
			{ newline(); }
		;

class XMLLexer extends Lexer;

options {
	k=10;
	charVocabulary='\u0000'..'\u007F';
	caseSensitive=true;
	exportVocab=XMLLexer;
}

ZZZ :"<xml/>" ;

DOCTYPE! :"<!DOCTYPE" WS rootElementName:NAME 
	        { System.out.println("ROOTELEMENT: "+rootElementName.getText()); }   
	        WS
	        ( 
	            ( "SYSTEM" WS sys1:STRING
	                { System.out.println("SYSTEM: "+sys1.getText()); }   

	            | "PUBLIC" WS pub:STRING WS sys2:STRING
	                { System.out.println("PUBLIC: "+pub.getText()); }   
	                { System.out.println("SYSTEM: "+sys2.getText()); }   
	            )
	            ( WS )?
	        )?
	        ( dtd:INTERNAL_DTD ( WS )? 
	            { System.out.println("DTD: "+dtd.getText()); }   

	        )?
			'>'
		;

protected INTERNAL_DTD :'['!
	        // reports warning, but is absolutely ok (checked generated code)
	        // besides this warning was not generated with k=1 which is 
	        // enough for this rule...
	        ( options {greedy=false;} : NL
	        | STRING // handle string specially to avoid to mistake ']' in string for end dtd
	        | .
	        )*
	        ']'!
	    ;

PI! :// { AttributesImpl attributes = new AttributesImpl(); }
	        "<?" 
	        target:NAME
	        ( WS )?
			( ATTR /*[attributes]*/ ( WS )? )*
	        {
	            if (target.getText().equalsIgnoreCase("xml")) {
	                // this is the xml declaration, handle it
	                System.out.println("XMLDECL: "+target.getText());
	            } else {
	                System.out.println("PI: "+target.getText());
	            }
	        }
			"?>"
		;

COMMENT! :"<!--" c:COMMENT_DATA "-->"
	        { System.out.println("COMMENT: "+c.getText()); }
		;

protected COMMENT_DATA :( options {greedy=false;} : NL
	        | .
	        )*
	    ;

ENDTAG! :"</" g:NAME ( WS )? '>'
	        { System.out.println("ENDTAG: "+g.getText()); }
		;

ZZZ! :"<xml/>" ;

STARTTAG! :// XXX should org.xml.sax.AttributesImpl be replaced by something else?
	        // { AttributesImpl attributes = new AttributesImpl(); }
	        '<' 
	        g:NAME
	        ( WS )?
			( ATTR /*[attributes]*/ ( WS )? )*
			( "/>"
	            { System.out.println("EMTYTAG: "+g.getText()); }
			| '>'
	            { System.out.println("STARTTAG: "+g.getText()); }
			)
		;

PCDATA! :p:PCDATA_DATA
	        { System.out.println("PCDATA: "+p.getText()); }
		;

protected PCDATA_DATA :( options {greedy=true;} : NL
	        | ~( '<' | '\n' | '\r' )
	        )+
	    ;

CDATABLOCK! :"<![CDATA[" p:CDATA_DATA "]]>"
	        { System.out.println("CDATABLOCK: "+p.getText()); }
		;

protected CDATA_DATA :( options {greedy=false;} : NL
	        | .
	        )*
	    ;

protected ATTR :name:NAME ( WS )? '=' ( WS )? value:STRING_NO_QUOTE
	        /*
			{ attributes.addAttribute("", "", name.getText(), "CDATA", 
	                value.getText()); 
	        }
	        */
	        { System.out.println("ATTRIBUTE: "+name.getText()+"="+value.getText()); }
		;

protected STRING_NO_QUOTE :'"'! (~'"')* '"'!
		|	'\''! (~'\'')* '\''!
		;

protected STRING :'"' (~'"')* '"'
		|	'\'' (~'\'')* '\''
		;

protected NAME :( LETTER | '_' | ':') ( options {greedy=true;} : NAMECHAR )*
		;

protected NAMECHAR :LETTER | DIGIT | '.' | '-' | '_' | ':'
		;

protected DIGIT :'0'..'9'
		;

protected LETTER :'a'..'z' 
		| 'A'..'Z'
		;

protected WS :(	options {
	                greedy = true;
				}
			:	' '
			|	ESC
			)+
		;

protected ESC :( '\t'
		 	|	NL
			)
		;

protected NL :(	options {
		generateAmbigWarnings=false;
		greedy = true;
	    }
			: '\n'
			|	"\r\n"
			|	'\r'
			)
			{ newline(); }
		;


