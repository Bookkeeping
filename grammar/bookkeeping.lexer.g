

/* LEXER 
 */
class BookkeepingLexer extends Lexer;

options { 
	
    k=10; // needed for newline junk
    //charVocabulary='\u0000'..'\u007F'; // allow ascii
	exportVocab=BookkeepingLexer; 
}


LEFT_PAREN: 	'('	{ System.out.println("LEFT PARENTHESIS '('"); }; 
RIGHT_PAREN: 	')' { System.out.println("RIGHT PARENTHESIS ')'"); }; 
DELIMITER:		',' { 
					System.out.println("DELIMITER ','"); 
					System.out.println( "" ); 
					System.out.println( "" ); 
				}; 


// OPTIONS 
OPT_ENTRY:		"-entry" WS( 'A'..'Z' | 'a'..'z' | '0'..'9' )*	{ System.out.println("-entry"); };
OPT_ACCOUNT:	"-account" WS( 'A'..'Z' | 'a'..'z' | '0'..'9' )*;
OPT_JOURNAL:	"-journal" WS( 'A'..'Z' | 'a'..'z' | '0'..'9' )*;
OPT_NAME:		"-name" WS( 'A'..'Z' | 'a'..'z' | '0'..'9' )*;
OPT_TYPE:		"-type" WS( 'A'..'Z' | 'a'..'z' | '0'..'9' )*; 
OPT_CWEIGHT:	"-counterWeight" WS("debit"|"credit"); 
OPT_AMOUNT:		"-amount" WS(('0'..'9')+'.'('0'..'9')('0'..'9'))*; 
OPT_ID:			"-id" WS( 'A'..'Z' | 'a'..'z' | '0'..'9' )*; 
OPT_ENTRYNUM:	"-entrynum" WS('0'..'9')*;	
OPT_DATE:		"-date" WS(('0'..'9')('0'..'9')'/'('0'..'9')('0'..'9')'/'('0'..'9')('0'..'9')('0'..'9')('0'..'9'))*; 
OPT_FILE:		"-F" WS( 'A'..'Z' | 'a'..'z' | '0'..'9' )*; 


// COMMANDS 
COMMAND_CREATE: 	("create")+		{ System.out.println("COMMAND_CREATE"); }; 
COMMAND_ADD!:		("add")+		{ System.out.println("COMMAND_ADD"); }; 
COMMAND_REMOVE:		("remove")+		{ System.out.println("COMMAND_REMOVE"); }; 
COMMAND_REVERSE:	("reverse")+		{ System.out.println("COMMAND_REVERSE"); }; 
COMMAND_FIND:		("find")+			{ System.out.println("COMMAND_FIND"); }; 
COMMAND_LOAD:		("load")+			{ System.out.println("COMMAND_LOAD"); }; 
COMMAND_LIST:		("list")+			{ System.out.println("COMMAND_LIST"); }; 
COMMAND_LOGIN:		("login")+			{ System.out.println("COMMAND_LOGIN"); }; 
COMMAND_LOGOUT:		("logout")+		{ System.out.println("COMMAND_LOGOUT"); }; 
COMMAND_EXIT:		("exit")+		{ System.out.println("COMMAND_EXIT"); }; 



// MISCELLANEOUS 
END_COMMAND:	';'	{ System.out.println("END COMMAND ';'"); }; 
/*WHITESPACE: 	( ' ' | '\r' | '\n' | '\t' ) {
		System.out.println("");
		System.out.println("WHITE SPACE ' '"); 
		$setType(Token.SKIP);
	}; 
*/


/** 
 * XML Grammar 
 */
DOCTYPE!
    :
        "<!DOCTYPE" WS rootElementName:NAME 
        { System.out.println("ROOTELEMENT: "+rootElementName.getText()); }   
        WS
        ( 
            ( "SYSTEM" WS sys1:STRING
                { System.out.println("SYSTEM: "+sys1.getText()); }   
                
            | "PUBLIC" WS pub:STRING WS sys2:STRING
                { System.out.println("PUBLIC: "+pub.getText()); }   
                { System.out.println("SYSTEM: "+sys2.getText()); }   
            )
            ( WS )?
        )?
        ( dtd:INTERNAL_DTD ( WS )? 
            { System.out.println("DTD: "+dtd.getText()); }   

        )?
		'>'
	;

protected INTERNAL_DTD
    :
        '['!
        // reports warning, but is absolutely ok (checked generated code)
        // besides this warning was not generated with k=1 which is 
        // enough for this rule...
        ( options {greedy=true;} : NL
        | STRING // handle string specially to avoid to mistake ']' in string for end dtd
        | .
        )*
        ']'!
    ;


protected PI! 
    :
        // { AttributesImpl attributes = new AttributesImpl(); }
        "<?" 
        target:NAME
        ( WS )?
		( ATTR /*[attributes]*/ ( WS )? )*
        {
            if (target.getText().equalsIgnoreCase("xml")) {
                // this is the xml declaration, handle it
                System.out.println("XMLDECL: "+target.getText());
            } else {
                System.out.println("PI: "+target.getText());
            }
        }
		"?>"
	;

//////////////////

COMMENT!
	:	"<!--" c:COMMENT_DATA "-->"
        { System.out.println("COMMENT: "+c.getText()); }
	;

protected COMMENT_DATA
    : 
        //( options {greedy=true;} : NL
        //| .
        //)*
        ( options {greedy=true;} : WS | NAME | CHAR )*
    ;

//////////////////

protected ENDTAG! :
        "</" g:NAME ( WS )? '>'
        { System.out.println("ENDTAG: "+g.getText()); }
	;


protected EMPTYTAG: 
		"ET";


protected STARTTAG: 
		"ST";
		
protected STARTTAG_VS_EMPTYTAG
	:
		STARTCHUNK (WS)?
		(   "/>"     { $setType(EMPTYTAG); System.out.println("EMTYTAG"); }
			|
			'>'     { $setType(STARTTAG); System.out.println("STARTTAG"); }
		);

protected STARTCHUNK! : 
        // XX should org.xml.sax.AttributesImpl be replaced by something else?
        // { AttributesImpl attributes = new AttributesImpl(); }
        (
			'<' 
			g:NAME  
			WS  
			( ATTR (WS)? )*
		)
		{ System.out.println("STARTCHUNK: "+g.getText()); } ;


/* Right now, not so necessary to determine that element will contain 'parsed'
 * character data
PCDATA!	: 
        p:PCDATA_DATA
        { System.out.println("PCDATA: "+p.getText()); }
	;

protected PCDATA_DATA
	: 
        ( options {greedy=true;} : NL
        //| ~( '<' | '\n' | '\r' )
        )+
    ;
*/
	
CDATABLOCK!
	: "<![CDATA[" p:CDATA_DATA "]]>"
        { System.out.println("CDATABLOCK: "+p.getText()); }
	;

protected CDATA_DATA
    : 
        ( options {greedy=true;} : NL
        | .
        )*
    ;

protected ATTR // [AttributesImpl attributes]
	:	name:NAME ( WS )? '=' ( WS )? value:STRING_NO_QUOTE
        /*
		{ attributes.addAttribute("", "", name.getText(), "CDATA", 
                value.getText()); 
        }
        */
        { System.out.println("ATTRIBUTE: "+name.getText()+"="+value.getText()); }
	;

protected STRING_NO_QUOTE
	:	'"'! (~'"')* '"'!
	|	'\''! (~'\'')* '\''!
	;

protected STRING
	:	'"' (~'"')* '"'
	|	'\'' (~'\'')* '\''
	;

protected NAME
	:	( LETTER | '_' | ':') ( options {greedy=true;} : NAMECHAR )*
	;

protected NAMECHAR
	: LETTER | DIGIT | '.' | '-' | '_' | ':'
	;

protected DIGIT
	:	'0'..'9'
	;

protected LETTER
	: 'a'..'z' 
	| 'A'..'Z'
	;

protected CHAR: ( options {greedy=true;}
	: '/' | '<' | '>' | '\'' | "\"" | '.' | '-' | '_' | ':'
	| 'a'..'z' 
	| 'A'..'Z'
	| '0'..'9')
	;

protected WS: 	( options{ greedy=true; } :  ' ' | '\r' | '\n' | '\t' )+ 
	{
		//System.out.println("");
		//System.out.println("WHITE SPACE ' '"); 
		$setType(Token.SKIP);
	}; 


/* protected WS
	:	(	options {
                greedy = true;
			}
		:	' '
		|	ESC
		)+
	{ System.out.println("WHITESPACE"); }
	;
*/


protected ESC
	: ( '\t'
	 	|	NL
		)
	;

// taken from html.g
// Alexander Hinds & Terence Parr
// from antlr 2.5.0: example/html 
//
// '\r' '\n' can be matched in one alternative or by matching
// '\r' in one iteration and '\n' in another.  I am trying to
// handle any flavor of newline that comes in, but the language
// that allows both "\r\n" and "\r" and "\n" to all be valid
// newline is ambiguous.  Consequently, the resulting grammar
// must be ambiguous.  I'm shutting this warning off.
protected NL
    : (	options {
	generateAmbigWarnings=false;
	greedy = true;
    }
		: '\n'
		|	"\r\n"
		|	'\r'
		)
		{ newline(); }
	;


TOKEN_LITERAL:
	(
		(PI)? (WS)?
		(
			( tag:STARTTAG_VS_EMPTYTAG
				( WS | PI   | COMMENT  | CDATABLOCK )* 
				(TOKEN_LITERAL)*
			ENDTAG )
			{ System.out.println("		TOKEN LITERAL ["+ tag.getText() +"]"); }
			|
			(tag2:STARTTAG_VS_EMPTYTAG)
			{ System.out.println("		TOKEN LITERAL ["+ tag2.getText() +"]"); }
			//|
			//(delim:',') { $setType(DELIMITER); }
		)
	) 
	{ 
		System.out.println(""); 
	}; 



