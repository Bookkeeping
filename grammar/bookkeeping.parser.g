
/* PARSER 
 */ 
class BookkeepingParser extends Parser;
options {
	
	k=10;
	importVocab=BookkeepingLexer; 
	buildAST=true;
	
}

/*
<!-- command -opts (token -opts(token)) -->
<!-- CREATE --> 
create(entry -entrynum -journal -date (
				create(debit -entry -account -amount),
					create(credit -entry -account -amount),
						create(credit -entry -account -amount)
			));
create(journal -name(entry,entry,...));
create(transaction -date(entry,entry,...));
create(account -name -type -counterweight(debit|credit,...));
*/



//token_literal:	TOKENLITERAL 	{ System.out.println("TOKEN LITERAL >>> Parser"); }; 


expr: t:TOKEN_LITERAL ( DELIMITER TOKEN_LITERAL )* 
	{ System.out.println( "EXPRESSION >>> Parser: "+ t.getText() ); }; 



