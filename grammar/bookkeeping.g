

/* PARSER 
 */ 
class BookkeepingParser extends Parser;

options {
	
	importVocab=XMLLexer; 
	importVocab=BookkeepingLexer; 
}

/*TOKEN_LITERAL:	( 
					STARTTAG 
						( PI | COMMENT | STARTTAG | ENDTAG | PCDATA | CDATABLOCK )* 
					ENDTAG 
				) { System.out.println("TOKEN LITERAL"); }; 
*/


/* LEXER 
 */
class BookkeepingLexer extends Lexer;

options {
    k=2; // needed for newline junk
    charVocabulary='\u0000'..'\u007F'; // allow ascii
	exportVocab=BookkeepingLexer; 
}


//TOKEN_LITERAL:	("debit" | "credit")+;
LEFT_PAREN: 	'('	{ System.out.println("LEFT PARENTHESESE '('"); }; 
RIGHT_PAREN: 	')' { System.out.println("RIGHT PARENTHESESE ')'"); }; 
DELIMITER:		',' { System.out.println("DELIMITER ','"); }; 


// OPTIONS 
OPT_ENTRY:		"-entry" WHITESPACE( 'A'..'Z' | 'a'..'z' | '0'..'9' )*	{ System.out.println("-entry") };
OPT_ACCOUNT:	"-account" WHITESPACE( 'A'..'Z' | 'a'..'z' | '0'..'9' )*;
OPT_JOURNAL:	"-journal" WHITESPACE( 'A'..'Z' | 'a'..'z' | '0'..'9' )*;
OPT_NAME:		"-name" WHITESPACE( 'A'..'Z' | 'a'..'z' | '0'..'9' )*;
OPT_TYPE:		"-type" WHITESPACE( 'A'..'Z' | 'a'..'z' | '0'..'9' )*; 
OPT_CWEIGHT:	"-counterWeight" WHITESPACE("debit"|"credit"); 
OPT_AMOUNT:		"-amount" WHITESPACE(('0'..'9')+'.'('0'..'9')+)*; 
OPT_ID:			"-id" WHITESPACE( 'A'..'Z' | 'a'..'z' | '0'..'9' )*; 
OPT_ENTRYNUM:	"-entrynum" WHITESPACE('0'..'9')*;	
OPT_DATE:		"-date" WHITESPACE(('0'..'9'){1,2}'/'('0'..'9'){1,2}'/'('0'..'9'){4})*; 
OPT_FILE:		"-F" WHITESPACE( 'A'..'Z' | 'a'..'z' | '0'..'9' )*; 



// COMMANDS 
COMMAND_CREATE: 	("create")+		{ System.out.println("COMMAND_CREATE"); }; 
COMMAND_ADD!:		("add")+		{ System.out.println("COMMAND_ADD"); }; 
COMMAND_REMOVE:		("remove")+		{ System.out.println("COMMAND_REMOVE"); }; 
COMMAND_REVERSE:	("reverse")+		{ System.out.println("COMMAND_REVERSE"); }; 
COMMAND_FIND:		("find")+			{ System.out.println("COMMAND_FIND"); }; 
COMMAND_LOAD:		("load")+			{ System.out.println("COMMAND_LOAD"); }; 
COMMAND_LIST:		("list")+			{ System.out.println("COMMAND_LIST"); }; 
COMMAND_LOGIN:		("login")+			{ System.out.println("COMMAND_LOGIN"); }; 
COMMAND_LOGOUT:		("logout")+		{ System.out.println("COMMAND_LOGOUT"); }; 
COMMAND_EXIT:		("exit")+		{ System.out.println("COMMAND_EXIT"); }; 



// MISCELLANEOUS 
END_COMMAND:	';'	{ System.out.println("END COMMAND ';'"); }; 
WHITESPACE: 	( ' ' | '\r' | '\n' | '\t' ) {
		System.out.println("");
		System.out.println("WHITE SPACE ' '"); 
		$setType(Token.SKIP);
	}; 



